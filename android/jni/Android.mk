#
# This file is part of libbox0.
#
# Copyright (C) 2014-2015 Kuldeep Singh Dhaka <kuldeep@madresistor.com>
#
# libbox0 is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# libbox0 is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with libbox0.  If not, see <http://www.gnu.org/licenses/>.
#

LOCAL_PATH:= $(call my-dir)

include $(CLEAR_VARS)

REL_PATH:= ../..
ABS_PATH:= $(LOCAL_PATH)/$(REL_PATH)

define all-c-files-under
	$(patsubst ./%, %, 									\
		$(shell cd $(LOCAL_PATH) ;						\
			find $(1) -name "*.c" -and -not -name ".*") \
		)
endef

LOCAL_SRC_FILES:= $(call all-c-files-under, $(REL_PATH)/libbox0)

LOCAL_C_INCLUDES:= 						\
	$(LOCAL_PATH)/../include/libbox0	\
	$(ABS_PATH)/libbox0

LOCAL_EXPORT_C_INCLUDES:= 				\
	$(LOCAL_PATH)/../include			\
	$(ABS_PATH)

LOCAL_MODULE:= box0
LOCAL_LDLIBS:= -llog
LOCAL_SHARED_LIBRARIES:= usb-1.0

ifeq ($(NDK_DEBUG),"1")
# debug build
LOCAL_CFLAGS += -g -Wall -Wwrite-strings -Wsign-compare
LOCAL_CFLAGS += -Wextra  -Wno-empty-body -Wno-unused-parameter
LOCAL_CFLAGS += -Wformat-security
endif

include $(BUILD_SHARED_LIBRARY)

# provide ndk-build LIBUSB_PATH=<path-to-libusb-code>

import-it = 										\
	$(call import-add-path,$(shell dirname $(1)));	\
	$(call import-module,$(shell basename $(1))/$(2))

#IMPORTANT NOTE:
# in libusb   in libusb.mk replace "libusb1.0" with "usb-1.0"
#  dont know why those guys did that :|
# also comment `include $(LOCAL_PATH)/examples.mk` and `include $(LOCAL_PATH)/tests.mk` in Android.mk
$(call import-it,$(LIBUSB_PATH),android/jni)
