/*
 * This file is part of libbox0.
 * Copyright (C) 2014-2016 Kuldeep Singh Dhaka <kuldeep@madresistor.com>
 *
 * libbox0 is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * libbox0 is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with libbox0.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef LIBBOX0_CONFIG_H
#define LIBBOX0_CONFIG_H

#include "common.h"

__BEGIN_DECLS

#if !defined(NDEBUG)
# define B0_DISABLE_LOGGING 0
#else
# define B0_DISABLE_LOGGING 1
#endif


/*
 * default log level set at opening of the device
 */
#define B0_DEFAULT_LOG B0_LOG_WARN

/*
 * use OpenMP for processing
 */
#define B0_USE_OPENMP 0

/* Version */
#define B0_VERSION(a, b, c)			(((a) << 16) + ((b) << 8) + (c))

#define B0_VERSION_MAJOR	0
#define B0_VERSION_MINOR	3
#define B0_VERSION_PATCH	0
#define B0_VERSION_STRING	"0.3.0"
#define B0_VERSION_CODE		\
	B0_VERSION(B0_VERSION_MAJOR, B0_VERSION_MINOR, B0_VERSION_PATCH)

__END_DECLS

#endif
