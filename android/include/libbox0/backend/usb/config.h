/*
 * This file is part of libbox0.
 * Copyright (C) 2013-2016 Kuldeep Singh Dhaka <kuldeep@madresistor.com>
 *
 * libbox0 is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * libbox0 is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with libbox0.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef LIBBOX0_BACKEND_USB_CONFIG_H
#define LIBBOX0_BACKEND_USB_CONFIG_H

#include "common.h"

__BEGIN_DECLS

/*
 * bulk endpoint read/write timeout
 * Unit: milliseconds
 */
#define B0_USB_BULK_TIMEOUT 100

/*
 * Control request timeout
 * Unit: milliseconds
 */
#define B0_USB_CTRLREQ_TIMEOUT 100

/*
 * best assumed platform callback delay when 0 is passed to stream prepare
 * @note platform dependent value
 */
#define B0_USB_AIN_STREAM_DELAY 30

/*
 * limit aout memory usage in kernel (unit: bytes)
 * @note platform dependent value
 */
#define B0_USB_AOUT_STREAM_PENDING 16384

__END_DECLS

#endif

