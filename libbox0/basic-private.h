/*
 * This file is part of libbox0.
 * Copyright (C) 2013-2015 Kuldeep Singh Dhaka <kuldeep@madresistor.com>
 *
 * libbox0 is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * libbox0 is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with libbox0.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef LIBBOX0_BASIC_PRIVATE_H
#define LIBBOX0_BASIC_PRIVATE_H

#include "common.h"
#include "device.h"
#include <stdbool.h>

__BEGIN_DECLS

B0_PRIV void b0_logger(b0_device *dev,
	const char *func, int log_level, const char *fmt, ...);

__END_DECLS

#endif
