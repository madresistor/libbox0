/*
 * This file is part of libbox0.
 * Copyright (C) 2016 Kuldeep Singh Dhaka <kuldeep@madresistor.com>
 *
 * libbox0 is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * libbox0 is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with libbox0.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "calib.h"
#include "nvm.h"
#include "../../libbox0-private.h"

/* Memory layout:
 *  --- header ---
 *  bcdVersion (xx.xx where x is a decimal number, in hex, ex: 0x0501 => 5.01)
 *  bAin0Count
 *  bAout0Count
 *  --- data ---
 *  struct {
 *   float spOffset;
 *   float spGain;
 *  } ain0, aout0
 */

#define B0V5_CALIB_VERSION 0x0010

struct b0v5_calib_header {
	uint16_t bcdVersion;
	uint8_t bAin0Count;
	uint8_t bAout0Count;
} B0_PACKED;

static void set_dummy_value(struct b0v5_calib_value *values, size_t count)
{
	size_t i;

	for (i = 0; i < count; i++) {
		values[i].offset = 0;
		values[i].gain = 1;
	}
}

B0_INLINE float le32_to_float(uint32_t value)
{
	value = B0_LE2H32(value);
	float *raw = (float *) &value;
	return *raw;
}

static void convert_from_raw(struct b0v5_calib_value *values,
			uint32_t *raw, size_t count)
{
	size_t i;

	for (i = 0; i < count; i++) {
		values[i].offset = le32_to_float(*raw++);
		values[i].gain = le32_to_float(*raw++);
	}
}

b0_result_code b0v5_calib_read(b0_device *dev,
			struct b0v5_calib *calib)
{
	B0_ASSERT_RETURN_ON_NULL_ARG(NULL, dev);
	B0_ASSERT_RETURN_ON_NULL_ARG(dev, calib);

	/* Fetch the header and see if we can parse it */
	struct b0v5_calib_header header;
	b0_result_code rc;

	rc = b0v5_nvm_read(dev, 0, &header, sizeof(header));
	if (B0_ERROR_RESULT_CODE(rc)) {
		B0_DEBUG(dev, "Unable to fetch calib header from Non volatile memory");
		return rc;
	}

	set_dummy_value(calib->ain0.values, calib->ain0.count);
	set_dummy_value(calib->aout0.values, calib->aout0.count);

	if (header.bcdVersion != B0_H2LE16(B0V5_CALIB_VERSION)) {
		/* Version not supported */
		B0_DEBUG(dev, "Version not supported");
		return B0_OK;
	}

	size_t bytes = 4 * 2 * (header.bAin0Count + header.bAout0Count);
	void *raw = B0_TEMP_BUFFER_ALLOC(bytes);
	B0_ASSERT_RETURN_ON_NULL_ALLOC(dev, raw);

	rc = b0v5_nvm_read(dev, 4, raw, bytes);
	if (B0_ERROR_RESULT_CODE(rc)) {
		B0_DEBUG(dev, "Unable to fetch calib data from Non volatile memory");
		goto done;
	}

	/* store to values */
	convert_from_raw(calib->ain0.values, raw,
		B0_MIN(calib->ain0.count, header.bAin0Count));

	convert_from_raw(calib->aout0.values,
		raw + (4 * 2 * header.bAin0Count),
		B0_MIN(calib->aout0.count, header.bAout0Count));

	done:
	B0_TEMP_BUFFER_FREE(raw);

	return rc;
}

B0_INLINE uint32_t float_to_le32(float value)
{
	uint32_t *raw = (uint32_t *) &value;
	return B0_H2LE32(*raw);
}

static void convert_to_raw(const struct b0v5_calib_value *values,
			uint32_t *raw, size_t count)
{
	size_t i;

	for (i = 0; i < count; i++) {
		*raw++ = float_to_le32(values[i].offset);
		*raw++ = float_to_le32(values[i].gain);
	}
}

b0_result_code b0v5_calib_write(b0_device *dev,
			const struct b0v5_calib *calib)
{
	B0_ASSERT_RETURN_ON_NULL_ARG(NULL, dev);
	B0_ASSERT_RETURN_ON_NULL_ARG(dev, calib);

	size_t size = 4 + ((calib->ain0.count + calib->aout0.count) * 4 * 2);
	void *raw = B0_TEMP_BUFFER_ALLOC(size);
	B0_ASSERT_RETURN_ON_NULL_ALLOC(dev, raw);

	struct b0v5_calib_header *header = raw;
	header->bcdVersion = B0_H2LE16(B0V5_CALIB_VERSION);
	header->bAin0Count = calib->ain0.count;
	header->bAout0Count = calib->aout0.count;

	convert_to_raw(calib->ain0.values, raw + 4, calib->ain0.count);
	convert_to_raw(calib->aout0.values, raw + 4 + (calib->ain0.count * 4 * 2),
						calib->aout0.count);

	b0_result_code rc = b0v5_nvm_write(dev, 0, raw, size);
	B0_TEMP_BUFFER_FREE(raw);
	return rc;
}
