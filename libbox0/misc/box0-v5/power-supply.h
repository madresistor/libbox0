/*
 * This file is part of libbox0.
 * Copyright (C) 2013-2016 Kuldeep Singh Dhaka <kuldeep@madresistor.com>
 *
 * libbox0 is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * libbox0 is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with libbox0.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef LIBBOX0_MISC_BOX0V5_POWER_SUPPLY_H
#define LIBBOX0_MISC_BOX0V5_POWER_SUPPLY_H

#include <stdint.h>
#include "../../common.h"
#include "../../libbox0.h"

__BEGIN_DECLS

#define B0V5_PS_PM5 0x01 /* ±5V power supply */
#define B0V5_PS_P3V3  0x02 /* +3.3V power supply */

#define B0V5_PS_ANALOG B0V5_PS_PM5
#define B0V5_PS_DIGITAL B0V5_PS_P3V3

/**
 * box0-v5 Power supply set
 * @param dev Device
 * @param mask bitmask.
 *   a bit in @a value will only be considered valid
 *   only if a crossponding 1 is set in @a mask.
 * @param value power supply to enable
 * @return result code
 */
B0_API b0_result_code b0v5_ps_en_set(b0_device *dev,
				uint8_t mask, uint8_t value);

/**
 * box0-v5 Power supply get
 * @param usbdh libusb device handle
 * @param[out] reg Register value
 * @return result code
 */
B0_API b0_result_code b0v5_ps_en_get(b0_device *dev,
				uint8_t *value);

/**
 * box0-v5 Power supply over-current get
 * @param usbdh libusb device handle
 * @param[out] reg Register value
 * @return result code
 */
B0_API b0_result_code b0v5_ps_oc_get(b0_device *dev,
				uint8_t *value);

/**
 * box0-v5 power supply over-current acknowledge
 * @param usbdh libusb device handle
 * @param mask The crossponding power supplies (with 1) will be ack for overcurrent
 * @return result code
 */
B0_API b0_result_code b0v5_ps_oc_ack(b0_device *dev,
				uint8_t mask);

__END_DECLS

#endif
