/*
 * This file is part of libbox0.
 * Copyright (C) 2013-2016 Kuldeep Singh Dhaka <kuldeep@madresistor.com>
 *
 * libbox0 is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * libbox0 is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with libbox0.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "power-supply.h"
#include "../../backend/usb/usb-private.h"

/**
 * Design consideration of power supply:
 *  Since the power supply module can be required by multiple code and
 *  implementing it as a interface will give access to one module at a time (by design).
 */

/* command ID */
#define B0V5_PS_EN_GET 201
#define B0V5_PS_EN_SET 202
#define B0V5_PS_OC_GET 203
#define B0V5_PS_OC_ACK 204

static int ps_set(b0_device *dev, uint8_t bRequest,
			uint16_t wValue, uint8_t *data, int len)
{
	int r = B0_USB_DEVICE_CTRLREQ_OUT(dev, bRequest, wValue, 0x0000, data, len);

	if (r < 0) {
		return B0_ERR_IO;
	} else if (r < len) {
		return B0_ERR_UNDERFLOW;
	}

	return B0_OK;
}

static int ps_get(b0_device *dev, uint8_t bRequest,
			uint16_t wValue, uint8_t *data, int len)
{
	int r = B0_USB_DEVICE_CTRLREQ_IN(dev, bRequest, wValue, 0x0000, data, len);

	if (r < 0) {
		return B0_ERR_IO;
	} else if (r < len) {
		return B0_ERR_UNDERFLOW;
	}

	return B0_OK;
}

int b0v5_ps_en_get(b0_device *dev, uint8_t *reg)
{
	return ps_get(dev, B0V5_PS_EN_GET, 0x00, reg, 1);
}

int b0v5_ps_en_set(b0_device *dev, uint8_t mask, uint8_t value)
{
	return ps_set(dev, B0V5_PS_EN_SET, (mask << 8) | value, NULL, 0);
}

int b0v5_ps_oc_get(b0_device *dev, uint8_t *reg)
{
	return ps_get(dev, B0V5_PS_OC_GET, 0x00, reg, 1);
}

int b0v5_ps_oc_ack(b0_device *dev, uint8_t reg)
{
	return ps_set(dev, B0V5_PS_OC_ACK, reg, NULL, 0);
}
