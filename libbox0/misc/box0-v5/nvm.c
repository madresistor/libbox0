/*
 * This file is part of libbox0.
 * Copyright (C) 2016 Kuldeep Singh Dhaka <kuldeep@madresistor.com>
 *
 * libbox0 is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * libbox0 is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with libbox0.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "nvm.h"
#include "../../backend/usb/usb-private.h"

/* command ID */
#define B0V5_NVM_GET 205
#define B0V5_NVM_SET 206

b0_result_code b0v5_nvm_read(b0_device *dev, size_t offset,
				void *data, size_t size)
{
	B0_ASSERT_RETURN_ON_NULL_ARG(NULL, dev);
	B0_ASSERT_RETURN_ON_NULL_ARG(dev, data);
	B0_ASSERT_RETURN_ON_FAIL_ARG(dev, size > 0);

	int r = B0_USB_DEVICE_CTRLREQ_IN(dev, B0V5_NVM_GET, 0x0000, offset,
								(void *) data, size);

	if (r < 0) {
		return B0_ERR_IO;
	}

	return B0_OK;
}

b0_result_code b0v5_nvm_write(b0_device *dev, size_t offset,
				const void *data, size_t size)
{
	B0_ASSERT_RETURN_ON_NULL_ARG(NULL, dev);
	B0_ASSERT_RETURN_ON_NULL_ARG(dev, data);
	B0_ASSERT_RETURN_ON_FAIL_ARG(dev, size > 0);

	int r = B0_USB_DEVICE_CTRLREQ_OUT(dev, B0V5_NVM_SET, 0x0000, offset,
								(void *) data, size);

	if (r < 0) {
		return B0_ERR_IO;
	}

	return B0_OK;
}
