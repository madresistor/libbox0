/*
 * This file is part of libbox0.
 * Copyright (C) 2016 Kuldeep Singh Dhaka <kuldeep@madresistor.com>
 *
 * libbox0 is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * libbox0 is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with libbox0.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef LIBBOX0_MISC_BOX0V5_CALIB_H
#define LIBBOX0_MISC_BOX0V5_CALIB_H

#include <stddef.h>
#include "../../common.h"
#include "../../libbox0.h"

__BEGIN_DECLS

struct b0v5_calib_value {
	/* output = (input * gain) - offset
	 * note: output is calibrated value
	 * note: input is uncalibrated value
	 */
	float offset;
	float gain;
};

typedef struct b0v5_calib_value b0v5_calib_value;

struct b0v5_calib {
	struct {
		struct b0v5_calib_value *values;
		size_t count;
	} ain0, aout0;
};

typedef struct b0v5_calib b0v5_calib;

/**
 * Read calibration data from device
 * @param dev Device
 * @param calib Calibration data
 * @return result code
 */
B0_API b0_result_code b0v5_calib_read(b0_device *dev,
			struct b0v5_calib *calib);

/**
 * Write calibration data to device
 * @param dev Device
 * @param calib Calibration data
 * @return result code
 */
B0_API b0_result_code b0v5_calib_write(b0_device *dev,
			const struct b0v5_calib *calib);

__END_DECLS

#endif
