/*
 * This file is part of libbox0.
 * Copyright (C) 2016 Kuldeep Singh Dhaka <kuldeep@madresistor.com>
 *
 * libbox0 is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * libbox0 is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with libbox0.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "box0-v5.h"
#include "../../backend/usb/usb-private.h"

b0_result_code b0v5_valid_test(b0_device *dev)
{
	B0_ASSERT_RETURN_ON_NULL_ARG(NULL, dev);

	if (dev->backend->type != B0_BACKEND_USB) {
		B0_DEBUG(dev, "Other than USB backend, nothing supported");
		return B0_ERR_SUPP;
	}

	libusb_device *usbd = B0_USB_DEVICE_BACKEND_DATA(dev)->usbd;

	struct libusb_device_descriptor desc;
	int r = libusb_get_device_descriptor(usbd, &desc);

	if (r < 0) {
		/* Unable to fetch descriptor (something wrong has already gone) */
		return B0_ERR_IO;
	}

	/* Thanks OpenMoko for the VID:PID
	 * bcdDevice should be 05.xx */
	if (desc.idVendor == 0x1d50 &&
		desc.idProduct == 0x8085 &&
		(desc.bcdDevice & 0xFF00) == 0x0500) {
		/* box0-v5 detected! */
		return B0_OK;
	}

	return B0_ERR_SUPP;
}
