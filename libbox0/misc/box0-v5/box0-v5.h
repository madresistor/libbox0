/*
 * This file is part of libbox0.
 * Copyright (C) 2016 Kuldeep Singh Dhaka <kuldeep@madresistor.com>
 *
 * libbox0 is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * libbox0 is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with libbox0.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef LIBBOX0_MISC_BOX0V5_H
#define LIBBOX0_MISC_BOX0V5_H

#include <stddef.h>
#include "../../common.h"
#include "../../libbox0.h"

__BEGIN_DECLS

/**
 * Check if the device is box0-v5
 * @param dev Device
 * @return B0_OK if box0-v5 detected
 * @return B0_ERR_SUPP If not a box0-v5
 * @return Other result code
 */
B0_API b0_result_code b0v5_valid_test(b0_device *dev);

__END_DECLS

#endif
