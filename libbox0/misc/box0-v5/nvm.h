/*
 * This file is part of libbox0.
 * Copyright (C) 2016 Kuldeep Singh Dhaka <kuldeep@madresistor.com>
 *
 * libbox0 is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * libbox0 is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with libbox0.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef LIBBOX0_MISC_BOX0V5_NVM_H
#define LIBBOX0_MISC_BOX0V5_NVM_H

#include <stddef.h>
#include "../../common.h"
#include "../../libbox0.h"

__BEGIN_DECLS

/**
 * Read data from NVM (Non volatile memory)
 * @param dev Device
 * @param offset Memory offset
 * @param data Data to be readed into
 * @param size Number of bytes to read
 * @return result code
 */
B0_API b0_result_code b0v5_nvm_read(b0_device *dev,
				size_t offset, void *data, size_t size);

/**
 * Write data to NVM (Non volatile memory)
 * @param dev Device
 * @param offset Memory offset
 * @param data Data to be readed into
 * @param size Number of bytes to read
 * @return result code
 * @note Memory left unwritten will have undefined content
 */
B0_API b0_result_code b0v5_nvm_write(b0_device *dev, size_t offset,
				const void *data, size_t size);

__END_DECLS

#endif
