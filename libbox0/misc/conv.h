/*
 * This file is part of libbox0.
 * Copyright (C) 2013-2015 Kuldeep Singh Dhaka <kuldeep@madresistor.com>
 *
 * libbox0 is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * libbox0 is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with libbox0.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef LIBBOX0_MISC_CONV_H
#define LIBBOX0_MISC_CONV_H

#include "../common.h"
#include <math.h>

__BEGIN_DECLS

/*
 * The Non SI unit conver are for user for
 * easy conver to their required value.
 *
 * consider adding any conver macro related to unit (though not limited to).
 *
 * Naming convention
 * =================
 *
 * 1. B0_<from>_TO_<to> for conver of units
 *		example:
 * 			B0_KELVIN_TO_CELSIUS
 *
 * 2. B0_<to>_USING_<param> for any mathamatical can give output as result.
 * 		example:
 * 			B0_ALTITUDE_USING_PRESSURE
 *
 * if possible, the parameter of the macro should be name on unit. like "celsius" instead of "temp"
 * 	because this will make it easier to spot mistakes.
 */

#define B0_KELVIN_TO_CELSIUS(kelvin) ((kelvin) - 273.15)
#define B0_KELVIN_TO_FARENHEIT(kelvin) (((kelvin) * 9.0/5.0) - 459.67)

#define B0_CELSIUS_TO_KELVIN(celsius) ((celsius) + 273.15)

/*
 * Altitude can be extracted from pressures.
 * pressure decrease as we go heigher.
 * see BMP180.pdf r2.5 p16 - 3.6 Calculating absolute altitude
 * @note 1 hPa = 100 Pa
 */
#define B0_ALTITUDE_USING_PRESSURE(pascal) \
	(44330 * (1 - pow((pascal) / 101325.0, 1 / 5.255)))

#define B0_RADIAN_TO_DEGREE(radian)	((radian) * (180.0 / M_PI))
#define B0_DEGREE_TO_RADIAN(degree)	((degree) * (M_PI / 180.0))

__END_DECLS

#endif
