/*
 * This file is part of libbox0.
 * Copyright (C) 2013-2016 Kuldeep Singh Dhaka <kuldeep@madresistor.com>
 *
 * libbox0 is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * libbox0 is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with libbox0.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "libbox0-private.h"

b0_result_code b0_device_close(b0_device *dev)
{
	B0_ASSERT_RETURN_ON_NULL_ARG(NULL, dev);

	B0_ASSERT_BACKEND_FUNC_NULL(dev, device, close);
	return B0_BACKEND(dev, device).close(dev);
}

b0_result_code b0_device_ping(b0_device *dev)
{
	B0_ASSERT_RETURN_ON_NULL_ARG(NULL, dev);

	B0_ASSERT_BACKEND_FUNC_NULL(dev, device, ping);
	return B0_BACKEND(dev, device).ping(dev);
}

b0_result_code b0_device_log(b0_device *dev, b0_log_level log)
{
	b0_frontend_device_data *frontend_data;

	/* logging is disabled */
#if (B0_DISABLE_LOGGING == 1)
	return B0_ERR_SUPP;
#endif

	B0_ASSERT_RETURN_ON_NULL_ARG(NULL, dev);
	B0_ASSERT_RETURN_ON_FAIL_ARG(dev,
		(log >= B0_LOG_NONE && log <= B0_LOG_DEBUG));

	frontend_data = B0_DEVICE_FRONTEND_DATA(dev);
	frontend_data->log = log;
	return B0_OK;
}

b0_result_code b0_device_alloc(b0_device **dev)
{
	b0_frontend_device_data *frontend_data = NULL;
	b0_device *dev_alloc = NULL;

	B0_ASSERT_RETURN_ON_NULL_ARG(NULL, dev);

	/* currently log value is only used to store in frontend_data */
#if (B0_DISABLE_LOGGING == 0)
	frontend_data = calloc(1, sizeof(*frontend_data));
	B0_ASSERT_RETURN_ON_NULL_ALLOC(NULL, frontend_data);
	frontend_data->log = B0_DEFAULT_LOG;
#endif

	dev_alloc = calloc(1, sizeof(*dev_alloc));
	if (B0_IS_NULL(dev_alloc) && B0_IS_NOT_NULL(frontend_data)) {
		free(frontend_data);
	}
	B0_ASSERT_RETURN_ON_NULL_ALLOC(NULL, dev_alloc);

	dev_alloc->modules_len = 0;
	dev_alloc->modules = NULL;
	dev_alloc->frontend_data = frontend_data;
	dev_alloc->backend_data = NULL;

	dev_alloc->name = NULL;
	dev_alloc->manuf = NULL;
	dev_alloc->serial = NULL;

	*dev = dev_alloc;
	return B0_OK;
}

b0_result_code b0_module_alloc(b0_device *dev, b0_module **mod)
{
	b0_module *module;
	b0_module **modules;

	/* check arguments */
	B0_ASSERT_RETURN_ON_NULL_ARG(NULL, dev);
	B0_ASSERT_RETURN_ON_NULL_ARG(dev, mod);

	/* allocate the memory */
	module = calloc(1, sizeof(*module));
	B0_ASSERT_RETURN_ON_NULL_ALLOC(dev, module);

	/* allocate memory for the list */
	modules = realloc(dev->modules, (dev->modules_len + 1) * sizeof(*modules));
	if (B0_IS_NULL(modules)) {
		free(module);
	}
	B0_ASSERT_RETURN_ON_NULL_ALLOC(dev, modules);

	/* increase the list */
	dev->modules = modules;
	dev->modules[dev->modules_len++] = module;

	/* initalize memers */
	module->backend_data = NULL;
	module->frontend_data = NULL;

	/* return the result */
	*mod = module;
	return B0_OK;
}

b0_result_code b0_device_free(b0_device *dev)
{
	size_t i;

	B0_ASSERT_RETURN_ON_NULL_ARG(NULL, dev);

	/* free modules */
	for (i = 0; i < dev->modules_len; i++) {
		free(dev->modules[i]);
	}

	if (dev->modules_len) {
		free(dev->modules);
	}

	/* free frontend data (if any) */
	if (B0_IS_NOT_NULL(dev->frontend_data)) {
		free(dev->frontend_data);
	}

	/* free device */
	free(dev);

	return B0_OK;
}
