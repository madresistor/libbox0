/*
 * This file is part of libbox0.
 * Copyright (C) 2013-2015 Kuldeep Singh Dhaka <kuldeep@madresistor.com>
 *
 * libbox0 is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * libbox0 is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with libbox0.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "libbox0-private.h"

/** @cond INTERNAL */

#define CAST_TO_UINT8(str) ((const uint8_t*)(str))

#define RESULT_NAME(result_code_enum) \
	case result_code_enum: return CAST_TO_UINT8(#result_code_enum);

const uint8_t* b0_result_name(b0_result_code r)
{
	switch(r) {
	RESULT_NAME(B0_FAIL_CALC)
	RESULT_NAME(B0_ERR_STATE)
	RESULT_NAME(B0_ERR_UNDERFLOW)
	RESULT_NAME(B0_ERR_OVERFLOW)
	RESULT_NAME(B0_ERR_UNAVAIL)
	RESULT_NAME(B0_ERR_TIMEOUT)
	RESULT_NAME(B0_ERR_BUSY)
	RESULT_NAME(B0_ERR_BOGUS)
	RESULT_NAME(B0_BUG)
	RESULT_NAME(B0_ERR_IO)
	RESULT_NAME(B0_ERR_ARG)
	RESULT_NAME(B0_ERR_SUPP)
	RESULT_NAME(B0_ERR_ALLOC)
	RESULT_NAME(B0_ERR)
	RESULT_NAME(B0_OK)
	default: return CAST_TO_UINT8("**UNKNOWN**");
	}
}

#define RESULT_EXPLAIN(result_code_enum, explain) \
	case result_code_enum: return CAST_TO_UINT8(explain);

const uint8_t* b0_result_explain(b0_result_code r)
{
	switch(r) {
	RESULT_EXPLAIN(B0_FAIL_CALC, "Calculation failed")
	RESULT_EXPLAIN(B0_ERR_STATE, "Invalid state, some step missed")
	RESULT_EXPLAIN(B0_ERR_UNDERFLOW, "Underflow occured, in calculation or memory")
	RESULT_EXPLAIN(B0_ERR_OVERFLOW, "Overflow occured, in calculation or memory")
	RESULT_EXPLAIN(B0_ERR_BUSY, "Cannot perform since because device is busy")
	RESULT_EXPLAIN(B0_BUG, "Raised when a bugg'y state occurs")
	RESULT_EXPLAIN(B0_ERR_UNAVAIL, "Resource unavailable")
	RESULT_EXPLAIN(B0_ERR_TIMEOUT, "Operation timeout occured")
	RESULT_EXPLAIN(B0_ERR_BOGUS, "Device returned bogus value")
	RESULT_EXPLAIN(B0_ERR_IO, "Device I/O error")
	RESULT_EXPLAIN(B0_ERR_ARG, "Invalid parameter passed")
	RESULT_EXPLAIN(B0_ERR_SUPP, "Some unsupported feature")
	RESULT_EXPLAIN(B0_ERR_ALLOC, "Memory allocation failed")
	RESULT_EXPLAIN(B0_OK, "Everything went OK")
	RESULT_EXPLAIN(B0_ERR, "Other libraries error/unspecified error")
	default: return CAST_TO_UINT8("**UNKNOWN**");
	}
}

uint32_t b0_version_extract(b0_version *ver)
{
	if (B0_IS_NOT_NULL(ver)) {
		ver->major = B0_VERSION_MAJOR;
		ver->minor = B0_VERSION_MINOR;
		ver->patch = B0_VERSION_PATCH;
	}

	return B0_VERSION_CODE;
}

#if (B0_DISABLE_LOGGING == 0)

static int get_env_log(void);
static int get_log(b0_device *dev);

/**
 * @brief extract env variable "LIBBOX0_LOG" value
 * @retval -1 invalid value
 * @retval >= 0 for valid value
 */
static int get_env_log(void)
{
	char *log = getenv("LIBBOX0_LOG");

	if (B0_IS_NULL(log) || log[0] == '\0') {
		/* not defined */
		return -1;
	}

	char log_0 = log[0];

	/* numeral {"0", "1", "2", "3", "4"} */
	if (log_0 >= '0' && log_0 <= '4') {
		return log_0 - '0';
	}

	/* character (case insensitive) {"none", "error", "warn", "info", "debug"} */
	log_0 = tolower(log_0);
	size_t i;
	for (i = 0; i < 5; i++) {
		if ("newid"[i] == log_0) {
			return i;
		}
	}

	return -1;
}

/**
 * @brief get log level of the device.
 * @details
 *   extract the configured log level of the device
 *   if logging capabilities of the library is disabled, B0_DEFAULT_LOG is returned
 * @param[in] dev device to extract log_level from
 * @return device log level
 * @see b0_device_log()
 */
static int get_log(b0_device *dev)
{
	/* env variable "LIBBOX0_LOG" */
	int env = get_env_log();
	if (env >= 0) {
		return env;
	}

	/* device provided */
	b0_frontend_device_data *frontend_data = NULL;
	if (B0_IS_NOT_NULL(dev)) {
		frontend_data = B0_DEVICE_FRONTEND_DATA(dev);
	}

	if (B0_IS_NOT_NULL(frontend_data)) {
		return frontend_data->log;
	}

	/* nothing found, backdrop to compile time value */
	return B0_DEFAULT_LOG;
}

/* android has its own logging library */
#if defined(__ANDROID__)
# include <android/log.h>
#endif

/**
 * @brief log the message to appropriate output.
 * @details internal function that route the log to logger
 *  if no platform specific log is implemented, log is routed to stderr
 * @param[in] dev device
 * @param[in] func name of the function
 * @param[in] log tag log limit, compared with device log variable to device output or not
 * @param[in] fmt print format (same as printf)
 * @param[in] ... argument (same as printf)
 * @note the output is dependent on platform
 * @note if nothing is available, print on stderr
 * @note Android __android_log_write supported
 */
void b0_logger(b0_device *dev, const char *func, int log, const char *fmt, ...)
{
	va_list args;
	va_start(args, fmt);

	if (log > get_log(dev)) {
		return;
	}

#if !defined(__ANDROID__)
	/*
	 * print to stderr in format ->
	 * libbox0: <type>: <function>: <message>
	 */
	const char *map[] = {"", "error", " warn", " info", "debug"};
	fprintf(stderr, "libbox0: %s: %s(): ", map[log], func);
	vfprintf(stderr, fmt, args); va_end(args);
	fputc('\n', stderr);
	fflush(stderr);
#else /* __ANDROID__ */
	int map[] = {
		ANDROID_LOG_SILENT,
		ANDROID_LOG_ERROR,
		ANDROID_LOG_WARN,
		ANDROID_LOG_INFO,
		ANDROID_LOG_DEBUG
	};

	/* use the android logging system. */
	__android_log_vprint(map[log], "libbox0", fmt, args);

# endif /* __ANDROID__ */
}
#endif /* B0_DISABLE_LOGGING */

/** @endcond */
