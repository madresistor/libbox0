/*
 * This file is part of libbox0.
 * Copyright (C) 2013-2015 Kuldeep Singh Dhaka <kuldeep@madresistor.com>
 *
 * libbox0 is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * libbox0 is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with libbox0.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef LIBBOX0_PLATFORM_H
#define LIBBOX0_PLATFORM_H

/* Platform Abstraction */

/* Platform detect */
#if defined(_WIN32) || defined(_MSC_VER) || defined(__MINGW32__)
# define B0_PLATFORM_WINDOWS 1
#else
# define B0_PLATFORM_WINDOWS 0
#endif

#if defined(__DOXYGEN__)
/**
 * @addtogroup endianess_conv Platform Independent Endianness conversion
 * @{
 */

/**
 * Convert host @a value to 16-bit little endian
 * @param value Host endian value
 */
#define B0_H2LE16(value)

/**
 * Convert host @a value to 32-bit little endian
 * @param value Host endian value
 */
#define B0_H2LE32(value)

/**
 * Convert host @a value to 64-bit little endian
 * @param value Host endian value
 */
#define B0_H2LE64(value)

/**
 * Convert little endian @a value to 16-bit host endian
 * @param value little endian value
 */
#define B0_LE2H16(value)

/**
 * Convert little endian @a value to 32-bit host endian
 * @param value little endian value
 */
#define B0_LE2H32(value)

/**
 * Convert little endian @a value to 64-bit host endian
 * @param value little endian value
 */
#define B0_LE2H64(value)
#endif


#if !defined(__DOXYGEN__)

/* Endianness conver macros
 * some platforms have difference than expected.
 * for example:
 *  Android: bionic have some endianness conver macro named differently
 *  Microsoft Windows: http://stackoverflow.com/questions/11040133
 * Reference: https://gist.github.com/panzi/6856583
 */
#if defined(__linux__) || defined(__CYGWIN__)
# include <endian.h>
# define B0_H2LE16(value) htole16(value)
# define B0_H2LE32(value) htole32(value)
# define B0_H2LE64(value) htole64(value)
# if defined(__ANDROID__)
#  define B0_LE2H16(value) letoh16(value)
#  define B0_LE2H32(value) letoh32(value)
#  define B0_LE2H64(value) letoh64(value)
# else /* defined(__ANDROID__) */
#  define B0_LE2H16(value) le16toh(value)
#  define B0_LE2H32(value) le32toh(value)
#  define B0_LE2H64(value) le64toh(value)
# endif
#elif defined(__APPLE__)
# include <libkern/OSByteOrder.h>
# define B0_H2LE16(value) OSSwapHostToLittleInt16(value)
# define B0_H2LE32(value) OSSwapHostToLittleInt32(value)
# define B0_H2LE64(value) OSSwapHostToLittleInt64(value)
# define B0_LE2H16(value) OSSwapLittleToHostInt16(value)
# define B0_LE2H32(value) OSSwapLittleToHostInt32(value)
# define B0_LE2H64(value) OSSwapLittleToHostInt64(value)
#elif defined(_WIN32) || defined(_MSC_VER) || defined(__MINGW32__)
# include <sys/param.h>
# if (BYTE_ORDER == LITTLE_ENDIAN)
#  define B0_H2LE16(value) (value)
#  define B0_H2LE32(value) (value)
#  define B0_H2LE64(value) (value)
#  define B0_LE2H16(value) (value)
#  define B0_LE2H32(value) (value)
#  define B0_LE2H64(value) (value)
# elif (BYTE_ORDER == BIG_ENDIAN)
#  define B0_H2LE16(value) __builtin_bswap16(value)
#  define B0_H2LE32(value) __builtin_bswap32(value)
#  define B0_H2LE64(value) __builtin_bswap64(value)
#  define B0_LE2H16(value) __builtin_bswap16(value)
#  define B0_LE2H32(value) __builtin_bswap32(value)
#  define B0_LE2H64(value) __builtin_bswap64(value)
# else
#  error "Endianness not supported"
# endif /* (BYTE_ORDER == BIG_ENDIAN) */
#else
# error "Platform not supported"
#endif

#endif /* !defined(__DOXYGEN__) */

#endif
