/*
 * This file is part of libbox0.
 * Copyright (C) 2013-2015 Kuldeep Singh Dhaka <kuldeep@madresistor.com>
 *
 * libbox0 is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * libbox0 is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with libbox0.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef LIBBOX0_COMMON_H
#define LIBBOX0_COMMON_H

/* __BEGIN_DECLS should be used at the beginning of your declarations,
   so that C++ compilers don't mangle their names.  Use __END_DECLS at
   the end of C declarations. */
#undef __BEGIN_DECLS
#undef __END_DECLS
#ifdef __cplusplus
# define __BEGIN_DECLS extern "C" {
# define __END_DECLS }
#else
# define __BEGIN_DECLS /* empty */
# define __END_DECLS /* empty */
#endif

__BEGIN_DECLS

/* Reference: http://gcc.gnu.org/wiki/Visibility */
#if defined(_WIN32) || defined(__CYGWIN__)
	#if defined(box0_EXPORTS)
		#if defined(__GNUC__)
			#define B0_API __attribute__ ((dllexport))
		#else /* defined(__GNUC__) */
			/* Note: actually gcc seems to also supports this syntax. */
			#define B0_API __declspec(dllexport)
		#endif /* defined(__GNUC__) */
	#else /* defined(box0_EXPORTS) */
		#if defined(__GNUC__)
			#define B0_API __attribute__ ((dllimport))
		#else
			/* Note: actually gcc seems to also supports this syntax. */
			#define B0_API __declspec(dllimport)
		#endif
	#endif /* defined(box0_EXPORTS) */
	#define B0_PRIV
#else /* defined(_WIN32) || defined(__CYGWIN__) */
	#if __GNUC__ >= 4
		#define B0_API __attribute__ ((visibility ("default")))
		#define B0_PRIV  __attribute__ ((visibility ("hidden")))
	#else /* __GNUC__ >= 4 */
		#define B0_API
		#define B0_PRIV
	#endif /* __GNUC__ >= 4 */
#endif /* defined(_WIN32) || defined(__CYGWIN__) */

#define B0_PACKED __attribute__((packed))

/* thanks Bionic */
#if defined(__STDC__VERSION__) && __STDC_VERSION__ >= 199901L
# define __restrict	restrict
#else
/* # if !__GNUC_PREREQ__(2, 92) (__GNU) */
# if (__GNUC__ == 2 && __GNUC_MINOR__ >= 92) || (__GNUC__ > 2)
#  define __restrict /* delete __restrict when not supported */
# endif
#endif

/* Just for readability */
#define B0_IS_NULL(ptr) (ptr == NULL)
#define B0_IS_NOT_NULL(ptr) (!B0_IS_NULL(ptr))

/* some commonly used macro's */
#define B0_MIN(a, b) (((a) > (b)) ? (b) : (a))
#define B0_MAX(a, b) (((a) > (b)) ? (a) : (b))
#define B0_ARRAY_SIZE(arr) (sizeof(arr)/sizeof(arr[0]))
#define B0_CLAMP(val, min, max) B0_MIN(B0_MAX(val, min), max)

/* http://stackoverflow.com/a/2745086.
 * WARNING: only intended for integer values */
#define B0_DIVIDE_AND_CEIL(val, by)	(((val) + (by) - 1) / (by))

/* excellent answer: http://stackoverflow.com/a/2745086/1500988 */
#define B0_BIT2BYTE(val)			B0_DIVIDE_AND_CEIL(val, 8)

#define B0_POW2(val) (1UL << (val))

__END_DECLS

#endif
