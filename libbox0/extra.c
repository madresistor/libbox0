/*
 * This file is part of libbox0.
 * Copyright (C) 2013-2016 Kuldeep Singh Dhaka <kuldeep@madresistor.com>
 *
 * libbox0 is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * libbox0 is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with libbox0.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "libbox0-private.h"
#include <stdio.h>

/** @cond INTERNAL */

/* in below all conver little endian is assumed for raw binary */

#define __fake_endian_conv /* im fake */

/**
 * convert binary value to ieee floating value.
 * the convertion take place reading reference value.
 * this is used to convert adc value to floating values
 * @param[in] low Low reference
 * @param[in] high High reference
 * @param[in] in_bitsize bitsize of input
 * @param[in] in_data pointer to memory
 * @param[in] out_bitsize
 * 		32: IEEE 32bit precision (commonly known as float)
 * 		64: IEEE 64bit precision (commonly known as double)
 * @param[out] out_data pointer to memory where converted value will be written
 * @param[in] samples_count the number of samples to convert.
 * @return result code
 */
b0_result_code b0_math_raw_to_ieee(b0_device *dev, double low, double high,
	unsigned in_bitsize, void *in_data, unsigned out_bitsize,
	void *out_data,  size_t samples_count)
{
	/* TODO: for differential */
	/* TODO: for big endian */

#define __RAW_CALCULATION_CODE_RAW_TO_IEEE(float_type, raw_type, endian_conv) 	\
	size_t i;															\
	raw_type mask = (1UL << in_bitsize) - 1;							\
	raw_type *storage_in = in_data;										\
	float_type *storage_out = out_data;									\
	float_type resolution = (high - low) / B0_POW2(in_bitsize);			\
	B0_PARALLEL_FOR														\
	for (i = 0; i < samples_count; i++) {								\
		/* endianness conver if bitsize required */						\
		float_type value = endian_conv (storage_in[i]) & mask;			\
		value *= resolution;											\
		value += low;													\
		storage_out[i] = value;											\
	}

	switch(B0_BIT2BYTE(in_bitsize)) {
	case 0:
		B0_DEBUG(dev, "BUG: bytes = 0, it should not be");
	return B0_BUG;
	case 1: {
		if (out_bitsize == 32 /* float */) {
			__RAW_CALCULATION_CODE_RAW_TO_IEEE(float, uint8_t, __fake_endian_conv)
		} else if (out_bitsize == 64 /* double */ ) {
			__RAW_CALCULATION_CODE_RAW_TO_IEEE(double, uint8_t, __fake_endian_conv)
		} else {
			B0_ERROR(dev,
				"IEEE Binary%" PRIu32 "not supported", out_bitsize);
			return B0_ERR_SUPP;
		}
	} break;
	case 2: {
		if (out_bitsize == 32 /* float */) {
			__RAW_CALCULATION_CODE_RAW_TO_IEEE(float, uint16_t, B0_LE2H16)
		} else if (out_bitsize == 64 /* double */ ) {
			__RAW_CALCULATION_CODE_RAW_TO_IEEE(double, uint16_t, B0_LE2H16)
		} else {
			B0_ERROR(dev,
				"IEEE Binary%" PRIu32 "not supported", out_bitsize);
			return B0_ERR_SUPP;
		}
	} break;
	default:
		/* TODO: for bitsize greater than 16 */
		B0_WARN(dev, "bitsize larger than 16 are not supported currently");
	return B0_ERR_SUPP;
	}

	return B0_OK;
}

/**
 * convert ieee floating point values to raw values.
 * used for converting floating values to DAC values using the reference
 * @param[in] low Low reference
 * @param[in] high High reference
 * @param[in] in_bitsize in bitsize
 * 		64: IEEE 64bit precision,
 * 		32: IEEE 32bit precision
 * @param[in] in_data pointer to ieee floating values
 * @param[in] out_bitsize bitsize to convert to
 * @param[out] out_data pointer to memory where converted values will be written
 * @param[in] samples_count the number of samples to generate
 * @return result code
 */
b0_result_code b0_math_ieee_to_raw(b0_device *dev, double low, double high,
	unsigned int in_bitsize, void *in_data,
	unsigned int out_bitsize, void *out_data,  size_t samples_count)
{
	/* TODO: for differential */
	/* TODO: for big endian */

#define __RAW_CALCULATION_CODE_IEEEE_TO_RAW(float_type, raw_type, endian_conv)	\
	size_t i;															\
	raw_type *storage_out = out_data;									\
	float_type *storage_in = in_data;									\
	unsigned long long steps = B0_POW2(out_bitsize);					\
	float_type resolution = (high - low) / steps;						\
	B0_PARALLEL_FOR														\
	for (i = 0; i < samples_count; i++) {								\
		float_type value = storage_in[i];								\
		value = B0_CLAMP(value, low, high);								\
		value -= low;													\
		unsigned long long code = value / resolution;					\
		code = B0_MIN(code, (steps - 1));								\
		storage_out[i] = code;											\
		storage_out[i] = endian_conv (storage_out[i]);					\
	}

	switch(B0_BIT2BYTE(out_bitsize)) {
	case 0:
		B0_DEBUG(dev, "BUG: bytes = 0, it should not be");
	return B0_BUG;
	case 1: {
		if (in_bitsize == 32 /* float */) {
			__RAW_CALCULATION_CODE_IEEEE_TO_RAW(float, uint8_t, __fake_endian_conv)
		} else if (in_bitsize == 64 /* double */ ) {
			__RAW_CALCULATION_CODE_IEEEE_TO_RAW(double, uint8_t, __fake_endian_conv)
		} else {
			B0_ERROR(dev, "IEEE Binary%" PRIu32 "not supported", in_bitsize);
			return B0_ERR_SUPP;
		}
	} break;
	case 2: {
		if (in_bitsize == 32 /* float */) {
			__RAW_CALCULATION_CODE_IEEEE_TO_RAW(float, uint16_t, B0_H2LE16)
		} else if (in_bitsize == 64 /* double */ ) {
			__RAW_CALCULATION_CODE_IEEEE_TO_RAW(double, uint16_t, B0_H2LE16)
		} else {
			B0_ERROR(dev,
				"IEEE Binary%" PRIu32 "not supported", in_bitsize);
			return B0_ERR_SUPP;
		}
	} break;
	default:
		/* TODO: for bitsize greater than 16 */
		B0_WARN(dev,
			"bitsize larger than 16 are not supported currently");
	return B0_ERR_SUPP;
	}

	return B0_OK;
}

/** @endcond */
