/*
 * This file is part of libbox0.
 * Copyright (C) 2013-2016 Kuldeep Singh Dhaka <kuldeep@madresistor.com>
 *
 * libbox0 is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * libbox0 is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with libbox0.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef LIBBOX0_PRIVATE_H
#define LIBBOX0_PRIVATE_H

#include <stdint.h>
#include <inttypes.h>
#include <math.h>
#include <stdbool.h>
#include <stdio.h>
#include <stddef.h>
#include <assert.h>
#include <pthread.h>
#include <stdarg.h>
#include <stdlib.h>
#include <ctype.h>
#include <string.h>
#include <unistd.h>
#include <errno.h>

#if defined(_WIN32) || defined(_MSC_VER) || defined(__MINGW32__)
/* Microsoft Windows specific bullshit */
# define WIN_SPECIFIC_BULLSHIT 1
#endif

#if defined(WIN_SPECIFIC_BULLSHIT)
# include <windows.h>
#endif

#include "libbox0.h"
#include "config.h"

#include "backend/backend.h"

#include "basic-private.h"
#include "device-private.h"
#include "extra-private.h"

#include "module/module-private.h"
#include "module/ain-private.h"
#include "module/aout-private.h"
#include "module/spi-private.h"
#include "module/i2c-private.h"
#include "module/dio-private.h"
#include "module/pwm-private.h"

/* size_t cross platform print support
 * http://stackoverflow.com/questions/1546789
 */
/* POSIX like printf for size_t */
#if defined(WIN_SPECIFIC_BULLSHIT)
# define B0_PRIS_PREFIX "I"
#elif defined(__GNUC__)
# define B0_PRIS_PREFIX "z"
#else
# error "size_t not supported, please specify"
#endif

#define PRIdS B0_PRIS_PREFIX "d"
#define PRIxS B0_PRIS_PREFIX "x"
#define PRIuS B0_PRIS_PREFIX "u"
#define PRIXS B0_PRIS_PREFIX "X"
#define PRIoS B0_PRIS_PREFIX "o"

/* base level logging macros
 * these macros are used mostly for higher level of logging and
 * provide easy access to internal logging
 */
#if (B0_DISABLE_LOGGING == 0)
#define B0_LOG(dev, level, fmt, ...)									\
	b0_logger((dev), __FUNCTION__, level, (fmt), ##__VA_ARGS__)
# define B0_ERROR(dev, fmt, ...) B0_LOG(dev, B0_LOG_ERROR, fmt, ##__VA_ARGS__)
# define B0_WARN(dev, fmt, ...) B0_LOG(dev, B0_LOG_WARN, fmt, ##__VA_ARGS__)
# define B0_INFO(dev, fmt, ...) B0_LOG(dev, B0_LOG_INFO, fmt, ##__VA_ARGS__)
# define B0_DEBUG(dev, fmt, ...) B0_LOG(dev, B0_LOG_DEBUG, fmt, ##__VA_ARGS__)
#else /* (B0_DISABLE_LOGGING == 0) */
# define B0_WARN(...) ((void)0)
# define B0_ERROR(...) ((void)0)
# define B0_INFO(...) ((void)0)
# define B0_DEBUG(...) ((void)0)
#endif /* (B0_DISABLE_LOGGING == 0) */

/* Medium level of macro for logging that utilizes base
 * level macros for some common task
 *
 * B0_ASSERT_RETURN_ON_FAIL is a macro that pretends like assert()
 *   but utilizes B0_ERROR for printing.
 *  the print format is like assert.
 *   http://www.gnu.org/software/libc/manual/html_node/Consistency-Checking.html
 *  also, instead of calling exit(), this will return the "ret" on failure.
 *
 * note: in case logging is disabled. nothing will be printed.
 * 		two case arises (to perform assertion or not):
 * 		if NDEBUG is defined:
 * 			assertion will also not done.  (seriously?)
 * 			every last drop of boolean comparision will be removed.
 * 			can have performance benifit (but....)
 * 		else:
 * 			assertion will be performed and "ret" is returned in case of failure
 */
#if (B0_DISABLE_LOGGING == 0)
# define B0_ASSERT_RETURN_ON_FAIL(dev, to_assert, ret)					\
	if (!(to_assert)) {													\
		B0_ERROR(dev, "%s:%"PRIuS": %s: Assertion `%s' failed.",		\
		__FILE__, __LINE__, __FUNCTION__, #to_assert);					\
		return ret;														\
	}
#else /* (B0_DISABLE_LOGGING == 0) */
# if defined(NDEBUG)
#  define B0_ASSERT_RETURN_ON_FAIL(dev, to_assert, ret)
# else /* defined(NDEBUG) */
#  define B0_ASSERT_RETURN_ON_FAIL(dev, to_assert, ret)				\
	if (!(to_assert)) {													\
		return (ret);													\
	}
# endif /* defined(NDEBUG) */
#endif /* (B0_DISABLE_LOGGING == 0) */

/* more heigher level of macros that utilize B0_ASSERT_RETURN_ON_FAIL
 * common task like checking NULL pointers.
 * 	NULL arguments, NULL return by memory allocator etc..
 */
#define B0_ASSERT_RETURN_ON_NULL(dev, ptr, ret)							\
	B0_ASSERT_RETURN_ON_FAIL(dev, B0_IS_NOT_NULL(ptr), ret)
#define B0_ASSERT_RETURN_ON_NULL_ALLOC(dev, ptr) 						\
	B0_ASSERT_RETURN_ON_NULL(dev, ptr, B0_ERR_ALLOC)
#define B0_ASSERT_RETURN_ON_NULL_ARG(dev, ptr)							\
	B0_ASSERT_RETURN_ON_NULL(dev, ptr, B0_ERR_ARG)

/* a variant of B0_ASSERT_ON_FAIL in
 *  which if the test fail, return B0_ERR_ARG
 */
#define B0_ASSERT_RETURN_ON_FAIL_ARG(dev, to_assert)					\
	B0_ASSERT_RETURN_ON_FAIL(dev, to_assert, B0_ERR_ARG)
/*
 * "stmt":
 * in libbox0, a code that return a integer value according result code.
 * result code are code that tell the caller about the success or failure.
 * generally "int r" is used to store the result code.
 * so,	r < 0: error
 * 		r == 0: ok
 * 		r > 0: ok with some more info.
 *
 *
 * libbox0 has a common function call style: b0_result_code b0_<func-name>(<arguments>);
 * so, their checking for error and make code much simpler to read and write
 *
 * _B0_DEBUG_STMT is a macro that does all the heavy lifting of
 *  getting, comparing and printing error.
 *  it is utilized by other level of "stmt" logger to do tasks.
 *  it store the result-code in my_fancy_r and does the comparing using it.
 *  this is intended for internal purpose and is expected not to use in code directly.
 */
#if (B0_DISABLE_LOGGING == 0)
# define _B0_DEBUG_STMT(dev, stmt, my_fancy_r) {						\
	my_fancy_r = (stmt);												\
	if (B0_ERROR_RESULT_CODE(my_fancy_r)) {								\
		const uint8_t *r_name = b0_result_name(my_fancy_r);				\
		const char *fmt = "`"#stmt "' returned %s(%i)";					\
		B0_DEBUG(dev, fmt, r_name, my_fancy_r);							\
	}																	\
}
#else /* (B0_DISABLE_LOGGING == 0) */
# define _B0_DEBUG_STMT(dev, stmt, my_fancy_r) my_fancy_r = (stmt)
#endif

/*
 * three variant used for debugging statement
 * ==========================================
 *
 * all these three uses _B0_DEBUG_STMT
 *
 * 1. B0_DEBUG_RETURN_ON_FAIL_STMT
 * 		return the result code when it the result code is an error
 * 		if the result code isnt error, it let further code exection
 * 		useful: when the stmt fail will half further execution of code
 *
 * 2. B0_DEBUG_RETURN_STMT
 * 		return what ever the result code it.
 * 		useful: when stmt is the last thing to do
 *
 * 3. B0_DEBUG_STMT
 * 		just a wrapper over _B0_DEBUG_STMT with an `int r' defined
 * 		useful: when stmt fails then nothing disastrous will
 * 			happend and code can go further
 *
 * @note statment are different from assertion error.
 *    assertion error assert statement for true false
 *    where as "stmt" in libbox0 are code which return result code and
 *    result-code < 0 are error codes
 *
 */

#define B0_DEBUG_RETURN_ON_FAIL_STMT(dev, stmt) {						\
	b0_result_code _r;													\
	_B0_DEBUG_STMT(dev, stmt, _r);										\
	if (B0_ERROR_RESULT_CODE(_r)) {										\
		return _r;														\
	}																	\
}

#define B0_DEBUG_RETURN_STMT(dev, stmt) {								\
	b0_result_code _r;													\
	_B0_DEBUG_STMT(dev, stmt, _r);										\
	return _r;															\
}

#define B0_DEBUG_STMT(dev, stmt) {										\
	int _r;																\
	_B0_DEBUG_STMT(dev, stmt, _r);										\
}

#define B0_GENERIC_MODULE(mod) ((b0_module *)(mod))
#define B0_MODULE_DEVICE(module) B0_GENERIC_MODULE(module)->device

/* OpenMP support
 *
 * _Pragma() require atleast c99
 */
#if (B0_USE_OPENMP == 1)
# include <omp.h>
# define B0_PARALLEL_FOR _Pragma("omp parallel for")
#else /* (B0_USE_OPENMP == 1) */
# define B0_PARALLEL_FOR
#endif /* (B0_USE_OPENMP == 1) */

/* Sleep API
 *
 * POSIX: "usleep" and "sleep" are used
 * Microsoft Windows: "Sleep" API is used
 *
 * all parameter are unsigned integer
 */
#if defined(WIN_SPECIFIC_BULLSHIT)
# define B0_SLEEP_MICROSECOND(value) Sleep((unsigned int)ceil(value / 1000.0))
# define B0_SLEEP_MILLISECOND(value) Sleep(value)
# define B0_SLEEP_SECOND(value) Sleep(value * 1000)
#else /* defined(WIN_SPECIFIC_BULLSHIT) */
# include <unistd.h>
# define B0_SLEEP_MICROSECOND(value) usleep(value)
# define B0_SLEEP_MILLISECOND(value) usleep(value * 1000)
# define B0_SLEEP_SECOND(value) sleep(value)
#endif

#define B0_BACKEND(dev, sec) (dev->backend->sec)

#define B0_ASSERT_BACKEND_FUNC_NULL(dev, sec, path)						\
	B0_ASSERT_RETURN_ON_NULL(dev, B0_BACKEND(dev, sec).path, B0_ERR)

#define B0_DEVICE_FRONTEND_DATA(dev) (dev->frontend_data)
#define B0_MODULE_FRONTEND_DATA(mod) (B0_GENERIC_MODULE(mod)->frontend_data)

#define B0_DEVICE_BACKEND_DATA(dev) ((dev)->backend_data)
#define B0_MODULE_BACKEND_DATA(mod) (B0_GENERIC_MODULE(mod)->backend_data)

/* Useful in case alloca()/_alloca() work without problem.
 * Enabling this feature is not recommended unless you know what you are doing.
 * Or readed https://stackoverflow.com/questions/1018853.
 *
 * Note: just use
 *  B0_TEMP_BUFFER_ALLOC as malloc() and
 *  B0_TEMP_BUFFER_FREE as free()
 * */
#if defined(B0_TEMP_BUFFER_USE_ALLOCA)

# if defined(WIN_SPECIFIC_BULLSHIT)
#  include <malloc.h>
#  define B0_TEMP_BUFFER_ALLOC(bytes) _alloca((bytes))
# else /* defined(WIN_SPECIFIC_BULLSHIT) */
#  include <alloca.h>
#  define B0_TEMP_BUFFER_ALLOC(bytes) alloca((bytes))
# endif /* defined(WIN_SPECIFIC_BULLSHIT) */

#  define B0_TEMP_BUFFER_FREE(ptr) /* Ignore */

#else /* defined(B0_TEMP_BUFFER_USE_ALLOCA) */
# include <malloc.h>
# define B0_TEMP_BUFFER_ALLOC(bytes) malloc((bytes))
# define B0_TEMP_BUFFER_FREE(ptr) free((ptr))

#endif /* defined(B0_TEMP_BUFFER_USE_ALLOCA) */

#define B0_FREE_IF_NOT_NULL(ptr)		\
	if (B0_IS_NOT_NULL((ptr))) {		\
		free((ptr));					\
	}

#define B0_UNUSED(item) (void)(item)

#define B0_INLINE inline __attribute__((always_inline))

#endif /* LIBBOX0_PRIVATE_H */
