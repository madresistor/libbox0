/*
 * This file is part of libbox0.
 * Copyright (C) 2013-2016 Kuldeep Singh Dhaka <kuldeep@madresistor.com>
 *
 * libbox0 is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * libbox0 is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with libbox0.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * @file
 * @todo for performing COMMAND, all step in procedure are not being performed.
 * 	RESPONSE should be cleared using NOP and make sure its 0 by readback.
 * 	and after command, read RESPONSE to see if the command was successful.
 * 	see dataseet p22 r1.3   4.2-Command Protocol
 * @todo b0_result_code b0_si114x_illuminance_read(b0_si114x *drv, double *value);
 */

#include "si114x.h"
#include "../../libbox0-private.h"

#if !defined(__DOXYGEN__)
struct b0_si114x {
	b0_i2c *module;
	uint8_t bSlaveAddress;
	b0_i2c_version version;
};
#endif

b0_result_code b0_si114x_open(b0_i2c *mod, b0_si114x **drv)
{
	b0_si114x *drv_alloc;
	b0_device *dev;

	B0_ASSERT_RETURN_ON_NULL_ARG(NULL, mod);
	dev = B0_MODULE_DEVICE(mod);

	B0_ASSERT_RETURN_ON_NULL_ARG(dev, drv);

	drv_alloc = calloc(1, sizeof(*drv_alloc));
	B0_ASSERT_RETURN_ON_NULL_ALLOC(dev, drv_alloc);

	drv_alloc->bSlaveAddress = 0x60;
	drv_alloc->module = mod;
	/* TODO: Use the most favourable version (HS > FM > SM)
	 *  instead of hardcoded version */
	drv_alloc->version = B0_I2C_VERSION_FM;

	/* TODO: detect slave */

	/* FIX: B0_DEBUG_RETURN_ON_FAIL_STMT, B0_ASSERT_RETURN_ON_FAIL
	 * after this point cause memory leak
	 */

	uint8_t id;
	B0_DEBUG_RETURN_ON_FAIL_STMT(dev, b0_si114x_reg_read(drv_alloc,
		B0_SI114X_PART_ID, &id));
	B0_ASSERT_RETURN_ON_FAIL(dev, id == 0x45, B0_ERR_UNAVAIL);

	/* startup code stolen from Adafruit Arduino library { */
	/* reset { */
	B0_DEBUG_RETURN_ON_FAIL_STMT(dev, b0_si114x_reg_write(drv_alloc,
		B0_SI114X_MEAS_RATE0, 0));
	B0_DEBUG_RETURN_ON_FAIL_STMT(dev, b0_si114x_reg_write(drv_alloc,
		B0_SI114X_MEAS_RATE1, 0));
	B0_DEBUG_RETURN_ON_FAIL_STMT(dev, b0_si114x_reg_write(drv_alloc,
		B0_SI114X_IRQ_ENABLE, 0));
	/* SI114X_REG_IRQMODE1(0x05) set to 0  from
	 *   where these Adafruit got this?? (nothing in r1.3)
	 */
	B0_DEBUG_RETURN_ON_FAIL_STMT(dev, b0_si114x_reg_write(drv_alloc,
		0x05, 0));
	/* SI114X_REG_IRQMODE2(0x06) set to 0  from
	 *   where these Adafruit got this?? (nothing in r1.3)
	 */
	B0_DEBUG_RETURN_ON_FAIL_STMT(dev, b0_si114x_reg_write(drv_alloc,
		0x06, 0));
	B0_DEBUG_RETURN_ON_FAIL_STMT(dev, b0_si114x_reg_write(drv_alloc,
		B0_SI114X_INT_CFG, 0));
	B0_DEBUG_RETURN_ON_FAIL_STMT(dev, b0_si114x_reg_write(drv_alloc,
		B0_SI114X_IRQ_STATUS, 0xFF));
	B0_DEBUG_RETURN_ON_FAIL_STMT(dev, b0_si114x_reg_write(drv_alloc,
		B0_SI114X_COMMAND, B0_SI114X_COMMAND_RESET));
	B0_SLEEP_MILLISECOND(10 /* minimum 1ms as per datasheet */);
	B0_DEBUG_RETURN_ON_FAIL_STMT(dev, b0_si114x_reg_write(drv_alloc,
		B0_SI114X_HW_KEY, 0x17));
	/* why delay of 10ms after HW_KEY in Adafruit code? */
	B0_SLEEP_MILLISECOND(10);
	/* }
	 *
	 * begin {
	 */

	/* enable UVindex measurement coefficients! */
	B0_DEBUG_RETURN_ON_FAIL_STMT(dev, b0_si114x_reg_write(drv_alloc,
		B0_SI114X_UCOEF0, 0x29));
	B0_DEBUG_RETURN_ON_FAIL_STMT(dev, b0_si114x_reg_write(drv_alloc,
		B0_SI114X_UCOEF1, 0x89));
	B0_DEBUG_RETURN_ON_FAIL_STMT(dev, b0_si114x_reg_write(drv_alloc,
		B0_SI114X_UCOEF2, 0x02));
	B0_DEBUG_RETURN_ON_FAIL_STMT(dev, b0_si114x_reg_write(drv_alloc,
		B0_SI114X_UCOEF3, 0x00));

	/* enable UV sensor */
	uint8_t chlist = B0_SI114X_CHLIST_EN_UV;
	chlist |= B0_SI114X_CHLIST_EN_ALS_IR;
	chlist |= B0_SI114X_CHLIST_EN_ALS_VIS;
	chlist |= B0_SI114X_CHLIST_EN_PS1;
	B0_DEBUG_RETURN_ON_FAIL_STMT(dev, b0_si114x_intseq_reg_write(
		drv_alloc, B0_SI114X_CHLIST, chlist));

	/* enable interrupt on every sample   why? */
	B0_DEBUG_RETURN_ON_FAIL_STMT(dev, b0_si114x_reg_write(drv_alloc,
		B0_SI114X_INT_CFG, B0_SI114X_INT_CFG_INT_OE));
	B0_DEBUG_RETURN_ON_FAIL_STMT(dev, b0_si114x_reg_write(drv_alloc,
		B0_SI114X_IRQ_ENABLE, B0_SI114X_IRQ_ENABLE_ALS_IE));

	/* Prox Sense 1 */
	/*    program LED current */
	B0_DEBUG_RETURN_ON_FAIL_STMT(dev, b0_si114x_reg_write(drv_alloc,
		B0_SI114X_PS_LED21, 0x03)); /* 20mA for LED 1 only */
	B0_DEBUG_RETURN_ON_FAIL_STMT(dev, b0_si114x_intseq_reg_write(drv_alloc,
		B0_SI114X_PS1_ADCMUX, B0_SI114X_PS1_ADCMUX_LARGE_IR));
	/*    prox sensor #1 uses LED #1 */
	B0_DEBUG_RETURN_ON_FAIL_STMT(dev,
		b0_si114x_intseq_reg_write(drv_alloc, B0_SI114X_PSLED12_SELECT,
			B0_SI114X_PSLED12_SELECT_PS1_LED(1)));
	/*    fastest clocks, clock div 1 */
	B0_DEBUG_RETURN_ON_FAIL_STMT(dev,
		b0_si114x_intseq_reg_write(drv_alloc, B0_SI114X_PS_ADC_GAIN,
			B0_SI114X_PS_ADC_GAIN_PS_ADC_GAIN(0)));
	/*    take 511 clocks to measure */
	B0_DEBUG_RETURN_ON_FAIL_STMT(dev,
		b0_si114x_intseq_reg_write(drv_alloc, B0_SI114X_PS_ADC_COUNTER,
			B0_SI114X_PS_ADC_COUNTER_PS_ADC_REC_511_ADC_CLK));
	/*    in prox mode, high range */
	B0_DEBUG_RETURN_ON_FAIL_STMT(dev,
		b0_si114x_intseq_reg_write(drv_alloc, B0_SI114X_PS_ADC_MISC,
		B0_SI114X_PS_ADC_MISC_PS_ADC_MODE | B0_SI114X_PS_ADC_MISC_PS_RANGE));
	/*    in prox mode, high range */
	B0_DEBUG_RETURN_ON_FAIL_STMT(dev,
		b0_si114x_intseq_reg_write(drv_alloc, B0_SI114X_ALS_IR_ADCMUX,
		B0_SI114X_ALS_IR_ADCMUX_ALS_IR_ADCMUX_SMALL_IR));
	/*    fastest clocks, clock div 1 */
	B0_DEBUG_RETURN_ON_FAIL_STMT(dev,
		b0_si114x_intseq_reg_write(drv_alloc, B0_SI114X_ALS_IR_ADC_GAIN, 0));
	/*    take 511 clocks to measure */
	B0_DEBUG_RETURN_ON_FAIL_STMT(dev,
		b0_si114x_intseq_reg_write(drv_alloc, B0_SI114X_ALS_IR_ADC_COUNTER,
		B0_SI114X_ALS_IR_ADC_COUNTER_IR_ADC_REC_511_ADC_CLK));
	/*    in high range mode */
	B0_DEBUG_RETURN_ON_FAIL_STMT(dev,
		b0_si114x_intseq_reg_write(drv_alloc, B0_SI114X_ALS_IR_ADC_MISC,
		B0_SI114X_ALS_IR_ADC_MISC_IR_RANGE));

	/*    fastest clocks, clock div 1 */
	B0_DEBUG_RETURN_ON_FAIL_STMT(dev,
		b0_si114x_intseq_reg_write(drv_alloc, B0_SI114X_ALS_VIS_ADC_GAIN, 0));
	/*    take 511 clocks to measure */
	B0_DEBUG_RETURN_ON_FAIL_STMT(dev,
		b0_si114x_intseq_reg_write(drv_alloc, B0_SI114X_ALS_VIS_ADC_COUNTER,
		 B0_SI114X_ALS_VIS_ADC_COUNTER_VIS_ADC_REC_511_ADC_CLK));
	/*    in high range mode (not normal signal) */
	B0_DEBUG_RETURN_ON_FAIL_STMT(dev,
		b0_si114x_intseq_reg_write(drv_alloc, B0_SI114X_ALS_VIS_ADC_MISC,
		B0_SI114X_ALS_VIS_ADC_MISC_VIS_RANGE));

	/*  } { */
	/*    measurement rate for auto      255 * 31.25uS = 8ms */
	B0_DEBUG_RETURN_ON_FAIL_STMT(dev, b0_si114x_reg_write(drv_alloc,
		B0_SI114X_MEAS_RATE0, 0xFF));
	/*    auto run */
	B0_DEBUG_RETURN_ON_FAIL_STMT(dev, b0_si114x_reg_write(drv_alloc,
		B0_SI114X_COMMAND, B0_SI114X_COMMAND_ALS_AUTO));
	/* } */

	*drv = drv_alloc;

	return B0_OK;
}

b0_result_code b0_si114x_close(b0_si114x *drv)
{
	B0_ASSERT_RETURN_ON_NULL_ARG(NULL, drv);

	free(drv);
	return B0_OK;
}

b0_result_code b0_si114x_uv_index_read(b0_si114x *drv, double *value)
{
	b0_device *dev;
	B0_ASSERT_RETURN_ON_NULL_ARG(NULL, drv);

	dev = B0_MODULE_DEVICE(drv->module);

	const b0_i2c_sugar_arg arg = {
		.addr = drv->bSlaveAddress,
		.version = drv->version
	};

	uint8_t data[2];
	B0_DEBUG_RETURN_ON_FAIL_STMT(dev, b0_i2c_master_write8_read(
		drv->module, &arg, B0_SI114X_AUX_UVINDEX0, data, sizeof(data)));

	uint16_t _v = (data[1] << 8) | data[0];
	B0_DEBUG(dev, "B0_SI114X_AUX_UVINDEX = %"PRIu16, _v);
	*value = _v / 100.0;

	return B0_OK;
}

b0_result_code b0_si114x_reg_write(b0_si114x *drv, uint8_t reg, uint8_t val)
{
	b0_device *dev;
	B0_ASSERT_RETURN_ON_NULL_ARG(NULL, drv);

	dev = B0_MODULE_DEVICE(drv->module);

	const b0_i2c_sugar_arg arg = {
		.addr = drv->bSlaveAddress,
		.version = drv->version
	};

	uint8_t data[2] = {reg, val};
	/* return */ B0_DEBUG_RETURN_STMT(dev,
		b0_i2c_master_write(drv->module, &arg, data, sizeof(data)));
}

b0_result_code b0_si114x_reg_read(b0_si114x *drv, uint8_t reg, uint8_t *val)
{
	b0_device *dev;
	B0_ASSERT_RETURN_ON_NULL_ARG(NULL, drv);

	dev = B0_MODULE_DEVICE(drv->module);
	B0_ASSERT_RETURN_ON_NULL_ARG(dev, val);

	const b0_i2c_sugar_arg arg = {
		.addr = drv->bSlaveAddress,
		.version = drv->version
	};

	/* return */ B0_DEBUG_RETURN_STMT(dev,
		b0_i2c_master_write8_read(drv->module, &arg, reg, val, 1));
}

b0_result_code b0_si114x_intseq_reg_write(b0_si114x *drv, uint8_t reg, uint8_t val)
{
	b0_device *dev;
	B0_ASSERT_RETURN_ON_NULL_ARG(NULL, drv);

	dev = B0_MODULE_DEVICE(drv->module);

	uint8_t param_wr[2] = {B0_SI114X_PARAM_WR, val};
	uint8_t cmd[2] = {B0_SI114X_COMMAND, B0_SI114X_COMMAND_PARAM_SET(reg)};

	const b0_i2c_task tasks[] = {{
		.addr = drv->bSlaveAddress,
		.flags = B0_I2C_TASK_WRITE,
		.version = drv->version,
		.data = param_wr,
		.count = 2,
	}, {
		.addr = drv->bSlaveAddress,
		.flags = B0_I2C_TASK_WRITE | B0_I2C_TASK_LAST,
		.version = drv->version,
		.data = cmd,
		.count = 2
	}};

	B0_DEBUG_RETURN_STMT(dev, b0_i2c_master_start(drv->module, tasks, NULL, NULL));
}

b0_result_code b0_si114x_intseq_reg_read(b0_si114x *drv, uint8_t reg, uint8_t *val)
{
	b0_device *dev;
	B0_ASSERT_RETURN_ON_NULL_ARG(NULL, drv);

	dev = B0_MODULE_DEVICE(drv->module);
	B0_ASSERT_RETURN_ON_NULL_ARG(dev, val);

	uint8_t cmd[2] = {
		B0_SI114X_COMMAND,
		B0_SI114X_COMMAND_PARAM_QUERY(reg)
	};

	uint8_t param_rd_addr = B0_SI114X_PARAM_RD;

	const b0_i2c_task tasks[] = {{
		.addr = drv->bSlaveAddress,
		.flags = B0_I2C_TASK_WRITE,
		.version = drv->version,
		.data = cmd,
		.count = 2
	}, {
		.addr = drv->bSlaveAddress,
		.flags = B0_I2C_TASK_WRITE,
		.version = drv->version,
		.data = &param_rd_addr,
		.count = 1
	}, {
		.addr = drv->bSlaveAddress,
		.flags = B0_I2C_TASK_READ | B0_I2C_TASK_LAST,
		.version = drv->version,
		.data = val,
		.count = 1
	}};

	B0_DEBUG_RETURN_STMT(dev, b0_i2c_master_start(drv->module, tasks, NULL, NULL));
}

b0_result_code b0_si114x_proximity_read(b0_si114x *drv, double *proximity)
{
	b0_device *dev;
	uint8_t data[2];
	uint16_t _v;

	B0_ASSERT_RETURN_ON_NULL_ARG(NULL, drv);
	dev = B0_MODULE_DEVICE(drv->module);

	B0_ASSERT_RETURN_ON_NULL_ARG(dev, proximity);

	const b0_i2c_sugar_arg arg = {
		.addr = drv->bSlaveAddress,
		.version = drv->version
	};

	B0_DEBUG_RETURN_ON_FAIL_STMT(dev, b0_i2c_master_write8_read(
		drv->module, &arg,
		B0_SI114X_PS1_DATA0, data, sizeof(data)));

	B0_DEBUG(dev,
		"TODO: b0_si114x_proximity_read(), returning raw uint16 value");
	_v = (data[1] << 8) | data[0];
	*proximity = _v;

	return B0_OK;
}
