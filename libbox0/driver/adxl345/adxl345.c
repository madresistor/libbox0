/*
 * This file is part of libbox0.
 * Copyright (C) 2013-2016 Kuldeep Singh Dhaka <kuldeep@madresistor.com>
 *
 * libbox0 is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * libbox0 is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with libbox0.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "adxl345.h"
#include "../../libbox0-private.h"

#if !defined(__DOXYGEN__)
struct b0_adxl345 {
	b0_i2c *module;
	uint8_t bSlaveAddress;
	b0_i2c_version version;
	double resolution;
};
#endif

b0_result_code b0_adxl345_open_i2c(b0_i2c *mod, b0_adxl345 **drv, bool ALT_ADDRESS)
{
	b0_adxl345 *drv_alloc;
	b0_device *dev;
	uint8_t devid;
	uint8_t reg_write[2];
	b0_result_code r;

	B0_ASSERT_RETURN_ON_NULL_ARG(NULL, mod);
	dev = B0_MODULE_DEVICE(mod);

	B0_ASSERT_RETURN_ON_NULL_ARG(dev, drv);

	/* allocate memory */
	drv_alloc = calloc(1, sizeof(*drv_alloc));
	B0_ASSERT_RETURN_ON_NULL_ALLOC(dev, drv_alloc);

	/* fill in the object */
	drv_alloc->module = mod;
	drv_alloc->bSlaveAddress = ALT_ADDRESS ? 0x1D : 0x53;
	/* TODO: Use the most favourable version (FM > SM)
	 *  instead of hardcoded version */
	drv_alloc->version = B0_I2C_VERSION_FM;

	const b0_i2c_sugar_arg arg = {
		.addr = drv_alloc->bSlaveAddress,
		.version = drv_alloc->version
	};

	/* test the slave by reading DEVID */
	r = b0_i2c_master_write8_read(drv_alloc->module, &arg,
			B0_ADXL345_DEVID, &devid, 1);
	if (B0_ERROR_RESULT_CODE(r)) {
		B0_WARN(dev, "Unable to slave DEVID, identification failed");
	} else if (devid != B0_ADXL345_DEVID_RESET_VALUE) {
		B0_WARN(dev, "DEVID mismached with already know value");
	}



	/* set range to +/-16G with maximum resolution (13bit) */
	reg_write[0] = B0_ADXL345_DATA_FORMAT;
	reg_write[1] = 0x0B;
	r = b0_i2c_master_write(drv_alloc->module, &arg, reg_write, 2);
	if (B0_ERROR_RESULT_CODE(r)) {
		B0_ERROR(dev, "Unable to set maximum resolution");
		return r;
	}

	/* calculate resolution */
	drv_alloc->resolution = 32.0 / ((uintmax_t)1 << 13);

	/* fix: memory leak when stmt fails */
	B0_DEBUG_RETURN_ON_FAIL_STMT(dev, b0_adxl345_power_up(drv_alloc));

	/* done */
	*drv = drv_alloc;
	return B0_OK;
}

b0_result_code b0_adxl345_power_up(b0_adxl345 *drv)
{
	b0_device *dev;
	uint8_t param[2];

	B0_ASSERT_RETURN_ON_NULL_ARG(NULL, drv);
	dev = B0_MODULE_DEVICE(drv->module);

	const b0_i2c_sugar_arg arg = {
		.addr = drv->bSlaveAddress,
		.version = drv->version
	};

	/* as per AXL345 doc p25
	 * Setting MEASURE bit (D3) to enable measurement
	 */
	param[0] = B0_ADXL345_POWER_CTL;
	param[1] = 0x08;

	/* let the horsy go! muuaaaa.. */
	/* return */B0_DEBUG_RETURN_STMT(dev, b0_i2c_master_write(
		drv->module, &arg, param, 2));
}

b0_result_code b0_adxl345_close(b0_adxl345 *drv)
{
	B0_ASSERT_RETURN_ON_NULL_ARG(NULL, drv);
	/* TODO: power down */
	free(drv);
	return B0_OK;
}

/**
 * Convert L3GD20 register data to Value
 * @param rl Register LOW
 * @param rh Register HIGH
 * @param res Resolution
 * @return value
 */
inline static double adxl345_reg_to_value(uint8_t rl, uint8_t rh, double res)
{
	int16_t value = (rh << 8) | rl;
	return value * res;
}

b0_result_code b0_adxl345_read(b0_adxl345 *drv, double *x, double *y, double *z)
{
	uint8_t data[6];
	b0_device *dev;

	B0_ASSERT_RETURN_ON_NULL_ARG(NULL, drv);
	dev = B0_MODULE_DEVICE(drv->module);

	B0_ASSERT_RETURN_ON_NULL_ARG(dev, x);
	B0_ASSERT_RETURN_ON_NULL_ARG(dev, y);
	B0_ASSERT_RETURN_ON_NULL_ARG(dev, z);

	const b0_i2c_sugar_arg arg = {
		.addr = drv->bSlaveAddress,
		.version = drv->version
	};

	/* perform the query */
	B0_DEBUG_RETURN_ON_FAIL_STMT(dev, b0_i2c_master_write8_read(
		drv->module, &arg,
		B0_ADXL345_DATAX0, data, sizeof(data)));

	/* extract the values */
	*x = adxl345_reg_to_value(data[0], data[1], drv->resolution);
	*y = adxl345_reg_to_value(data[2], data[3], drv->resolution);
	*z = adxl345_reg_to_value(data[4], data[5], drv->resolution);

	return B0_OK;
}

/*
b0_result_code b0_adxl345_mode_set(b0_adxl345 *adxl345, enum b0_adxl345_mode mode)
{
	uint8_t reg_value;
	b0_device *dev;

	B0_ASSERT_RETURN_ON_NULL_ARG(NULL, adxl345);
	context = B0_CAST2MODULE(adxl345->module)->device->context;

	switch(mode) {
	case B0_ADXL345_MODE_2G:
		reg_value =
}
*/
