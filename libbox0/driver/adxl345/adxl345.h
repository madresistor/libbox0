/*
 * This file is part of libbox0.
 * Copyright (C) 2013-2016 Kuldeep Singh Dhaka <kuldeep@madresistor.com>
 *
 * libbox0 is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * libbox0 is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with libbox0.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef LIBBOX0_DRIVER_ADXL345_H
#define LIBBOX0_DRIVER_ADXL345_H

#include <stdint.h>
#include <stdbool.h>
#include "../../common.h"
#include "../../module/i2c.h"

__BEGIN_DECLS

#if defined(__DOXYGEN__)
struct b0_adxl345 {};
#endif

/**
 * @class b0_adxl345
 * @brief ADXL345 Digital Accelerometer
 */

#define B0_ADXL345_DEVID			0x00
#define B0_ADXL345_THRESH_TAP		0x1D
#define B0_ADXL345_OFSX				0x1E
#define B0_ADXL345_OFSY				0x1F
#define B0_ADXL345_OFSZ				0x20
#define B0_ADXL345_DUR				0x21
#define B0_ADXL345_LATENT			0x22
#define B0_ADXL345_WINDOW			0x23
#define B0_ADXL345_THRESH_ACT		0x24
#define B0_ADXL345_THRESH_INACT		0x25
#define B0_ADXL345_TIME_INACT		0x26
#define B0_ADXL345_ACT_INACT_CTL	0x27
#define B0_ADXL345_THRESH_FF		0x28
#define B0_ADXL345_TIME_FF			0x29
#define B0_ADXL345_TAP_AXES			0x2A
#define B0_ADXL345_ACT_TAP_STATUS	0x2B
#define B0_ADXL345_BW_RATE			0x2C
#define B0_ADXL345_POWER_CTL		0x2D
#define B0_ADXL345_INT_ENABLE		0x2E
#define B0_ADXL345_INT_MAP			0x2F
#define B0_ADXL345_INT_SOURCE		0x30
#define B0_ADXL345_DATA_FORMAT		0x31
#define B0_ADXL345_DATAX0			0x32
#define B0_ADXL345_DATAX1			0x33
#define B0_ADXL345_DATAY0			0x34
#define B0_ADXL345_DATAY1			0x35
#define B0_ADXL345_DATAZ0			0x36
#define B0_ADXL345_DATAZ1			0x37
#define B0_ADXL345_FIFO_CTL			0x38
#define B0_ADXL345_FIFO_STATUS		0x39

#define B0_ADXL345_DEVID_RESET_VALUE 0xE5

typedef struct b0_adxl345 b0_adxl345;

enum b0_adxl345_mode {
	B0_ADXL345_MODE_2G,
	B0_ADXL345_MODE_4G,
	B0_ADXL345_MODE_8G,
	B0_ADXL345_MODE_16G
};

/**
 * @brief allocate a ADXL345 (using I2C)
 * @param[in] mod libbox0 I2C module
 * @param[out] drv memory location to store the ADXL345 driver
 * @param[in] ALT_ADDRESS alternate address bit
 * @return result code
 * @note see datasheet for ALT_ADDRESS documentation
 */
B0_API b0_result_code b0_adxl345_open_i2c(b0_i2c *mod, b0_adxl345 **drv, bool ALT_ADDRESS);

/**
 * @brief free ADXL345 driver
 * @param[in] drv libbox0 ADXL345 driver
 * @return result code
 */
B0_API b0_result_code b0_adxl345_close(b0_adxl345 *drv);

/**
 * @brief read acceleration data from ADXL345
 * @details data of axis X, Y, Z
 * @param[in] drv libbox0 ADXL345 driver
 * @param[out] x pointer to acceleration X axis
 * @param[out] y pointer to acceleration Y axis
 * @param[out] z pointer to acceleration Z axis
 * @return result code
 */
B0_API b0_result_code b0_adxl345_read(b0_adxl345 *drv, double *x, double *y, double *z);

/**
 * @brief power up ADXL345
 * @param[in] drv libbox0 ADXL345 object
 * @return result code
 * @note see POWER_CTL datasheet
 */
B0_API b0_result_code b0_adxl345_power_up(b0_adxl345 *drv);

__END_DECLS

#endif
