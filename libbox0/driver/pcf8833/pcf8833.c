/*
 * This file is part of libbox0.
 * Copyright (C) 2015, 2016 Kuldeep Singh Dhaka <kuldeep@madresistor.com>
 *
 * libbox0 is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * libbox0 is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with libbox0.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "pcf8833.h"
#include "../../libbox0-private.h"

#define __CONFIG (B0_SPI_TASK_MODE0 | B0_SPI_TASK_MSB_FIRST) /* HD */
#define __BITSIZE 9
#define __SPEED B0_SPI_TASK_SPEED_USE_FALLBACK

#if !defined(__DOXYGEN__)
struct b0_pcf8833 {
	b0_spi *module;
	unsigned addr;
};
#endif

b0_result_code b0_pcf8833_open(b0_spi *mod, b0_pcf8833 **drv, unsigned addr)
{
	b0_pcf8833 *drv_alloc;
	b0_device *dev;

	B0_ASSERT_RETURN_ON_NULL_ARG(NULL, mod);
	dev = B0_MODULE_DEVICE(mod);

	B0_ASSERT_RETURN_ON_NULL_ARG(dev, drv);

	drv_alloc = calloc(1, sizeof(*drv_alloc));
	B0_ASSERT_RETURN_ON_NULL_ALLOC(dev, drv_alloc);

	drv_alloc->module = mod;
	drv_alloc->addr = addr;

	B0_DEBUG_STMT(dev, b0_pcf8833_reset(drv_alloc));

	*drv = drv_alloc;

	return B0_OK;
}

b0_result_code b0_pcf8833_close(b0_pcf8833 *drv)
{
	B0_ASSERT_RETURN_ON_NULL_ARG(NULL, drv);

	free(drv);
	return B0_OK;
}

b0_result_code b0_pcf8833_reset(b0_pcf8833 *drv)
{
	b0_device *dev;

	B0_ASSERT_RETURN_ON_NULL_ARG(NULL, drv);
	dev = B0_MODULE_DEVICE(drv->module);

	/* software Reset the controller */
	B0_DEBUG_RETURN_ON_FAIL_STMT(dev,
			b0_pcf8833_cmd(drv, B0_PCF8833_SWRESET));

	/* Turn sleep mode off */
	B0_DEBUG_RETURN_ON_FAIL_STMT(dev,
			b0_pcf8833_cmd(drv, B0_PCF8833_SLEEPOUT));

	/* Turn off inversion */
	B0_DEBUG_RETURN_ON_FAIL_STMT(dev,
			b0_pcf8833_cmd(drv, B0_PCF8833_INVOFF));

	/* set the color mode to 0x03 (12 bits-per-pixel - 4R 4G 4B) */
	B0_DEBUG_RETURN_ON_FAIL_STMT(dev,
			b0_pcf8833_write_reg8(drv, B0_PCF8833_COLMOD, 0x03));

	/* establish how the memory should be read Memory access controler.
	 * - mirror x & mirror y so that top, left is 0,0
	 * - scan in x direction top to bottom
	 * - color order is RGB */
	B0_DEBUG_RETURN_ON_FAIL_STMT(dev,
			b0_pcf8833_write_reg8(drv, B0_PCF8833_MADCTL, 0xC0));

	/* Write contrast */
	B0_DEBUG_RETURN_ON_FAIL_STMT(dev,
			b0_pcf8833_write_reg8(drv, B0_PCF8833_SETCON, 0x38));

	/* Turn On display */
	B0_DEBUG_RETURN_ON_FAIL_STMT(dev,
			b0_pcf8833_cmd(drv, B0_PCF8833_DISPON));

	return B0_OK;
}

b0_result_code b0_pcf8833_write_reg8(b0_pcf8833 *drv, uint8_t addr, uint8_t arg)
{
	return b0_pcf8833_write_reg(drv, addr, &arg, 1);
}

b0_result_code b0_pcf8833_write_reg(b0_pcf8833 *drv, uint8_t addr,
		uint8_t *arg, size_t len)
{
	b0_device *dev;
	size_t i;

	B0_ASSERT_RETURN_ON_NULL_ARG(NULL, drv);
	dev = B0_MODULE_DEVICE(drv->module);

	B0_ASSERT_RETURN_ON_NULL_ARG(dev, arg);
	B0_ASSERT_RETURN_ON_FAIL_ARG(dev, len > 0);

	b0_spi_sugar_arg sugar_arg = {
		.addr = drv->addr,
		.flags = __CONFIG,
		.bitsize = __BITSIZE,
		.speed = __SPEED
	};

	uint16_t data[255];
	data[0] = B0_PCF8833_CMD(addr);

	for (i = 0; i < len; i++) {
		data[i + 1] = B0_PCF8833_DATA(arg[i]);
	}

	B0_DEBUG_RETURN_STMT(dev,
		b0_spi_master_hd_write(drv->module, &sugar_arg, data, len + 1));
}

b0_result_code b0_pcf8833_pixel(b0_pcf8833 *drv, uint8_t r, uint8_t g, uint8_t b)
{
	b0_device *dev;

	B0_ASSERT_RETURN_ON_NULL_ARG(NULL, drv);
	dev = B0_MODULE_DEVICE(drv->module);

	B0_ASSERT_RETURN_ON_FAIL_ARG(dev, r <= 0xF);
	B0_ASSERT_RETURN_ON_FAIL_ARG(dev, g <= 0xF);
	B0_ASSERT_RETURN_ON_FAIL_ARG(dev, b <= 0xF);

	uint16_t data[4] = {
		/* set RAM WRITE mode on */
		B0_PCF8833_CMD(B0_PCF8833_RAMWR),

		/* Send the red, green (low 4bits) */
		B0_PCF8833_DATA(((r & 0xF) << 4) | (g & 0xF)),

		/* Send the blue (low 4-bits)
		 * NOTE: the next pixel Red will not be affected because of the NOP
		 */
		B0_PCF8833_DATA((b & 0xF) << 4),

		/* but because we send a NOP immediately,
		 *  the next pixel is ignored and the given pixel is written
		 */
		B0_PCF8833_CMD(B0_PCF8833_NOP)
	};

	b0_spi_sugar_arg arg = {
		.addr = drv->addr,
		.flags = __CONFIG,
		.bitsize = __BITSIZE,
		.speed = __SPEED
	};

	B0_DEBUG_RETURN_STMT(dev, b0_spi_master_hd_write(drv->module, &arg, data, 4));
}

b0_result_code b0_pcf8833_cmd(b0_pcf8833 *drv, uint8_t addr)
{
	b0_device *dev;

	B0_ASSERT_RETURN_ON_NULL_ARG(NULL, drv);
	dev = B0_MODULE_DEVICE(drv->module);

	uint16_t data = B0_PCF8833_CMD(addr);

	b0_spi_sugar_arg arg = {
		.addr = drv->addr,
		.flags = __CONFIG,
		.bitsize = __BITSIZE,
		.speed = __SPEED
	};

	B0_DEBUG_RETURN_STMT(dev, b0_spi_master_hd_write(drv->module, &arg, &data, 1));
}

b0_result_code b0_pcf8833_col_set(b0_pcf8833 *drv, uint8_t start, uint8_t end)
{
	uint8_t data[2];
	b0_device *dev;

	B0_ASSERT_RETURN_ON_NULL_ARG(NULL, drv);
	dev = B0_MODULE_DEVICE(drv->module);

	B0_ASSERT_RETURN_ON_FAIL_ARG(dev, start <= 0x83);
	B0_ASSERT_RETURN_ON_FAIL_ARG(dev, start <= end);
	B0_ASSERT_RETURN_ON_FAIL_ARG(dev, end <= 0x83);

	data[0] = start; data[1] = end;
	B0_DEBUG_RETURN_STMT(dev,
		b0_pcf8833_write_reg(drv, B0_PCF8833_CASET, data, sizeof(data)));
}

b0_result_code b0_pcf8833_page_set(b0_pcf8833 *drv, uint8_t start, uint8_t end)
{
	uint8_t data[2];
	b0_device *dev;

	B0_ASSERT_RETURN_ON_NULL_ARG(NULL, drv);
	dev = B0_MODULE_DEVICE(drv->module);

	B0_ASSERT_RETURN_ON_FAIL_ARG(dev, start <= 0x83);
	B0_ASSERT_RETURN_ON_FAIL_ARG(dev, start <= end);
	B0_ASSERT_RETURN_ON_FAIL_ARG(dev, end <= 0x83);

	data[0] = start; data[1] = end;
	B0_DEBUG_RETURN_STMT(dev,
		b0_pcf8833_write_reg(drv, B0_PCF8833_PASET, data, sizeof(data)));
}
