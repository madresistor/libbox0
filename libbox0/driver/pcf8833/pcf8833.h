/*
 * This file is part of libbox0.
 * Copyright (C) 2015, 2016 Kuldeep Singh Dhaka <kuldeep@madresistor.com>
 *
 * libbox0 is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * libbox0 is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with libbox0.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef LIBBOX0_DRIVER_PCF8833_H
#define LIBBOX0_DRIVER_PCF8833_H

#include "../../common.h"
#include "../../module/spi.h"

__BEGIN_DECLS

#if defined(__DOXYGEN__)
struct b0_pcf8833 {};
#endif

typedef struct b0_pcf8833 b0_pcf8833;

/**
 * @class b0_pcf8833
 * @brief PCF8833
 */

#define B0_PCF8833_DATA(v) ((uint16_t)(((v) & 0xFF) | (1 << 8)))
#define B0_PCF8833_CMD(v) ((uint16_t)((v) & 0xFF))

/* Register adapted from ArdGrafix6100 project!
 *  Thanks Hariharan Srinath! */

#define B0_PCF8833_NOP			0x00 /* no operation */
#define B0_PCF8833_SWRESET		0x01 /* software reset */
#define B0_PCF8833_BSTROFF		0x02 /* booster voltage OFF */
#define B0_PCF8833_BSTRON		0x03 /* booster voltage ON */
#define B0_PCF8833_RDDIDIF		0x04 /* read display identification */
#define B0_PCF8833_RDDST		0x09 /* read display status */
#define B0_PCF8833_SLEEPIN		0x10 /* sleep in */
#define B0_PCF8833_SLEEPOUT		0x11 /* sleep out */
#define B0_PCF8833_PTLON		0x12 /* partial display mode */
#define B0_PCF8833_NORON		0x13 /* display normal mode */
#define B0_PCF8833_INVOFF		0x20 /* inversion OFF */
#define B0_PCF8833_INVON		0x21 /* inversion ON */
#define B0_PCF8833_DALO			0x22 /* all pixel OFF */
#define B0_PCF8833_DAL			0x23 /* all pixel ON */
#define B0_PCF8833_SETCON		0x25 /* write contrast */
#define B0_PCF8833_DISPOFF		0x28 /* display OFF */
#define B0_PCF8833_DISPON		0x29 /* display ON */
#define B0_PCF8833_CASET		0x2A /* column address set */
#define B0_PCF8833_PASET		0x2B /* page address set */
#define B0_PCF8833_RAMWR		0x2C /* memory write */
#define B0_PCF8833_RGBSET		0x2D /* colour set */
#define B0_PCF8833_PTLAR		0x30 /* partial area */
#define B0_PCF8833_VSCRDEF		0x33 /* vertical scrolling definition */
#define B0_PCF8833_TEOFF		0x34 /* test mode */
#define B0_PCF8833_TEON			0x35 /* test mode */
#define B0_PCF8833_MADCTL		0x36 /* memory access control */
#define B0_PCF8833_SEP			0x37 /* vertical scrolling start address */
#define B0_PCF8833_IDMOFF		0x38 /* idle mode OFF */
#define B0_PCF8833_IDMON		0x39 /* idle mode ON */
#define B0_PCF8833_COLMOD		0x3A /* interface pixel format */
#define B0_PCF8833_SETVOP		0xB0 /* set Vop */
#define B0_PCF8833_BRS			0xB4 /* bottom row swap */
#define B0_PCF8833_TRS			0xB6 /* top row swap */
#define B0_PCF8833_DISCTR		0xB9 /* display control */
#define B0_PCF8833_DOR			0xBA /* data order */
#define B0_PCF8833_TCDFE		0xBD /* enable/disable DF temperature compensation */
#define B0_PCF8833_TCVOPE		0xBF /* enable/disable Vop temp comp */
#define B0_PCF8833_EC			0xC0 /* internal or external oscillator */
#define B0_PCF8833_SETMUL		0xC2 /* set multiplication factor */
#define B0_PCF8833_TCVOPAB		0xC3 /* set TCVOP slopes A and B */
#define B0_PCF8833_TCVOPCD		0xC4 /* set TCVOP slopes c and d */
#define B0_PCF8833_TCDF			0xC5 /* set divider frequency */
#define B0_PCF8833_DF8COLOR		0xC6 /* set divider frequency 8-color mode */
#define B0_PCF8833_SETBS		0xC7 /* set bias system */
#define B0_PCF8833_RDTEMP		0xC8 /* temperature read back */
#define B0_PCF8833_NLI			0xC9 /* n-line inversion */

/*
 *         1|--|10    <--- pinout
 *  _______5|__|6_
 * |              |
 * |    FRONT     |
 * |              |
 * |______________|
 *
 * 1: VCC-Digital (3.3V)
 * 2: RESET
 * 3: SDATA
 * 4: SCK
 * 5: CS
 * 6: VCC-Display (3.3V)
 * 7: N/C
 * 8: GND
 * 9:  LED V-
 * 10: LED V+
 *
 * Led require 6V to 7V
 */

B0_API b0_result_code b0_pcf8833_open(b0_spi *mod, b0_pcf8833 **drv,
	unsigned addr);
B0_API b0_result_code b0_pcf8833_close(b0_pcf8833 *drv);
B0_API b0_result_code b0_pcf8833_reset(b0_pcf8833 *drv);

B0_API b0_result_code b0_pcf8833_cmd(b0_pcf8833 *drv, uint8_t cmd);
B0_API b0_result_code b0_pcf8833_write_reg8(b0_pcf8833 *drv, uint8_t addr, uint8_t arg);
B0_API b0_result_code b0_pcf8833_write_reg(b0_pcf8833 *drv, uint8_t cmd, uint8_t *arg, size_t len);

B0_API b0_result_code b0_pcf8833_pixel(b0_pcf8833 *drv, uint8_t r, uint8_t g, uint8_t b);

B0_API b0_result_code b0_pcf8833_col_set(b0_pcf8833 *drv, uint8_t start, uint8_t end);
B0_API b0_result_code b0_pcf8833_page_set(b0_pcf8833 *drv, uint8_t start, uint8_t end);

__END_DECLS

#endif
