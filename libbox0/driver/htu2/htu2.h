/*
 * This file is part of libbox0.
 * Copyright (C) 2016 Kuldeep Singh Dhaka <kuldeep@madresistor.com>
 *
 * libbox0 is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * libbox0 is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with libbox0.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef LIBBOX0_DRIVER_HTU2_H
#define LIBBOX0_DRIVER_HTU2_H

#include <stdint.h>
#include <stdbool.h>
#include "../../common.h"
#include "../../module/i2c.h"

__BEGIN_DECLS

#if defined(__DOXYGEN__)
struct b0_htu2 {};
#endif

/**
 * @class b0_htu2
 * @brief HTU2 Digital Relative Humidity sensor with Temperature output
 */

#define B0_HTU2_TRIG_TEMP_MEAS_HOLD			0xE3
#define B0_HTU2_TRIG_HUMIDITY_MEAS_HOLD		0xE5
#define B0_HTU2_TRIG_TEMP_MEAS_NOHOLD		0xF3
#define B0_HTU2_TRIG_HUMIDITY_MEAS_NOHOLD	0xF5
#define B0_HTU2_WRITE_USER_REG				0xE6
#define B0_HTU2_READ_USER_REG				0xE7
#define B0_HTU2_SOFT_RESET					0xFE

#define B0_HTU2_USER_REG_DISABLE_OTP	(1 << 1)
#define B0_HTU2_USER_REG_ENABLE_HEATER	(1 << 2)
#define B0_HTU2_USER_REG_END_OF_BAT		(1 << 6)
#define B0_HTU2_USER_REG_RESOL_MASK		(0x81)
#define B0_HTU2_USER_REG_RESOL_12RH_14T	(0x00)
#define B0_HTU2_USER_REG_RESOL_8RH_12T	(0x01)
#define B0_HTU2_USER_REG_RESOL_10RH_13T	(0x80)
#define B0_HTU2_USER_REG_RESOL_11RH_11T	(0x81)

typedef struct b0_htu2 b0_htu2;

enum b0_htu2_resol {
	/** Relative Humidity = 12bit, Temperature = 14bit */
	B0_HTU2_RESOL_12RH_14T = 0,

	/** Relative Humidity = 8bit, Temperature = 12bit */
	B0_HTU2_RESOL_8RH_12T = 1,

	/** Relative Humidity = 10bit, Temperature = 13bit */
	B0_HTU2_RESOL_10RH_13T = 2,

	/** Relative Humidity = 11bit, Temperature = 11bit */
	B0_HTU2_RESOL_11RH_11T = 3
};

typedef enum b0_htu2_resol b0_htu2_resol;

/**
 * @brief allocate a HTU2 (using I2C)
 * @param[in] mod libbox0 I2C module
 * @param[out] drv memory location to store the HTU2 driver
 * @return result code
 */
B0_API b0_result_code b0_htu2_open_i2c(b0_i2c *mod, b0_htu2 **drv);

/**
 * @brief free HTU2 driver
 * @param[in] drv libbox0 HTU2 driver
 * @return result code
 */
B0_API b0_result_code b0_htu2_close(b0_htu2 *drv);

/**
 * Read Relative Humidity
 * @param[in] drv libbox0 HTU2 driver
 * @param[out] value pointer to relative humidity result
 * @return result code
 */
B0_API b0_result_code b0_htu2_read_rh(b0_htu2 *drv, double *value);

/**
 * Read Temperature
 * @param[in] drv libbox0 HTU2 driver
 * @param[out] value pointer to temperature result
 * @return result code
 */
B0_API b0_result_code b0_htu2_read_temp(b0_htu2 *drv, double *value);

/**
 * Set the resolution @a resol of the slave
 * @param drv libbox0 HTU2 driver
 * @param resol Resolution
 * @return result code
 */
B0_API b0_result_code b0_htu2_resol_set(b0_htu2 *drv, b0_htu2_resol resol);

/**
 * Reset the slave
 * @param drv libbox0 HTU2 driver
 * @return result code
 */
B0_API b0_result_code b0_htu2_reset(b0_htu2 *drv);

__END_DECLS

#endif
