/*
 * This file is part of libbox0.
 * Copyright (C) 2016 Kuldeep Singh Dhaka <kuldeep@madresistor.com>
 *
 * libbox0 is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * libbox0 is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with libbox0.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "htu2.h"
#include "../../libbox0-private.h"
#include "../../misc/conv.h"

#if !defined(__DOXYGEN__)
struct b0_htu2 {
	b0_i2c *module;
	uint8_t user_reg;
	uint8_t bSlaveAddress;
	b0_i2c_version version;
};
#endif

b0_result_code b0_htu2_open_i2c(b0_i2c *mod, b0_htu2 **drv)
{
	b0_htu2 *drv_alloc;
	b0_device *dev;

	B0_ASSERT_RETURN_ON_NULL_ARG(NULL, mod);
	dev = B0_MODULE_DEVICE(mod);

	B0_ASSERT_RETURN_ON_NULL_ARG(dev, drv);

	/* allocate memory */
	drv_alloc = calloc(1, sizeof(*drv_alloc));
	B0_ASSERT_RETURN_ON_NULL_ALLOC(dev, drv_alloc);

	/* fill in the object */
	drv_alloc->module = mod;
	drv_alloc->bSlaveAddress = 0x40;
	/* TODO: Use the most favourable version (FM > SM)
	 *  instead of hardcoded version */
	drv_alloc->version = B0_I2C_VERSION_FM;
	drv_alloc->user_reg = B0_HTU2_USER_REG_DISABLE_OTP; /* default value */

	/* fix: memory leak when stmt fails */
	B0_DEBUG_RETURN_ON_FAIL_STMT(dev, b0_htu2_reset(drv_alloc));

	/* done */
	*drv = drv_alloc;
	return B0_OK;
}

b0_result_code b0_htu2_close(b0_htu2 *drv)
{
	B0_ASSERT_RETURN_ON_NULL_ARG(NULL, drv);
	free(drv);
	return B0_OK;
}

/* Mask of the actual data */
#define DATA_MASK 0xFFFC

/* Number of milliseconds to wait for
 *  relative humidity data to become available */
static const unsigned delay_rh[4 /* resol */] = {16, 3, 5, 8};

/* Number of milliseconds to wait for
 *  temperature data to become available */
static const unsigned delay_temp[4 /* resol */] = {50, 13, 25, 7};

/**
 * Convert user_reg to resolution
 * @param user_reg User Register
 * @return resolution
 */
static b0_htu2_resol user_reg_to_resol(uint8_t user_reg)
{
	switch (user_reg & B0_HTU2_USER_REG_RESOL_MASK) {
	case B0_HTU2_USER_REG_RESOL_8RH_12T:
	return B0_HTU2_RESOL_8RH_12T;

	case B0_HTU2_USER_REG_RESOL_12RH_14T:
	return B0_HTU2_RESOL_12RH_14T;

	case B0_HTU2_USER_REG_RESOL_11RH_11T:
	return B0_HTU2_RESOL_11RH_11T;

	case B0_HTU2_USER_REG_RESOL_10RH_13T:
	return B0_HTU2_RESOL_10RH_13T;
	}

	/* just to make compiler happy! */
	return B0_HTU2_RESOL_12RH_14T;
}

/**
 * Read result from HTU2
 * @param drv libbox0 HTU2 driver
 * @param cmd Command
 * @param wait Number of milliseconds to wait before reading
 * @param[out] result Result
 * @return libbox0 result code
 */
static b0_result_code read_result(b0_htu2 *drv, uint8_t cmd, unsigned wait,
				uint16_t *result)
{
	uint8_t data[3];
	b0_device *dev = B0_MODULE_DEVICE(drv->module);

	const b0_i2c_sugar_arg arg = {
		.addr = drv->bSlaveAddress,
		.version = drv->version
	};

	/* send the command */
	B0_DEBUG_RETURN_ON_FAIL_STMT(dev,
		b0_i2c_master_write(drv->module, &arg, &cmd, 1));

	/* wait */
	B0_SLEEP_MILLISECOND(wait);

	/* read */
	B0_DEBUG_RETURN_ON_FAIL_STMT(dev,
		b0_i2c_master_read(drv->module, &arg, data, sizeof(data)));

	/* TODO: check CRC */
	*result = (data[0] << 8) | data[1];
	return B0_OK;
}

b0_result_code b0_htu2_read_rh(b0_htu2 *drv, double *value)
{
	b0_device *dev;
	uint16_t result;

	B0_ASSERT_RETURN_ON_NULL_ARG(NULL, drv);
	dev = B0_MODULE_DEVICE(drv->module);

	B0_ASSERT_RETURN_ON_NULL_ARG(dev, value);

	b0_htu2_resol resol = user_reg_to_resol(drv->user_reg);
	unsigned delay = delay_rh[resol];

	B0_DEBUG_RETURN_ON_FAIL_STMT(dev,
		read_result(drv, B0_HTU2_TRIG_HUMIDITY_MEAS_NOHOLD, delay, &result));

	result &= DATA_MASK;
	*value = -6 + (125 * (result / 65536.0));

	return B0_OK;
}

b0_result_code b0_htu2_read_temp(b0_htu2 *drv, double *value)
{
	b0_device *dev;
	uint16_t result;
	double value_celsius;

	B0_ASSERT_RETURN_ON_NULL_ARG(NULL, drv);
	dev = B0_MODULE_DEVICE(drv->module);

	B0_ASSERT_RETURN_ON_NULL_ARG(dev, value);

	b0_htu2_resol resol = user_reg_to_resol(drv->user_reg);
	unsigned delay = delay_temp[resol];

	B0_DEBUG_RETURN_ON_FAIL_STMT(dev,
		read_result(drv, B0_HTU2_TRIG_TEMP_MEAS_NOHOLD, delay, &result));

	result &= DATA_MASK;
	value_celsius = -46.85 + (175.72 * (result / 65536.0));
	*value = B0_CELSIUS_TO_KELVIN(value_celsius);

	return B0_OK;
}

b0_result_code b0_htu2_reset(b0_htu2 *drv)
{
	b0_device *dev;

	B0_ASSERT_RETURN_ON_NULL_ARG(NULL, drv);
	dev = B0_MODULE_DEVICE(drv->module);

	const b0_i2c_sugar_arg arg = {
		.addr = drv->bSlaveAddress,
		.version = drv->version
	};

	uint8_t payload = B0_HTU2_SOFT_RESET;
	B0_DEBUG_RETURN_ON_FAIL_STMT(dev,
		b0_i2c_master_write(drv->module, &arg, &payload, sizeof(payload)));

	return B0_OK;
}

b0_result_code b0_htu2_resol_set(b0_htu2 *drv, b0_htu2_resol resol)
{
	b0_device *dev;

	B0_ASSERT_RETURN_ON_NULL_ARG(NULL, drv);
	dev = B0_MODULE_DEVICE(drv->module);

	static const uint8_t resol_map[4 /* resol */ ] = {
		B0_HTU2_USER_REG_RESOL_12RH_14T,
		B0_HTU2_USER_REG_RESOL_8RH_12T,
		B0_HTU2_USER_REG_RESOL_10RH_13T,
		B0_HTU2_USER_REG_RESOL_11RH_11T
	};

	uint8_t user_reg = drv->user_reg;
	user_reg &= ~B0_HTU2_USER_REG_RESOL_MASK;
	user_reg |= resol_map[resol & 0x3];

	const b0_i2c_sugar_arg arg = {
		.addr = drv->bSlaveAddress,
		.version = drv->version
	};

	uint8_t data[2] = {B0_HTU2_WRITE_USER_REG, user_reg};
	B0_DEBUG_RETURN_ON_FAIL_STMT(dev,
		b0_i2c_master_write(drv->module, &arg, data, sizeof(data)));

	drv->user_reg = user_reg;
	return B0_OK;
}
