/*
 * This file is part of libbox0.
 * Copyright (C) 2013-2016 Kuldeep Singh Dhaka <kuldeep@madresistor.com>
 *
 * libbox0 is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * libbox0 is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with libbox0.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef LIBBOX0_DRIVER_BMP180_H
#define LIBBOX0_DRIVER_BMP180_H

#include <stdint.h>
#include <stdbool.h>
#include "../../common.h"
#include "../../module/i2c.h"

__BEGIN_DECLS

#if defined(__DOXYGEN__)
struct b0_bmp180 {};
#endif

/**
 * @class b0_bmp180
 * @brief BMP180: I2C Pressure Sensor
 */

/* over sampling */
enum b0_bmp180_over_samp {
	B0_BMP180_OVER_SAMP_1 = 0,
	B0_BMP180_OVER_SAMP_2 = 1,
	B0_BMP180_OVER_SAMP_4 = 2,
	B0_BMP180_OVER_SAMP_8 = 3
};

typedef enum b0_bmp180_over_samp b0_bmp180_over_samp;

typedef struct b0_bmp180 b0_bmp180;

#define B0_BMP180_AC1	0xAA
#define B0_BMP180_AC2	0xAC
#define B0_BMP180_AC3	0xAE
#define B0_BMP180_AC4	0xB0
#define B0_BMP180_AC5	0xB2
#define B0_BMP180_AC6	0xB4
#define B0_BMP180_B1	0xB6
#define B0_BMP180_B2	0xB8
#define B0_BMP180_MB	0xBA
#define B0_BMP180_MC	0xBC
#define B0_BMP180_MD	0xBE

#define B0_BMP180_CTRL_MEAS 	0xF4
#define B0_BMP180_OUT_MSB		0xF6
#define B0_BMP180_OUT_LSB		0xF7
#define B0_BMP180_OUT_XLSB		0xF8
#define B0_BMP180_ID			0xD0
#define B0_BMP180_SOFT_RESET	0xE0

B0_API b0_result_code b0_bmp180_open(b0_i2c *mod, b0_bmp180 **drv);

/* set over sampling */
B0_API b0_result_code b0_bmp180_over_samp_set(b0_bmp180 *drv,
	b0_bmp180_over_samp over_sampling);
B0_API b0_result_code b0_bmp180_close(b0_bmp180 *drv);
B0_API b0_result_code b0_bmp180_read(b0_bmp180 *drv, double *pressure, double *temp);

__END_DECLS

#endif
