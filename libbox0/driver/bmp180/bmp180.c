/*
 * This file is part of libbox0.
 * Copyright (C) 2013-2016 Kuldeep Singh Dhaka <kuldeep@madresistor.com>
 *
 * libbox0 is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * libbox0 is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with libbox0.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "bmp180.h"
#include "../../libbox0-private.h"

#if !defined(__DOXYGEN__)
struct b0_bmp180 {
	b0_i2c *module;
	uint8_t bSlaveAddress;
	b0_i2c_version version;

	b0_bmp180_over_samp over_sampling;
	struct {
		int16_t AC1, AC2, AC3;
		uint16_t AC4, AC5, AC6;
		int16_t B1, B2;
		int16_t MB, MC, MD;
	} eeprom B0_PACKED;
};
#endif

b0_result_code b0_bmp180_open(b0_i2c *mod, b0_bmp180 **drv)
{
	b0_bmp180 *drv_alloc;
	b0_device *dev;
	b0_result_code r;

	B0_ASSERT_RETURN_ON_NULL_ARG(NULL, mod);
	dev = B0_MODULE_DEVICE(mod);

	B0_ASSERT_RETURN_ON_NULL_ARG(dev, drv);

	/*
	 * Allocate drv
	 */
	drv_alloc = calloc(1, sizeof(*drv_alloc));
	B0_ASSERT_RETURN_ON_NULL_ALLOC(dev, drv_alloc);

	drv_alloc->module = mod;
	drv_alloc->bSlaveAddress = 0x77;
	drv_alloc->over_sampling = B0_BMP180_OVER_SAMP_1;
	/* TODO: Use the most favourable version (HS > FM > SM)
	 *  instead of hardcoded version */
	drv_alloc->version = B0_I2C_VERSION_FM;

	/* TODO: test if slave connected */
	/* TODO: ID test */

	const b0_i2c_sugar_arg arg = {
		.addr = drv_alloc->bSlaveAddress,
		.version = drv_alloc->version
	};

	/* read calibration value from eeprom */
	uint8_t calib[22];
	r = b0_i2c_master_write8_read(drv_alloc->module, &arg,
		B0_BMP180_AC1, calib, sizeof(calib));

	if (B0_ERROR_RESULT_CODE(r)) {
		B0_ERROR(dev, "unable to read calibration from EEPROM");
		goto free_and_ret;
	}

	drv_alloc->eeprom.AC1 = (calib[0] << 8) | calib[1];
	drv_alloc->eeprom.AC2 = (calib[2] << 8) | calib[3];
	drv_alloc->eeprom.AC3 = (calib[4] << 8) | calib[5];
	drv_alloc->eeprom.AC4 = (calib[6] << 8) | calib[7];
	drv_alloc->eeprom.AC5 = (calib[8] << 8) | calib[9];
	drv_alloc->eeprom.AC6 = (calib[10] << 8) | calib[11];

	drv_alloc->eeprom.B1 = (calib[12] << 8) | calib[13];
	drv_alloc->eeprom.B2 = (calib[14] << 8) | calib[15];

	drv_alloc->eeprom.MB = (calib[16] << 8) | calib[17];
	drv_alloc->eeprom.MC = (calib[18] << 8) | calib[19];
	drv_alloc->eeprom.MD = (calib[20] << 8) | calib[21];

	*drv = drv_alloc;
	return B0_OK;

	/* error occured! */
	free_and_ret:
	free(drv_alloc);
	return r;
}

b0_result_code b0_bmp180_over_samp_set(b0_bmp180 *drv, b0_bmp180_over_samp over_sampling)
{
	B0_ASSERT_RETURN_ON_NULL_ARG(NULL, drv);

	drv->over_sampling = over_sampling;

	return B0_OK;
}

b0_result_code b0_bmp180_close(b0_bmp180 *drv)
{
	B0_ASSERT_RETURN_ON_NULL_ARG(NULL, drv);

	free(drv);
	return B0_OK;
}

b0_result_code b0_bmp180_read(b0_bmp180 *drv, double *pressure, double *temp)
{
	b0_device *dev;
	uint8_t tmp[3];

	B0_ASSERT_RETURN_ON_NULL_ARG(NULL, drv);
	dev = B0_MODULE_DEVICE(drv->module);

	B0_ASSERT_RETURN_ON_NULL_ARG(dev, pressure);
	/* temp can be NULL */

	const b0_i2c_sugar_arg arg = {
		.addr = drv->bSlaveAddress,
		.version = drv->version
	};

	/* read uncompensated temprature value { */
	tmp[0] = B0_BMP180_CTRL_MEAS;
	tmp[1] = 0x2E;
	B0_DEBUG_RETURN_ON_FAIL_STMT(dev, b0_i2c_master_write(
		drv->module, &arg, tmp, 2));

	/* 4.5 milliseconds sleep as per datasheet */
	B0_SLEEP_MICROSECOND(4500);

	B0_DEBUG_RETURN_ON_FAIL_STMT(dev, b0_i2c_master_write8_read(
		drv->module, &arg, B0_BMP180_OUT_MSB, tmp, 2));

	long UT = (tmp[0] << 8) | tmp[1];

	/* } read uncompensated pressure value { */
	uint8_t oss = drv->over_sampling & 0x0F;
	tmp[0] = B0_BMP180_CTRL_MEAS;
	tmp[1] = 0x34 | (oss << 6);
	B0_DEBUG_RETURN_ON_FAIL_STMT(dev,
		b0_i2c_master_write(drv->module, &arg, tmp, 2));

	/* oss dependent milliseconds sleep as per datasheet */
	const unsigned _sleeps[4] = {4500, 7500, 13500, 25500};
	B0_SLEEP_MICROSECOND(_sleeps[oss]);

	B0_DEBUG_RETURN_ON_FAIL_STMT(dev, b0_i2c_master_write8_read(
		drv->module, &arg, B0_BMP180_OUT_MSB, tmp, 3));

	long UP = ((tmp[0] << 16) | (tmp[1] << 8) | tmp[2]) >> (8 - oss);

	const int16_t AC1 = drv->eeprom.AC1;
	const int16_t  AC2 = drv->eeprom.AC2;
	const int16_t AC3 = drv->eeprom.AC3;
	const uint16_t AC4 = drv->eeprom.AC4;
	const uint16_t AC5 = drv->eeprom.AC5;
	const uint16_t AC6 = drv->eeprom.AC6;
	const int16_t B1 = drv->eeprom.B1;
	const int16_t B2 = drv->eeprom.B2;
	/* const int16_t MB = drv->eeprom.MB; */
	const int16_t MC = drv->eeprom.MC;
	const int16_t MD = drv->eeprom.MD;

	long X1, X2, X3;
	long B3, B4, B5, B6, B7;
	long T, p;

	/* test values
	UT = 27898;
	UP = 23843;
	const uint16_t AC6 = 23153;
	const uint16_t AC5 = 32757;
	const int16_t MC = -8711;
	const int16_t MD = 2868;
	const int16_t B1 = 6190;
	const int16_t B2 = 4;
	const int16_t AC3 = -14383;
	const int16_t AC2 = -72;
	const int16_t AC1 = 408;
	const uint16_t AC4 = 32741;
	oss = 0;
	*/

	/* } calculate true temprature { */
	X1 = ((UT - AC6) *  AC5) >> 15;
	X2 = (MC << 11) / (X1 + MD);
	B5 = X1 + X2;
	T = (B5 + 8) >> 4;

	/* } calculate true pressure { */
	B6 = B5 - 4000;
	X1 = (B2 * ((B6 * B6) >> 12)) >> 11;
	X2 = (AC2 * B6) >> 11;
	X3 = X1 + X2;
	B3 = ((((AC1 * 4) + X3) << oss) + 2) >> 2;
	X1 = (AC3 * B6) >> 13;
	X2 = (B1 * ((B6 * B6) >> 12)) >> 16;
	X3 = ((X1 + X2) + 2) >> 2;
	B4 = (AC4 * (unsigned long)(X3 + 32768)) >> 15;
	B7 = ((unsigned long)UP - B3) * (50000 >> oss);
	p = (B7 < (1 << 31)/* 0x80000000 */) ? ((B7 * 2) / B4) : (B7 / B4) * 2;
	X1 = (p >> 8) * (p >> 8);
	X1 = (X1 * 3038) >> 16;
	X2 = (-7357 * p) >> 16;
	p = p + ((X1 + X2 + 3791) >> 4);

	/* } */

	*pressure = p;

	/* temp is optional */
	if (temp != NULL) {
		*temp = T * 0.1;
	}

	return B0_OK;
}
