/*
 * This file is part of libbox0.
 * Copyright (C) 2016 Kuldeep Singh Dhaka <kuldeep@madresistor.com>
 *
 * libbox0 is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * libbox0 is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with libbox0.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef LIBBOX0_DRIVER_ADS1220_H
#define LIBBOX0_DRIVER_ADS1220_H

#include "../../common.h"
#include "../../module/spi.h"

__BEGIN_DECLS

#if defined(__DOXYGEN__)
struct b0_ads1220 {};
#endif

typedef struct b0_ads1220 b0_ads1220;

/**
 * @class b0_ads1220
 * @brief ADS1220
 */

/* commands */
#define B0_ADS1220_RESET 0x06
#define B0_ADS1220_START 0x08
#define B0_ADS1220_SYNC 0x08
#define B0_ADS1220_POWERDOWN 0x02
#define B0_ADS1220_RDATA 0x10
#define B0_ADS1220_RREG 0x20
#define B0_ADS1220_WREG 0x40

/* registers */
#define B0_ADS1220_CFG0 0x0
#define B0_ADS1220_CFG1 0x1
#define B0_ADS1220_CFG2 0x2
#define B0_ADS1220_CFG3 0x3

#define B0_ADS1220_CFG0_PGA_BYPASS (1 << 0)

#define B0_ADS1220_CFG0_GAIN_SHIFT 1
#define B0_ADS1220_CFG0_GAIN_MASK (0x7 << B0_ADS1220_CFG0_GAIN_SHIFT)
#define B0_ADS1220_CFG0_GAIN(v) \
	((v) << B0_ADS1220_CFG0_GAIN_SHIFT) & B0_ADS1220_CFG0_GAIN_MASK
#define B0_ADS1220_CFG0_GAIN_1 B0_ADS1220_CFG0_GAIN(0x0)
#define B0_ADS1220_CFG0_GAIN_2 B0_ADS1220_CFG0_GAIN(0x1)
#define B0_ADS1220_CFG0_GAIN_4 B0_ADS1220_CFG0_GAIN(0x2)
#define B0_ADS1220_CFG0_GAIN_8 B0_ADS1220_CFG0_GAIN(0x3)
#define B0_ADS1220_CFG0_GAIN_16 B0_ADS1220_CFG0_GAIN(0x4)
#define B0_ADS1220_CFG0_GAIN_32 B0_ADS1220_CFG0_GAIN(0x5)
#define B0_ADS1220_CFG0_GAIN_64 B0_ADS1220_CFG0_GAIN(0x6)
#define B0_ADS1220_CFG0_GAIN_128 B0_ADS1220_CFG0_GAIN(0x7)

#define B0_ADS1220_CFG0_MUX_SHIFT 4
#define B0_ADS1220_CFG0_MUX_MASK (0xF << B0_ADS1220_CFG0_MUX_SHIFT)
#define B0_ADS1220_CFG0_MUX(v) \
	((v) << B0_ADS1220_CFG0_MUX_SHIFT) & B0_ADS1220_CFG0_MUX_MASK
#define B0_ADS1220_CFG0_MUX_AIN0_AIN1 B0_ADS1220_CFG0_MUX(0x0)
#define B0_ADS1220_CFG0_MUX_AIN0_AIN2 B0_ADS1220_CFG0_MUX(0x1)
#define B0_ADS1220_CFG0_MUX_AIN0_AIN3 B0_ADS1220_CFG0_MUX(0x2)
#define B0_ADS1220_CFG0_MUX_AIN1_AIN2 B0_ADS1220_CFG0_MUX(0x3)
#define B0_ADS1220_CFG0_MUX_AIN1_AIN3 B0_ADS1220_CFG0_MUX(0x4)
#define B0_ADS1220_CFG0_MUX_AIN2_AIN3 B0_ADS1220_CFG0_MUX(0x5)
#define B0_ADS1220_CFG0_MUX_AIN1_AIN0 B0_ADS1220_CFG0_MUX(0x6)
#define B0_ADS1220_CFG0_MUX_AIN3_AIN2 B0_ADS1220_CFG0_MUX(0x7)
#define B0_ADS1220_CFG0_MUX_AIN0_AVSS B0_ADS1220_CFG0_MUX(0x8)
#define B0_ADS1220_CFG0_MUX_AIN1_AVSS B0_ADS1220_CFG0_MUX(0x9)
#define B0_ADS1220_CFG0_MUX_AIN2_AVSS B0_ADS1220_CFG0_MUX(0xA)
#define B0_ADS1220_CFG0_MUX_AIN3_AVSS B0_ADS1220_CFG0_MUX(0xB)
#define B0_ADS1220_CFG0_MUX_VREF_PN_4 B0_ADS1220_CFG0_MUX(0xC)
#define B0_ADS1220_CFG0_MUX_AVDD_AVSS_4 B0_ADS1220_CFG0_MUX(0xD)
#define B0_ADS1220_CFG0_MUX_SHORTED_AVDD_AVSS_2 B0_ADS1220_CFG0_MUX(0xE)

#define B0_ADS1220_CFG1_BCS (1 << 0)
#define B0_ADS1220_CFG1_TS (1 << 1)
#define B0_ADS1220_CFG1_CM (1 << 2)

#define B0_ADS1220_CFG1_MODE_SHIFT 3
#define B0_ADS1220_CFG1_MODE_MASK (0x3 << B0_ADS1220_CFG1_MODE_SHIFT)
#define B0_ADS1220_CFG1_MODE(v) \
	((v) << B0_ADS1220_CFG1_MODE_SHIFT) & B0_ADS1220_CFG1_MODE_MASK
#define B0_ADS1220_CFG1_MODE_NORMAL B0_ADS1220_CFG1_MODE(0)
#define B0_ADS1220_CFG1_MODE_DUTY_CYCLE B0_ADS1220_CFG1_MODE(1)
#define B0_ADS1220_CFG1_MODE_TURBO B0_ADS1220_CFG1_MODE(2)

#define B0_ADS1220_CFG1_DR_SHIFT 5
#define B0_ADS1220_CFG1_DR_MASK (0x3 << B0_ADS1220_CFG1_DR_SHIFT)
#define B0_ADS1220_CFG1_DR(v) \
	((v) << B0_ADS1220_CFG1_DR_SHIFT) & B0_ADS1220_CFG1_DR_MASK

#define B0_ADS1220_CFG2_IDAC_SHIFT 0
#define B0_ADS1220_CFG2_IDAC_MASK (0x7 << B0_ADS1220_CFG2_IDAC_SHIFT)
#define B0_ADS1220_CFG2_IDAC(v) \
	((v) << B0_ADS1220_CFG2_IDAC_SHIFT) & B0_ADS1220_CFG2_IDAC_MASK
#define B0_ADS1220_CFG2_IDAC_OFF B0_ADS1220_CFG2_IDAC(0)
#define B0_ADS1220_CFG2_IDAC_10UA B0_ADS1220_CFG2_IDAC(1)
#define B0_ADS1220_CFG2_IDAC_50UA B0_ADS1220_CFG2_IDAC(2)
#define B0_ADS1220_CFG2_IDAC_100UA B0_ADS1220_CFG2_IDAC(3)
#define B0_ADS1220_CFG2_IDAC_250UA B0_ADS1220_CFG2_IDAC(4)
#define B0_ADS1220_CFG2_IDAC_500UA B0_ADS1220_CFG2_IDAC(5)
#define B0_ADS1220_CFG2_IDAC_1000UA B0_ADS1220_CFG2_IDAC(6)
#define B0_ADS1220_CFG2_IDAC_1500UA B0_ADS1220_CFG2_IDAC(7)

#define B0_ADS1220_CFG2_PSW (1 << 3)

#define B0_ADS1220_CFG2_FILTER_SHIFT 4
#define B0_ADS1220_CFG2_FILTER_MASK (0x3 << B0_ADS1220_CFG2_FILTER_SHIFT)
#define B0_ADS1220_CFG2_FILTER(v) \
	((v) << B0_ADS1220_CFG2_FILTER_SHIFT) & B0_ADS1220_CFG2_FILTER_MASK
#define B0_ADS1220_CFG2_FILTER_NONE B0_ADS1220_CFG2_FILTER(0)
#define B0_ADS1220_CFG2_FILTER_50HZ B0_ADS1220_CFG2_FILTER(1)
#define B0_ADS1220_CFG2_FILTER_60HZ B0_ADS1220_CFG2_FILTER(2)
#define B0_ADS1220_CFG2_FILTER_50HZ_60HZ B0_ADS1220_CFG2_FILTER(3)

#define B0_ADS1220_CFG2_VREF_SHIFT 6
#define B0_ADS1220_CFG2_VREF_MASK (0x3 << B0_ADS1220_CFG2_VREF_SHIFT)
#define B0_ADS1220_CFG2_VREF(v) \
	((v) << B0_ADS1220_CFG2_VREF_SHIFT) & B0_ADS1220_CFG2_VREF_MASK
#define B0_ADS1220_CFG2_VREF_INTERNAL B0_ADS1220_CFG2_VREF(0)
#define B0_ADS1220_CFG2_VREF_EXTERNAL_REFP0_REFN0 B0_ADS1220_CFG2_VREF(1)
#define B0_ADS1220_CFG2_VREF_EXTERNAL_REFP1_REFN1 B0_ADS1220_CFG2_VREF(2)
#define B0_ADS1220_CFG2_VREF_EXTERNAL_AVDD_AVSS B0_ADS1220_CFG2_VREF(3)

#define B0_ADS1220_CFG3_DRDYM (1 << 1)

#define B0_ADS1220_CFG3_I2MUX_SHIFT 2
#define B0_ADS1220_CFG3_I2MUX_MASK (0x7 << B0_ADS1220_CFG3_I2MUX_SHIFT)
#define B0_ADS1220_CFG3_I2MUX(v) \
	((v) << B0_ADS1220_CFG3_I2MUX_SHIFT) & B0_ADS1220_CFG3_I2MUX_MASK
#define B0_ADS1220_CFG3_I2MUX_DISABLED B0_ADS1220_CFG3_I2MUX(0)
#define B0_ADS1220_CFG3_I2MUX_AIN0 B0_ADS1220_CFG3_I2MUX(1)
#define B0_ADS1220_CFG3_I2MUX_AIN1 B0_ADS1220_CFG3_I2MUX(2)
#define B0_ADS1220_CFG3_I2MUX_AIN2 B0_ADS1220_CFG3_I2MUX(3)
#define B0_ADS1220_CFG3_I2MUX_AIN3 B0_ADS1220_CFG3_I2MUX(4)
#define B0_ADS1220_CFG3_I2MUX_REFP0 B0_ADS1220_CFG3_I2MUX(5)
#define B0_ADS1220_CFG3_I2MUX_REFN0 B0_ADS1220_CFG3_I2MUX(6)

#define B0_ADS1220_CFG3_I1MUX_SHIFT 5
#define B0_ADS1220_CFG3_I1MUX_MASK (0x7 << B0_ADS1220_CFG3_I1MUX_SHIFT)
#define B0_ADS1220_CFG3_I1MUX(v) \
	((v) << B0_ADS1220_CFG3_I1MUX_SHIFT) & B0_ADS1220_CFG3_I1MUX_MASK
#define B0_ADS1220_CFG3_I1MUX_DISABLED B0_ADS1220_CFG3_I1MUX(0)
#define B0_ADS1220_CFG3_I1MUX_AIN0 B0_ADS1220_CFG3_I1MUX(1)
#define B0_ADS1220_CFG3_I1MUX_AIN1 B0_ADS1220_CFG3_I1MUX(2)
#define B0_ADS1220_CFG3_I1MUX_AIN2 B0_ADS1220_CFG3_I1MUX(3)
#define B0_ADS1220_CFG3_I1MUX_AIN3 B0_ADS1220_CFG3_I1MUX(4)
#define B0_ADS1220_CFG3_I1MUX_REFP0 B0_ADS1220_CFG3_I1MUX(5)
#define B0_ADS1220_CFG3_I1MUX_REFN0 B0_ADS1220_CFG3_I1MUX(6)

enum b0_ads1220_filter {
	B0_ADS1220_FILTER_NONE = 0,
	B0_ADS1220_FILTER_50HZ = 1,
	B0_ADS1220_FILTER_60HZ = 2,
	B0_ADS1220_FILTER_50HZ_60HZ = 3
};

typedef enum b0_ads1220_filter b0_ads1220_filter;

enum b0_ads1220_vref_source {
	B0_ADS1220_VREF_INTERNAL = 0,
	B0_ADS1220_VREF_REFP0_REFN0 = 1,
	B0_ADS1220_VREF_REFP1_REFN1 = 2,
	B0_ADS1220_VREF_AVDD_AVSS = 3
};

typedef enum b0_ads1220_vref_source b0_ads1220_vref_source;

/**
 * @param mod SPI Module
 * @param drv Pointer to where the object to be stored
 * @param addr slave select pin
 */
B0_API b0_result_code b0_ads1220_open(b0_spi *mod, b0_ads1220 **drv,
	unsigned addr);

B0_API b0_result_code b0_ads1220_close(b0_ads1220 *drv);

/**
 * Read and store the result in @a value
 * @param drv Driver
 * @param value Pointer to value to be readed
 */
B0_API b0_result_code b0_ads1220_read(b0_ads1220 *drv, double *value);

/**
 * Reset the ADS1220
 * @param drv Driver
 * @return result code
 */
B0_API b0_result_code b0_ads1220_reset(b0_ads1220 *drv);

/**
 * Send the START/SYNC to device
 * @param drv Driver
 * @return result code
 */
B0_API b0_result_code b0_ads1220_start(b0_ads1220 *drv);

/**
 * Power down the device.
 * @note use b0_ads1220_start() to resume
 * @param drv Driver
 * @return result code
 */
B0_API b0_result_code b0_ads1220_power_down(b0_ads1220 *drv);

/**
 * Set the filter @a type
 * @param drv Driver
 * @param type Filter type
 * @return result code
 */
B0_API b0_result_code b0_ads1220_filter_set(b0_ads1220 *drv, b0_ads1220_filter type);

/**
 * Set the Voltage reference value for conversion
 * @param drv Driver
 * @param source Source
 * @param low Voltage reference low
 * @param high Voltage reference high
 * @return result code
 * @note for @a type = B0_ADS1220_VREF_INTERNAL, @a low and @a high is ignored
 */
B0_API b0_result_code b0_ads1220_vref_set(b0_ads1220 *drv,
	b0_ads1220_vref_source source, double low, double high);

enum b0_ads1220_gain {
	B0_ADS1220_GAIN_1 = 0,
	B0_ADS1220_GAIN_2 = 1,
	B0_ADS1220_GAIN_4 = 2,
	B0_ADS1220_GAIN_8 = 3,
	B0_ADS1220_GAIN_16 = 4,
	B0_ADS1220_GAIN_32 = 5,
	B0_ADS1220_GAIN_64 = 6,
	B0_ADS1220_GAIN_128 = 7
};

typedef enum b0_ads1220_gain b0_ads1220_gain;

/**
 * Set the Gain
 * @param drv Driver
 * @param gain Gain value
 * @return result code
 */
B0_API b0_result_code b0_ads1220_gain_set(b0_ads1220 *drv, b0_ads1220_gain gain);

/**
 * By Pass PGA
 * @param drv Driver
 * @param bypass By Pass
 * @return result code
 */
B0_API b0_result_code b0_ads1220_pga_bypass_set(b0_ads1220 *drv, bool bypass);

__END_DECLS

#endif
