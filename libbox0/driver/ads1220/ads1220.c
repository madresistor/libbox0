/*
 * This file is part of libbox0.
 * Copyright (C) 2016 Kuldeep Singh Dhaka <kuldeep@madresistor.com>
 *
 * libbox0 is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * libbox0 is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with libbox0.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "ads1220.h"
#include "../../libbox0-private.h"

/* TODO: gain support */

#define __CONFIG (B0_SPI_TASK_MODE1 | B0_SPI_TASK_MSB_FIRST) /* FD */
#define __BITSIZE 8
#define __SPEED B0_SPI_TASK_SPEED_USE_FALLBACK

#define VREF_INTERNAL_LOW 0
#define VREF_INTERNAL_HIGH 2.048

#if !defined(__DOXYGEN__)
struct b0_ads1220 {
	b0_spi *module;
	unsigned addr;
	uint8_t cfg[4];
	struct { double low, high; } vref;
};
#endif

b0_result_code b0_ads1220_open(b0_spi *mod, b0_ads1220 **drv, unsigned addr)
{
	b0_ads1220 *drv_alloc;
	b0_device *dev;

	B0_ASSERT_RETURN_ON_NULL_ARG(NULL, mod);
	dev = B0_MODULE_DEVICE(mod);

	B0_ASSERT_RETURN_ON_NULL_ARG(dev, drv);

	drv_alloc = calloc(1, sizeof(*drv_alloc));
	B0_ASSERT_RETURN_ON_NULL_ALLOC(dev, drv_alloc);

	drv_alloc->module = mod;
	drv_alloc->addr = addr;

	*drv = drv_alloc;

	/* initalize */
	/* fixme: memory leak if fail */
	B0_DEBUG_RETURN_ON_FAIL_STMT(dev, b0_ads1220_reset(drv_alloc));

	return B0_OK;
}

b0_result_code b0_ads1220_close(b0_ads1220 *drv)
{
	B0_ASSERT_RETURN_ON_NULL_ARG(NULL, drv);

	free(drv);
	return B0_OK;
}

static b0_result_code write_reg(b0_ads1220 *drv, uint8_t reg, uint8_t data)
{
	b0_device *dev = B0_MODULE_DEVICE(drv->module);

	b0_spi_sugar_arg arg = {
		.addr = drv->addr,
		.flags = __CONFIG,
		.bitsize = __BITSIZE,
		.speed = __SPEED
	};

	uint8_t out[2] = {B0_ADS1220_WREG | (reg & 0x3) << 2, data};
	B0_DEBUG_RETURN_ON_FAIL_STMT(dev,
		b0_spi_master_fd(drv->module, &arg, out, out, 2));

	/* cache the result */
	drv->cfg[reg] = data;

	return B0_OK;
}

#define COMMAND_FUNC(name, cmd_value)					\
b0_result_code b0_ads1220_##name(b0_ads1220 *drv)		\
{														\
	B0_ASSERT_RETURN_ON_NULL_ARG(NULL, drv);			\
	b0_spi_sugar_arg arg = {							\
		.addr = drv->addr,								\
		.flags = __CONFIG,								\
		.bitsize = __BITSIZE,							\
		.speed = __SPEED								\
	};													\
														\
	uint8_t cmd = cmd_value;							\
	return b0_spi_master_fd(drv->module, &arg, &cmd, &cmd, 1);	\
}

/* COMMAND_FUNC(reset, B0_ADS1220_RESET) */
COMMAND_FUNC(start, B0_ADS1220_START)
COMMAND_FUNC(power_down, B0_ADS1220_POWERDOWN)

b0_result_code b0_ads1220_reset(b0_ads1220 *drv)
{
	B0_ASSERT_RETURN_ON_NULL_ARG(NULL, drv);
	b0_device *dev = B0_MODULE_DEVICE(drv->module);

	b0_spi_sugar_arg arg = {
		.addr = drv->addr,
		.flags = __CONFIG,
		.bitsize = __BITSIZE,
		.speed = __SPEED
	};

	uint8_t cmd = B0_ADS1220_RESET;
	B0_DEBUG_RETURN_ON_FAIL_STMT(dev,
		b0_spi_master_fd(drv->module, &arg, &cmd, &cmd, 1));

	memset(drv->cfg, 0, sizeof(drv->cfg));
	drv->vref.low = VREF_INTERNAL_LOW;
	drv->vref.high = VREF_INTERNAL_HIGH;

	return B0_OK;
}

b0_result_code b0_ads1220_read(b0_ads1220 *drv, double *value)
{
	b0_device *dev;

	B0_ASSERT_RETURN_ON_NULL_ARG(NULL, drv);
	dev = B0_MODULE_DEVICE(drv->module);
	B0_ASSERT_RETURN_ON_NULL_ARG(dev, value);

	b0_spi_sugar_arg arg = {
		.addr = drv->addr,
		.flags = __CONFIG,
		.bitsize = __BITSIZE,
		.speed = __SPEED
	};
	uint8_t write[4] = {B0_ADS1220_RDATA, 0x00, 0x00, 0x00};
	uint8_t read[4];
	B0_DEBUG_RETURN_ON_FAIL_STMT(dev,
		b0_spi_master_fd(drv->module, &arg, write, read, 4));

	/* convert the value in floating equivalent */

	static const double gains[8] = {1, 2, 4, 8, 16, 32, 64, 128};
	uint8_t gain_index = drv->cfg[B0_ADS1220_CFG0];
	gain_index &= B0_ADS1220_CFG0_GAIN_MASK;
	gain_index >>= B0_ADS1220_CFG0_GAIN_SHIFT;

	double pow_2_23 = 8388608; /* pow(2, 23) */
	double gain = gains[gain_index];
	double res = (drv->vref.high - drv->vref.low) / (pow_2_23 * gain);

	uint32_t rvalue = (read[1] << 16) | (read[2] << 8) | read[3];
	double fvalue = (rvalue > 0x7FFFFF) ?
		(0xFFFFFF - rvalue + 1) * -res :
		rvalue * res;
	fvalue += drv->vref.low;

	*value = fvalue;

	return B0_OK;
}

b0_result_code b0_ads1220_filter_set(b0_ads1220 *drv, b0_ads1220_filter type)
{
	b0_device *dev;
	uint8_t cfg2;

	B0_ASSERT_RETURN_ON_NULL_ARG(NULL, drv);
	dev = B0_MODULE_DEVICE(drv->module);

	cfg2 = drv->cfg[B0_ADS1220_CFG2];
	cfg2 &= ~B0_ADS1220_CFG2_FILTER_MASK;
	cfg2 |= B0_ADS1220_CFG2_FILTER(type);

	B0_DEBUG_RETURN_STMT(dev, write_reg(drv, B0_ADS1220_CFG2, cfg2));
}

b0_result_code b0_ads1220_vref_set(b0_ads1220 *drv,
	b0_ads1220_vref_source source, double low, double high)
{
	B0_ASSERT_RETURN_ON_NULL_ARG(NULL, drv);
	b0_device  *dev = B0_MODULE_DEVICE(drv->module);

	if (source == B0_ADS1220_VREF_INTERNAL) {
		low = VREF_INTERNAL_LOW;
		high = VREF_INTERNAL_HIGH;
	}

	uint8_t cfg2 = drv->cfg[B0_ADS1220_CFG2];
	cfg2 &= ~B0_ADS1220_CFG2_VREF_MASK;
	cfg2 |= B0_ADS1220_CFG2_VREF(source);

	B0_DEBUG_RETURN_ON_FAIL_STMT(dev, write_reg(drv, B0_ADS1220_CFG2, cfg2));

	drv->vref.low = low;
	drv->vref.high = high;

	return B0_OK;
}

b0_result_code b0_ads1220_gain_set(b0_ads1220 *drv, b0_ads1220_gain gain)
{
	b0_device *dev;
	uint8_t cfg0;

	B0_ASSERT_RETURN_ON_NULL_ARG(NULL, drv);
	dev = B0_MODULE_DEVICE(drv->module);

	cfg0 = drv->cfg[B0_ADS1220_CFG0];
	cfg0 &= ~B0_ADS1220_CFG0_GAIN_MASK;
	cfg0 |= B0_ADS1220_CFG0_GAIN(gain);

	B0_DEBUG_RETURN_STMT(dev, write_reg(drv, B0_ADS1220_CFG0, cfg0));
}

b0_result_code b0_ads1220_pga_bypass_set(b0_ads1220 *drv, bool bypass)
{
	b0_device *dev;
	uint8_t cfg0;

	B0_ASSERT_RETURN_ON_NULL_ARG(NULL, drv);
	dev = B0_MODULE_DEVICE(drv->module);

	cfg0 = drv->cfg[B0_ADS1220_CFG0];

	if (bypass) {
		cfg0 |= B0_ADS1220_CFG0_PGA_BYPASS;
	} else {
		cfg0 &= ~B0_ADS1220_CFG0_PGA_BYPASS;
	}

	B0_DEBUG_RETURN_STMT(dev, write_reg(drv, B0_ADS1220_CFG0, cfg0));
}
