/*
 * This file is part of libbox0.
 * Copyright (C) 2013-2016 Kuldeep Singh Dhaka <kuldeep@madresistor.com>
 *
 * libbox0 is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * libbox0 is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with libbox0.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef LIBBOX0_DRIVER_MAX31855_H
#define LIBBOX0_DRIVER_MAX31855_H

/* MAX31855: Cold-Junction Compensated Thermocouple-to-Digital Converter */

#include "../../common.h"
#include "../../module/spi.h"

__BEGIN_DECLS

#if defined(__DOXYGEN__)
struct b0_max31855 {};
#endif

/**
 * @class b0_max31855
 * @brief MAX31855 Cold-Junction Compensated Thermocouple-to-Digital Converter
 */

typedef struct b0_max31855 b0_max31855;

/* bits wrt 32bit value */
#define B0_MAX31855_FAULT_BIT (1 << 16)
#define B0_MAX31855_SCV_BIT (1 << 2)
#define B0_MAX31855_SCG_BIT (1 << 1)
#define B0_MAX31855_OC_BIT (1 << 0)

/**
 * @brief allocate a MAX31855 driver
 * @brief allocate a libbox0 MAX31855 driver object
 * @param[in] mod libbox0 SPI module
 * @param[out] drv pointer to memory location to store the allocate driver
 * @param[in] addr slave select pin connected to
 * @return result code
 * @note Mode: 0
 * @note Bitsize: 8
 * @note Data Order: MSB first
 * @note Variant: Half Duplex
 * @see b0_max31855_close()
 * @see b0_max31855_read()
 */
B0_API b0_result_code b0_max31855_open(b0_spi *mod,
	b0_max31855 **drv, unsigned addr);

/**
 * @brief free MAX31855 driver
 * @param[in] drv libbox0 MAX31855 driver
 * @return result code
 * @see b0_max31855_open()
 */
B0_API b0_result_code b0_max31855_close(b0_max31855 *drv);

/**
 * @brief read temperature from MAX31855
 * @param[in] drv libbox0 MAX31855 driver
 * @param[out] result pointer to memory to store resultant temperature
 * @return result code
 * @see b0_max31855_open()
 * @note will set @a result to nan if FAULT bit set by slave
 */
B0_API b0_result_code b0_max31855_read(b0_max31855 *drv, double *value);

__END_DECLS

#endif
