/*
 * This file is part of libbox0.
 * Copyright (C) 2013-2016 Kuldeep Singh Dhaka <kuldeep@madresistor.com>
 *
 * libbox0 is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * libbox0 is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with libbox0.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "max31855.h"
#include "../../libbox0-private.h"
#include "../../misc/conv.h"
#include <stdlib.h>

#if !defined(__DOXYGEN__)
struct b0_max31855 {
	b0_spi *module;
	unsigned addr;
};
#endif

/* assuming SPI mode - 0 (afaiu from diagram)
 *  - Clock Polarity LOW
 *  - Data is sampled at Rising Edge
 *
 * 8bit
 *
 * by default use half duplex, but full duplex can be used
 *
 * msb first
 *
 * TODO: set slave select pin low
 *
 * TODO: set slave select pin low
 */

#define __CONFIG (B0_SPI_TASK_MODE0 | B0_SPI_TASK_MSB_FIRST) /* HD */
#define __BITSIZE 8
#define __SPEED B0_SPI_TASK_SPEED_USE_FALLBACK

b0_result_code b0_max31855_open(b0_spi *mod, b0_max31855 **drv, unsigned addr)
{
	b0_max31855 *drv_alloc;
	b0_device *dev;

	B0_ASSERT_RETURN_ON_NULL_ARG(NULL, mod);
	dev = B0_MODULE_DEVICE(mod);

	B0_ASSERT_RETURN_ON_NULL_ARG(dev, drv);

	/*
	 * Allocate driver
	 */
	drv_alloc = calloc(1, sizeof(*drv_alloc));
	B0_ASSERT_RETURN_ON_NULL_ALLOC(dev, drv_alloc);

	drv_alloc->module = mod;
	drv_alloc->addr = addr;

	*drv = drv_alloc;
	return B0_OK;
}

b0_result_code b0_max31855_close(b0_max31855 *drv)
{
	B0_ASSERT_RETURN_ON_NULL_ARG(NULL, drv);

	/* free the memory */
	free(drv);
	return B0_OK;
}

b0_result_code b0_max31855_read(b0_max31855 *drv, double *result)
{
	b0_device *dev;
	B0_ASSERT_RETURN_ON_NULL_ARG(NULL, drv);
	dev = B0_MODULE_DEVICE(drv->module);

	B0_ASSERT_RETURN_ON_NULL_ARG(dev, result);

	b0_spi_sugar_arg arg = {
		.addr = drv->addr,
		.flags = __CONFIG,
		.bitsize = __BITSIZE,
		.speed = __SPEED
	};

	/* TODO: Thermal Considerations */

	uint8_t data[2];

	B0_DEBUG_RETURN_ON_FAIL_STMT(dev,
		b0_spi_master_hd_read(drv->module, &arg, data, 2));

	/* Fault occured */
	if (data[1] & 0x01 /* FAULT_BIT */) {
		B0_WARN(dev, "MAX31855 Fault bit set");
		*result = 0.0 / 0.0;
		return B0_OK;
	}

	int16_t value = ((data[0] << 8) | data[1]) >> 2;
	if (value & (1 << 13)) {
		/* extend the sign bit */
		value |= (1 << 15) | (1 << 14);
	}

	double temp /* celsius */ = value * 0.25;
	*result = B0_CELSIUS_TO_KELVIN(temp);

	return B0_OK;
}
