/*
 * This file is part of libbox0.
 * Copyright (C) 2013-2016 Kuldeep Singh Dhaka <kuldeep@madresistor.com>
 *
 * libbox0 is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * libbox0 is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with libbox0.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <stdio.h>
#include "../../libbox0-private.h"
#include "ad9833.h"

#if !defined(__DOXYGEN__)
struct b0_ad9833 {
	b0_spi *module;
	unsigned addr;
	unsigned int mclk;

	uint16_t control;
};
#endif

static int send(b0_ad9833 *drv, uint16_t data);
static int control_register_refresh(b0_ad9833 *drv);

/* SPI mode - 1
 *  - Clock Polarity LOW
 *  - Data is sampled at Falling Edge
 *
 * 8bit can be used
 *
 * just require write (no read facility)
 *
 * msb first
 */

#define __CONFIG (B0_SPI_TASK_MODE1 | B0_SPI_TASK_MSB_FIRST) /* HD */
#define __BITSIZE 8
#define __SPEED B0_SPI_TASK_SPEED_USE_FALLBACK

b0_result_code b0_ad9833_open(b0_spi *mod, b0_ad9833 **drv, unsigned addr,
	unsigned int mclk)
{
	b0_result_code r;
	b0_ad9833 *drv_alloc;
	b0_device *dev;

	B0_ASSERT_RETURN_ON_NULL_ARG(NULL, mod);
	dev = B0_MODULE_DEVICE(mod);
	B0_ASSERT_RETURN_ON_NULL_ARG(dev, drv);
	B0_ASSERT_RETURN_ON_FAIL_ARG(dev, mclk);

	/*
	 * Allocate drv_alloc
	 */
	drv_alloc = calloc(1, sizeof(*drv_alloc));
	B0_ASSERT_RETURN_ON_NULL_ALLOC(dev, drv_alloc);

	drv_alloc->module = mod;
	drv_alloc->addr = addr;
	drv_alloc->mclk = mclk;
	drv_alloc->control = B0_AD9833_CONTROL_B28;

	b0_spi_sugar_arg arg = {
		.addr = drv_alloc->addr,
		.flags = __CONFIG,
		.bitsize = __BITSIZE,
		.speed = __SPEED
	};

	/*
	 * Initalization procedure:
	 * - Reset Enable
	 * - Write FreqX (here X = 0)
	 * - Write PhaseY (here Y = 0)
	 * - Reset Disable
	 */
	uint16_t data[5 * 2] = {
		B0_H2LE16(B0_AD9833_CONTROL | drv_alloc->control | B0_AD9833_CONTROL_RESET),
		B0_H2LE16(B0_AD9833_FREQ0   | 0x3FFF),
		B0_H2LE16(B0_AD9833_FREQ0   | 0x3FFF),
		B0_H2LE16(B0_AD9833_PHASE0  | 0),
		B0_H2LE16(B0_AD9833_CONTROL | drv_alloc->control),
	};

	r = b0_spi_master_hd_write(mod, &arg, data, 10);
	if (B0_ERROR_RESULT_CODE(r)) {
		free(drv_alloc);
		return r;
	}

	*drv = drv_alloc;
	return B0_OK;
}

b0_result_code b0_ad9833_close(b0_ad9833 *drv)
{
	B0_ASSERT_RETURN_ON_NULL_ARG(NULL, drv);

	free(drv);
	return B0_OK;
}

static b0_result_code send(b0_ad9833 *drv, uint16_t data)
{
	const b0_spi_sugar_arg arg = {
		.bitsize = __BITSIZE,
		.addr = drv->addr,
		.flags = __CONFIG,
		.speed = __SPEED
	};

	return b0_spi_master_hd_write(drv->module, &arg, &data, 1);
}

static int control_register_refresh(b0_ad9833 *drv)
{
	return send(drv, drv->control & 0xC000);
}

/* value in Hertz (0, MCLK] */
b0_result_code b0_ad9833_freq_value_set(b0_ad9833 *drv, unsigned int  num, double value)
{
	B0_ASSERT_RETURN_ON_NULL_ARG(NULL, drv);
	b0_device *dev = B0_MODULE_DEVICE(drv->module);

	B0_ASSERT_RETURN_ON_FAIL_ARG(dev, num < 2);
	B0_ASSERT_RETURN_ON_FAIL_ARG(dev, value > 0);

	uintmax_t _reg = ((2 << 28) * value) / drv->mclk;

	/* maximum value is 2^28 - 1 */
	if (_reg > ((2*28) - 1)) {
		/* not supported */
		return B0_ERR_OVERFLOW;
	}

	const b0_spi_sugar_arg arg = {
		.addr = drv->addr,
		.flags = __CONFIG,
		.bitsize = __BITSIZE,
		.speed = __SPEED
	};

	uint32_t _freqX = num ? B0_AD9833_FREQ1 : B0_AD9833_FREQ0;
	uint32_t data = B0_H2LE32((_reg & 0xFFFFFFF) | _freqX);
	return b0_spi_master_hd_write(drv->module, &arg, &data, 4);
}

b0_result_code b0_ad9833_freq_select_set(b0_ad9833 *drv, unsigned int num)
{
	B0_ASSERT_RETURN_ON_NULL_ARG(NULL, drv);
	b0_device *dev = B0_MODULE_DEVICE(drv->module);

	B0_ASSERT_RETURN_ON_FAIL_ARG(dev, num < 2);

	if (num) {
		drv->control |= B0_AD9833_CONTROL_FSELECT;
	} else {
		drv->control &= ~B0_AD9833_CONTROL_FSELECT;
	}

	return send(drv, drv->control | B0_AD9833_CONTROL);


	return B0_OK;
}

b0_result_code b0_ad9833_phase_value_set(b0_ad9833 *drv, unsigned int num, double value)
{
	B0_ASSERT_RETURN_ON_NULL_ARG(NULL, drv);
	b0_device *dev = B0_MODULE_DEVICE(drv->module);

	B0_DEBUG(dev, "TODO: b0_ad9833_phase_value_set()");

	return B0_OK;
}

b0_result_code b0_ad9833_phase_select_set(b0_ad9833 *drv, unsigned int num)
{
	B0_ASSERT_RETURN_ON_NULL_ARG(NULL, drv);

	b0_device *dev = B0_MODULE_DEVICE(drv->module);

	B0_DEBUG(dev, "TODO: b0_ad9833_phase_select_set()");

	return B0_OK;
}

b0_result_code b0_ad9833_sleep_set(b0_ad9833 *drv, enum b0_ad9833_sleep level)
{
	B0_ASSERT_RETURN_ON_NULL_ARG(NULL, drv);
	b0_device *dev = B0_MODULE_DEVICE(drv->module);

	B0_DEBUG(dev, "TODO: b0_ad9833_sleep_set()");

	return B0_OK;
}

b0_result_code b0_ad9833_output_set(b0_ad9833 *drv, enum b0_ad9833_output output)
{
	B0_ASSERT_RETURN_ON_NULL_ARG(NULL, drv);
	b0_device *dev = B0_MODULE_DEVICE(drv->module);

	B0_DEBUG(dev, "TODO: b0_ad9833_output_set()");

	return B0_OK;
}
