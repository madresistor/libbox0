/*
 * This file is part of libbox0.
 * Copyright (C) 2013-2016 Kuldeep Singh Dhaka <kuldeep@madresistor.com>
 *
 * libbox0 is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * libbox0 is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with libbox0.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef LIBBOX0_DRIVER_AD9833_H
#define LIBBOX0_DRIVER_AD9833_H

#include <stdint.h>
#include <stdbool.h>
#include "../../common.h"
#include "../../module/spi.h"

__BEGIN_DECLS

#if defined(__DOXYGEN__)
struct b0_ad9833 {};
#endif

/**
 * @class b0_ad9833
 * @brief AD9833
 */

typedef struct b0_ad9833 b0_ad9833;

#define B0_AD9833_CONTROL 0
#define B0_AD9833_FREQ0 0x4000
#define B0_AD9833_FREQ1 0x8000
#define B0_AD9833_PHASE0 0xC000
#define B0_AD9833_PHASE1 0xE000

#define B0_AD9833_CONTROL_B28		(1 << 13)
#define B0_AD9833_CONTROL_HIB		(1 << 12)
#define B0_AD9833_CONTROL_FSELECT	(1 << 11)
#define B0_AD9833_CONTROL_PSELECT	(1 << 10)
#define B0_AD9833_CONTROL_RESET		(1 << 8)
#define B0_AD9833_CONTROL_SLEEP1	(1 << 7)
#define B0_AD9833_CONTROL_SLEEP12	(1 << 6)
#define B0_AD9833_CONTROL_OPBITEN	(1 << 5)
#define B0_AD9833_CONTROL_DIV2		(1 << 3)
#define B0_AD9833_CONTROL_MODE		(1 << 1)

enum b0_ad9833_sleep {
	B0_AD9833_SLEEP_NONE,			/**< None */
	B0_AD9833_SLEEP_DAC,			/**< only DAC */
	B0_AD9833_SLEEP_CLOCK,			/**< only internal clock */
	B0_AD9833_SLEEP_DAC_AND_CLOCK	/**< both DAC and internal clock */
};

enum b0_ad9833_output {
	B0_AD9833_OUTPUT_SINUSOID,
	B0_AD9833_OUTPUT_TRIANGULAR,
	B0_AD9833_OUTPUT_CLOCK
};

B0_API b0_result_code b0_ad9833_open(b0_spi *mod, b0_ad9833 **drv,
	unsigned addr, unsigned int mclk);
B0_API b0_result_code b0_ad9833_close(b0_ad9833 *drv);

B0_API b0_result_code b0_ad9833_freq_value_set(b0_ad9833 *drv, unsigned int num, double value);
B0_API b0_result_code b0_ad9833_freq_select_set(b0_ad9833 *drv, unsigned int num);

B0_API b0_result_code b0_ad9833_phase_value_set(b0_ad9833 *drv, unsigned int num, double value);
B0_API b0_result_code b0_ad9833_phase_select_set(b0_ad9833 *drv, unsigned int num);

B0_API b0_result_code b0_ad9833_sleep_set(b0_ad9833 *drv, enum b0_ad9833_sleep level);
B0_API b0_result_code b0_ad9833_output_set(b0_ad9833 *drv, enum b0_ad9833_output output);

__END_DECLS

#endif
