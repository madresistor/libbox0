/*
 * This file is part of libbox0.
 * Copyright (C) 2013-2016 Kuldeep Singh Dhaka <kuldeep@madresistor.com>
 *
 * libbox0 is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * libbox0 is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with libbox0.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef LIBBOX0_DRIVER_PCF8814_H
#define LIBBOX0_DRIVER_PCF8814_H

#include <stdint.h>
#include <stdbool.h>
#include "../../common.h"
#include "../../module/spi.h"

__BEGIN_DECLS

#if defined(__DOXYGEN__)
struct b0_pcf8814 {};
#endif


/**
 * @class b0_pcf8814
 * @brief PCF8814 65 × 96 pixels matrix LCD driver
 * @details Device driver for
 * 			*Nokia 1100 LCD* controller
 *
 * @note
 * Nokia 1100 PIN configuration:           \n
 * See docs/datasheets/PCF8814/pcf8814.pdf \n
 * |-----------------| \n
 * |  screen         | \n
 * |    front        | \n
 * |_________________| \n
 *  | | | | | | | | |  \n
 *  1 2 3 4 5 6 7 8 9  \n
 *                     \n
 * Pin 1: RESET  \n
 * Pin 2: CS     \n
 * Pin 3: GND    \n
 * Pin 4: SDA    \n
 * Pin 5: SCLK   \n
 * Pin 6: VDDI   \n
 * Pin 7: VDD    \n
 * Pin 8: LED+   \n
 * Pin 9: Unused \n
 */

enum b0_pcf8814_ram_addressing_mode {HORIZONTAL, VERTIAL};
typedef enum b0_pcf8814_ram_addressing_mode b0_pcf8814_ram_addressing_mode;

enum b0_pcf8814_power {OFF, ON, PARTIAL};
typedef enum b0_pcf8814_power b0_pcf8814_power;

typedef struct b0_pcf8814 b0_pcf8814;

/* configurations */
#define B0_PFC8814_DEFAULT_CONTRAST 0x55

/* reset (HIGH->LOW->HIGH RESET) the LCD is to be done externally */

/**
 * @brief allocate a PCF8814
 * @param[in] mod libbox0 SPI module
 * @param[out] drv pointer to memory location to store allocated PCF8814
 * @param[in] addr slave select pin connected to
 * @return result code
 * @note Assuming SPI Module is already initalized (applies to all driver for all type of module)
 * @note Mode: 0
 * @note Bitsize: 9
 * @note Data Order: MSB First
 * @see b0_pcf8814_close()
 */
B0_API b0_result_code b0_pcf8814_open(b0_spi *mod, b0_pcf8814 **drv,
		unsigned addr);

/**
 * @brief free PCF8814 driver
 * @param[in] drv libbox0 PCF8814 driver object
 * @return result code
 * @see b0_pcf8814_open()
 */
B0_API b0_result_code b0_pcf8814_close(b0_pcf8814 *drv);

B0_API b0_result_code b0_pcf8814_pixel_invert_set(b0_pcf8814 *drv, bool on_off);
B0_API b0_result_code b0_pcf8814_pixel_show_set(b0_pcf8814 *drv, bool on_off);
B0_API b0_result_code b0_pcf8814_pixel_all_set(b0_pcf8814 *drv, bool on_off);
B0_API b0_result_code b0_pcf8814_contrast_set(b0_pcf8814 *drv, uint8_t range);

B0_API b0_result_code b0_pcf8814_ram_addressing_mode_set(b0_pcf8814 *drv,
	b0_pcf8814_ram_addressing_mode mode);
B0_API b0_result_code b0_pcf8814_charge_pump_value_set(b0_pcf8814 *drv, uint8_t value);
B0_API b0_result_code b0_pcf8814_charge_pump_set(b0_pcf8814 *drv, bool on);

/* only `PARTIAL` is applicable to this function only */
B0_API b0_result_code b0_pcf8814_power_set(b0_pcf8814 *drv, b0_pcf8814_power pow);

B0_API b0_result_code b0_pcf8814_inital_display_line_set(b0_pcf8814 *drv, uint8_t row);
B0_API b0_result_code b0_pcf8814_gotoyx(b0_pcf8814 *drv, uint8_t y, uint8_t x);
B0_API b0_result_code b0_pcf8814_gotox(b0_pcf8814 *drv, uint8_t x);
B0_API b0_result_code b0_pcf8814_gotoy(b0_pcf8814 *drv, uint8_t y);

/**
 * @brief send a command to PCF8814 controller
 * @param[in] drv libbox0 PCF8814 driver object
 * @param[in] payload command to be send
 * @return result code
 * @note low level API
 */
B0_API b0_result_code b0_pcf8814_send_cmd(b0_pcf8814 *drv, uint8_t payload);

/**
 * @brief clear screen
 * @param[in] drv libbox0 PCF8814 driver
 * @return result code
 * @note this operation require lots of zero byte write and is an *expensive operation*.
 * @see b0_pcf8814_open()
 */
B0_API b0_result_code b0_pcf8814_clear(b0_pcf8814 *drv);

/**
 * @brief write a character to screen
 * @details character is written to screen by using controller internal position
 * @param[in] drv libbox0 pcf8814 driver
 * @param[in] ch ASCII character to be written
 * @return result code
 * @see b0_pcf8814_puts()
 * @see b0_pcf8814_open()
 */
B0_API b0_result_code b0_pcf8814_putchar(b0_pcf8814 *drv, char ch);

/**
 * @brief write a string to screen
 * @details string is written to screen in contineous write
 * @param[in] drv libbox0 pcf8814 driver
 * @param[in] str C style string (ie. ending with 0) (7bit ASCII)
 * @return result code
 * @see b0_pcf8814_putchar()
 * @see b0_pcf8814_open()
 */
B0_API b0_result_code b0_pcf8814_puts(b0_pcf8814 *drv, const char *str);

__END_DECLS

#endif
