/*
 * This file is part of libbox0.
 * Copyright (C) 2013-2016 Kuldeep Singh Dhaka <kuldeep@madresistor.com>
 *
 * libbox0 is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * libbox0 is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with libbox0.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * @file
 * @todo parameter testing for all PCF8815 functions
 * @todo multiple font size
 * @todo support multiple language
 * @todo instead of sending each command alone,
 *   commands could be packed into one ds to
 *   conserve bandwidth as well as processing
 */

#include <stdio.h>
#include "../../libbox0-private.h"
#include "pcf8814.h"

#if !defined(__DOXYGEN__)
struct b0_pcf8814 {
	b0_spi *module;
	unsigned addr;
};
#endif

#define B0_PCF8814_WIDTH_PIXEL 96
#define B0_PCF8814_HEIGHT_PIXEL 65

#define B0_PCF8814_WIDTH_CHAR 6
#define B0_PCF8814_HEIGHT_CHAR 8

#define __CONFIG (B0_SPI_TASK_MODE0 | B0_SPI_TASK_MSB_FIRST)
#define __BITSIZE 9
#define __SPEED B0_SPI_TASK_SPEED_USE_FALLBACK

#define __CMD(payload) B0_H2LE16(0x0000 | (payload))
#define __DAT(payload) B0_H2LE16(0x0100 | (payload))

static const uint8_t char_maps[][B0_PCF8814_WIDTH_CHAR] =
{
	 {0x00, 0x00, 0x00, 0x00, 0x00, 0x00} // 20 [SPACE]
	,{0x00, 0x00, 0x00, 0x5f, 0x00, 0x00} // 21 !
	,{0x00, 0x00, 0x07, 0x00, 0x07, 0x00} // 22 "
	,{0x00, 0x14, 0x7f, 0x14, 0x7f, 0x14} // 23 #
	,{0x00, 0x24, 0x2a, 0x7f, 0x2a, 0x12} // 24 $
	,{0x00, 0x23, 0x13, 0x08, 0x64, 0x62} // 25 %
	,{0x00, 0x36, 0x49, 0x55, 0x22, 0x50} // 26 &
	,{0x00, 0x00, 0x05, 0x03, 0x00, 0x00} // 27 '
	,{0x00, 0x00, 0x1c, 0x22, 0x41, 0x00} // 28 (
	,{0x00, 0x00, 0x41, 0x22, 0x1c, 0x00} // 29 )
	,{0x00, 0x14, 0x08, 0x3e, 0x08, 0x14} // 2a *
	,{0x00, 0x08, 0x08, 0x3e, 0x08, 0x08} // 2b +
	,{0x00, 0x00, 0x50, 0x30, 0x00, 0x00} // 2c ,
	,{0x00, 0x08, 0x08, 0x08, 0x08, 0x08} // 2d -
	,{0x00, 0x00, 0x60, 0x60, 0x00, 0x00} // 2e .
	,{0x00, 0x20, 0x10, 0x08, 0x04, 0x02} // 2f /
	,{0x00, 0x3e, 0x51, 0x49, 0x45, 0x3e} // 30 0
	,{0x00, 0x00, 0x42, 0x7f, 0x40, 0x00} // 31 1
	,{0x00, 0x42, 0x61, 0x51, 0x49, 0x46} // 32 2
	,{0x00, 0x21, 0x41, 0x45, 0x4b, 0x31} // 33 3
	,{0x00, 0x18, 0x14, 0x12, 0x7f, 0x10} // 34 4
	,{0x00, 0x27, 0x45, 0x45, 0x45, 0x39} // 35 5
	,{0x00, 0x3c, 0x4a, 0x49, 0x49, 0x30} // 36 6
	,{0x00, 0x01, 0x71, 0x09, 0x05, 0x03} // 37 7
	,{0x00, 0x36, 0x49, 0x49, 0x49, 0x36} // 38 8
	,{0x00, 0x06, 0x49, 0x49, 0x29, 0x1e} // 39 9
	,{0x00, 0x00, 0x36, 0x36, 0x00, 0x00} // 3a :
	,{0x00, 0x00, 0x56, 0x36, 0x00, 0x00} // 3b ;
	,{0x00, 0x08, 0x14, 0x22, 0x41, 0x00} // 3c <
	,{0x00, 0x14, 0x14, 0x14, 0x14, 0x14} // 3d =
	,{0x00, 0x00, 0x41, 0x22, 0x14, 0x08} // 3e >
	,{0x00, 0x02, 0x01, 0x51, 0x09, 0x06} // 3f ?
	,{0x00, 0x32, 0x49, 0x79, 0x41, 0x3e} // 40 @
	,{0x00, 0x7e, 0x11, 0x11, 0x11, 0x7e} // 41 A
	,{0x00, 0x7f, 0x49, 0x49, 0x49, 0x36} // 42 B
	,{0x00, 0x3e, 0x41, 0x41, 0x41, 0x22} // 43 C
	,{0x00, 0x7f, 0x41, 0x41, 0x22, 0x1c} // 44 D
	,{0x00, 0x7f, 0x49, 0x49, 0x49, 0x41} // 45 E
	,{0x00, 0x7f, 0x09, 0x09, 0x09, 0x01} // 46 F
	,{0x00, 0x3e, 0x41, 0x49, 0x49, 0x7a} // 47 G
	,{0x00, 0x7f, 0x08, 0x08, 0x08, 0x7f} // 48 H
	,{0x00, 0x00, 0x41, 0x7f, 0x41, 0x00} // 49 I
	,{0x00, 0x20, 0x40, 0x41, 0x3f, 0x01} // 4a J
	,{0x00, 0x7f, 0x08, 0x14, 0x22, 0x41} // 4b K
	,{0x00, 0x7f, 0x40, 0x40, 0x40, 0x40} // 4c L
	,{0x00, 0x7f, 0x02, 0x0c, 0x02, 0x7f} // 4d M
	,{0x00, 0x7f, 0x04, 0x08, 0x10, 0x7f} // 4e N
	,{0x00, 0x3e, 0x41, 0x41, 0x41, 0x3e} // 4f O
	,{0x00, 0x7f, 0x09, 0x09, 0x09, 0x06} // 50 P
	,{0x00, 0x3e, 0x41, 0x51, 0x21, 0x5e} // 51 Q
	,{0x00, 0x7f, 0x09, 0x19, 0x29, 0x46} // 52 R
	,{0x00, 0x46, 0x49, 0x49, 0x49, 0x31} // 53 S
	,{0x00, 0x01, 0x01, 0x7f, 0x01, 0x01} // 54 T
	,{0x00, 0x3f, 0x40, 0x40, 0x40, 0x3f} // 55 U
	,{0x00, 0x1f, 0x20, 0x40, 0x20, 0x1f} // 56 V
	,{0x00, 0x3f, 0x40, 0x38, 0x40, 0x3f} // 57 W
	,{0x00, 0x63, 0x14, 0x08, 0x14, 0x63} // 58 X
	,{0x00, 0x07, 0x08, 0x70, 0x08, 0x07} // 59 Y
	,{0x00, 0x61, 0x51, 0x49, 0x45, 0x43} // 5a Z
	,{0x00, 0x00, 0x7f, 0x41, 0x41, 0x00} // 5b [
	,{0x00, 0x02, 0x04, 0x08, 0x10, 0x20} // 5c backslash "\"
	,{0x00, 0x00, 0x41, 0x41, 0x7f, 0x00} // 5d ]
	,{0x00, 0x04, 0x02, 0x01, 0x02, 0x04} // 5e ^
	,{0x00, 0x40, 0x40, 0x40, 0x40, 0x40} // 5f _
	,{0x00, 0x00, 0x01, 0x02, 0x04, 0x00} // 60 `
	,{0x00, 0x20, 0x54, 0x54, 0x54, 0x78} // 61 a
	,{0x00, 0x7f, 0x48, 0x44, 0x44, 0x38} // 62 b
	,{0x00, 0x38, 0x44, 0x44, 0x44, 0x20} // 63 c
	,{0x00, 0x38, 0x44, 0x44, 0x48, 0x7f} // 64 d
	,{0x00, 0x38, 0x54, 0x54, 0x54, 0x18} // 65 e
	,{0x00, 0x08, 0x7e, 0x09, 0x01, 0x02} // 66 f
	,{0x00, 0x0c, 0x52, 0x52, 0x52, 0x3e} // 67 g
	,{0x00, 0x7f, 0x08, 0x04, 0x04, 0x78} // 68 h
	,{0x00, 0x00, 0x44, 0x7d, 0x40, 0x00} // 69 i
	,{0x00, 0x20, 0x40, 0x44, 0x3d, 0x00} // 6a j
	,{0x00, 0x7f, 0x10, 0x28, 0x44, 0x00} // 6b k
	,{0x00, 0x00, 0x41, 0x7f, 0x40, 0x00} // 6c l
	,{0x00, 0x7c, 0x04, 0x18, 0x04, 0x78} // 6d m
	,{0x00, 0x7c, 0x08, 0x04, 0x04, 0x78} // 6e n
	,{0x00, 0x38, 0x44, 0x44, 0x44, 0x38} // 6f o
	,{0x00, 0x7c, 0x14, 0x14, 0x14, 0x08} // 70 p
	,{0x00, 0x08, 0x14, 0x14, 0x18, 0x7c} // 71 q
	,{0x00, 0x7c, 0x08, 0x04, 0x04, 0x08} // 72 r
	,{0x00, 0x48, 0x54, 0x54, 0x54, 0x20} // 73 s
	,{0x00, 0x04, 0x3f, 0x44, 0x40, 0x20} // 74 t
	,{0x00, 0x3c, 0x40, 0x40, 0x20, 0x7c} // 75 u
	,{0x00, 0x1c, 0x20, 0x40, 0x20, 0x1c} // 76 v
	,{0x00, 0x3c, 0x40, 0x30, 0x40, 0x3c} // 77 w
	,{0x00, 0x44, 0x28, 0x10, 0x28, 0x44} // 78 x
	,{0x00, 0x0c, 0x50, 0x50, 0x50, 0x3c} // 79 y
	,{0x00, 0x44, 0x64, 0x54, 0x4c, 0x44} // 7a z
	,{0x00, 0x00, 0x08, 0x36, 0x41, 0x00} // 7b {
	,{0x00, 0x00, 0x00, 0x7f, 0x00, 0x00} // 7c |
	,{0x00, 0x00, 0x41, 0x36, 0x08, 0x00} // 7d }
	,{0x00, 0x10, 0x20, 0x10, 0x80, 0x10} // 7E ~
};

b0_result_code b0_pcf8814_clear(b0_pcf8814 *drv)
{
	unsigned int i;
	b0_device *dev = B0_MODULE_DEVICE(drv->module);

	B0_DEBUG(dev, "Clearing full screen");

	b0_pcf8814_gotoyx(drv, 0,0);

	/* NOTE: this require 18byte of buffer */
	uint16_t out[8] = {
		__DAT(0), __DAT(0), __DAT(0), __DAT(0),
		__DAT(0), __DAT(0), __DAT(0), __DAT(0)
	};

	b0_spi_sugar_arg arg = {
		.addr = drv->addr,
		.flags = __CONFIG,
		.bitsize = __BITSIZE,
		.speed = __SPEED
	};

	/* total written memory is 27*32 RAM memory bytes bytes written */
	for (i = 0; i < 108; i++) {
		B0_DEBUG_RETURN_ON_FAIL_STMT(dev,
			b0_spi_master_hd_write(drv->module, &arg, out, 8));
	}

	/* sleeping for 1ms for LCD to refresh itself after a full clean
	 * sleep for 1ms | delay to let the LCD manage itself
	 */
	B0_SLEEP_MILLISECOND(1);

	return B0_OK;
}

b0_result_code b0_pcf8814_putchar(b0_pcf8814 *drv, char ch)
{
	b0_spi_sugar_arg arg = {
		.addr = drv->addr,
		.flags = __CONFIG,
		.bitsize = __BITSIZE,
		.speed = __SPEED
	};

	/* WARNING NOTE: character width is hardcoded (6) */
	uint16_t data[6] = {
		__DAT(char_maps[ch - 0x20][0]),
		__DAT(char_maps[ch - 0x20][1]),
		__DAT(char_maps[ch - 0x20][2]),
		__DAT(char_maps[ch - 0x20][3]),
		__DAT(char_maps[ch - 0x20][4]),
		__DAT(char_maps[ch - 0x20][5]),
	};

	return b0_spi_master_hd_write(drv->module, &arg, data, 6);
}

b0_result_code b0_pcf8814_puts(b0_pcf8814 *drv, const char *str)
{
	B0_ASSERT_RETURN_ON_NULL_ARG(NULL, drv);

	b0_device *dev = B0_MODULE_DEVICE(drv->module);
	B0_ASSERT_RETURN_ON_NULL_ARG(dev, str);

	while (*str) {
		B0_DEBUG_RETURN_ON_FAIL_STMT(dev,
			b0_pcf8814_putchar(drv, *str));
		str++;
	}

	return B0_OK;
}

/* SPI mode - 0
 *  - Clock Polarity LOW
 *  - Data is sampled at Rising Edge
 *
 * 9bit
 *
 * msb first
 *
 * half duplex
 */

b0_result_code b0_pcf8814_open(b0_spi *mod, b0_pcf8814 **drv, unsigned addr)
{
	b0_result_code r;
	b0_pcf8814 *drv_alloc;
	b0_device *dev;

	B0_ASSERT_RETURN_ON_NULL_ARG(NULL, mod);
	dev = B0_MODULE_DEVICE(mod);

	B0_ASSERT_RETURN_ON_NULL_ARG(dev, drv);

	/*
	 * Allocate driver
	 */
	drv_alloc = calloc(1, sizeof(*drv_alloc));
	B0_ASSERT_RETURN_ON_NULL_ALLOC(dev, drv_alloc);

	drv_alloc->module = mod;
	drv_alloc->addr = addr;

	/* initalization commands */
	if (B0_ERROR_RESULT_CODE((r = b0_pcf8814_charge_pump_set(drv_alloc, ON))) ||
		B0_ERROR_RESULT_CODE(r = b0_pcf8814_contrast_set(
			drv_alloc, B0_PFC8814_DEFAULT_CONTRAST)) ||
		B0_ERROR_RESULT_CODE(r = b0_pcf8814_power_set(drv_alloc, ON)) ||
		B0_ERROR_RESULT_CODE(r = b0_pcf8814_clear(drv_alloc)) ||
		B0_ERROR_RESULT_CODE(r = b0_pcf8814_gotoyx(drv_alloc, 0, 0))) {
		free(drv_alloc);
		return r;
	}

	*drv = drv_alloc;
	return B0_OK;
}

b0_result_code b0_pcf8814_close(b0_pcf8814 *drv)
{
	B0_ASSERT_RETURN_ON_NULL_ARG(NULL, drv);

	free(drv);
	return B0_OK;
}

b0_result_code b0_pcf8814_send_cmd(b0_pcf8814 *drv, uint8_t payload)
{
	B0_ASSERT_RETURN_ON_NULL_ARG(NULL, drv);

	b0_spi_sugar_arg arg = {
		.addr = drv->addr,
		.flags = __CONFIG,
		.bitsize = __BITSIZE,
		.speed = __SPEED
	};

	uint16_t data = __CMD(payload);

	return b0_spi_master_hd_write(drv->module, &arg, &data, 1);
}

b0_result_code b0_pcf8814_gotoy(b0_pcf8814 *drv, uint8_t y)
{
	B0_ASSERT_RETURN_ON_NULL_ARG(NULL, drv);

	b0_device *dev = B0_MODULE_DEVICE(drv->module);
	B0_DEBUG(dev, "Goto Y: %"PRIx8, (y & 0x0F));
	return b0_pcf8814_send_cmd(drv, 0xB0 /* VERT ADDR */ | (y & 0x0F));
}

b0_result_code b0_pcf8814_gotox(b0_pcf8814 *drv, uint8_t x)
{
	B0_ASSERT_RETURN_ON_NULL_ARG(NULL, drv);

	b0_spi_sugar_arg arg = {
		.addr = drv->addr,
		.flags = __CONFIG,
		.bitsize = __BITSIZE,
		.speed = __SPEED
	};

	uint16_t data[2] = {
		__CMD(0x00 | (x & 0x0F)) /* HORZ ADDR LSB */,
		__CMD(0x18 | (x >> 4)) /* HORZ ADDR MSB */,
	};

	b0_device *dev = B0_MODULE_DEVICE(drv->module);
	B0_DEBUG(dev, "Goto X: %"PRIx8, x);
	return b0_spi_master_hd_write(drv->module, &arg, data, 2);
}

b0_result_code b0_pcf8814_gotoyx(b0_pcf8814 *drv, uint8_t y, uint8_t x)
{
	B0_ASSERT_RETURN_ON_NULL_ARG(NULL, drv);

	b0_spi_sugar_arg arg = {
		.addr = drv->addr,
		.flags = __CONFIG,
		.bitsize = __BITSIZE,
		.speed = __SPEED
	};

	uint16_t data[3] = {
		__CMD(0x00 | (x & 0x0F)), /* HORZ ADDR LSB */
		__CMD(0x18 | (x >> 4)),   /* HORZ ADDR MSB */
		__CMD(0xB0 | (y & 0x0F))  /* VERT ADDR */
	};

	b0_device *dev = B0_MODULE_DEVICE(drv->module);
	B0_DEBUG(dev, "Goto (Y,X): (%"PRIx8", %"PRIx8")", (y & 0x0F), x);
	return b0_spi_master_hd_write(drv->module, &arg, data, 3);
}

b0_result_code b0_pcf8814_inital_display_line_set(b0_pcf8814 *drv, uint8_t row)
{
	B0_ASSERT_RETURN_ON_NULL_ARG(NULL, drv);

	b0_device *dev = B0_MODULE_DEVICE(drv->module);
	B0_DEBUG(dev, "Inital Display Line: %"PRIx8, row);
	return b0_pcf8814_send_cmd(drv,0x40  /* INITAL DISPLAY LINE */| (row & 0x3F));
}

b0_result_code b0_pcf8814_power_set(b0_pcf8814 *drv, b0_pcf8814_power power)
{
	b0_result_code r;

	B0_ASSERT_RETURN_ON_NULL_ARG(NULL, drv);
	b0_device *dev = B0_MODULE_DEVICE(drv->module);

	switch(power) {
	case OFF:
		B0_DEBUG(dev, "Power set: OFF");
		if (B0_ERROR_RESULT_CODE(r = b0_pcf8814_pixel_show_set(drv, false)) ||
			B0_ERROR_RESULT_CODE(r = b0_pcf8814_pixel_all_set(drv, true))) {
			 return r;
		}
	break;
	case ON:
		B0_DEBUG(dev, "Power set: ON");
		if (B0_ERROR_RESULT_CODE(r = b0_pcf8814_pixel_show_set(drv, true))  ||
			B0_ERROR_RESULT_CODE(r = b0_pcf8814_pixel_all_set(drv, false))   ||
			B0_ERROR_RESULT_CODE(r = b0_pcf8814_pixel_invert_set(drv, false))) {
			return r;
		}
	break;
	case PARTIAL:
		B0_DEBUG(dev, "Power set: PARTIAL");
		if (B0_ERROR_RESULT_CODE(r = b0_pcf8814_pixel_show_set(drv, false)) ||
			B0_ERROR_RESULT_CODE(r = b0_pcf8814_pixel_all_set(drv, false))) {
			return r;
		}
	break;
	default:
	return B0_ERR_ARG;
	}

	return B0_OK;
}

b0_result_code b0_pcf8814_contrast_set(b0_pcf8814 *drv, uint8_t value)
{
	B0_ASSERT_RETURN_ON_NULL_ARG(NULL, drv);

	b0_spi_sugar_arg arg = {
		.addr = drv->addr,
		.flags = __CONFIG,
		.bitsize = __BITSIZE,
		.speed = __SPEED
	};

	uint16_t data[2] = {
		__CMD(0x20 | (value >> 5)), /* VOP-MSB */
		__CMD(0x80 | (value & 0x1F)) /* VOP-LSB */
	};

	b0_device *dev = B0_MODULE_DEVICE(drv->module);
	B0_DEBUG(dev, "Contrast set: %"PRIx8, value);
	return b0_spi_master_hd_write(drv->module, &arg, data, 2);
}

b0_result_code b0_pcf8814_charge_pump_set(b0_pcf8814 *drv, bool on)
{
	B0_ASSERT_RETURN_ON_NULL_ARG(NULL, drv);

	b0_device *dev = B0_MODULE_DEVICE(drv->module);
	B0_DEBUG(dev, "Charge Pump: %s", on ? "ON" : "OFF");
	return b0_pcf8814_send_cmd(drv,
		(on ? 0x2F : (0x28 | 0x02)) /* CHARGE PUMP CONTROL */);
}

b0_result_code b0_pcf8814_charge_pump_value_set(b0_pcf8814 *drv, uint8_t value)
{
	B0_ASSERT_RETURN_ON_NULL_ARG(NULL, drv);

	b0_spi_sugar_arg arg = {
		.addr = drv->addr,
		.flags = __CONFIG,
		.bitsize = __BITSIZE,
		.speed = __SPEED
	};

	uint16_t data[2] = {
		__CMD(0x3D), /* CHARGE PUMP */
		__CMD(value & 0x03)
	};

	b0_device *dev = B0_MODULE_DEVICE(drv->module);
	B0_DEBUG(dev, "Charge Pump set to %"PRIx8, (value & 0x03));
	return b0_spi_master_hd_write(drv->module, &arg, data, 2);
}

b0_result_code b0_pcf8814_ram_addressing_mode_set(b0_pcf8814 *drv,
	b0_pcf8814_ram_addressing_mode mode)
{
	B0_ASSERT_RETURN_ON_NULL_ARG(NULL, drv);

	b0_device *dev = B0_MODULE_DEVICE(drv->module);
	B0_DEBUG(dev, "Ram Addressing: %s", mode ? "VERTICAL" : "HORIZONTAL");
	return b0_pcf8814_send_cmd(drv, 0xAA /* RAM ADDRESSING */ | (mode & 0x01));
}

b0_result_code b0_pcf8814_pixel_invert_set(b0_pcf8814 *drv, bool on)
{
	B0_ASSERT_RETURN_ON_NULL_ARG(NULL, drv);

	b0_device *dev = B0_MODULE_DEVICE(drv->module);
	B0_DEBUG(dev, "Invert Pixel: %s", on ? "ON" : "OFF");
	return b0_pcf8814_send_cmd(drv, 0xA6 /* PIXEL INVERT */ | (on ? 0x01 : 0x00));
}

b0_result_code b0_pcf8814_pixel_show_set(b0_pcf8814 *drv, bool on)
{
	B0_ASSERT_RETURN_ON_NULL_ARG(NULL, drv);

	b0_device *dev = B0_MODULE_DEVICE(drv->module);
	B0_DEBUG(dev, "Show Pixel: %s", on ? "ON" : "OFF");
	return b0_pcf8814_send_cmd(drv, 0xAE /* DISPLAY POWER */ | (on ? 0x01 : 0x00));
}

b0_result_code b0_pcf8814_pixel_all_set(b0_pcf8814 *drv, bool on)
{
	B0_ASSERT_RETURN_ON_NULL_ARG(NULL, drv);

	b0_device *dev = B0_MODULE_DEVICE(drv->module);
	B0_DEBUG(dev, "All Pixel: %s", on ? "ON" : "OFF");
	return b0_pcf8814_send_cmd(drv, 0xA4 /* PIXEL ALL */ | (on ? 0x01 : 0x00));
}
