/*
 * This file is part of libbox0.
 * Copyright (C) 2013-2016 Kuldeep Singh Dhaka <kuldeep@madresistor.com>
 *
 * libbox0 is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * libbox0 is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with libbox0.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef LIBBOX0_DRIVER_H_BRIDGE_H
#define LIBBOX0_DRIVER_H_BRIDGE_H

#include <stdint.h>
#include <stdbool.h>
#include "../../common.h"
#include "../../module/dio.h"

__BEGIN_DECLS

#if defined(__DOXYGEN__)
struct b0_hbridge {};
#endif

/**
 * @class b0_hbridge
 * @brief H Bridge (see L293D)
 */

typedef struct b0_hbridge b0_hbridge;

enum {
	/* primitve values */
	B0_HBRIDGE_EN = (1 << 0),
	B0_HBRIDGE_A1 = (1 << 1),
	B0_HBRIDGE_A2 = (1 << 2),

	/* Higher level functions */
	B0_HBRIDGE_DISABLE = 0,
	B0_HBRIDGE_FORWARD = (B0_HBRIDGE_EN | B0_HBRIDGE_A1),
	B0_HBRIDGE_BACKWARD = (B0_HBRIDGE_EN | B0_HBRIDGE_A2),
};

B0_API b0_result_code b0_hbridge_open(b0_dio *mod,
	b0_hbridge **drv, unsigned int EN, unsigned int A1, unsigned int A2);
B0_API b0_result_code b0_hbridge_close(b0_hbridge *drv);

B0_API b0_result_code b0_hbridge_set(b0_hbridge *drv, int value);

__END_DECLS

#endif
