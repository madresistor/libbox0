/*
 * This file is part of libbox0.
 * Copyright (C) 2013-2016 Kuldeep Singh Dhaka <kuldeep@madresistor.com>
 *
 * libbox0 is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * libbox0 is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with libbox0.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <stdio.h>
#include "../../libbox0-private.h"
#include "h-bridge.h"

#if !defined(__DOXYGEN__)
struct b0_hbridge {
	b0_dio *module;

	unsigned int EN, A1, A2;
};
#endif

b0_result_code b0_hbridge_open(b0_dio *mod, b0_hbridge **drv,
			unsigned int EN, unsigned int A1, unsigned int A2)
{
	b0_hbridge *drv_alloc;
	b0_device *dev;

	B0_ASSERT_RETURN_ON_NULL_ARG(NULL, mod);
	dev = B0_MODULE_DEVICE(mod);

	B0_ASSERT_RETURN_ON_NULL_ARG(dev, drv);

	B0_ASSERT_RETURN_ON_FAIL_ARG(dev, EN < mod->pin_count);
	B0_ASSERT_RETURN_ON_FAIL_ARG(dev, A1 < mod->pin_count);
	B0_ASSERT_RETURN_ON_FAIL_ARG(dev, A2 < mod->pin_count);

	unsigned int pins[3] = {EN, A1, A2};
	B0_DEBUG_RETURN_ON_FAIL_STMT(dev,
		b0_dio_multiple_dir_set(mod, pins, 3, B0_DIO_OUTPUT));
	B0_DEBUG_RETURN_ON_FAIL_STMT(dev,
		b0_dio_multiple_value_set(mod, pins, 3, B0_DIO_LOW));
	B0_DEBUG_RETURN_ON_FAIL_STMT(dev,
		b0_dio_multiple_hiz_set(mod, pins, 3, B0_DIO_DISABLE));

	/* allocate memory */
	drv_alloc = calloc(1, sizeof(*drv_alloc));
	B0_ASSERT_RETURN_ON_NULL_ALLOC(dev, drv_alloc);

	/* fill in the object */
	drv_alloc->module = mod;
	drv_alloc->EN = EN;
	drv_alloc->A1 = A1;
	drv_alloc->A2 = A2;

	/* done */
	*drv = drv_alloc;
	return B0_OK;
}

b0_result_code b0_hbridge_set(b0_hbridge *drv, int data)
{
	B0_ASSERT_RETURN_ON_NULL_ARG(NULL, drv);
	b0_dio *mod = drv->module;
	b0_device *dev = B0_MODULE_DEVICE(mod);

	unsigned int high_pins[3], low_pins[3];
	size_t high_size = 0, low_size = 0;

	if (data & B0_HBRIDGE_EN) {
		high_pins[high_size++] = drv->EN;
	} else {
		low_pins[low_size++] = drv->EN;
	}

	if (data & B0_HBRIDGE_A1) {
		high_pins[high_size++] = drv->A1;
	} else {
		low_pins[low_size++] = drv->A1;
	}

	if (data & B0_HBRIDGE_A2) {
		high_pins[high_size++] = drv->A2;
	} else {
		low_pins[low_size++] = drv->A2;
	}

	if (high_size) {
		B0_DEBUG_RETURN_ON_FAIL_STMT(dev,
			b0_dio_multiple_value_set(mod, high_pins, high_size, B0_DIO_HIGH));
	}

	if (low_size) {
		B0_DEBUG_RETURN_ON_FAIL_STMT(dev,
			b0_dio_multiple_value_set(mod, low_pins, low_size, B0_DIO_LOW));
	}

	return B0_OK;
}

b0_result_code b0_hbridge_close(b0_hbridge *drv)
{
	B0_ASSERT_RETURN_ON_NULL_ARG(NULL, drv);

	free(drv);
	return B0_OK;
}
