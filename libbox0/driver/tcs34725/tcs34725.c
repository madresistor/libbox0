/*
 * This file is part of libbox0.
 * Copyright (C) 2015, 2016 Kuldeep Singh Dhaka <kuldeep@madresistor.com>
 *
 * libbox0 is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * libbox0 is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with libbox0.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "tcs34725.h"
#include "../../libbox0-private.h"

#if !defined(__DOXYGEN__)
struct b0_tcs34725 {
	b0_i2c *module;
	uint8_t bSlaveAddress;
	b0_i2c_version version;

	uint8_t enable;
	uint8_t again;
	uint8_t atime;
};
#endif

b0_result_code b0_tcs34725_open(b0_i2c *mod, b0_tcs34725 **drv)
{
	b0_tcs34725 *drv_alloc;
	b0_device *dev;

	B0_ASSERT_RETURN_ON_NULL_ARG(NULL, mod);
	dev = B0_MODULE_DEVICE(mod);

	B0_ASSERT_RETURN_ON_NULL_ARG(dev, drv);

	drv_alloc = calloc(1, sizeof(*drv_alloc));
	B0_ASSERT_RETURN_ON_NULL_ALLOC(dev, drv_alloc);

	drv_alloc->bSlaveAddress = 0x29;
	drv_alloc->module = mod;
	/* TODO: Use the most favourable version (FM > SM)
	 *  instead of hardcoded version */
	drv_alloc->version = B0_I2C_VERSION_FM;
	drv_alloc->again = B0_TCS34725_AGAIN_GAIN_1X;
	drv_alloc->atime = 0xFF;

	B0_DEBUG_STMT(dev, b0_tcs34725_again_set(drv_alloc, drv_alloc->again));
	B0_DEBUG_STMT(dev, b0_tcs34725_atime_set(drv_alloc, drv_alloc->atime));

	B0_DEBUG_STMT(dev, b0_tcs34725_reg_write(drv_alloc, B0_TCS34725_ENABLE,
			B0_TCS34725_ENABLE_PON));

	/* Datasheet says after set-high PON,
	 *  a minimum of 2.4ms interval should passed before set-high AEN */
	B0_SLEEP_MILLISECOND(2.4);

	B0_DEBUG_STMT(dev, b0_tcs34725_reg_write(drv_alloc, B0_TCS34725_ENABLE,
			B0_TCS34725_ENABLE_PON | B0_TCS34725_ENABLE_AEN));

	/* TODO: compare ID value */

	/* TODO: identify package and load appropriate constants */

	*drv = drv_alloc;

	return B0_OK;
}

b0_result_code b0_tcs34725_close(b0_tcs34725 *drv)
{
	B0_ASSERT_RETURN_ON_NULL_ARG(NULL, drv);

	free(drv);
	return B0_OK;
}

b0_result_code b0_tcs34725_reg_write(b0_tcs34725 *drv, uint8_t reg_addr, uint8_t value)
{
	b0_device *dev;
	uint8_t data[2];

	B0_ASSERT_RETURN_ON_NULL_ARG(NULL, drv);

	dev = B0_MODULE_DEVICE(drv->module);

	data[0] = B0_TCS34725_COMMAND_CMD | B0_TCS34725_COMMAND_ADDR(reg_addr);
	data[1] = value;

	const b0_i2c_sugar_arg arg = {
		.addr = drv->bSlaveAddress,
		.version = drv->version
	};

	/* return */ B0_DEBUG_RETURN_STMT(dev, b0_i2c_master_write(
		drv->module, &arg, data, sizeof(data)));
}

b0_result_code b0_tcs34725_read(b0_tcs34725 *drv,
			double *red, double *green, double *blue, double *clear)
{
	double div;
	uint16_t raw_red, raw_green, raw_blue, raw_clear;

	B0_ASSERT_RETURN_ON_NULL_ARG(NULL, drv);
	b0_device *dev = B0_MODULE_DEVICE(drv->module);

	//const double gain_list[] = {1, 4, 16, 60};
	//gain = gain_list[drv->again & B0_TCS34725_AGAIN_GAIN_MASK];
	div = B0_TCS34725_MAX_COUNT_FROM_ATIME(drv->atime);

	B0_DEBUG_RETURN_ON_FAIL_STMT(dev,
		b0_tcs34725_raw_read(drv, &raw_red, &raw_green, &raw_blue, &raw_clear));

	if (B0_IS_NOT_NULL(red)) {
		*red = raw_red / div;
	}

	if (B0_IS_NOT_NULL(green)) {
		*green = raw_green / div;
	}

	if (B0_IS_NOT_NULL(blue)) {
		*blue = raw_blue / div;
	}

	if (B0_IS_NOT_NULL(clear)) {
		*clear = raw_clear / div;
	}

	return B0_OK;
}

b0_result_code b0_tcs34725_raw_read(b0_tcs34725 *drv,
			uint16_t *red, uint16_t *green, uint16_t *blue, uint16_t *clear)
{
	B0_ASSERT_RETURN_ON_NULL_ARG(NULL, drv);

	b0_device *dev = B0_MODULE_DEVICE(drv->module);

	/* as per datasheet, it is recommended to
	 *  read the data register {clear, red, green, blue}
	 *  using two byte i2c read transaction.
	 *
	 * status is readed to confirm that the results are valid */
#define FILL_CMD(reg) \
	(B0_TCS34725_COMMAND_CMD | B0_TCS34725_COMMAND_ADDR(reg))

	uint8_t _cmd[5] = {
		FILL_CMD(B0_TCS34725_STATUS),
		FILL_CMD(B0_TCS34725_CDATAL),
		FILL_CMD(B0_TCS34725_RDATAL),
		FILL_CMD(B0_TCS34725_GDATAL),
		FILL_CMD(B0_TCS34725_BDATAL)
	};

	uint8_t _data[9]; /* STATUS {C, R, G, B, L}DATA{L, H} */

	const b0_i2c_task tasks[] ={

#define FILL_TASK(cmd_data, data_data, data_bytes, last_flag) {			\
			.addr = drv->bSlaveAddress,									\
			.flags = B0_I2C_TASK_WRITE,									\
			.version = drv->version,									\
			.data = cmd_data,											\
			.count = 1,													\
		}, {															\
			.addr = drv->bSlaveAddress,									\
			.flags = B0_I2C_TASK_READ | last_flag,						\
			.version = drv->version,									\
			.data = data_data,											\
			.count = data_bytes											\
		}

		FILL_TASK(&_cmd[0], &_data[0], 1, 0), /* STATUS */
		FILL_TASK(&_cmd[1], &_data[1], 2, 0), /* CLEAR */
		FILL_TASK(&_cmd[2], &_data[3], 2, 0), /* RED */
		FILL_TASK(&_cmd[3], &_data[5], 2, 0), /* GREEN */
		FILL_TASK(&_cmd[4], &_data[7], 2, B0_I2C_TASK_LAST), /* BLUE */
	};

	B0_DEBUG_RETURN_ON_FAIL_STMT(dev,
		b0_i2c_master_start(drv->module, tasks, NULL, NULL));

	/* FIXME: confirm that the data values are valid using status register */

#define EXTRACT_VALUE(off) (_data[off + 1] << 8 | _data[off])
	uint16_t CDATA = EXTRACT_VALUE(1);
	uint16_t RDATA = EXTRACT_VALUE(3);
	uint16_t GDATA = EXTRACT_VALUE(5);
	uint16_t BDATA = EXTRACT_VALUE(7);

	if (B0_IS_NOT_NULL(red)) {
		*red = RDATA;
	}

	if (B0_IS_NOT_NULL(green)) {
		*green = GDATA;
	}

	if (B0_IS_NOT_NULL(blue)) {
		*blue = BDATA;
	}

	if (B0_IS_NOT_NULL(clear)) {
		*clear = CDATA;
	}

	return B0_OK;
}

b0_result_code b0_tcs34725_again_set(b0_tcs34725 *drv, uint8_t value)
{
	b0_device *dev;
	B0_ASSERT_RETURN_ON_NULL_ARG(NULL, drv);

	dev = B0_MODULE_DEVICE(drv->module);

	B0_DEBUG_STMT(dev, b0_tcs34725_reg_write(drv, B0_TCS34725_CONTROL, value));

	drv->again = value;
	return B0_OK;
}

b0_result_code b0_tcs34725_atime_set(b0_tcs34725 *drv, uint8_t value)
{
	b0_device *dev;
	B0_ASSERT_RETURN_ON_NULL_ARG(NULL, drv);

	dev = B0_MODULE_DEVICE(drv->module);

	B0_DEBUG_STMT(dev, b0_tcs34725_reg_write(drv, B0_TCS34725_ATIME, value));

	drv->atime = value;
	return B0_OK;
}
