/*
 * This file is part of libbox0.
 * Copyright (C) 2015, 2016 Kuldeep Singh Dhaka <kuldeep@madresistor.com>
 *
 * libbox0 is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * libbox0 is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with libbox0.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef LIBBOX0_DRIVER_TCS34725_H
#define LIBBOX0_DRIVER_TCS34725_H

#include "../../common.h"
#include "../../module/i2c.h"
#include <math.h>

__BEGIN_DECLS

#if defined(__DOXYGEN__)
struct b0_tcs34725 {};
#endif

typedef struct b0_tcs34725 b0_tcs34725;

/**
 * @class b0_tcs34725
 * @brief TCS34725
 */

/* Register address and defination adapted from Adafruit code.
 *   Thanks Kevin (KTOWN) Townsend!
 * Note: a few macro were added for calculation of ATIME and WTIME
 *   and minor defination changes. */

#define B0_TCS34725_ENABLE			0x00
#define B0_TCS34725_ATIME			0x01
#define B0_TCS34725_WTIME			0x03
#define B0_TCS34725_AILTL			0x04
#define B0_TCS34725_AILTH			0x05
#define B0_TCS34725_AIHTL			0x06
#define B0_TCS34725_AIHTH			0x07
#define B0_TCS34725_PERS			0x0C
#define B0_TCS34725_CONFIG			0x0D
#define B0_TCS34725_CONTROL			0x0F
#define B0_TCS34725_ID				0x12
#define B0_TCS34725_STATUS			0x13
#define B0_TCS34725_CDATAL			0x14
#define B0_TCS34725_CDATAH			0x15
#define B0_TCS34725_RDATAL			0x16
#define B0_TCS34725_RDATAH			0x17
#define B0_TCS34725_GDATAL			0x18
#define B0_TCS34725_GDATAH			0x19
#define B0_TCS34725_BDATAL			0x1A
#define B0_TCS34725_BDATAH			0x1B

#define B0_TCS34725_COMMAND_CMD					(1 << 7)
#define B0_TCS34725_COMMAND_TYPE(i)				((i & 0x3) << 5)
#define B0_TCS34725_COMMAND_TYPE_ADDR_REPEAT	B0_TCS34725_COMMAND_TYPE(0)
#define B0_TCS34725_COMMAND_TYPE_ADDR_INC		B0_TCS34725_COMMAND_TYPE(1)
#define B0_TCS34725_COMMAND_TYPE_SPECIAL		B0_TCS34725_COMMAND_TYPE(3)
#define B0_TCS34725_COMMAND_ADDR(v)				((v) & 0x1F)
#define B0_TCS34725_COMMAND_SF(v)				((v) & 0x1F)

#define B0_TCS34725_ENABLE_AIEN		(1 << 4)
#define B0_TCS34725_ENABLE_WEN		(1 << 3)
#define B0_TCS34725_ENABLE_AEN		(1 << 1)
#define B0_TCS34725_ENABLE_PON		(1 << 0)

/**
 * Calculate ATIME register value from Integeration cycles ( @a integ_cycles)
 * @param integ_cycles Integeration cycles
 * @return ATIME register value (uint8)
 * @note integeraton time = 256 - integ_cycles (where integ_cycles >= 1)
 */
#define B0_TCS34725_ATIME_FROM_INTEG_CYCLES(integ_cycles) \
	((uint8_t)(256 - B0_CLAMP(integ_cycles, 1, 256)))

/**
 * Calculate Integeration cycles from ATIME register value ( @a atime)
 * @param atime ATIME register value
 * @return Integeration cycles
 * @note integeraton time = 256 - integ_cycles (where integ_cycles >= 1)
 */
#define B0_TCS34725_INTEG_CYCLES_FROM_ATIME(atime) (256 - (atime))

/**
 * Calculate ATIME register value from integeration time (unit: seconds).
 * @param integ_time Integeration time (seconds)
 * @return ATIME register value (uint8)
 * @note ATIME = 256 − Integration Time / 2.4 ms
 */
#define B0_TCS34725_ATIME_FROM_INTEG_TIME(integ_time)						\
	((uint8_t)(256 - B0_MIN((unsigned)ceil(									\
			B0_MAX(integ_time, 2.4E-3) / 2.4E-3), 256)))

/**
 * Calculate integeration time (unit: seconds) from ATIME register value ( @a atime)
 * @param atime ATIME register value (uint8)
 * @return floating value in seconds
 * @note ATIME = 256 − Integration Time / 2.4 ms
 */
#define B0_TCS34725_INTEG_TIME_FROM_ATIME(atime)							\
		(2.4E-3 * B0_TCS34725_INTEG_CYCLES_FROM_ATIME(atime))

/**
 * Calculate ATIME register value from Max count ( @a max_count)
 * @param max_count Max count value
 * @return ATIME register value (uint8)
 * @note Max RGBC Count = (256 − ATIME) × 1024 up to a maximum of 65535.
 */
#define B0_TCS34725_ATIME_FROM_MAX_COUNT(max_count)						\
		((uint8_t)(256 - (B0_CLAMP(max_count, 1024, 65535) / 1024)))

/**
 * Calculate Max count from ATIME register value ( @a atime)
 * @param atime ATIME register value
 * @return Max count
 * @note Max Count = (256 − ATIME) × 1024 up to a maximum of 65535.
 */
#define B0_TCS34725_MAX_COUNT_FROM_ATIME(atime)								\
		B0_MIN(((256 - (atime)) * 1024), 65535)

/**
 * Calculate WTIME register value from Wait cycles ( @a wait_cycles).
 * @param wait_cycles Wait cycles.
 * @return WTIME register value
 * @note WTIME = 256 - Wait time
 * @note Datasheet use the term "WAIT TIME" instead of "wait cycles".
 *   "wait cycles" was adopted to prevent any confusion with "time"
 */
#define B0_TCS34725_WTIME_FROM_WAIT_CYCLES(wait_cycles)						\
	((uint8_t)(256 - B0_CLAMP(wait_cycles, 1, 256)))

/**
 * Calculate wait time from WTIME register value ( @a wtime)
 * @param wtime WTIME register value
 * @return wait cycles
 * @note WTIME = 256 - Wait time
 * @note Datasheet use the term "WAIT TIME" instead of "wait cycles".
 *   "wait cycles" was adopted to prevent any confusion with "time"
 */
#define B0_TCS34725_WAIT_CYCLES_FROM_WTIME(wtime) (256 - (wtime))

/**
 * Calculate time (unit: seconds)
 *  from WTIME register ( @a wtime) and CONFIG_WLONG register bit ( @a config_wlong_bit).
 * @param wtime WTIME register
 * @param config_wlong_bit CONFIG_WLONG bit (if non-zero, assumed as low else high)
 * @return time (unit: seconds)
 * @note if CONFIG_WLONG is set, time becomes 12x
 */
#define B0_TCS34725_TIME_FROM_WTIME(wtime, config_wlong_bit)				\
	(((config_wlong_bit) ? 12 : 1) *										\
		B0_TCS34725_WAIT_CYCLES_FROM_WTIME(wtime) * 2.4E-3)

#define B0_TCS34725_PERS_APERS(i)			((i) & 0xF)
#define B0_TCS34725_PERS_APERS_NONE			B0_TCS34725_PERS_APERS(0)
#define B0_TCS34725_PERS_APERS_1_CYCLE		B0_TCS34725_PERS_APERS(1)
#define B0_TCS34725_PERS_APERS_2_CYCLE		B0_TCS34725_PERS_APERS(2)
#define B0_TCS34725_PERS_APERS_3_CYCLE		B0_TCS34725_PERS_APERS(3)
#define B0_TCS34725_PERS_APERS_5_CYCLE		B0_TCS34725_PERS_APERS(4)
#define B0_TCS34725_PERS_APERS_10_CYCLE		B0_TCS34725_PERS_APERS(5)
#define B0_TCS34725_PERS_APERS_15_CYCLE		B0_TCS34725_PERS_APERS(6)
#define B0_TCS34725_PERS_APERS_20_CYCLE		B0_TCS34725_PERS_APERS(7)
#define B0_TCS34725_PERS_APERS_25_CYCLE		B0_TCS34725_PERS_APERS(8)
#define B0_TCS34725_PERS_APERS_30_CYCLE		B0_TCS34725_PERS_APERS(9)
#define B0_TCS34725_PERS_APERS_35_CYCLE		B0_TCS34725_PERS_APERS(10)
#define B0_TCS34725_PERS_APERS_40_CYCLE		B0_TCS34725_PERS_APERS(11)
#define B0_TCS34725_PERS_APERS_45_CYCLE		B0_TCS34725_PERS_APERS(12)
#define B0_TCS34725_PERS_APERS_50_CYCLE		B0_TCS34725_PERS_APERS(13)
#define B0_TCS34725_PERS_APERS_55_CYCLE		B0_TCS34725_PERS_APERS(14)
#define B0_TCS34725_PERS_APERS_60_CYCLE		B0_TCS34725_PERS_APERS(15)

#define B0_TCS34725_CONFIG_WLONG			(1 << 1)

#define B0_TCS34725_AGAIN_GAIN_MASK			(0x3)
#define B0_TCS34725_AGAIN_GAIN(i)			((i) & B0_TCS34725_AGAIN_GAIN_MASK)
#define B0_TCS34725_AGAIN_GAIN_1X			B0_TCS34725_AGAIN_GAIN(0)
#define B0_TCS34725_AGAIN_GAIN_4X			B0_TCS34725_AGAIN_GAIN(1)
#define B0_TCS34725_AGAIN_GAIN_16X			B0_TCS34725_AGAIN_GAIN(2)
#define B0_TCS34725_AGAIN_GAIN_60X			B0_TCS34725_AGAIN_GAIN(3)

#define B0_TCS34725_ID_TCS34721			0x44
#define B0_TCS34725_ID_TCS34725			0x44
#define B0_TCS34725_ID_TCS34723			0x4D
#define B0_TCS34725_ID_TCS34727			0x4D

#define B0_TCS34725_STATUS_AINT			0x10
#define B0_TCS34725_STATUS_AVALID		0x01

B0_API b0_result_code b0_tcs34725_open(b0_i2c *mod, b0_tcs34725 **drv);
B0_API b0_result_code b0_tcs34725_close(b0_tcs34725 *drv);

/* returned values are in range of 0-1 in intensity */
B0_API b0_result_code b0_tcs34725_read(b0_tcs34725 *drv,
			double *red, double *green, double *blue, double *clear);

B0_API b0_result_code b0_tcs34725_raw_read(b0_tcs34725 *drv,
			uint16_t *red, uint16_t *green, uint16_t *blue, uint16_t *clear);

B0_API b0_result_code b0_tcs34725_again_set(b0_tcs34725 *drv, uint8_t again);
B0_API b0_result_code b0_tcs34725_atime_set(b0_tcs34725 *drv, uint8_t atime);

B0_API b0_result_code b0_tcs34725_reg_write(b0_tcs34725 *drv,
			uint8_t reg_addr, uint8_t value);

__END_DECLS

#endif
