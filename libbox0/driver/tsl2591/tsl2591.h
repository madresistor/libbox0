/*
 * This file is part of libbox0.
 * Copyright (C) 2013-2016 Kuldeep Singh Dhaka <kuldeep@madresistor.com>
 *
 * libbox0 is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * libbox0 is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with libbox0.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef LIBBOX0_DRIVER_TSL2591_H
#define LIBBOX0_DRIVER_TSL2591_H

#include "../../common.h"
#include "../../module/i2c.h"

__BEGIN_DECLS

#if defined(__DOXYGEN__)
struct b0_tsl2591 {};
#endif

/**
 * @class b0_tsl2591
 * @brief TSL2591
 */

#define B0_TSL2591_ENABLE	0x00
#define B0_TSL2591_CONFIG	0x01
#define B0_TSL2591_AILTL	0x04
#define B0_TSL2591_AILTH	0x05
#define B0_TSL2591_AIHTL	0x06
#define B0_TSL2591_AIHTH	0x07
#define B0_TSL2591_NPAILTL	0x08
#define B0_TSL2591_NPAILTH	0x09
#define B0_TSL2591_NPAIHTL	0x0A
#define B0_TSL2591_NPAIHTH	0x0B
#define B0_TSL2591_PERSIST	0x0C
#define B0_TSL2591_PID		0x11
#define B0_TSL2591_ID		0x12
#define B0_TSL2591_STATUS	0x13
#define B0_TSL2591_C0DATAL	0x14
#define B0_TSL2591_C0DATAH	0x15
#define B0_TSL2591_C1DATAL	0x16
#define B0_TSL2591_C1DATAH	0x17

#define B0_TSL2591_COMMAND_CMD					(1 << 7)
#define B0_TSL2591_COMMAND_TRANSACTION_NORMAL	(1 << 5)
#define B0_TSL2591_COMMAND_TRANSACTION_SF		(3 << 5)
#define B0_TSL2591_COMMAND_ADDR(v)				(v & 0x1F)
#define B0_TSL2591_COMMAND_SF(v)				(v & 0x1F)
/* TODO: SF , problem is their is no naming convention in datasheet */

#define B0_TSL2591_ENABLE_NPIEN	(1 << 7)
#define B0_TSL2591_ENABLE_SAI	(1 << 6)
#define B0_TSL2591_ENABLE_AIEN	(1 << 4)
#define B0_TSL2591_ENABLE_AEN	(1 << 1)
#define B0_TSL2591_ENABLE_PON	(1 << 0)

#define B0_TSL2591_CONTROL 0x01
#define B0_TSL2591_CONTROL_SRESET			(1 << 7)
#define B0_TSL2591_CONTROL_AGAIN(i)			((i & 0x3) << 4)
#define B0_TSL2591_CONTROL_AGAIN_LOW		(0 << 4)
#define B0_TSL2591_CONTROL_AGAIN_MEDIUM		(1 << 4)
#define B0_TSL2591_CONTROL_AGAIN_HIGH		(2 << 4)
#define B0_TSL2591_CONTROL_AGAIN_MAXIMUM	(3 << 4)
#define B0_TSL2591_CONTROL_ATIME(i)			((i & 0x7))
#define B0_TSL2591_CONTROL_ATIME_100MS		(0)
#define B0_TSL2591_CONTROL_ATIME_200MS		(1)
#define B0_TSL2591_CONTROL_ATIME_300MS		(2)
#define B0_TSL2591_CONTROL_ATIME_400MS		(3)
#define B0_TSL2591_CONTROL_ATIME_500MS		(4)
#define B0_TSL2591_CONTROL_ATIME_600MS		(5)

#define B0_TSL2591_PERSIST_APERS(i) (i & 0x0F)

#define B0_TSL2591_STATUS_NPINTR	(1 << 5)
#define B0_TSL2591_STATUS_AINT		(1 << 4)
#define B0_TSL2591_STATUS_AVALID	(1 << 0)

/*
 * DN28 - Developing a custom Lux equation
 *
 * Second Order Lux Equation Format
 * --------------------------------
 * CPL = (ATIME_ms * AGAINx) / (DF * GA) = (ATIME_ms * AGAINx) / (DGF)
 * Lux1 = C0DATA – CoefB * C1DATA / CPL
 * Lux2 = CoefC * C0DATA – CoefD * C1DATA / CPL
 * Lux = MAX(Lux1, Lux2, 0);
 */
struct b0_tsl2591_config {
	double DGF;
	double CoefB, CoefC, CoefD;
};

typedef struct b0_tsl2591_config b0_tsl2591_config;

enum b0_tsl2591_gain {
	B0_TSL2591_GAIN_LOW = 0,
	B0_TSL2591_GAIN_MEDIUM = 1,
	B0_TSL2591_GAIN_HIGH = 2,
	B0_TSL2591_GAIN_MAX = 3,
};

typedef enum b0_tsl2591_gain b0_tsl2591_gain;

enum b0_tsl2591_integ {
	B0_TSL2591_INTEG_100MS = 0,
	B0_TSL2591_INTEG_200MS = 1,
	B0_TSL2591_INTEG_300MS = 2,
	B0_TSL2591_INTEG_400MS = 3,
	B0_TSL2591_INTEG_500MS = 4,
	B0_TSL2591_INTEG_600MS = 5
};

typedef enum b0_tsl2591_integ b0_tsl2591_integ;

typedef struct b0_tsl2591 b0_tsl2591;

B0_API b0_result_code b0_tsl2591_open(b0_i2c *mod, b0_tsl2591 **drv,
	const b0_tsl2591_config *config);
B0_API b0_result_code b0_tsl2591_close(b0_tsl2591 *drv);

B0_API b0_result_code b0_tsl2591_reg_write(b0_tsl2591 *drv, uint8_t reg_addr, uint8_t val);

B0_API b0_result_code b0_tsl2591_read(b0_tsl2591 *drv, double *value);

B0_API b0_result_code b0_tsl2591_gain_set(b0_tsl2591 *drv, b0_tsl2591_gain gain);
B0_API b0_result_code b0_tsl2591_integ_set(b0_tsl2591 *drv, b0_tsl2591_integ integ);

__END_DECLS

#endif
