/*
 * This file is part of libbox0.
 * Copyright (C) 2013-2016 Kuldeep Singh Dhaka <kuldeep@madresistor.com>
 *
 * libbox0 is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * libbox0 is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with libbox0.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "tsl2591.h"
#include "../../libbox0-private.h"
#include <math.h>

#if !defined(__DOXYGEN__)
struct b0_tsl2591 {
	b0_i2c *module;
	uint8_t bSlaveAddress;
	b0_i2c_version version;

	b0_tsl2591_config config;
	b0_tsl2591_integ integ;
	b0_tsl2591_gain gain;
};
#endif

static const b0_tsl2591_config adafruit_config = {
	.DGF = 408,
	.CoefB = 1.64,
	.CoefC = 0.59,
	.CoefD = 0.86
};

b0_result_code b0_tsl2591_open(b0_i2c *mod,
	b0_tsl2591 **drv, const b0_tsl2591_config *config)
{
	b0_tsl2591 *drv_alloc;
	b0_device *dev;

	B0_ASSERT_RETURN_ON_NULL_ARG(NULL, mod);
	dev = B0_MODULE_DEVICE(mod);

	B0_ASSERT_RETURN_ON_NULL_ARG(dev, drv);

	if (B0_IS_NULL(config)) {
		B0_DEBUG(dev, "using internal adafruit_config");
		config = &adafruit_config;
	}

	drv_alloc = calloc(1, sizeof(*drv_alloc));
	B0_ASSERT_RETURN_ON_NULL_ALLOC(dev, drv_alloc);

	drv_alloc->bSlaveAddress = 0x29;
	drv_alloc->module = mod;
	/* TODO: Use the most favourable version (FM > SM)
	 *  instead of hardcoded version */
	drv_alloc->version = B0_I2C_VERSION_FM;
	memcpy(&drv_alloc->config, config, sizeof(drv_alloc->config));
	drv_alloc->integ = B0_TSL2591_INTEG_100MS;
	drv_alloc->gain = B0_TSL2591_GAIN_MEDIUM;

	/* update CONTROL */
	uint8_t reg = B0_TSL2591_CONTROL;
	uint8_t val = B0_TSL2591_CONTROL_AGAIN(drv_alloc->gain) |
		B0_TSL2591_CONTROL_ATIME(drv_alloc->integ);
	B0_DEBUG_STMT(dev, b0_tsl2591_reg_write(drv_alloc, reg, val));

	/* TODO: compare ID value */

	/* TODO: identify package and load appropriate constants */

	*drv = drv_alloc;

	return B0_OK;
}

b0_result_code b0_tsl2591_close(b0_tsl2591 *drv)
{
	B0_ASSERT_RETURN_ON_NULL_ARG(NULL, drv);

	free(drv);
	return B0_OK;
}

b0_result_code b0_tsl2591_reg_write(b0_tsl2591 *drv, uint8_t reg_addr, uint8_t value)
{
	b0_device *dev;
	uint8_t data[2];

	B0_ASSERT_RETURN_ON_NULL_ARG(NULL, drv);

	dev = B0_MODULE_DEVICE(drv->module);

	data[0] = B0_TSL2591_COMMAND_CMD;
	data[0] |= B0_TSL2591_COMMAND_TRANSACTION_NORMAL;
	data[0] |= B0_TSL2591_COMMAND_ADDR(reg_addr);
	data[1] = value;

	const b0_i2c_sugar_arg arg = {
		.addr = drv->bSlaveAddress,
		.version = drv->version
	};

	/* return */ B0_DEBUG_RETURN_STMT(dev, b0_i2c_master_write(
		drv->module, &arg, data, sizeof(data)));
}

/* value is in Lux */
b0_result_code b0_tsl2591_read(b0_tsl2591 *drv, double *value)
{
	B0_ASSERT_RETURN_ON_NULL_ARG(NULL, drv);

	b0_device *dev = B0_MODULE_DEVICE(drv->module);

	//why enable B0_TSL2591_ENABLE_AEN | B0_TSL2591_ENABLE_AIEN ?
	uint8_t reg = B0_TSL2591_ENABLE;
	uint8_t val = B0_TSL2591_ENABLE_PON | B0_TSL2591_ENABLE_AEN;
	B0_DEBUG_RETURN_ON_FAIL_STMT(dev,
		b0_tsl2591_reg_write(drv, reg, val));

	B0_SLEEP_MILLISECOND(120 * (1 + drv->integ));

	uint8_t cmd = B0_TSL2591_COMMAND_CMD |
					B0_TSL2591_COMMAND_TRANSACTION_NORMAL |
					B0_TSL2591_COMMAND_ADDR(B0_TSL2591_C0DATAL);

	uint8_t data[4];

	const b0_i2c_task tasks[] = {{
		.addr = drv->bSlaveAddress,
		.flags = B0_I2C_TASK_WRITE,
		.version = drv->version,
		.data = &cmd,
		.count = 1
	}, {
		.addr = drv->bSlaveAddress,
		.version = drv->version,
		.flags = B0_I2C_TASK_READ | B0_I2C_TASK_LAST,
		.data = data,
		.count = 4
	}};

	B0_DEBUG_RETURN_ON_FAIL_STMT(dev,
		b0_i2c_master_start(drv->module, tasks, NULL, NULL));

	uint16_t data0, data1;

	data0 = (data[1] << 8) | data[0];
	data1 = (data[3] << 8) | data[2];

	//~ B0_DEBUG(dev, "DATA0: %"PRIu16", DATA1: %"PRIu16, data0, data1);

	if (data0 == 0xFFFF) {
		B0_DEBUG(dev, "CH0 (Visible) data register overflow");
		*value = NAN;
		return B0_OK;
	} else if (data1 == 0xFFFF) {
		B0_DEBUG(dev, "CH1 (IR) data register overflow");
		*value = NAN;
		return B0_OK;
	}

	double C0DATA = data0, C1DATA = data1;
	double ATIME_ms = (1 + drv->integ) * 100;
	double gains[] = {1, 25, 428, 9876};
	double AGAINx = gains[drv->gain  & 0x03];

	double DGF = drv->config.DGF;
	double CoefB = drv->config.CoefB;
	double CoefC = drv->config.CoefC;
	double CoefD = drv->config.CoefD;

	double CPL = (ATIME_ms * AGAINx) / DGF;
	double Lux1 = (C0DATA - (CoefB * C1DATA)) / CPL;
	double Lux2 = ((CoefC * C0DATA) - (CoefD * C1DATA)) / CPL;
	double Lux = fmax(fmax(Lux1, Lux2), 0);

	/* algorithm to calculate the type of source
	double RATIO = C1DATA / C0DATA;
	double IRF = (1 - (2*RATIO));
	B0_DEBUG(dev, "RATIO: %f, IRF: %f", RATIO, IRF);

	B0_DEBUG(dev, "CPL: %f, Lux1: %f, Lux2: %f", CPL, Lux1, Lux2);
	*/

	*value = Lux;
	return B0_OK;
}

b0_result_code b0_tsl2591_gain_set(b0_tsl2591 *drv, b0_tsl2591_gain gain)
{
	b0_device *dev;
	B0_ASSERT_RETURN_ON_NULL_ARG(NULL, drv);

	dev = B0_MODULE_DEVICE(drv->module);

	uint8_t reg = B0_TSL2591_CONTROL;
	uint8_t val = B0_TSL2591_CONTROL_AGAIN(gain) |
		B0_TSL2591_CONTROL_ATIME(drv->integ);
	B0_DEBUG_STMT(dev, b0_tsl2591_reg_write(drv, reg, val));

	drv->gain = gain;
	return B0_OK;
}

b0_result_code b0_tsl2591_integ_set(b0_tsl2591 *drv, b0_tsl2591_integ integ)
{
	b0_device *dev;
	B0_ASSERT_RETURN_ON_NULL_ARG(NULL, drv);

	dev = B0_MODULE_DEVICE(drv->module);

	uint8_t reg = B0_TSL2591_CONTROL;
	uint8_t val = B0_TSL2591_CONTROL_AGAIN(drv->gain) |
		B0_TSL2591_CONTROL_ATIME(integ);
	B0_DEBUG_STMT(dev, b0_tsl2591_reg_write(drv, reg, val));

	drv->integ = integ;
	return B0_OK;
}
