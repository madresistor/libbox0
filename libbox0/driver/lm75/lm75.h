/*
 * This file is part of libbox0.
 * Copyright (C) 2013-2016 Kuldeep Singh Dhaka <kuldeep@madresistor.com>
 *
 * libbox0 is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * libbox0 is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with libbox0.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef LIBBOX0_DRIVER_LM75_H
#define LIBBOX0_DRIVER_LM75_H

#include <stdint.h>
#include <stdbool.h>
#include "../../common.h"
#include "../../module/i2c.h"

__BEGIN_DECLS

#if defined(__DOXYGEN__)
struct b0_lm75 {};
#endif

/**
 * @class b0_lm75
 * @brief LM75 Digital Temperature Sensor
 * @details tested with
 *   AT30TS75A: Digital Temperature Sensor and
 *    Thermal Watchdog with 2-Wire Interface
 */

typedef struct b0_lm75 b0_lm75;

/**
 * @brief allocate a LM75 driver
 * @param[in] mod libbox0 I2C module
 * @param[out] drv pointer to store LM75 driver object
 * @param[in] A2 LM75 pin A2
 * @param[in] A1 LM75 pin A1
 * @param[in] A0 LM75 pin A0
 * @return result code
 * @note A2, A1, A0 is used to configuring device address (refer datasheet)
 * @see b0_lm75_close()
 * @see b0_lm75_read()
 */
B0_API b0_result_code b0_lm75_open(b0_i2c *mod, b0_lm75 **drv, bool A2, bool A1, bool A0);

/**
 * @brief free a LM75 driver
 * @param[in] drv libbox0 LM75 driver
 * @return result code
 * @see b0_lm75_open()
 */
B0_API b0_result_code b0_lm75_close(b0_lm75 *drv);

/**
 * @brief read temperature
 * @details Read signed temperature value from LM75
 * @param[in] drv libbox0 LM75 driver
 * @param[out] val pointer to memory to store the value
 * @return result code
 * @see b0_lm75_read16()
 * @see b0_lm75_open()
 */
B0_API b0_result_code b0_lm75_read(b0_lm75 *drv, double *val);

/**
 * @brief read 16bit value
 * @details read 16bit value from LM75 and converted to double
 * 			this is an extenstion to LM75 and is not supported by all.
 * 			some might support 12 bit, but this function should be valid
 * 			as I2C is 8bit oriented. refer to datasheet to make sure that
 * 			the remaining bits are zero.
 * @param[in] drv libbox0 LM75 driver
 * @param[out] val memory location to store 16 bit value to double
 * @return result code
 * @see b0_lm75_read()
 * @see b0_lm75_open()
 */
B0_API b0_result_code b0_lm75_read16(b0_lm75 *drv, double *val);

__END_DECLS

#endif
