/*
 * This file is part of libbox0.
 * Copyright (C) 2013-2016 Kuldeep Singh Dhaka <kuldeep@madresistor.com>
 *
 * libbox0 is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * libbox0 is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with libbox0.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <stdio.h>
#include "../../libbox0-private.h"
#include "lm75.h"
#include "../../misc/conv.h"

#if !defined(__DOXYGEN__)
struct b0_lm75 {
	b0_i2c *module;
	uint8_t bSlaveAddress;
	b0_i2c_version version;
};
#endif

b0_result_code b0_lm75_open(b0_i2c *mod, b0_lm75 **drv, bool A2, bool A1, bool A0)
{
	b0_lm75 *drv_alloc;
	b0_device *dev;

	B0_ASSERT_RETURN_ON_NULL_ARG(NULL, mod);
	dev = B0_MODULE_DEVICE(mod);

	B0_ASSERT_RETURN_ON_NULL_ARG(dev, drv);

	/*
	 * Allocate driver
	 */
	drv_alloc = calloc(1, sizeof(*drv_alloc));
	B0_ASSERT_RETURN_ON_NULL_ALLOC(dev, drv_alloc);

	drv_alloc->module = mod;
	drv_alloc->bSlaveAddress = 0x48 | (A2 ? 0x04 : 0x00) | (A1 ? 0x02 : 0x00) | (A0 ? 0x01 : 0x00);
	/* TODO: Use the most favourable version (HS > FM > SM)
	 *  instead of hardcoded version */
	drv_alloc->version = B0_I2C_VERSION_FM;

	/* TODO: test if slave connected */

	*drv = drv_alloc;
	return B0_OK;
}

b0_result_code b0_lm75_close(b0_lm75 *drv)
{
	B0_ASSERT_RETURN_ON_NULL_ARG(NULL, drv);

	free(drv);
	return B0_OK;
}

b0_result_code b0_lm75_read(b0_lm75 *drv, double *val)
{
	b0_device *dev;
	int16_t _val;

	B0_ASSERT_RETURN_ON_NULL_ARG(NULL, drv);
	dev = B0_MODULE_DEVICE(drv->module);

	B0_ASSERT_RETURN_ON_NULL_ARG(dev, val);

	const b0_i2c_sugar_arg arg = {
		.addr = drv->bSlaveAddress,
		.version = drv->version
	};

	B0_DEBUG_RETURN_ON_FAIL_STMT(dev, b0_i2c_master_read(drv->module, &arg,
		(uint8_t *)&_val, sizeof(_val)));

	*val = _val;

	return B0_OK;
}

b0_result_code b0_lm75_read16(b0_lm75 *drv, double *val)
{
	uint8_t data[2];
	b0_device *dev;

	B0_ASSERT_RETURN_ON_NULL_ARG(NULL, drv);
	dev = B0_MODULE_DEVICE(drv->module);

	B0_ASSERT_RETURN_ON_NULL_ARG(dev, val);

	const b0_i2c_sugar_arg arg = {
		.addr = drv->bSlaveAddress,
		.version = drv->version
	};

	B0_DEBUG_RETURN_ON_FAIL_STMT(dev, b0_i2c_master_read(drv->module, &arg,
		data, sizeof(data)));

	int16_t raw_val = (data[0] << 8) | data[1];
	double temp /* celsius */ = raw_val / 256.0;

	*val = B0_CELSIUS_TO_KELVIN(temp);

	return B0_OK;
}
