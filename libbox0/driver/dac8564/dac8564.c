/*
 * This file is part of libbox0.
 * Copyright (C) 2016 Kuldeep Singh Dhaka <kuldeep@madresistor.com>
 *
 * libbox0 is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * libbox0 is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with libbox0.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "dac8564.h"
#include "../../libbox0-private.h"

#define __CONFIG (B0_SPI_TASK_MODE0 | B0_SPI_TASK_MSB_FIRST) /* HD */
#define __BITSIZE 8
#define __SPEED B0_SPI_TASK_SPEED_USE_FALLBACK

#if !defined(__DOXYGEN__)
struct b0_dac8564 {
	b0_spi *module;
	unsigned addr;
	uint8_t addr_sel;
};
#endif

b0_result_code b0_dac8564_open(b0_spi *mod, b0_dac8564 **drv, unsigned addr,
	bool A1, bool A0)
{
	b0_dac8564 *drv_alloc;
	b0_device *dev;

	B0_ASSERT_RETURN_ON_NULL_ARG(NULL, mod);
	dev = B0_MODULE_DEVICE(mod);

	B0_ASSERT_RETURN_ON_NULL_ARG(dev, drv);

	drv_alloc = calloc(1, sizeof(*drv_alloc));
	B0_ASSERT_RETURN_ON_NULL_ALLOC(dev, drv_alloc);

	drv_alloc->module = mod;
	drv_alloc->addr = addr;
	drv_alloc->addr_sel = 0x00;
	drv_alloc->addr_sel |= A1 ? B0_DAC8564_A1 : 0x00;
	drv_alloc->addr_sel |= A0 ? B0_DAC8564_A0 : 0x00;

	*drv = drv_alloc;

	return B0_OK;
}

b0_result_code b0_dac8564_close(b0_dac8564 *drv)
{
	B0_ASSERT_RETURN_ON_NULL_ARG(NULL, drv);

	free(drv);
	return B0_OK;
}

static b0_result_code perform_op(b0_dac8564 *drv, uint8_t cmd, uint16_t data)
{
	b0_device *dev = B0_MODULE_DEVICE(drv->module);

	b0_spi_sugar_arg arg = {
		.addr = drv->addr,
		.flags = __CONFIG,
		.bitsize = __BITSIZE,
		.speed = __SPEED
	};

	uint8_t write[3] = {cmd, (data >> 8) & 0xFF, data & 0xFF};
	B0_DEBUG_RETURN_STMT(dev, b0_spi_master_hd_write(drv->module, &arg, write, 3));
}

b0_result_code b0_dac8564_write(b0_dac8564 *drv, uint8_t ch, uint16_t value)
{
	uint8_t cmd;
	B0_ASSERT_RETURN_ON_NULL_ARG(NULL, drv);

	cmd = drv->addr_sel | B0_DAC8564_LD0 | ((ch & 0x3) << 1);
	return perform_op(drv, cmd, value);
}
