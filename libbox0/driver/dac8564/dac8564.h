/*
 * This file is part of libbox0.
 * Copyright (C) 2016 Kuldeep Singh Dhaka <kuldeep@madresistor.com>
 *
 * libbox0 is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * libbox0 is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with libbox0.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef LIBBOX0_DRIVER_DAC8564_H
#define LIBBOX0_DRIVER_DAC8564_H

#include "../../common.h"
#include "../../module/spi.h"

__BEGIN_DECLS

#if defined(__DOXYGEN__)
struct b0_dac8564 {};
#endif

typedef struct b0_dac8564 b0_dac8564;

/**
 * @class b0_dac8564
 * @brief DAC8564
 */

/* note: command format, 8bit */
#define B0_DAC8564_A1 (1 << 7)
#define B0_DAC8564_A0 (1 << 6)
#define B0_DAC8564_LD1 (1 << 5)
#define B0_DAC8564_LD0 (1 << 4)
#define B0_DAC8564_DACSEL1 (1 << 2)
#define B0_DAC8564_DACSEL0 (1<< 1)
#define B0_DAC8564_PD0 (1 << 0)

/* note: data format, 16bit */
#define B0_DAC8564_PD_1KOHM_TO_GND (0x1 << 14)
#define B0_DAC8564_PD_100KOHM_TO_GND (0x2 << 14)
#define B0_DAC8564_PD_HIGH_IMPEDANCE (0x3 << 14)

/* note: data format, 16bit */
#define B0_DAC8564_REF_PD_ALWAYS (0x1 << 13)
#define B0_DAC8564_REF_PU_DEFAULT (0x0)
#define B0_DAC8564_REF_PU_ALWAYS (0x1 << 12)

/**
 * @param mod SPI Module
 * @param drv Pointer to where the object to be stored
 * @param addr slave select pin
 * @param A1 DAC8564 physical pin "A1"
 * @param A0 DAC8564 physical pin "A0"
 */
B0_API b0_result_code b0_dac8564_open(b0_spi *mod, b0_dac8564 **drv,
	unsigned addr, bool A1, bool A0);

B0_API b0_result_code b0_dac8564_close(b0_dac8564 *drv);

/**
 * Write and load to buffer with data ( @a value)
 * @param drv Driver
 * @param buf Buffer (0 = A, 1 = B, 2 = C, 3 = D)
 * @param value Value to be written
 */
B0_API b0_result_code b0_dac8564_write(b0_dac8564 *drv, uint8_t buf, uint16_t value);

__END_DECLS

#endif
