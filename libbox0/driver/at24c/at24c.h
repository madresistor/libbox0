/*
 * This file is part of libbox0.
 * Copyright (C) 2013-2016 Kuldeep Singh Dhaka <kuldeep@madresistor.com>
 *
 * libbox0 is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * libbox0 is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with libbox0.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef LIBBOX0_DRIVER_AT24C_H
#define LIBBOX0_DRIVER_AT24C_H

#include <stdint.h>
#include "../../common.h"
#include "../../module/i2c.h"

__BEGIN_DECLS

#if defined(__DOXYGEN__)
struct b0_at24c {};
#endif

/**
 * @class b0_at24c
 * @brief AT24C: I2C EEPROM
 */

typedef struct b0_at24c b0_at24c;

B0_API b0_result_code b0_at24c_open(b0_i2c *mod,
	b0_at24c **drv, bool A2, bool A1, bool A0);
B0_API b0_result_code b0_at24c_close(b0_at24c *drv);
B0_API b0_result_code b0_at24c_read(b0_at24c *drv,
	uint8_t word_address, uint8_t *data, uint8_t len);
B0_API b0_result_code b0_at24c_write(b0_at24c *drv,
	uint8_t word_address, uint8_t *data, uint8_t len);

__END_DECLS

#endif
