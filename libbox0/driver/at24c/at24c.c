/*
 * This file is part of libbox0.
 * Copyright (C) 2013-2016 Kuldeep Singh Dhaka <kuldeep@madresistor.com>
 *
 * libbox0 is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * libbox0 is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with libbox0.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "at24c.h"
#include <stdlib.h>
#include "../../libbox0-private.h"

#if !defined(__DOXYGEN__)
struct b0_at24c {
	b0_i2c *module;
	uint8_t bSlaveAddress;
	b0_i2c_version version;
};
#endif

b0_result_code b0_at24c_open(b0_i2c *mod, b0_at24c **drv, bool A2, bool A1, bool A0)
{
	b0_at24c *drv_alloc;
	b0_device *dev;

	B0_ASSERT_RETURN_ON_NULL_ARG(NULL, mod);
	dev = B0_MODULE_DEVICE(mod);

	B0_ASSERT_RETURN_ON_NULL_ARG(dev, drv);

	/*
	 * Allocate driver
	 */
	drv_alloc = calloc(1, sizeof(*drv_alloc));
	B0_ASSERT_RETURN_ON_NULL_ALLOC(dev, drv_alloc);

	drv_alloc->module = mod;
	drv_alloc->bSlaveAddress = 0x50 | (A2 ? 0x04 : 0x00) |
		(A1 ? 0x02 : 0x00) | (A0 ? 0x01 : 0x00);
	/* TODO: Use the most favourable version (FM > SM)
	 *  instead of hardcoded version */
	drv_alloc->version = B0_I2C_VERSION_FM;

	/* TODO: test if slave connected */

	*drv = drv_alloc;
	return B0_OK;
}

b0_result_code b0_at24c_close(b0_at24c *drv)
{
	B0_ASSERT_RETURN_ON_NULL_ARG(NULL, drv);

	free(drv);
	return B0_OK;
}

b0_result_code b0_at24c_read(b0_at24c *drv, uint8_t word_address, uint8_t *data, uint8_t len)
{
	const b0_i2c_sugar_arg arg = {
		.addr = drv->bSlaveAddress,
		.version = drv->version
	};

	return b0_i2c_master_write8_read(drv->module, &arg, word_address, data, len);
}

/* len should be > 0 and <= 8 */
b0_result_code b0_at24c_write(b0_at24c *drv, uint8_t word_address, uint8_t *data, uint8_t len)
{
	uint8_t _data[9] = {word_address};

	B0_ASSERT_RETURN_ON_NULL_ARG(NULL, drv);
	b0_device *dev = B0_MODULE_DEVICE(drv->module);

	B0_ASSERT_RETURN_ON_NULL_ARG(dev, data);
	B0_ASSERT_RETURN_ON_FAIL_ARG(dev, len > 0 && len <= 8);

	const b0_i2c_sugar_arg arg = {
		.addr = drv->bSlaveAddress,
		.version = drv->version
	};

	memcpy(&_data[1], data, len);
	return b0_i2c_master_write(drv->module, &arg, _data, len + 1);
}
