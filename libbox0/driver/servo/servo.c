/*
 * This file is part of libbox0.
 * Copyright (C) 2013-2016 Kuldeep Singh Dhaka <kuldeep@madresistor.com>
 *
 * libbox0 is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * libbox0 is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with libbox0.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <string.h>
#include "servo.h"
#include "../../libbox0-private.h"

#if !defined(__DOXYGEN__)
struct b0_servo {
	b0_pwm *module;
	b0_servo_config config;
	unsigned int ch;
	unsigned int bitsize;
};
#endif

static const b0_servo_config STANDARD_CONFIG = {
	.min = {
		.angle = -M_PI_2,
		.duty_cycle = 5,
	},

	.max = {
		.angle = +M_PI_2,
		.duty_cycle = 10,
	},

	.period = 20.0 / 1000,
	.calc_error = B0_SERVO_CALC_ERROR
};

b0_result_code b0_servo_open(b0_pwm *mod, b0_servo **drv,
			const b0_servo_config *config, unsigned int ch,
			unsigned int bitsize)
{
	b0_device *dev;
	b0_servo *drv_alloc;

	B0_ASSERT_RETURN_ON_NULL_ARG(NULL, mod);
	dev = B0_MODULE_DEVICE(mod);

	B0_ASSERT_RETURN_ON_NULL_ARG(dev, drv);
	B0_ASSERT_RETURN_ON_FAIL_ARG(dev, bitsize > 0);

	if (B0_IS_NULL(config)) {
		B0_DEBUG(dev, "using standard servo configuration");
		config = &STANDARD_CONFIG;
	}

	/* allocate memory */
	drv_alloc = malloc(sizeof(*drv_alloc));
	B0_ASSERT_RETURN_ON_NULL_ALLOC(dev, drv_alloc);

	/* fill in the object */
	drv_alloc->ch = ch;
	drv_alloc->module = mod;
	memcpy(&drv_alloc->config, config, sizeof(drv_alloc->config));
	drv_alloc->bitsize = bitsize;

	if (B0_ERROR_RESULT_CODE(b0_servo_goto(drv_alloc, 0))) {
		B0_WARN(dev, "Unable to set position of minimum");
	}

	if (B0_ERROR_RESULT_CODE(b0_pwm_output_start(drv_alloc->module))) {
		B0_WARN(dev, "Unable to start PWM");
	}

	*drv = drv_alloc;

	/* done */
	return B0_OK;
}

b0_result_code b0_servo_close(b0_servo *drv)
{
	b0_device *dev;

	B0_ASSERT_RETURN_ON_NULL_ARG(NULL, drv);
	dev = B0_MODULE_DEVICE(drv->module);

	if (B0_ERROR_RESULT_CODE(b0_pwm_output_stop(drv->module))) {
		B0_WARN(dev, "Unable to stop PWM");
	}

	free(drv);
	return B0_OK;
}

b0_result_code b0_servo_goto_max(b0_servo *drv)
{
	B0_ASSERT_RETURN_ON_NULL_ARG(NULL, drv);

	return b0_servo_goto(drv, drv->config.max.angle);
}

b0_result_code b0_servo_goto_min(b0_servo *drv)
{
	B0_ASSERT_RETURN_ON_NULL_ARG(NULL, drv);

	return b0_servo_goto(drv, drv->config.min.angle);
}

static double calc_duty_cycle(double angle, const b0_servo_config *config)
{
	double duty_cycle;
	double duty_cycle_per_radian;

	/* calculate duty cycle from angle */
	duty_cycle_per_radian = (config->max.duty_cycle - config->min.duty_cycle) /
			(config->max.angle - config->min.angle);

	duty_cycle = (duty_cycle_per_radian *
		(angle - config->min.angle)) + config->min.duty_cycle;

	return duty_cycle;
}

b0_result_code b0_servo_goto(b0_servo *drv, double angle)
{
	b0_device *dev;

	B0_ASSERT_RETURN_ON_NULL_ARG(NULL, drv);
	dev = B0_MODULE_DEVICE(drv->module);

	/* test angle */
	B0_ASSERT_RETURN_ON_FAIL_ARG(dev, angle >= drv->config.min.angle);
	B0_ASSERT_RETURN_ON_FAIL_ARG(dev, angle <= drv->config.max.angle);

	double freq = 1.0 / drv->config.period;
	double max_error = drv->config.calc_error;
	unsigned long speed;
	b0_pwm_reg period;
	B0_DEBUG_RETURN_ON_FAIL_STMT(dev, b0_pwm_output_calc(drv->module,
			drv->bitsize, freq, &speed, &period, max_error, true));

	double duty_cycle = calc_duty_cycle(angle, &drv->config);
	b0_pwm_reg width = B0_PWM_OUTPUT_CALC_WIDTH(period, duty_cycle);

	/* set value on device */
	B0_DEBUG_RETURN_ON_FAIL_STMT(dev, b0_pwm_period_set(drv->module, period));
	B0_DEBUG_RETURN_ON_FAIL_STMT(dev, b0_pwm_width_set(drv->module, drv->ch, width));
	B0_DEBUG_RETURN_ON_FAIL_STMT(dev, b0_pwm_speed_set(drv->module, speed));

	B0_DEBUG_RETURN_STMT(dev, b0_pwm_output_start(drv->module));
}
