/*
 * This file is part of libbox0.
 * Copyright (C) 2013-2016 Kuldeep Singh Dhaka <kuldeep@madresistor.com>
 *
 * libbox0 is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * libbox0 is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with libbox0.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef LIBBOX0_DRIVER_SERVO_H
#define LIBBOX0_DRIVER_SERVO_H

#include "../../common.h"
#include "../../module/pwm.h"

__BEGIN_DECLS

#if defined(__DOXYGEN__)
struct b0_servo {};
#endif

/**
 * @class b0_servo
 * @brief Servo
 * @implements b0_driver
 */

/* atmost acceptable is .1% error */
#define B0_SERVO_CALC_ERROR (0.1)

/* if any servo has any deviation from standard config,
 * pass the config using these
 */
struct b0_servo_config {
	struct {
		double angle;
		double duty_cycle;
	} max, min;

	double period;
	double calc_error;
};

typedef struct b0_servo_config b0_servo_config;
typedef struct b0_servo b0_servo;

/**
 * @brief allocate Servo object using PWM module + configuration.
 * @details
 *  it is the responsibility of user to pass configuration as per servo
 *  could be useful for servo that have more angle or deviate from standard
 *
 * Standard servo configuration:
 * =============================
 * 	Period: 20mS
 * 	Angle:
 * 		-PI/2 (-90 degree): 5% ontime
 * 		+PI/2 (+90 degree): 10% ontime
 *
 * @note Error is *not* reported if unable to start PWM
 * @pre it is the resposibility of user to b0_module_load()
 *  the PWM module before allocating the Servo object
 *
 * @param[in] mod PWM module
 * @param[out] drv Servo object final destination
 * @param[in] config pointer to configuration. pass NULL to use standard
 * @param ch PWM Channel
 * @param bitsize Bitsize
 * @return result code
 * @memberof b0_servo
 */
B0_API b0_result_code b0_servo_open(b0_pwm *mod, b0_servo **drv,
						const b0_servo_config *config, unsigned int ch,
						unsigned int bitsize);

/**
 * @brief free Servo object
 * it will try to stop the PWM, leading to servo turn off.
 * it is the responsibilty of user to unload the PWM module that was used for Servo object
 * @param[in] drv Servo to be freed
 * @return result code
 * @note Error is *not* reported if unable to stop PWM
 */
B0_API b0_result_code b0_servo_close(b0_servo *drv);

/**
 * @brief goto angle
 * @param[in] drv Servo
 * @param[in] angle angle (in Radian)
 *  must be greather than minimum angle and \n
 *  smaller than maximun angle
 * @return result code
 * @see b0_pwm_set_by_error()
 */
B0_API b0_result_code b0_servo_goto(b0_servo *drv, double angle);

/**
 * @brief goto maximum angle
 * @param[in] drv Servo
 * @return result code
 * @see b0_servo_goto()
 * @see b0_servo_goto_min()
 */
B0_API b0_result_code b0_servo_goto_max(b0_servo *drv);

/**
 * @brief goto minimum angle
 * @param[in] drv Servo
 * @return result code
 * @see b0_servo_goto()
 * @see b0_servo_goto_max()
 */
B0_API b0_result_code b0_servo_goto_min(b0_servo *drv);

__END_DECLS

#endif
