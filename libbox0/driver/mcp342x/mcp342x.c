/*
 * This file is part of libbox0.
 * Copyright (C) 2013-2016 Kuldeep Singh Dhaka <kuldeep@madresistor.com>
 *
 * libbox0 is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * libbox0 is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with libbox0.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "mcp342x.h"
#include "../../libbox0-private.h"

#if !defined(__DOXYGEN__)
struct b0_mcp342x {
	b0_i2c *module;
	uint8_t bSlaveAddress;
	b0_i2c_version version;

	/* cached config value */
	b0_mcp342x_chan chan;
	b0_mcp342x_gain gain;
	b0_mcp342x_samp_rate samp_rate;
};
#endif

static const uint8_t address_map[3 /* Adr1 */ ][3 /* Adr0 */] = {
	{0x68, 0x6B, 0x6C},
	{0x69, 0x68, 0x6D},
	{0x6A, 0x6F, 0x6E}
};

static b0_result_code b0_mcp342x_config_sync(b0_mcp342x *drv)
{
	b0_device *dev = B0_MODULE_DEVICE(drv->module);
	uint8_t data = 0;
	data |= (drv->chan & 0x3) << 5;
	data |= (1 << 4);
	data |= (drv->samp_rate & 0x3) << 2;
	data |= (drv->gain & 0x3);

	const b0_i2c_sugar_arg arg = {
		.addr = drv->bSlaveAddress,
		.version = drv->version
	};

	/* return */ B0_DEBUG_RETURN_STMT(dev, b0_i2c_master_write(
		drv->module, &arg, &data, 1));
}

b0_result_code b0_mcp342x_open(b0_i2c *mod, b0_mcp342x **drv,
	b0_mcp342x_adr Adr1, b0_mcp342x_adr Adr0)
{
	b0_device *dev;
	b0_mcp342x *drv_alloc;

	B0_ASSERT_RETURN_ON_NULL_ARG(NULL, mod);
	dev = B0_MODULE_DEVICE(mod);

	B0_ASSERT_RETURN_ON_NULL_ARG(dev, drv);
	B0_ASSERT_RETURN_ON_FAIL_ARG(dev, (Adr0 < 3) && (Adr0 >= 0));
	B0_ASSERT_RETURN_ON_FAIL_ARG(dev, (Adr1 < 3) && (Adr1 >= 0));

	drv_alloc = malloc(sizeof(*drv_alloc));
	B0_ASSERT_RETURN_ON_NULL_ALLOC(dev, drv_alloc);

	drv_alloc->module = mod;
	drv_alloc->bSlaveAddress = address_map[Adr1][Adr0];
	/* TODO: Use the most favourable version (HS > FM > SM)
	 *  instead of hardcoded version */
	drv_alloc->version = B0_I2C_VERSION_FM;
	drv_alloc->samp_rate = B0_MCP342X_SAMP_RATE_15;
	drv_alloc->chan = B0_MCP342X_CH1;
	drv_alloc->gain = B0_MCP342X_GAIN1;

	/* set device config to inital state */
	B0_DEBUG_STMT(dev, b0_mcp342x_config_sync(drv_alloc));

	*drv = drv_alloc;
	return B0_OK;
}

b0_result_code b0_mcp342x_close(b0_mcp342x *drv)
{
	B0_ASSERT_RETURN_ON_NULL_ARG(NULL, drv);

	free(drv);
	return B0_OK;
}

b0_result_code b0_mcp342x_chan_set(b0_mcp342x *drv, b0_mcp342x_chan chan)
{
	b0_device *dev;
	B0_ASSERT_RETURN_ON_NULL_ARG(NULL, drv);
	dev = B0_MODULE_DEVICE(drv->module);
	B0_ASSERT_RETURN_ON_FAIL_ARG(dev,
		(chan >= B0_MCP342X_CH1) && (chan <= B0_MCP342X_CH1));

	drv->chan = chan;
	/* return */ B0_DEBUG_RETURN_STMT(dev,
		b0_mcp342x_config_sync(drv));
}

b0_result_code b0_mcp342x_gain_set(b0_mcp342x *drv, b0_mcp342x_gain gain)
{
	b0_device *dev;
	B0_ASSERT_RETURN_ON_NULL_ARG(NULL, drv);
	dev = B0_MODULE_DEVICE(drv->module);
	B0_ASSERT_RETURN_ON_FAIL_ARG(dev,
		(gain >= B0_MCP342X_GAIN1) && (gain <= B0_MCP342X_GAIN8));

	drv->gain = gain;
	/* return */ B0_DEBUG_RETURN_STMT(dev,
		b0_mcp342x_config_sync(drv));
}

b0_result_code b0_mcp342x_samp_rate_set(b0_mcp342x *drv, b0_mcp342x_samp_rate samp_rate)
{
	b0_device *dev;
	B0_ASSERT_RETURN_ON_NULL_ARG(NULL, drv);
	dev = B0_MODULE_DEVICE(drv->module);
	B0_ASSERT_RETURN_ON_FAIL_ARG(dev, (samp_rate >= 0) && (samp_rate < 3));

	drv->samp_rate = samp_rate;
	/* return */ B0_DEBUG_RETURN_STMT(dev,
		b0_mcp342x_config_sync(drv));
}

b0_result_code b0_mcp342x_read(b0_mcp342x *drv, double *value)
{
	uint8_t data[2];
	int16_t val;

	b0_device *dev;
	B0_ASSERT_RETURN_ON_NULL_ARG(NULL, drv);
	dev = B0_MODULE_DEVICE(drv->module);

	B0_ASSERT_RETURN_ON_NULL_ARG(dev, value);

	const b0_i2c_sugar_arg arg = {
		.addr = drv->bSlaveAddress,
		.version = drv->version
	};

	B0_DEBUG_RETURN_ON_FAIL_STMT(dev, b0_i2c_master_read(drv->module,
					&arg, data, sizeof(data)));

	static const double resolutions[] = {0.001, 0.00025, 0.0000625};
	static const double gains[] = {1, 2, 4, 8};

	val = (data[0] << 8) | data[1];

	/* TODO: send nan when adc reg overflow */

	*value = val * resolutions[drv->samp_rate] / gains[drv->gain];
	return B0_OK;
}
