/*
 * This file is part of libbox0.
 * Copyright (C) 2013-2016 Kuldeep Singh Dhaka <kuldeep@madresistor.com>
 *
 * libbox0 is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * libbox0 is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with libbox0.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef LIBBOX0_DRIVER_MCP342X_H
#define LIBBOX0_DRIVER_MCP342X_H

#include "../../common.h"
#include "../../module/i2c.h"

__BEGIN_DECLS

#if defined(__DOXYGEN__)
struct b0_mcp342x {};
#endif

/**
 * @class b0_mcp342x
 * @brief MCP342x: I2C ADC
 */

enum b0_mcp342x_adr {
	B0_MCP342X_LOW = 0,
	B0_MCP342X_FLOAT = 1,
	B0_MCP342X_HIGH = 2
};

enum b0_mcp342x_chan {
	B0_MCP342X_CH1 = 0,
	B0_MCP342X_CH2 = 1,
	B0_MCP342X_CH3 = 2,
	B0_MCP342X_CH4 = 3
};

enum b0_mcp342x_gain {
	B0_MCP342X_GAIN1 = 0,
	B0_MCP342X_GAIN2 = 1,
	B0_MCP342X_GAIN4 = 2,
	B0_MCP342X_GAIN8 = 3
};

enum b0_mcp342x_samp_rate {
	B0_MCP342X_SAMP_RATE_240 = 0,
	B0_MCP342X_SAMP_RATE_60 = 1,
	B0_MCP342X_SAMP_RATE_15 = 2
};

typedef enum b0_mcp342x_adr b0_mcp342x_adr;
typedef enum b0_mcp342x_chan b0_mcp342x_chan;
typedef enum b0_mcp342x_gain b0_mcp342x_gain;
typedef enum b0_mcp342x_samp_rate b0_mcp342x_samp_rate;

typedef struct b0_mcp342x b0_mcp342x;

B0_API b0_result_code b0_mcp342x_open(b0_i2c *mod, b0_mcp342x **drv,
	b0_mcp342x_adr Adr1, b0_mcp342x_adr Adr0);
B0_API b0_result_code b0_mcp342x_close(b0_mcp342x *drv);

B0_API b0_result_code b0_mcp342x_chan_set(b0_mcp342x *drv, b0_mcp342x_chan chan);
B0_API b0_result_code b0_mcp342x_gain_set(b0_mcp342x *drv, b0_mcp342x_gain gain);
B0_API b0_result_code b0_mcp342x_samp_rate_set(b0_mcp342x *drv,
	b0_mcp342x_samp_rate samp_rate);

B0_API b0_result_code b0_mcp342x_read(b0_mcp342x *drv, double *value);

__END_DECLS

#endif
