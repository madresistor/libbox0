/*
 * This file is part of libbox0.
 * Copyright (C) 2013-2016 Kuldeep Singh Dhaka <kuldeep@madresistor.com>
 *
 * libbox0 is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * libbox0 is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with libbox0.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef LIBBOX0_DRIVER_74HC595_H
#define LIBBOX0_DRIVER_74HC595_H

#include <stdint.h>
#include <stdbool.h>
#include "../../common.h"
#include "../../module/spi.h"

__BEGIN_DECLS

#if defined(__DOXYGEN__)
struct b0_74hc595 {};
#endif

/**
 * @class b0_74hc595
 * @brief 74HC595
 * @details
 *  8-bit serial-in, serial or parallel-out \n
 *  shift register with output latches
 */

typedef struct b0_74hc595 b0_74hc595;

/**
 * @brief allocate a 74HC595
 * @details SPI module is used as transport
 * @param[in] mod libbox0 SPI module
 * @param[out] drv memory location where to store 74HC595 object
 * @param[in] addr slave select pin on which 74HC595 is attached on SPI
 * @return result code
 * @note
 * @note Bitsize: 8 bit
 * @note Data Order: MSB first
 * @note Mode: 0
 */
B0_API b0_result_code b0_74hc595_open(b0_spi *mod, b0_74hc595 **drv,
	unsigned addr);

/**
 * @brief free a libbox0 74HC595 driver object
 * @note spi module wont be unloaded
 * @param[in] drv libbox0 74HC595 driver
 * @return result code
 */
B0_API b0_result_code b0_74hc595_close(b0_74hc595 *drv);

/**
 * @brief set data on 74HC595 register
 * @details write on 74HC595 register
 * 		write to output is done externally by connecting to dio or SS
 * @param[in] drv 74HC595 driver
 * @param[in] data data to be written
 * @return result code
 */
B0_API b0_result_code b0_74hc595_set(b0_74hc595 *drv, uint8_t data);

__END_DECLS

#endif
