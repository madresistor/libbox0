/*
 * This file is part of libbox0.
 * Copyright (C) 2013-2016 Kuldeep Singh Dhaka <kuldeep@madresistor.com>
 *
 * libbox0 is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * libbox0 is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with libbox0.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <stdio.h>
#include "../../libbox0-private.h"
#include "74hc595.h"

/* SPI mode - 0
 *  - Clock Polarity LOW
 *  - Data is sampled at Rising Edge
 *
 * bitsize 8bit
 *
 * msb first
 *
 * TODO: set slave-select pin low
 */
#define __CONFIG (B0_SPI_TASK_MODE0 | B0_SPI_TASK_MSB_FIRST) /* FD */
#define __BITSIZE 8
#define __SPEED B0_SPI_TASK_SPEED_USE_FALLBACK

#if !defined(__DOXYGEN__)
struct b0_74hc595 {
	b0_spi *module;
	unsigned addr;
};
#endif

/*
 * MR	Master Reset 					Should be controlled externally
 * SHCP Shift Register Clock Input		Should be connected to CLK
 * STCP Storage Register Clock Input	Slave Select (Note: Active Low)
 * OE	Ouptut Enable					Controlled externally or connect to GND
 * DS	Serial Data Input				Connected to MOSI
 * Q7S	Serial Data Output				Connected to MISO or unconnected
 * 		- in daisy chaining this should be connected connected to
 * 			the friend 595 DS and above statement is then valid for the last Q7S
 *
 * ---
 * SPI Mode0 8Bit
 */

b0_result_code b0_74hc595_open(b0_spi *mod, b0_74hc595 **drv, unsigned addr)
{
	b0_74hc595 *drv_alloc;
	b0_device *dev;

	B0_ASSERT_RETURN_ON_NULL_ARG(NULL, mod);
	dev = B0_MODULE_DEVICE(mod);

	B0_ASSERT_RETURN_ON_NULL_ARG(dev, drv);

	/* allocate memory */
	drv_alloc = calloc(1, sizeof(*drv_alloc));
	B0_ASSERT_RETURN_ON_NULL_ALLOC(dev, drv_alloc);

	/* fill in the object */
	drv_alloc->module = mod;
	drv_alloc->addr = addr;

	/* done */
	*drv = drv_alloc;
	return B0_OK;
}

b0_result_code b0_74hc595_set(b0_74hc595 *drv, uint8_t data)
{
	b0_device *dev;

	B0_ASSERT_RETURN_ON_NULL_ARG(NULL, drv);
	dev = B0_MODULE_DEVICE(drv->module);

	b0_spi_sugar_arg arg = {
		.addr = drv->addr,
		.flags = __CONFIG,
		.bitsize = __BITSIZE,
		.speed = __SPEED
	};

	/* return */ B0_DEBUG_RETURN_STMT(dev,
		b0_spi_master_fd(drv->module, &arg, &data, &data, 1));
}

/*
b0_result_code b0_74hc595_daisychain_set(b0_74hc595 *_74hc595, uint8_t *data, size_t data_len)
{
	b0_device *dev;
	aaaa check me 000 if (_74hc595 == NULL) {
		return B0_ERR_ARG;
	}
	context = B0_CAST2MODULE(_74hc595->module)->device->context;

	B0_ERROR(dev, "TODO: b0_74hc595_daisychain_set()");
	return B0_OK;
}
*/

b0_result_code b0_74hc595_close(b0_74hc595 *drv)
{
	B0_ASSERT_RETURN_ON_NULL_ARG(NULL, drv);

	free(drv);
	return B0_OK;
}
