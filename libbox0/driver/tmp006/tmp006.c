/*
 * This file is part of libbox0.
 * Copyright (C) 2013-2016 Kuldeep Singh Dhaka <kuldeep@madresistor.com>
 *
 * libbox0 is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * libbox0 is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with libbox0.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "tmp006.h"
#include "../../libbox0-private.h"
#include "../../misc/conv.h"

#if !defined(__DOXYGEN__)
struct b0_tmp006 {
	b0_i2c *module;
	uint8_t bSlaveAddress;
	b0_i2c_version version;
	double calib_factor;
};
#endif

#define VOBJ_SENSOR_LSB 156.25e-9

#define CONST_A1 1.75e-3
#define CONST_A2 -1.678e-5
#define CONST_TREF 298.15
#define CONST_B0 -2.94e-5
#define CONST_B1 -5.7e-7
#define CONST_B2 4.63e-9
#define CONST_C2 13.4

b0_result_code b0_tmp006_open(b0_i2c *mod, b0_tmp006 **drv,
	double calib_factor, bool ADR1, b0_tmp006_adr0 ADR0)
{
	b0_tmp006 *drv_alloc;
	b0_device *dev;

	B0_ASSERT_RETURN_ON_NULL_ARG(NULL, mod);
	dev = B0_MODULE_DEVICE(mod);

	B0_ASSERT_RETURN_ON_NULL_ARG(dev, drv);
	B0_ASSERT_RETURN_ON_FAIL_ARG(dev, isnormal(calib_factor));

	drv_alloc = calloc(1, sizeof(*drv_alloc));
	B0_ASSERT_RETURN_ON_NULL_ALLOC(dev, drv_alloc);

	drv_alloc->bSlaveAddress = 0x40 | (ADR1 ? 0x04 : 0x00) | (0x03 & ADR0);
	/* TODO: Use the most favourable version (FM > SM)
	 *  instead of hardcoded version */
	drv_alloc->version = B0_I2C_VERSION_FM;
	drv_alloc->calib_factor = calib_factor;
	drv_alloc->module = mod;

	/* TODO: detect slave */

	/* TODO: compare ID value */

	/* TODO: identify package and load appropriate constants */

	*drv = drv_alloc;

	return B0_OK;
}

b0_result_code b0_tmp006_close(b0_tmp006 *drv)
{
	B0_ASSERT_RETURN_ON_NULL_ARG(NULL, drv);

	free(drv);
	return B0_OK;
}

b0_result_code b0_tmp006_read(b0_tmp006 *drv, double *temp)
{
	b0_device *dev;
	int16_t value;
	uint8_t values[4];

	B0_ASSERT_RETURN_ON_NULL_ARG(NULL, drv);

	dev = B0_MODULE_DEVICE(drv->module);

	B0_ASSERT_RETURN_ON_NULL_ARG(dev, temp);

	const b0_i2c_sugar_arg arg = {
		.addr = drv->bSlaveAddress,
		.version = drv->version
	};

	B0_DEBUG_RETURN_ON_FAIL_STMT(dev, b0_i2c_master_write8_read(
			drv->module, &arg, B0_TMP006_VOBJECT, values, sizeof(values)));

	value = (values[0] << 8) | (values[1]);
	double Vobj /* volt */ = value * VOBJ_SENSOR_LSB;

	value = (values[2] << 8) | (values[3]);
	double Tdie /* kelvin */ = B0_CELSIUS_TO_KELVIN(value / (32.0 * 4));

	/* temp */
	double tmp_1 = Tdie - CONST_TREF;
	double tmp_2 = pow(tmp_1, 2);

	/* equation 1 */
	double S = drv->calib_factor * (1 + (CONST_A1 * tmp_1) + (CONST_A2 * tmp_2));

	/* equation 2 */
	double Vos = CONST_B0 + (CONST_B1 * tmp_1) + (CONST_B2 * tmp_2);

	/* temp */
	double tmp_3 = Vobj - Vos;
	double tmp_4 = pow(tmp_3, 2);

	/* equation 3 */
	double f_Vobj = tmp_3 + (CONST_C2 * tmp_4);

	/* temp */
	double tmp_5 = pow(Tdie, 4);
	double tmp_6 = f_Vobj / S;

	/* equation 4 */
	/* sqrt(sqrt()) is faster than pow(, 1/4)     3x */
	double Tobj = sqrt(sqrt(tmp_5 + tmp_6));

	*temp = Tobj;

	return B0_OK;
}
