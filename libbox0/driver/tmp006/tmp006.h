/*
 * This file is part of libbox0.
 * Copyright (C) 2013-2016 Kuldeep Singh Dhaka <kuldeep@madresistor.com>
 *
 * libbox0 is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * libbox0 is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with libbox0.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef LIBBOX0_DRIVER_TMP006_H
#define LIBBOX0_DRIVER_TMP006_H

#include "../../common.h"
#include "../../module/i2c.h"

__BEGIN_DECLS

#if defined(__DOXYGEN__)
struct b0_tmp006 {};
#endif

typedef struct b0_tmp006 b0_tmp006;

/* used for address */
enum b0_tmp006_adr0 {
	B0_TMP006_GND = 0,
	B0_TMP006_VCC = 1,
	B0_TMP006_SDA = 2,
	B0_TMP006_SCL = 3,
};

typedef enum b0_tmp006_adr0 b0_tmp006_adr0;

#define B0_TMP006_VOBJECT	0x00
#define B0_TMP006_TAMBIANT	0x01
#define B0_TMP006_CONFIG	0x02
#define B0_TMP006_MANUF_ID	0xFE
#define B0_TMP006_DEVICE_ID	0xFF

#define B0_TMP006_CONFIG_RST	(1 << 15)
#define B0_TMP006_CONFIG_MOD(i)	((i & 0x7) << 12)
#define B0_TMP006_CONFIG_CR(i)	((i & 0x7) << 9)
#define B0_TMP006_CONFIG_EN		(1 << 8)
#define B0_TMP006_CONFIG_nDRDY	(1 << 7)

#define B0_TMP006_CONFIG_AVG1	B0_TMP006_CONFIG_MOD(0)
#define B0_TMP006_CONFIG_AVG2	B0_TMP006_CONFIG_MOD(1)
#define B0_TMP006_CONFIG_AVG4	B0_TMP006_CONFIG_MOD(2)
#define B0_TMP006_CONFIG_AVG8	B0_TMP006_CONFIG_MOD(3)
#define B0_TMP006_CONFIG_AVG16	B0_TMP006_CONFIG_MOD(4)

#define B0_TMP006_DUMMY_CALIB_FACTOR (6.4e-14)

B0_API b0_result_code b0_tmp006_open(b0_i2c *mod, b0_tmp006 **drv,
	double calib_factor, bool ADR1, b0_tmp006_adr0 ADR0);
B0_API b0_result_code b0_tmp006_close(b0_tmp006 *drv);

B0_API b0_result_code b0_tmp006_read(b0_tmp006 *drv, double *value);

__END_DECLS

#endif
