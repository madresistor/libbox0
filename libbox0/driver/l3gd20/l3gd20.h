/*
 * This file is part of libbox0.
 * Copyright (C) 2013-2016 Kuldeep Singh Dhaka <kuldeep@madresistor.com>
 *
 * libbox0 is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * libbox0 is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with libbox0.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef LIBBOX0_DRIVER_L3GD20_H
#define LIBBOX0_DRIVER_L3GD20_H

#include <stdint.h>
#include <stdbool.h>
#include "../../common.h"
#include "../../module/i2c.h"

__BEGIN_DECLS

#if defined(__DOXYGEN__)
struct b0_l3gd20 {};
#endif

/**
 * @class b0_l3gd20
 * L3GD20: 3 Axis Angular rate sensor
 * @note Currently only I2C is supported.
 */

#define B0_L3GD20_WHO_AM_I 0x0F
#define B0_L3GD20_CTRL_REG1 0x20
#define B0_L3GD20_CTRL_REG2 0x21
#define B0_L3GD20_CTRL_REG3 0x22
#define B0_L3GD20_CTRL_REG4 0x23
#define B0_L3GD20_CTRL_REG5 0x24
#define B0_L3GD20_REFERENCE 0x25
#define B0_L3GD20_OUT_TEMP 0x26
#define B0_L3GD20_STATUS_REG 0x27
#define B0_L3GD20_OUT_X_L 0x28
#define B0_L3GD20_OUT_X_H 0x29
#define B0_L3GD20_OUT_Y_L 0x2A
#define B0_L3GD20_OUT_Y_H 0x2B
#define B0_L3GD20_OUT_Z_L 0x2C
#define B0_L3GD20_OUT_Z_H 0x2D
#define B0_L3GD20_FIFO_CTRL_REG 0x2E
#define B0_L3GD20_FIFO_SRC_REG 0x2F
#define B0_L3GD20_INT1_CFG 0x30
#define B0_L3GD20_INT1_SRC 0x31
#define B0_L3GD20_INT1_TSH_XL 0x32
#define B0_L3GD20_INT1_TSH_XH 0x33
#define B0_L3GD20_INT1_TSH_YL 0x34
#define B0_L3GD20_INT1_TSH_YH 0x35
#define B0_L3GD20_INT1_TSH_ZL 0x36
#define B0_L3GD20_INT1_TSH_ZH 0x37
#define B0_L3GD20_INT1_DURATION 0x38

/** if send as part of the sub-address,
 *   address is increment on each read */
#define B0_L3GD20_AUTO_INC 0x80

typedef struct b0_l3gd20 b0_l3gd20;

/** L3GD20 axis */
enum b0_l3gd20_axis {
	B0_L3GD20_AXIS_X = 0, /**< X axis */
	B0_L3GD20_AXIS_Y = 1, /**< Y axis */
	B0_L3GD20_AXIS_Z = 2, /**< Z axis */
};

/**
 * @brief Power up the slave
 * @details power up of slave is done by writing 0x0F to CTRL_REG1
 * @param[in] drv L3GD20
 * @return result code
 */
B0_API b0_result_code b0_l3gd20_power_up(b0_l3gd20 *drv);

/**
 * @brief Free @a L3GD20
 * @details Free the resources allocated in the @a drv.
 * @param[in] drv L3GD20
 * @return result code
 */
B0_API b0_result_code b0_l3gd20_close(b0_l3gd20 *drv);

/**
 * @brief Read all 3 Axis
 * @details All data is readed in one transaction
 * Benifits:
 * 	- Effecient
 *  - Fast
 *  - Convinent
 * Axis values in \b Degrees/Second (aka dps)
 * @pre First initalize the l3gd20 argument.
 *
 * @param[in] drv L3GD20
 * @param[out] x X axis value
 * @param[out] y Y axis value
 * @param[out] z Z axis value
 * @return result code
 */
B0_API b0_result_code b0_l3gd20_read(b0_l3gd20 *drv, double *x, double *y, double *z);

/**
 * @brief Initalize the L3GD20.
 * This will use the I2C object to communicate on with the Slave.
 * @param[in] mod The I2C bus object
 * @param[out] drv Pointer to the L3GD20.
 * @param[in] SAD0 Slave Address 0 bit.
 * @return result code
 */
B0_API b0_result_code b0_l3gd20_open_i2c(b0_i2c *mod, b0_l3gd20 **drv, bool SAD0);

__END_DECLS

#endif
