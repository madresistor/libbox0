/*
 * This file is part of libbox0.
 * Copyright (C) 2013-2016 Kuldeep Singh Dhaka <kuldeep@madresistor.com>
 *
 * libbox0 is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * libbox0 is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with libbox0.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <stdlib.h>
#include <stdint.h>
#include "../../libbox0-private.h"
#include "../../misc/conv.h"
#include "l3gd20.h"

/**
 * @file
 * @todo re-test slave output
 * @todo Support SPI mode for ST L3GD20
 */

#if !defined(__DOXYGEN__)
struct b0_l3gd20 {
	b0_i2c *module;
	uint8_t bSlaveAddress;
	b0_i2c_version version;
};
#endif

b0_result_code b0_l3gd20_open_i2c(b0_i2c *mod, b0_l3gd20 **drv, bool SAD0)
{
	b0_result_code r;
	b0_l3gd20 *drv_alloc;
	b0_device *dev;
	uint8_t address;
	b0_i2c_version version;
	uint8_t who_am_i;

	B0_ASSERT_RETURN_ON_NULL_ARG(NULL, mod);
	dev = B0_MODULE_DEVICE(mod);

	B0_ASSERT_RETURN_ON_NULL_ARG(dev, drv);

	address = 0x6A | (SAD0 ? 0x01 : 0x00);

	/* TODO: Use the most favourable version (FM > SM)
	 *  instead of hardcoded version */
	version = B0_I2C_VERSION_FM;

	const b0_i2c_sugar_arg arg = {
		.addr = address,
		.version = version
	};

	/* Test slave on bus by reading WHO_AM_I register */
	r = b0_i2c_master_write8_read(mod, &arg, B0_L3GD20_WHO_AM_I, &who_am_i, 1);
	if (B0_ERROR_RESULT_CODE(r)) {
		B0_WARN(dev, "Unable to read WHO_AM_I register");
		return r;
	}

	if (who_am_i != 0xD4) {
		B0_WARN(dev,
			"WHO_AM_I register mismactch, probebly not a L3GD20 slave");
		return B0_ERR_BOGUS;
	}

	drv_alloc = malloc(sizeof(*drv_alloc));
	B0_ASSERT_RETURN_ON_NULL_ALLOC(dev, drv_alloc);

	drv_alloc->module = mod;
	drv_alloc->bSlaveAddress = address;
	drv_alloc->version = version;

	/* TODO: test who am i */
	*drv = drv_alloc;
	return B0_OK;
}

/**
 * Convert L3GD20 register data to Radian
 * @param rl Register LOW
 * @param rh Register HIGH
 * @param res Resolution
 * @return rotation in radian
 */
inline static double l3gd20_reg_to_radian(uint8_t rl, uint8_t rh, double res)
{
	int16_t tmp = (rh << 8) | rl;
	double value = tmp * res;
	return B0_DEGREE_TO_RADIAN(value);
}

/* Sensitivity at (+/-)250 dps */
#define L3GD20_SENSITIVITY_250 8.75e-3

b0_result_code b0_l3gd20_read(b0_l3gd20 *drv, double *x, double *y, double *z)
{
	b0_device *dev;
	uint8_t data[6];

	B0_ASSERT_RETURN_ON_NULL_ARG(NULL, drv);
	dev = B0_MODULE_DEVICE(drv->module);

	B0_ASSERT_RETURN_ON_NULL_ARG(dev, x);
	B0_ASSERT_RETURN_ON_NULL_ARG(dev, y);
	B0_ASSERT_RETURN_ON_NULL_ARG(dev, z);

	const b0_i2c_sugar_arg arg = {
		.addr = drv->bSlaveAddress,
		.version = drv->version
	};

	B0_DEBUG_RETURN_ON_FAIL_STMT(dev, b0_i2c_master_write8_read(
		drv->module, &arg,
		B0_L3GD20_OUT_X_L | B0_L3GD20_AUTO_INC, data, sizeof(data)));

	/* extract the results */
	*x = l3gd20_reg_to_radian(data[0], data[1], L3GD20_SENSITIVITY_250);
	*y = l3gd20_reg_to_radian(data[2], data[3], L3GD20_SENSITIVITY_250);
	*z = l3gd20_reg_to_radian(data[4], data[5], L3GD20_SENSITIVITY_250);

	return B0_OK;
}

b0_result_code b0_l3gd20_close(b0_l3gd20 *drv)
{
	B0_ASSERT_RETURN_ON_NULL_ARG(NULL, drv);

	free(drv);
	return B0_OK;
}

b0_result_code b0_l3gd20_power_up(b0_l3gd20 *drv)
{
	B0_ASSERT_RETURN_ON_NULL_ARG(NULL, drv);
	uint8_t param[2] = {B0_L3GD20_CTRL_REG1, 0x0F};

	const b0_i2c_sugar_arg arg = {
		.addr = drv->bSlaveAddress,
		.version = drv->version
	};

	return b0_i2c_master_write(drv->module, &arg, param, sizeof(param));
}
