/*
 * This file is part of libbox0.
 * Copyright (C) 2013-2015 Kuldeep Singh Dhaka <kuldeep@madresistor.com>
 *
 * libbox0 is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * libbox0 is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with libbox0.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef LIBBOX0_DEVICE_PRIVATE_H
#define LIBBOX0_DEVICE_PRIVATE_H

#include <stdint.h>
#include <stddef.h>
#include "common.h"
#include "device.h"

__BEGIN_DECLS

/** @cond INTERNAL */

struct b0_frontend_device_data {
	int log;
};

typedef struct b0_frontend_device_data b0_frontend_device_data;

/**
 * allocate the device memory and frontend only data.
 * @param[out] dev pointer to memory location where device pointer will be written
 * @return result code
 * @see b0_usb_open()
 * @see b0_usb_open_vid_pid()
 * @see b0_usb_open_handle()
 * @see b0_usb_open_supported()
 * @see b0_usb_search_backend()
 * @memberof b0_device
 * @protected
 */
B0_API b0_result_code b0_device_alloc(b0_device **dev);

/**
 * Allocate a module (and return) and add it to the device module list
 */
B0_API b0_result_code b0_module_alloc(b0_device *dev, b0_module **mod);

/**
 * @brief function used by b0_device_close to free up any frontend resource.
 * @param[in] dev device
 * @return result code
 * @memberof b0_device
 * @protected
 */
B0_API b0_result_code b0_device_free(b0_device *dev);

/** @endcond */

__END_DECLS

#endif
