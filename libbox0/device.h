/*
 * This file is part of libbox0.
 * Copyright (C) 2013-2016 Kuldeep Singh Dhaka <kuldeep@madresistor.com>
 *
 * libbox0 is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * libbox0 is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with libbox0.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef LIBBOX0_DEVICE_H
#define LIBBOX0_DEVICE_H

#include <stdint.h>
#include <stddef.h>
#include "common.h"
#include "basic.h"

__BEGIN_DECLS

typedef struct b0_module b0_module;
typedef struct b0_device b0_device;
typedef struct b0_backend b0_backend;

/**
 * Device
 */
struct b0_device {
	/**
	 * Length of @a modules array
	 */
	size_t modules_len;

	/**
	 * a pointer to array of the module
	 *  extracted from device (provided by backend)
	 * @note this is just a list, \n
	 *  <b>never be sure that backend will realloc the
	 *   memory to extend it into derived module \n
	 *  or will make a copy of the memory content
	 *   to derived module header.</b>
	 */
	b0_module **modules;

	/** Device name */
	uint8_t *name;

	/** Manufacturer name */
	uint8_t *manuf;

	/** Serial Number */
	uint8_t *serial;

	/**
	 * Backend data
	 * @protected
	 */
	void *backend_data;

	/**
	 * Frontend data
	 * @protected
	 */
	void *frontend_data;

	/**
	 * @brief backend functions
	 * @protected
	 */
	const b0_backend *backend;
};

/**
 * @brief close a previous opened libbox0 device
 * @details free up resources of the device.
 * after calling this function, no further call to any is to be made.
 * @param[in] dev libbox0 device
 * @return status code
 * @note internally generated resources will be freed.
 *   even if the result code descrive failure,
 *   all part of code should free up it resource.
 *
 * @see b0_device_alloc()
 * @see b0_device_free()
 *
 * @see b0_usb_open()
 * @see b0_usb_open_vid_pid()
 * @see b0_usb_open_handle()
 * @see b0_usb_open_supported()
 * @see b0_usb_search_backend()
 *
 * @memberof b0_device
 */
B0_API b0_result_code b0_device_close(b0_device *dev);

/**
 * try to ping device.
 * if this fails, the device is most likely disconnected.
 * instead of relying on platform to tell device is connected,
 *  a command to device is send, to actually understand the device respond.
 * @param[in] dev device
 * @return result code
 * @note this is transport dependent.
 * @memberof b0_device
 */
B0_API b0_result_code b0_device_ping(b0_device *dev);

/**
 * @brief configure debug level
 * @details configure debug level for b0_device
 * @param[in] dev device
 * @param[in] log log level value
 * @return B0_OK on success
 * @return B0_ERR_SUPP on logging is disabled
 * @memberof b0_device
 */
B0_API b0_result_code b0_device_log(b0_device *dev, b0_log_level log);

__END_DECLS

#endif
