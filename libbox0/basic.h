/*
 * This file is part of libbox0.
 * Copyright (C) 2013-2016 Kuldeep Singh Dhaka <kuldeep@madresistor.com>
 *
 * libbox0 is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * libbox0 is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with libbox0.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef LIBBOX0_BASIC_H
#define LIBBOX0_BASIC_H

#include <stdint.h>
#include "common.h"

__BEGIN_DECLS

/**
 * @addtogroup result_code Result Code
 * @{
 */

/**
 * @page Result Code
 *
 * @section usage Usage
 *  result code emitted by libbox0 \n
 *  all success result code are >= 0 (example: B0_OK = 0) \n
 *  all failure result code are < 0 \n
 *
 * @section example1 Example
 * 	an easy way to test error is
 * 	b0_result_code r = b0_foo(bar)
 * 	if (B0_ERROR_RESULT_CODE(r)) {    // same as (r < 0)  OR (r < B0_OK)
 *		fprintf(stderr, "something bad happened");
 * 	}
 * @see b0_result_name()
 * @see b0_result_explain()
 */
enum b0_result_code {
	B0_FAIL_CALC = -14,	/**< Calculation failed */
	B0_ERR_STATE = -13,	/**< Invalid state, some step missed */
	B0_ERR_UNDERFLOW = -12,/**< Underflow occured, in calculation or memory */
	B0_ERR_OVERFLOW = -11,/**< Overflow occured, in calculation or memory */
	B0_ERR_UNAVAIL = -10, /**< Resource unavailable */
	B0_ERR_TIMEOUT = -9,/**< Operation timeout occured */
	B0_ERR_BUSY = -8,	/**< Cannot perform since because target is busy */
	B0_ERR_BOGUS = -7,	/**< Device returned bogus data */
	B0_BUG = -6,		/**< Raised when a bugg'y state occurs */
	B0_ERR_IO = -5,		/**< Device I/O error */
	B0_ERR_ARG = -4,	/**< Invalid parameter passed */
	B0_ERR_SUPP = -3,	/**< Some unsupported feature */
	B0_ERR_ALLOC = -2,	/**< Memory allocation failed */
	B0_ERR = -1,		/**< Other libraries error/unspecified error */
	B0_OK = 0			/**< Everything went ok */
};

typedef enum b0_result_code b0_result_code;

#define B0_ERROR_RESULT_CODE(r) ((r) < B0_OK)
#define B0_SUCCESS_RESULT_CODE(r) ((r) >= B0_OK)

#define B0_ERR_RC(r) B0_ERROR_RESULT_CODE(r)
#define B0_SUC_RC(r) B0_SUCCESS_RESULT_CODE(r)

/**
 * @brief stringification of result code
 * @param[in] r result code to be be stringified
 * @return stringified @a status
 * @note UTF-8 with C Style NULL termination
 * @see b0_result_desc()
 */
B0_API const uint8_t* b0_result_name(b0_result_code r);

/**
 * @brief String description of result code
 * @param[in] r status status code
 * @return description of @a status
 * @note UTF-8 with C Style NULL termination
 * @see b0_result_desc()
 */
B0_API const uint8_t* b0_result_explain(b0_result_code r);

/**
 * @}
 */

/**
 * @defgroup version Version
 * @{
 */

struct b0_version {
	/** Major */
	uint8_t major;

	/** Minor */
	uint8_t minor;

	/** Patch */
	uint8_t patch;
};

typedef struct b0_version b0_version;

/**
 * @brief extract version information of library
 * @param[out] ver version seperate value
 * @return B0_VERSION_CODE
 * @note if @a ver is NULL, nothing is written to it
 */
B0_API uint32_t b0_version_extract(b0_version *ver);

/**
 * @}
 */

/**
 * @defgroup log Logging
 * @{
 */

/** logging level */
enum b0_log_level {
	B0_LOG_NONE = 0,		/**< No print */
	B0_LOG_ERROR = 1,		/**< Log: Error */
	B0_LOG_WARN = 2,		/**< Log: Warning, Error */
	B0_LOG_INFO = 3,		/**< Log: Info, Warning, Error  */
	B0_LOG_DEBUG = 4		/**< Log: Debug, Info, Warning, Error */
};

typedef enum b0_log_level b0_log_level;

/**
 * @page usage Usage
 *
 * @section support Support
 * if logging is disabled at compile time, nothing is logged.
 *   see "B0_DISABLE_LOGGING"
 *
 * @section log_level_value Log Level Value
 *  * if enviroment variable "LIBBOX0_LOG" is defined, it is used
 *  * if device != NULL passed to internal logger, device log value is used
 *  * if device == NULL passed to internal logger,
 *       compile time value "B0_DEFAULT_LOG" is used
 *
 *  usually, (device == NULL) passed to internal logger is
 *   used to check those paramter contain reference to device.
 *
 *  @section Overriding using Enviroment variable
 *  env variable "LIBBOX0_LOG" acceptable values (case insensitive)
 *   * "0" or "none"  -> @a B0_LOG_NONE
 *   * "1" or "error" -> @a B0_LOG_ERROR
 *   * "2" or "warn"  -> @a B0_LOG_WARN
 *   * "3" or "info"  -> @a B0_LOG_INFO
 *   * "4" or "debug" -> @a B0_LOG_DEBUG
 * @}
 */

enum b0_ref_type {
	B0_REF_VOLTAGE = 0, /**< Voltage */
	B0_REF_CURRENT = 1 /**< Current */
};

typedef enum b0_ref_type b0_ref_type;

__END_DECLS

#endif
