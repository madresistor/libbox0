/*
 * This file is part of libbox0.
 * Copyright (C) 2013-2016 Kuldeep Singh Dhaka <kuldeep@madresistor.com>
 *
 * libbox0 is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * libbox0 is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with libbox0.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "../libbox0-private.h"
#include <string.h>

/** @copydoc INTERNAL */

b0_result_code b0_i2c_close(b0_i2c *mod)
{
	b0_device *dev;

	B0_ASSERT_RETURN_ON_NULL_ARG(NULL, mod);
	dev = B0_MODULE_DEVICE(mod);

	B0_ASSERT_BACKEND_FUNC_NULL(dev, i2c, close);
	return B0_BACKEND(dev, i2c).close(mod);
}

b0_result_code b0_i2c_open(b0_device *dev, b0_i2c **mod, int index)
{
	B0_ASSERT_RETURN_ON_NULL_ARG(NULL, dev);
	B0_ASSERT_RETURN_ON_NULL_ARG(dev, mod);
	B0_ASSERT_RETURN_ON_FAIL_ARG(dev, index >= 0);

	B0_ASSERT_BACKEND_FUNC_NULL(dev, i2c, open);
	return B0_BACKEND(dev, i2c).open(dev, mod, index);
}

b0_result_code b0_i2c_master_prepare(b0_i2c *mod)
{
	b0_device *dev;

	B0_ASSERT_RETURN_ON_NULL_ARG(NULL, mod);
	dev = B0_MODULE_DEVICE(mod);

	B0_ASSERT_BACKEND_FUNC_NULL(dev, i2c, master.prepare);
	return B0_BACKEND(dev, i2c).master.prepare(mod);
}

b0_result_code b0_i2c_master_start(b0_i2c *mod, const b0_i2c_task *tasks,
	int *failed_task_index, int *failed_task_ack)
{
	b0_device *dev;

	B0_ASSERT_RETURN_ON_NULL_ARG(NULL, mod);
	dev = B0_MODULE_DEVICE(mod);

	B0_ASSERT_RETURN_ON_NULL_ARG(dev, tasks);

	if (B0_IS_NOT_NULL(failed_task_index)) {
		*failed_task_index = -1;
	}

	if (B0_IS_NOT_NULL(failed_task_ack)) {
		*failed_task_ack = -1;
	}

	B0_ASSERT_BACKEND_FUNC_NULL(dev, i2c, master.start);
	return B0_BACKEND(dev, i2c).master.start(mod, tasks,
		failed_task_index, failed_task_ack);
}

b0_result_code b0_i2c_master_stop(b0_i2c *mod)
{
	b0_device *dev;

	B0_ASSERT_RETURN_ON_NULL_ARG(NULL, mod);
	dev = B0_MODULE_DEVICE(mod);

	B0_ASSERT_BACKEND_FUNC_NULL(dev, i2c, master.stop);
	return B0_BACKEND(dev, i2c).master.stop(mod);
}

/* TODO: 10bit slave detect */
b0_result_code b0_i2c_master_slave_detect(b0_i2c *mod,
		const b0_i2c_sugar_arg *arg, bool *detected)
{
	b0_device *dev;

	B0_ASSERT_RETURN_ON_NULL_ARG(NULL, mod);
	dev = B0_MODULE_DEVICE(mod);
	B0_ASSERT_RETURN_ON_NULL_ARG(dev, arg);
	B0_ASSERT_RETURN_ON_NULL_ARG(dev, detected);

	B0_ASSERT_BACKEND_FUNC_NULL(dev, i2c, master.slave_detect);
	return B0_BACKEND(dev, i2c).master.slave_detect(mod, arg, detected);
}

b0_result_code b0_generic_i2c_master_slave_detect(b0_i2c *mod,
		const b0_i2c_sugar_arg *arg, bool *detected)
{
	const b0_i2c_task tasks[] = {{
		.addr = arg->addr,
		.version = arg->version,
		.flags = B0_I2C_TASK_READ | B0_I2C_TASK_LAST,
		.data = NULL,
		.count = 0,
	}};

	int ack;
	b0_result_code r = b0_i2c_master_start(mod, tasks, NULL, &ack);
	if (B0_SUCCESS_RESULT_CODE(r)) {
		*detected = true;
		return B0_OK;
	} else if (ack == 0) {
		*detected = false;
		return B0_OK;
	} else {
		return r;
	}
}

b0_result_code b0_i2c_master_slaves_detect(b0_i2c *mod,
		b0_i2c_version version, const uint8_t *addresses, bool *detected,
		size_t count, size_t *processed)
{
	b0_device *dev;

	B0_ASSERT_RETURN_ON_NULL_ARG(NULL, mod);
	dev = B0_MODULE_DEVICE(mod);

	B0_ASSERT_RETURN_ON_NULL_ARG(dev, addresses);
	B0_ASSERT_RETURN_ON_NULL_ARG(dev, detected);
	B0_ASSERT_RETURN_ON_FAIL_ARG(dev, count > 0);

	B0_ASSERT_BACKEND_FUNC_NULL(dev, i2c, master.slave_detect);
	return B0_BACKEND(dev, i2c).master.slaves_detect(mod, version, addresses,
					detected, count, processed);
}

b0_result_code b0_generic_i2c_master_slaves_detect(b0_i2c *mod,
		b0_i2c_version version, const uint8_t *addresses, bool *detected,
		size_t count, size_t *processed)
{
	size_t i;
	b0_result_code r;

	for (i = 0; i < count; i++) {
		struct b0_i2c_sugar_arg arg = {
			.addr = addresses[i],
			.version = version
		};

		r = b0_i2c_master_slave_detect(mod, &arg, &detected[i]);

		if (B0_ERROR_RESULT_CODE(r)) {
			if (B0_IS_NOT_NULL(processed)) {
				*processed = i;
			}

			return r;
		}
	}

	if (B0_IS_NOT_NULL(processed)) {
		*processed = count;
	}

	return B0_OK;
}

b0_result_code b0_i2c_master_slave_id(b0_i2c *mod,
			const b0_i2c_sugar_arg *arg, uint16_t *manuf, uint16_t *part,
			uint8_t *rev)
{
	b0_device *dev;

	B0_ASSERT_RETURN_ON_NULL_ARG(NULL, mod);
	dev = B0_MODULE_DEVICE(mod);

	B0_ASSERT_RETURN_ON_NULL_ARG(dev, arg);
	B0_ASSERT_RETURN_ON_NULL_ARG(dev, manuf);
	B0_ASSERT_RETURN_ON_NULL_ARG(dev, part);
	B0_ASSERT_RETURN_ON_NULL_ARG(dev, dev);

	B0_ASSERT_BACKEND_FUNC_NULL(dev, i2c, master.slave_id);
	return B0_BACKEND(dev, i2c).master.slave_id(mod, arg, manuf, part, rev);
}

b0_result_code b0_i2c_master_read(b0_i2c *mod,
			const b0_i2c_sugar_arg *arg, void *data, size_t count)
{
	b0_device *dev;

	B0_ASSERT_RETURN_ON_NULL_ARG(NULL, mod);
	dev = B0_MODULE_DEVICE(mod);

	B0_ASSERT_RETURN_ON_NULL_ARG(dev, arg);
	B0_ASSERT_RETURN_ON_NULL_ARG(dev, data);
	B0_ASSERT_RETURN_ON_FAIL_ARG(dev, count > 0);

	B0_ASSERT_BACKEND_FUNC_NULL(dev, i2c, master.read);
	return B0_BACKEND(dev, i2c).master.read(mod, arg, data, count);
}

b0_result_code b0_i2c_master_write(b0_i2c *mod,
			const b0_i2c_sugar_arg *arg, const void *data, size_t count)
{
	b0_device *dev;

	B0_ASSERT_RETURN_ON_NULL_ARG(NULL, mod);
	dev = B0_MODULE_DEVICE(mod);

	B0_ASSERT_RETURN_ON_NULL_ARG(dev, arg);
	B0_ASSERT_RETURN_ON_NULL_ARG(dev, data);
	B0_ASSERT_RETURN_ON_FAIL_ARG(dev, count > 0);

	B0_ASSERT_BACKEND_FUNC_NULL(dev, i2c, master.write);
	return B0_BACKEND(dev, i2c).master.write(mod, arg, data, count);
}

b0_result_code b0_generic_i2c_master_write(b0_i2c *mod,
		const b0_i2c_sugar_arg *arg, const void *data, size_t count)
{
	b0_device *dev = B0_MODULE_DEVICE(mod);

	const b0_i2c_task tasks[] = {{
		.addr = arg->addr,
		.version = arg->version,
		.flags = B0_I2C_TASK_WRITE | B0_I2C_TASK_LAST,
		.data = (void *) data,
		.count = count
	}};

	B0_DEBUG_RETURN_STMT(dev, b0_i2c_master_start(mod, tasks, NULL, NULL));
}

b0_result_code b0_i2c_master_write8_read(b0_i2c *mod,
		const b0_i2c_sugar_arg *arg, uint8_t write,
		void *read_data, size_t read_count)
{
	b0_device *dev;

	B0_ASSERT_RETURN_ON_NULL_ARG(NULL, mod);
	dev = B0_MODULE_DEVICE(mod);
	B0_ASSERT_RETURN_ON_NULL_ARG(dev, arg);
	B0_ASSERT_RETURN_ON_NULL_ARG(dev, read_data);
	B0_ASSERT_RETURN_ON_FAIL_ARG(dev, read_count > 0);

	B0_ASSERT_BACKEND_FUNC_NULL(dev, i2c, master.write8_read);
	return B0_BACKEND(dev, i2c).master.write8_read(mod, arg, write,
		read_data, read_count);
}

b0_result_code b0_generic_i2c_master_slave_id(b0_i2c *mod,
		const b0_i2c_sugar_arg *arg,
		uint16_t *manuf, uint16_t *part, uint8_t *rev)
{
	b0_device *dev = B0_MODULE_DEVICE(mod);

	uint8_t data[3];

	const struct b0_i2c_sugar_arg _arg = {
		.addr = 0x7C,
		.version = arg->version
	};

	B0_DEBUG_RETURN_ON_FAIL_STMT(dev, b0_i2c_master_write8_read(mod, &_arg,
			arg->addr << 1, data, sizeof(data)));

	*manuf = (data[0] << 4) | ((data[1] & 0xF0) >> 4);
	*part = ((data[1] & 0x0F) << 5) | ((data[2] & 0xF8) >> 3);
	*rev = (data[2] & 0x07);

	return B0_OK;
}

b0_result_code b0_generic_i2c_master_read(b0_i2c *mod, const b0_i2c_sugar_arg *arg,
			void *data, size_t count)
{
	b0_device *dev = B0_MODULE_DEVICE(mod);

	const b0_i2c_task tasks[] = {{
		.addr = arg->addr,
		.version = arg->version,
		.flags = B0_I2C_TASK_READ | B0_I2C_TASK_LAST,
		.data = data,
		.count = count
	}};

	B0_DEBUG_RETURN_STMT(dev, b0_i2c_master_start(mod, tasks, NULL, NULL));
}

b0_result_code b0_generic_i2c_master_write8_read(b0_i2c *mod,
			const b0_i2c_sugar_arg *arg, uint8_t write, void *read_data,
			size_t read_count)
{
	b0_device *dev = B0_MODULE_DEVICE(mod);

	B0_DEBUG_RETURN_STMT(dev,
		b0_i2c_master_write_read(mod, arg, &write, 1, read_data, read_count));
}

b0_result_code b0_i2c_master_write_read(b0_i2c *mod,
			const b0_i2c_sugar_arg *arg, const void *write_data,
			size_t write_count, void *read_data, size_t read_count)
{
	b0_device *dev;

	B0_ASSERT_RETURN_ON_NULL_ARG(NULL, mod);
	dev = B0_MODULE_DEVICE(mod);
	B0_ASSERT_RETURN_ON_NULL_ARG(dev, arg);
	B0_ASSERT_RETURN_ON_NULL_ARG(dev, write_data);
	B0_ASSERT_RETURN_ON_FAIL_ARG(dev, write_count > 0);
	B0_ASSERT_RETURN_ON_NULL_ARG(dev, read_data);
	B0_ASSERT_RETURN_ON_FAIL_ARG(dev, read_count > 0);

	B0_ASSERT_BACKEND_FUNC_NULL(dev, i2c, master.write_read);
	return B0_BACKEND(dev, i2c).master.write_read(mod, arg, write_data,
			write_count, read_data, read_count);
}

b0_result_code b0_generic_i2c_master_write_read(b0_i2c *mod,
			const b0_i2c_sugar_arg *arg, const void *write_data,
			size_t write_count, void *read_data, size_t read_count)
{
	b0_device *dev = B0_MODULE_DEVICE(mod);

	const b0_i2c_task tasks[] = {{
		.flags = B0_I2C_TASK_WRITE,
		.addr = arg->addr,
		.version = arg->version,
		.data = (void *) write_data,
		.count = write_count,
	}, {
		.flags = B0_I2C_TASK_READ | B0_I2C_TASK_LAST,
		.addr = arg->addr,
		.version = arg->version,
		.data = read_data,
		.count = read_count
	}};

	B0_DEBUG_RETURN_STMT(dev, b0_i2c_master_start(mod, tasks, NULL, NULL));
}

/** @endcond */
