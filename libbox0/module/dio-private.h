/*
 * This file is part of libbox0.
 * Copyright (C) 2013-2016 Kuldeep Singh Dhaka <kuldeep@madresistor.com>
 *
 * libbox0 is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * libbox0 is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with libbox0.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef LIBBOX0_MODULE_DIO_PRIVATE_H
#define LIBBOX0_MODULE_DIO_PRIVATE_H

#include <stdint.h>
#include <stddef.h>
#include <stdbool.h>
#include "../common.h"
#include "dio.h"

__BEGIN_DECLS

/* generic   - */
/* multiple */
B0_PRIV b0_result_code b0_generic_dio_multiple_value_set(b0_dio *mod,
				unsigned int *pins, size_t size, bool value);

B0_PRIV b0_result_code b0_generic_dio_multiple_value_get(b0_dio *mod,
				unsigned int *pins, bool *values, size_t size);

B0_PRIV b0_result_code b0_generic_dio_multiple_value_toggle(b0_dio *mod,
				unsigned int *pins, size_t size);


B0_PRIV b0_result_code b0_generic_dio_multiple_dir_set(b0_dio *mod,
				unsigned int *pins, size_t size, bool value);

B0_PRIV b0_result_code b0_generic_dio_multiple_dir_get(b0_dio *mod,
				unsigned int *pins, bool *values, size_t size);

B0_PRIV b0_result_code b0_generic_dio_multiple_hiz_get(b0_dio *mod,
				unsigned int *pins, bool *values, size_t size);

B0_PRIV b0_result_code b0_generic_dio_multiple_hiz_set(b0_dio *mod,
				unsigned int *pins, size_t size, bool value);

/* all */
B0_PRIV b0_result_code b0_generic_dio_all_value_set(b0_dio *mod, bool value);
B0_PRIV b0_result_code b0_generic_dio_all_value_get(b0_dio *mod, bool *values);
B0_PRIV b0_result_code b0_generic_dio_all_value_toggle(b0_dio *mod);

B0_PRIV b0_result_code b0_generic_dio_all_dir_set(b0_dio *mod, bool value);
B0_PRIV b0_result_code b0_generic_dio_all_dir_get(b0_dio *mod, bool *values);

B0_PRIV b0_result_code b0_generic_dio_all_hiz_get(b0_dio *mod, bool *values);
B0_PRIV b0_result_code b0_generic_dio_all_hiz_set(b0_dio *mod, bool value);

__END_DECLS

#endif
