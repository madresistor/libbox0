/*
 * This file is part of libbox0.
 * Copyright (C) 2013-2016 Kuldeep Singh Dhaka <kuldeep@madresistor.com>
 *
 * libbox0 is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * libbox0 is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with libbox0.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <math.h>
#include <stddef.h>
#include <stdint.h>
#include <stdbool.h>
#include <string.h>
#include <malloc.h>
#include "../libbox0-private.h"

/** @cond INTERNAL */

b0_result_code b0_ain_close(b0_ain *mod)
{
	b0_device *dev;
	b0_frontend_ain_data *frontend_data;
	B0_ASSERT_RETURN_ON_NULL_ARG(NULL, mod);

	dev = B0_MODULE_DEVICE(mod);
	frontend_data = B0_MODULE_FRONTEND_DATA(mod);

	if (pthread_mutex_destroy(&frontend_data->in_progress)) {
		B0_WARN(dev, "`in_progress' failed to destory'");
	}

	free(frontend_data);

	B0_ASSERT_BACKEND_FUNC_NULL(dev, ain, close);
	return B0_BACKEND(dev, ain).close(mod);
}

b0_result_code b0_ain_open(b0_device *dev, b0_ain **mod, int index)
{
	B0_ASSERT_RETURN_ON_NULL_ARG(NULL, dev);
	B0_ASSERT_RETURN_ON_NULL_ARG(dev, mod);
	B0_ASSERT_RETURN_ON_FAIL_ARG(dev, index >= 0);

	B0_ASSERT_BACKEND_FUNC_NULL(dev, ain, open);
	b0_result_code r = B0_BACKEND(dev, ain).open(dev, mod, index);

	/* allocate frontend data */
	if (B0_SUCCESS_RESULT_CODE(r)) {
		b0_frontend_ain_data *frontend_data = calloc(1, sizeof(*frontend_data));
		(*mod)->header.frontend_data = frontend_data;
		if (B0_IS_NULL(frontend_data)) {
			r = B0_ERR_ALLOC;
			goto done;
		}

		/* initalize lock */
		if (pthread_mutex_init(&frontend_data->in_progress, NULL)) {
			B0_ERROR(dev, "failed to initalize mutex `in_progress`");
			return B0_ERR;
		}

		frontend_data->cache.bitsize = 0;
	}

	done:
	return r;
}

b0_result_code b0_ain_chan_seq_set(b0_ain *mod,
			unsigned int *values, size_t count)
{
	b0_device *dev;

	B0_ASSERT_RETURN_ON_NULL_ARG(NULL, mod);
	dev = B0_MODULE_DEVICE(mod);

	B0_ASSERT_RETURN_ON_NULL_ARG(dev, values);
	B0_ASSERT_RETURN_ON_FAIL_ARG(dev, count > 0);

	B0_ASSERT_BACKEND_FUNC_NULL(dev, ain, chan_seq.set);
	return B0_BACKEND(dev, ain).chan_seq.set(mod, values, count);
}

b0_result_code b0_ain_chan_seq_get(b0_ain *mod,
			unsigned int *values, size_t *count)
{
	b0_device *dev;

	B0_ASSERT_RETURN_ON_NULL_ARG(NULL, mod);
	dev = B0_MODULE_DEVICE(mod);

	B0_ASSERT_RETURN_ON_NULL_ARG(dev, values);
	B0_ASSERT_RETURN_ON_NULL_ARG(dev, count);
	B0_ASSERT_RETURN_ON_FAIL_ARG(dev, count[0] > 0);

	B0_ASSERT_BACKEND_FUNC_NULL(dev, ain, chan_seq.get);
	return B0_BACKEND(dev, ain).chan_seq.get(mod, values, count);
}

b0_result_code b0_ain_bitsize_speed_set(b0_ain *mod,
			unsigned int bitsize, unsigned long speed)
{
	b0_result_code r;
	b0_frontend_ain_data *frontend_data = B0_MODULE_FRONTEND_DATA(mod);
	b0_device *dev;

	B0_ASSERT_RETURN_ON_NULL_ARG(NULL, mod);
	dev = B0_MODULE_DEVICE(mod);

	B0_ASSERT_RETURN_ON_FAIL_ARG(dev, bitsize > 0);
	B0_ASSERT_RETURN_ON_FAIL_ARG(dev, speed > 0);

	B0_ASSERT_BACKEND_FUNC_NULL(dev, ain, bitsize_speed.set);
	r = B0_BACKEND(dev, ain).bitsize_speed.set(mod, bitsize, speed);

	if (B0_SUCCESS_RESULT_CODE(r)) {
		frontend_data->cache.bitsize = bitsize;
	}

	return r;
}

b0_result_code b0_ain_bitsize_speed_get(b0_ain *mod,
			unsigned int *bitsize, unsigned long *speed)
{
	b0_result_code r;
	b0_frontend_ain_data *frontend_data = B0_MODULE_FRONTEND_DATA(mod);
	b0_device *dev;

	B0_ASSERT_RETURN_ON_NULL_ARG(NULL, mod);
	dev = B0_MODULE_DEVICE(mod);

	B0_ASSERT_RETURN_ON_FAIL_ARG(dev, !(bitsize == NULL && speed == NULL));

	B0_ASSERT_BACKEND_FUNC_NULL(dev, ain, bitsize_speed.get);
	r = B0_BACKEND(dev, ain).bitsize_speed.get(mod, bitsize, speed);

	if (B0_SUCCESS_RESULT_CODE(r) && B0_IS_NOT_NULL(bitsize)) {
		frontend_data->cache.bitsize = *bitsize;
	}

	return r;
}

static int snapshot_start_ieee(b0_ain *mod, unsigned int ieee_binary_size,
	void *samples, size_t count);

b0_result_code b0_ain_snapshot_prepare(b0_ain *mod)
{
	b0_device *dev;

	B0_ASSERT_RETURN_ON_NULL_ARG(NULL, mod);
	dev = B0_MODULE_DEVICE(mod);

	b0_frontend_ain_data *frontend_data = B0_MODULE_FRONTEND_DATA(mod);
	frontend_data->cache.bitsize = mod->snapshot.values[0].bitsize;

	B0_ASSERT_BACKEND_FUNC_NULL(dev, ain, snapshot.prepare);
	return B0_BACKEND(dev, ain).snapshot.prepare(mod);
}

#define SNAPSHOT_START_ROUTE_GENERIC_CODE(fn_name)						\
	b0_device *dev;													\
																		\
	B0_ASSERT_RETURN_ON_NULL_ARG(NULL, mod);							\
	dev = B0_MODULE_DEVICE(mod);										\
																		\
	B0_ASSERT_RETURN_ON_NULL_ARG(dev, samples);							\
	B0_ASSERT_RETURN_ON_FAIL_ARG(dev, count > 0);						\
																		\
	B0_ASSERT_BACKEND_FUNC_NULL(dev, ain, snapshot.fn_name);			\
	return B0_BACKEND(dev, ain).snapshot.fn_name(mod, samples, count);

b0_result_code b0_ain_snapshot_start(b0_ain *mod, void *samples, size_t count)
{
	SNAPSHOT_START_ROUTE_GENERIC_CODE(start)
}

b0_result_code b0_ain_snapshot_start_double(b0_ain *mod, double *samples, size_t count)
{
	SNAPSHOT_START_ROUTE_GENERIC_CODE(start_double)
}

b0_result_code b0_ain_snapshot_start_float(b0_ain *mod, float *samples, size_t count)
{
	SNAPSHOT_START_ROUTE_GENERIC_CODE(start_float)
}

static int snapshot_start_ieee(b0_ain *mod, unsigned int ieee_binary_size,
	void *samples, size_t count)
{
	b0_result_code r;
	b0_device *dev;

	B0_ASSERT_RETURN_ON_NULL_ARG(NULL, mod);
	dev = B0_MODULE_DEVICE(mod);

	B0_ASSERT_RETURN_ON_NULL_ARG(dev, samples);
	B0_ASSERT_RETURN_ON_FAIL_ARG(dev, count > 0);

	b0_frontend_ain_data *frontend_data = B0_MODULE_FRONTEND_DATA(mod);
	unsigned int bitsize = frontend_data->cache.bitsize;
	unsigned int bytes_per_sample = B0_BIT2BYTE(bitsize);

	/* we are ready to read from device */
	void *binary = B0_TEMP_BUFFER_ALLOC(bytes_per_sample * count);
	B0_ASSERT_RETURN_ON_NULL_ALLOC(dev, binary);

	r = b0_ain_snapshot_start(mod, binary, count);
	if (B0_ERROR_RESULT_CODE(r)) {
		goto done;
	}

	r = b0_math_raw_to_ieee(dev, mod->ref.low, mod->ref.high, bitsize, binary,
			ieee_binary_size, samples, count);

	done:
	B0_TEMP_BUFFER_FREE(binary);

	return r;
}

b0_result_code b0_generic_ain_snapshot_start_double(b0_ain *mod,
			double *samples, size_t count)
{
	return snapshot_start_ieee(mod, 64, samples, count);
}

b0_result_code b0_generic_ain_snapshot_start_float(b0_ain *mod,
			float *samples, size_t count)
{
	return snapshot_start_ieee(mod, 32, samples, count);
}

b0_result_code b0_ain_snapshot_stop(b0_ain *mod)
{
	b0_device *dev;
	B0_ASSERT_RETURN_ON_NULL_ARG(NULL, mod);

	dev = B0_MODULE_DEVICE(mod);

	B0_ASSERT_BACKEND_FUNC_NULL(dev, ain, snapshot.stop);
	return B0_BACKEND(dev, ain).snapshot.stop(mod);
}

static b0_result_code stream_read_ieee(b0_ain *mod, size_t ieee_binary_size,
	void *samples, size_t count, size_t *actual_count);

b0_result_code b0_ain_stream_prepare(b0_ain *mod)
{
	b0_device *dev;

	B0_ASSERT_RETURN_ON_NULL_ARG(NULL, mod);
	dev = B0_MODULE_DEVICE(mod);

	b0_frontend_ain_data *frontend_data = B0_MODULE_FRONTEND_DATA(mod);
	frontend_data->cache.bitsize = mod->stream.values[0].bitsize;

	B0_ASSERT_BACKEND_FUNC_NULL(dev, ain, stream.prepare);
	return B0_BACKEND(dev, ain).stream.prepare(mod);
}

b0_result_code b0_ain_stream_start(b0_ain *mod)
{
	b0_device *dev;

	B0_ASSERT_RETURN_ON_NULL_ARG(NULL, mod);
	dev = B0_MODULE_DEVICE(mod);

	B0_ASSERT_BACKEND_FUNC_NULL(dev, ain, stream.start);
	return B0_BACKEND(dev, ain).stream.start(mod);
}

b0_result_code b0_ain_stream_stop(b0_ain *mod)
{
	b0_device *dev;

	B0_ASSERT_RETURN_ON_NULL_ARG(NULL, mod);
	dev = B0_MODULE_DEVICE(mod);

	B0_ASSERT_BACKEND_FUNC_NULL(dev, ain, stream.stop);
	return B0_BACKEND(dev, ain).stream.stop(mod);
}

#define STREAM_READ_ROUTE_GENERIC_CODE(fn_name)						\
	b0_device *dev;														\
																		\
	B0_ASSERT_RETURN_ON_NULL_ARG(NULL, mod);							\
	dev = B0_MODULE_DEVICE(mod);										\
																		\
	B0_ASSERT_RETURN_ON_NULL_ARG(dev, samples);							\
	B0_ASSERT_RETURN_ON_FAIL_ARG(dev, count > 0);						\
																		\
	B0_ASSERT_BACKEND_FUNC_NULL(dev, ain, stream.fn_name);				\
	return B0_BACKEND(dev, ain).stream.fn_name(mod, samples, count, actual_count);

b0_result_code b0_ain_stream_read(b0_ain *mod, void *samples, size_t count,
	size_t *actual_count)
{
	STREAM_READ_ROUTE_GENERIC_CODE(read)
}

b0_result_code b0_ain_stream_read_float(b0_ain *mod, float *samples,
	size_t count, size_t *actual_count)
{
	STREAM_READ_ROUTE_GENERIC_CODE(read_float)
}

b0_result_code b0_ain_stream_read_double(b0_ain *mod, double *samples,
	size_t count, size_t *actual_count)
{
	STREAM_READ_ROUTE_GENERIC_CODE(read_double)
}

static b0_result_code stream_read_ieee(b0_ain *mod, size_t ieee_binary_size,
	void *samples, size_t count, size_t *actual_count)
{
	b0_device *dev;
	b0_result_code r;

	B0_ASSERT_RETURN_ON_NULL_ARG(NULL, mod);
	dev = B0_MODULE_DEVICE(mod);

	B0_ASSERT_RETURN_ON_NULL_ARG(dev, samples);
	B0_ASSERT_RETURN_ON_FAIL_ARG(dev, count > 0);

	b0_frontend_ain_data *frontend_data = B0_MODULE_FRONTEND_DATA(mod);
	unsigned int bitsize = frontend_data->cache.bitsize;
	unsigned int bytes_per_sample = B0_BIT2BYTE(bitsize);

	void *binary = B0_TEMP_BUFFER_ALLOC(bytes_per_sample * count);
	B0_ASSERT_RETURN_ON_NULL_ALLOC(dev, binary);

	r = b0_ain_stream_read(mod, binary, count, actual_count);
	if (B0_SUCCESS_RESULT_CODE(r)) {
		/* partial read allowed? */
		if (B0_IS_NOT_NULL(actual_count)) {
			count = *actual_count;
		}

		/* convert to floating values */
		r = b0_math_raw_to_ieee(dev, mod->ref.low, mod->ref.high, bitsize,
				binary, ieee_binary_size, samples, count);
	}

	B0_TEMP_BUFFER_FREE(binary);
	return r;
}

b0_result_code b0_generic_ain_stream_read_float(b0_ain *mod, float *samples,
	size_t count, size_t *actual_count)
{
	return stream_read_ieee(mod, 32, samples, count, actual_count);
}

b0_result_code b0_generic_ain_stream_read_double(b0_ain *mod, double *samples,
	size_t count, size_t *actual_count)
{
	return stream_read_ieee(mod, 64, samples, count, actual_count);
}

/** @endcond */
