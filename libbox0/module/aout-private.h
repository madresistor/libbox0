/*
 * This file is part of libbox0.
 * Copyright (C) 2013-2016 Kuldeep Singh Dhaka <kuldeep@madresistor.com>
 *
 * libbox0 is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * libbox0 is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with libbox0.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef LIBBOX0_MODULE_AOUT_PRIVATE_H
#define LIBBOX0_MODULE_AOUT_PRIVATE_H

#include <stdint.h>
#include <stddef.h>
#include "../common.h"
#include "aout.h"

__BEGIN_DECLS


/**
 * @private
 */
struct b0_frontend_aout_data {
	/** Currently set bitsize for streaming or snapshot mode */
	struct {
		unsigned int bitsize;
	} cache;
};

typedef struct b0_frontend_aout_data b0_frontend_aout_data;

B0_API b0_result_code b0_generic_aout_snapshot_start_float(b0_aout *mod,
							float *data, size_t len);
B0_API b0_result_code b0_generic_aout_snapshot_start_double(b0_aout *mod,
							double *data, size_t len);

B0_API b0_result_code b0_generic_aout_stream_write_double(b0_aout *mod,
							double *data, size_t len);
B0_API b0_result_code b0_generic_aout_stream_write_float(b0_aout *mod,
							float *data, size_t len);

__END_DECLS

#endif
