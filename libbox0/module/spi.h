/*
 * This file is part of libbox0.
 * Copyright (C) 2013-2016 Kuldeep Singh Dhaka <kuldeep@madresistor.com>
 *
 * libbox0 is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * libbox0 is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with libbox0.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef LIBBOX0_MODULE_SPI_H
#define LIBBOX0_MODULE_SPI_H

#include <stdint.h>
#include <stdbool.h>
#include "../common.h"
#include "module.h"

__BEGIN_DECLS

enum b0_spi_task_flags {
	B0_SPI_TASK_LAST = (1 << 0), /* Last task to execute */

	B0_SPI_TASK_CPHA = (1 << 1),
	B0_SPI_TASK_CPOL = (1 << 2),

	B0_SPI_TASK_MODE0 = 0,
	B0_SPI_TASK_MODE1 = B0_SPI_TASK_CPHA,
	B0_SPI_TASK_MODE2 = B0_SPI_TASK_CPOL,
	B0_SPI_TASK_MODE3 = B0_SPI_TASK_CPOL | B0_SPI_TASK_CPHA,

	B0_SPI_TASK_FD = (0x0 << 3),
	B0_SPI_TASK_HD_READ = (0x3 << 3),
	B0_SPI_TASK_HD_WRITE = (0x1 << 3),

	B0_SPI_TASK_MSB_FIRST = (0 << 5),
	B0_SPI_TASK_LSB_FIRST = (1 << 5),

	B0_SPI_TASK_MODE_MASK = (0x3 << 1),
	B0_SPI_TASK_DUPLEX_MASK = (0x3 << 3),
	B0_SPI_TASK_ENDIAN_MASK = (1 << 5)
};

typedef enum b0_spi_task_flags b0_spi_task_flags;

/**
 * @note Number of bytes (to transfer) = count * CEIL(bitsize / 8)
 * for  Full duplex,
 *  - @a rdata is used for receiving from device
 *  - @a wdata is used for transmiting to device
 *
 * for Half Duplex Write
 *  - @a rdata is ignored
 *  - @a wdata is used for transmiting to device
 *
 * for Half Duplex Read
 *  - @a rdata is used for receiving from device
 *  - @a wdata is ignored
 *
 * If speed is 0, fallback speed is used
 */
struct b0_spi_task {
	b0_spi_task_flags flags; /**< Task flags */
	unsigned int addr; /**< Slave address */
	unsigned long speed; /**< Communication speed */
	unsigned int bitsize; /**< Bitsize to use for transfer */
	const void *wdata; /**< Write memory */
	void *rdata; /**< Read memory */
	size_t count; /**< Number of data unit */
};

#define B0_SPI_TASK_SPEED_USE_FALLBACK 0

typedef struct b0_spi_task b0_spi_task;

/**
 * Serial Peripherial Interface
 * @extends b0_module
 */
struct b0_spi {
	b0_module header;

	unsigned ss_count;

	struct {
		uint8_t *sclk, *mosi, *miso;
		uint8_t **ss;
	} label;

	struct {
		unsigned int *values;
		size_t count;
	} bitsize;

	struct {
		unsigned long *values;
		size_t count;
	} speed;

	struct {
		double high, low;
		b0_ref_type type;
	} ref;
};

typedef struct b0_spi b0_spi;

/**
 * @copydoc b0_modimpl_open
 * @memberof b0_spi
 */
B0_API b0_result_code b0_spi_open(b0_device *dev, b0_spi **mod, int index);

/**
 * @copydoc b0_modimpl_close
 * @memberof b0_spi
 */
B0_API b0_result_code b0_spi_close(b0_spi *mod);

/**
 * If the speed is 0 in task, then this speed is used as fallback
 * @param mod SPI module
 * @param speed Fallback speed for task
 * @return result code
 * @memberof b0_spi
 */
B0_API b0_result_code b0_spi_speed_set(b0_spi *mod, unsigned long speed);

/**
 * Get the fallback speed for task with speed = 0
 * @param mod SPI module
 * @param[out] speed Fallback speed for task
 * @return result code
 * @memberof b0_spi
 */
B0_API b0_result_code b0_spi_speed_get(b0_spi *mod, unsigned long *speed);

/**
 * @memberof b0_spi
 */
B0_API b0_result_code b0_spi_active_state_set(b0_spi *mod, unsigned int addr,
					bool value);

/**
 * @memberof b0_spi
 */
B0_API b0_result_code b0_spi_active_state_get(b0_spi *mod, unsigned int addr,
						bool *value);

/**
 * Prepare SPI for Master mode
 * @param mod SPI module
 * @return result code
 * @memberof b0_spi
 */
B0_API b0_result_code b0_spi_master_prepare(b0_spi *mod);

/**
 * Start a transaction
 * @param[in] mod Module
 * @param[in] tasks Task to execute
 * @param[out] failed_task_index Last failed task index (can be NULL)
 * @param[out] failed_task_count Last failed task count (can be NULL)
 * @return result code
 * @memberof b0_spi
 * @see b0_spi_stop()
 * @note failed_task_index = -1 means the value is invalid
 * @note failed_task_ack = -1 means the value is invalid
 */
B0_API b0_result_code b0_spi_master_start(b0_spi *mod, const b0_spi_task *tasks,
	int *failed_task_index, int *failed_task_count);

/**
 * @copydoc b0_busimpl_stop
 * @memberof b0_spi
 * @see b0_spi_start()
 */
B0_API b0_result_code b0_spi_master_stop(b0_spi *mod);

struct b0_spi_sugar_arg {
	unsigned int addr;
	b0_spi_task_flags flags;
	unsigned int bitsize;
	unsigned long speed;
};

typedef struct b0_spi_sugar_arg b0_spi_sugar_arg;

/**
 * Communicate with slave in full duplex mode
 * @param mod Module
 * @param arg Argument
 * @param write_data Memory to write data from
 * @param read_data Memory to read data to
 * @param count Number of data unit to transfer
 * @return result code
 * @memberof b0_spi
 */
B0_API b0_result_code b0_spi_master_fd(b0_spi *mod,
		const b0_spi_sugar_arg *arg,
		const void *write_data, void *read_data, size_t count);

/**
 * Read data from slave in half duplex mode
 * @param mod Module
 * @param arg Argument
 * @param data Memory to read data to
 * @param count Number of data unit to read
 * @return result code
 * @memberof b0_spi
 */
B0_API b0_result_code b0_spi_master_hd_read(b0_spi *mod,
		const b0_spi_sugar_arg *arg,
		void *data, size_t count);

/**
 * Write to slave in half duplex mode
 * @param mod Module
 * @param arg Argument
 * @param data Memory to write data from
 * @param count Number of data unit to write
 * @return result code
 * @memberof b0_spi
 */
B0_API b0_result_code b0_spi_master_hd_write(b0_spi *mod,
		const b0_spi_sugar_arg *arg,
		const void *data, size_t count);

/**
 * Write and then read data from slave in half duplex mode
 * @param mod Module
 * @param arg Argument
 * @param write_data Memory to write data from
 * @param write_count Number of data unit to write
 * @param read_data Memory to read data to
 * @param read_count Number of data units to read
 * @return result code
 * @memberof b0_spi
 */
B0_API b0_result_code b0_spi_master_hd_write_read(b0_spi *mod,
		const b0_spi_sugar_arg *arg,
		const void *write_data, size_t write_count,
		void *read_data, size_t read_count);

__END_DECLS

#endif
