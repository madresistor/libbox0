/*
 * This file is part of libbox0.
 * Copyright (C) 2013-2016 Kuldeep Singh Dhaka <kuldeep@madresistor.com>
 *
 * libbox0 is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * libbox0 is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with libbox0.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef LIBBOX0_MODULE_PWM_H
#define LIBBOX0_MODULE_PWM_H

#include <stdbool.h>
#include <math.h>
#include "../common.h"
#include "module.h"

__BEGIN_DECLS

/**
 * Pulse Width Modulation
 * @extends b0_module
 */
struct b0_pwm {
	b0_module header;

	unsigned pin_count;

	struct {
		uint8_t **pin;
	} label;

	struct {
		unsigned int *values;
		size_t count;
	} bitsize;

	struct {
		unsigned long *values;
		size_t count;
	} speed;

	struct {
		double high, low;
		b0_ref_type type;
	} ref;
};

typedef struct b0_pwm b0_pwm;

typedef unsigned long long int b0_pwm_reg;

#define B0_PWM_REG_H2LE(reg) B0_H2LE64(reg)
#define B0_PWM_REG_LE2H(reg) B0_LE2H64(reg)

/**
 * Cast to double
 * @param[in] val
 */
#define B0_PWM_CAST_FLOATING(val) ((double)(val))

/**
 * Cast to b0_pwm_reg
 * @param[in] val
 */
#define B0_PWM_CAST_REG(val) ((b0_pwm_reg)(val))

/**
 * Calculate width value from period and duty cycle.
 * @param[in] period Period
 * @param[in] duty_cycle Duty Cycle
 * @note duty_cycle denoted in term of percentage. (floating point value)
 * @note duty_cycle should be greater than 0
 * @note duty_cycle should be smaller than 100
 * @note https://en.wikipedia.org/wiki/Pulse-width_modulation
 * @return Width value
 */
#define B0_PWM_OUTPUT_CALC_WIDTH(period, duty_cycle) \
	B0_PWM_CAST_REG((B0_PWM_CAST_FLOATING(period) * \
		B0_PWM_CAST_FLOATING(duty_cycle)) / 100.0)

/**
 * Calculate Duty cycle from @a period and @a width
 * @param[in] period Period
 * @param[in] width Width
 */
#define B0_PWM_OUTPUT_DUTY_CYCLE(period, width) \
	((B0_PWM_CAST_FLOATING(width) * 100.0) / B0_PWM_CAST_FLOATING(period))

/**
 * Calculate frequency from period and speed
 * @param[in] speed Speed
 * @param[in] period Period
 * @return Frequency
 */
#define B0_PWM_OUTPUT_CALC_FREQ(speed, period) \
	(B0_PWM_CAST_FLOATING(speed) / B0_PWM_CAST_FLOATING(period))

/**
 * Calculate error.
 * @param[in] required_freq Required frequency (user need)
 * @param[in] calc_freq Calculated frequency (user can have)
 * @return Error (in percentage)
 * @note *Relative error*
 */
#define B0_PWM_OUTPUT_CALC_FREQ_ERR(required_freq, calc_freq)					\
	((fabsl(B0_PWM_CAST_FLOATING(calc_freq) - B0_PWM_CAST_FLOATING(required_freq))	\
		* 100) / B0_PWM_CAST_FLOATING(required_freq))

/**
 * @copydoc b0_modimpl_open
 * @memberof b0_pwm
 */
B0_API b0_result_code b0_pwm_open(b0_device *dev, b0_pwm **mod, int index);

/**
 * @copydoc b0_modimpl_close
 * @memberof b0_pwm
 */
B0_API b0_result_code b0_pwm_close(b0_pwm *mod);

/**
 * @memberof b0_pwm
 */
B0_API b0_result_code b0_pwm_speed_set(b0_pwm *mod, unsigned long value);

/**
 * @memberof b0_pwm
 */
B0_API b0_result_code b0_pwm_speed_get(b0_pwm *mod, unsigned long *value);

/**
 * @memberof b0_pwm
 */
B0_API b0_result_code b0_pwm_bitsize_set(b0_pwm *mod, unsigned int value);

/**
 * @memberof b0_pwm
 */
B0_API b0_result_code b0_pwm_bitsize_get(b0_pwm *mod, unsigned int *value);

/**
 * @memberof b0_pwm
 */
B0_API b0_result_code b0_pwm_width_set(b0_pwm *mod, unsigned int index,
				b0_pwm_reg width);

/**
 * @memberof b0_pwm
 */
B0_API b0_result_code b0_pwm_width_get(b0_pwm *mod, unsigned int index,
				b0_pwm_reg *width);

/**
 * @memberof b0_pwm
 */
B0_API b0_result_code b0_pwm_period_set(b0_pwm *mod, b0_pwm_reg period);

/**
 * @memberof b0_pwm
 */
B0_API b0_result_code b0_pwm_period_get(b0_pwm *mod, b0_pwm_reg *period);

/**
 * Calculate speed and period value
 * @param[in] freq user interested frequency
 * @param[out] speed best speed found
 * @param[out] period best period found
 * @param[in] error_max maximum error allowed
 * @param[in] best_result  Get the best result (that has lowest error)
 *     instead of *just* returning the
 *      first result that has error *less-than/equal* @a max_error.
 * @memberof b0_pwm
 * @note @a In best_result = true mode will do some extra calculation
 *    but the returned results are usually worth it!
 */
B0_API b0_result_code b0_pwm_output_calc(b0_pwm *mod, unsigned int bitsize,
		double freq, unsigned long *speed,
		b0_pwm_reg *period, double max_error, bool best_result);

/**
 * Prepare PWM for Output mode
 * @param mod PWM module
 * @memberof b0_pwm
 */
B0_API b0_result_code b0_pwm_output_prepare(b0_pwm *mod);

/**
 * @memberof b0_pwm
 */
B0_API b0_result_code b0_pwm_output_start(b0_pwm *mod);

/**
 * @memberof b0_pwm
 */
B0_API b0_result_code b0_pwm_output_stop(b0_pwm *mod);

__END_DECLS

#endif
