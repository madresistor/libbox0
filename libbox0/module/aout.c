/*
 * This file is part of libbox0.
 * Copyright (C) 2013-2016 Kuldeep Singh Dhaka <kuldeep@madresistor.com>
 *
 * libbox0 is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * libbox0 is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with libbox0.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "../libbox0-private.h"

/** @copydoc INTERNAL */

b0_result_code b0_aout_close(b0_aout *mod)
{
	b0_device *dev;
	B0_ASSERT_RETURN_ON_NULL_ARG(NULL, mod);

	dev = B0_MODULE_DEVICE(mod);
	free(B0_MODULE_FRONTEND_DATA(mod));

	B0_ASSERT_BACKEND_FUNC_NULL(dev, aout, close);
	b0_result_code r = B0_BACKEND(dev, aout).close(mod);

	return r;
}

b0_result_code b0_aout_open(b0_device *dev, b0_aout **mod, int index)
{
	B0_ASSERT_RETURN_ON_NULL_ARG(NULL, dev);
	B0_ASSERT_RETURN_ON_NULL_ARG(dev, mod);
	B0_ASSERT_RETURN_ON_FAIL_ARG(dev, index >= 0);

	B0_ASSERT_BACKEND_FUNC_NULL(dev, aout, open);
	b0_result_code r = B0_BACKEND(dev, aout).open(dev, mod, index);

	/* allocate frontend data */
	if (B0_SUCCESS_RESULT_CODE(r)) {
		b0_frontend_aout_data *frontend_data = calloc(1, sizeof(*frontend_data));
		(*mod)->header.frontend_data = frontend_data;
		if (B0_IS_NULL(frontend_data)) {
			r = B0_ERR_ALLOC;
			goto done;
		}

		frontend_data->cache.bitsize = 0;
	}

	done:
	return r;
}

b0_result_code b0_aout_chan_seq_set(b0_aout *mod,
			unsigned int *values, size_t count)
{
	b0_device *dev;

	B0_ASSERT_RETURN_ON_NULL_ARG(NULL, mod);
	dev = B0_MODULE_DEVICE(mod);

	B0_ASSERT_RETURN_ON_NULL_ARG(dev, values);
	B0_ASSERT_RETURN_ON_FAIL_ARG(dev, count > 0);

	B0_ASSERT_BACKEND_FUNC_NULL(dev, aout, chan_seq.set);
	return B0_BACKEND(dev, aout).chan_seq.set(mod, values, count);
}

b0_result_code b0_aout_chan_seq_get(b0_aout *mod,
			unsigned int *values, size_t *count)
{
	b0_device *dev;

	B0_ASSERT_RETURN_ON_NULL_ARG(NULL, mod);
	dev = B0_MODULE_DEVICE(mod);

	B0_ASSERT_RETURN_ON_NULL_ARG(dev, values);
	B0_ASSERT_RETURN_ON_NULL_ARG(dev, count);
	B0_ASSERT_RETURN_ON_FAIL_ARG(dev, count[0] > 0);

	B0_ASSERT_BACKEND_FUNC_NULL(dev, aout, chan_seq.get);
	return B0_BACKEND(dev, aout).chan_seq.get(mod, values, count);
}

b0_result_code b0_aout_bitsize_speed_set(b0_aout *mod,
			unsigned int bitsize, unsigned long speed)
{
	b0_result_code r;
	b0_device *dev;

	B0_ASSERT_RETURN_ON_NULL_ARG(NULL, mod);
	dev = B0_MODULE_DEVICE(mod);

	B0_ASSERT_RETURN_ON_FAIL_ARG(dev, bitsize > 0);
	B0_ASSERT_RETURN_ON_FAIL_ARG(dev, speed > 0);

	B0_ASSERT_BACKEND_FUNC_NULL(dev, aout, bitsize_speed.set);
	r = B0_BACKEND(dev, aout).bitsize_speed.set(mod, bitsize, speed);

	if (B0_SUCCESS_RESULT_CODE(r)) {
		b0_frontend_aout_data *frontend_data = B0_MODULE_FRONTEND_DATA(mod);
		frontend_data->cache.bitsize = bitsize;
	}

	return r;
}

b0_result_code b0_aout_bitsize_speed_get(b0_aout *mod,
			unsigned int *bitsize, unsigned long *speed)
{
	b0_result_code r;
	b0_frontend_aout_data *frontend_data = B0_MODULE_FRONTEND_DATA(mod);
	b0_device *dev;

	B0_ASSERT_RETURN_ON_NULL_ARG(NULL, mod);
	dev = B0_MODULE_DEVICE(mod);

	B0_ASSERT_RETURN_ON_FAIL_ARG(dev, !(bitsize == NULL && speed == NULL));

	B0_ASSERT_BACKEND_FUNC_NULL(dev, aout, bitsize_speed.get);
	r = B0_BACKEND(dev, aout).bitsize_speed.get(mod, bitsize, speed);

	if (B0_SUCCESS_RESULT_CODE(r) && B0_IS_NOT_NULL(bitsize)) {
		frontend_data->cache.bitsize = *bitsize;
	}

	return r;
}

b0_result_code b0_aout_repeat_set(b0_aout *mod, unsigned long value)
{
	b0_device *dev;

	B0_ASSERT_RETURN_ON_NULL_ARG(NULL, mod);
	dev = B0_MODULE_DEVICE(mod);

	B0_ASSERT_BACKEND_FUNC_NULL(dev, aout, repeat.set);
	return B0_BACKEND(dev, aout).repeat.set(mod, value);
}

b0_result_code b0_aout_repeat_get(b0_aout *mod, unsigned long *value)
{
	b0_device *dev;

	B0_ASSERT_RETURN_ON_NULL_ARG(NULL, mod);
	dev = B0_MODULE_DEVICE(mod);

	B0_ASSERT_RETURN_ON_NULL_ARG(dev, value)

	B0_ASSERT_BACKEND_FUNC_NULL(dev, aout, repeat.get);
	return B0_BACKEND(dev, aout).repeat.get(mod, value);
}

static int snapshot_start_ieee(b0_aout *mod, unsigned int ieee_binary_size,
	void *samples, size_t samples_count);

#define SNAPSHOT_START_ROUTE_GENERIC_CODE(fn_name)							\
	b0_device *dev;														\
	b0_frontend_aout_data *frontend_data = B0_MODULE_FRONTEND_DATA(mod);	\
	unsigned int bitsize = frontend_data->cache.bitsize;					\
	unsigned int bytes_per_sample = B0_BIT2BYTE(bitsize);					\
																			\
	B0_ASSERT_RETURN_ON_NULL_ARG(NULL, mod);								\
	dev = B0_MODULE_DEVICE(mod);											\
																			\
	/* test if data is not bogus */											\
	B0_ASSERT_RETURN_ON_NULL_ARG(dev, samples);								\
	B0_ASSERT_RETURN_ON_FAIL_ARG(dev, count > 0);							\
																			\
	/* will we exceed the buffer? */										\
	B0_ASSERT_RETURN_ON_FAIL_ARG(dev,										\
		(count * bytes_per_sample) <= mod->buffer_size);					\
																			\
	B0_ASSERT_BACKEND_FUNC_NULL(dev, aout, snapshot.fn_name);				\
	return B0_BACKEND(dev, aout).snapshot.fn_name(mod, samples, count);

b0_result_code b0_aout_snapshot_start(b0_aout *mod, void *samples, size_t count)
{
	SNAPSHOT_START_ROUTE_GENERIC_CODE(start)
}

b0_result_code b0_aout_snapshot_start_double(b0_aout *mod, double *samples, size_t count)
{
	SNAPSHOT_START_ROUTE_GENERIC_CODE(start_double)
}

b0_result_code b0_aout_snapshot_start_float(b0_aout *mod, float *samples, size_t count)
{
	SNAPSHOT_START_ROUTE_GENERIC_CODE(start_float)
}

static int snapshot_start_ieee(b0_aout *mod, unsigned int ieee_binary_size,
				void *samples, size_t samples_count)
{
	b0_result_code r;
	b0_device *dev;

	B0_ASSERT_RETURN_ON_NULL_ARG(NULL, mod);
	dev = B0_MODULE_DEVICE(mod);

	/* test if data is not bogus */
	B0_ASSERT_RETURN_ON_NULL_ARG(dev, samples);
	B0_ASSERT_RETURN_ON_FAIL_ARG(dev, samples_count > 0);

	b0_frontend_aout_data *frontend_data = B0_MODULE_FRONTEND_DATA(mod);
	unsigned int bitsize = frontend_data->cache.bitsize;
	unsigned int bytes_per_sample = B0_BIT2BYTE(bitsize);

	/* will we exceed the buffer? */
	B0_ASSERT_RETURN_ON_FAIL_ARG(dev,
		!((samples_count * bytes_per_sample) > mod->buffer_size));

	/* get a buffer containing the raw values */
	size_t binary_len = samples_count * bytes_per_sample;
	void *binary = B0_TEMP_BUFFER_ALLOC(binary_len);
	B0_ASSERT_RETURN_ON_NULL_ALLOC(dev, binary);

	r = b0_math_ieee_to_raw(dev, mod->ref.low, mod->ref.high,
		ieee_binary_size, samples, bitsize, binary, samples_count);
	if (B0_ERROR_RESULT_CODE(r)) {
		goto error;
	}

	r = b0_aout_snapshot_start(mod, binary, samples_count);

	error:
	B0_TEMP_BUFFER_FREE(binary);
	return r;
}

b0_result_code b0_generic_aout_snapshot_start_double(b0_aout *mod, double *data, size_t len)
{
	return snapshot_start_ieee(mod, 64, data, len);
}

b0_result_code b0_generic_aout_snapshot_start_float(b0_aout *mod, float *data, size_t len)
{
	return snapshot_start_ieee(mod, 32, data, len);
}

b0_result_code b0_aout_snapshot_prepare(b0_aout *mod)
{
	b0_device *dev;

	B0_ASSERT_RETURN_ON_NULL_ARG(NULL, mod);
	dev = B0_MODULE_DEVICE(mod);

	b0_frontend_aout_data *frontend_data = B0_MODULE_FRONTEND_DATA(mod);
	frontend_data->cache.bitsize = mod->snapshot.values[0].bitsize;

	B0_ASSERT_BACKEND_FUNC_NULL(dev, aout, snapshot.prepare);
	return B0_BACKEND(dev, aout).snapshot.prepare(mod);
}

b0_result_code b0_aout_snapshot_stop(b0_aout *mod)
{
	b0_device *dev;

	B0_ASSERT_RETURN_ON_NULL_ARG(NULL, mod);
	dev = B0_MODULE_DEVICE(mod);

	B0_ASSERT_BACKEND_FUNC_NULL(dev, aout, snapshot.stop);
	return B0_BACKEND(dev, aout).snapshot.stop(mod);
}

#define STREAM_WRITE_ROUTE_GENERIC_CODE(fn_name)							\
	b0_device *dev;															\
																			\
	B0_ASSERT_RETURN_ON_NULL_ARG(NULL, mod);								\
	dev = B0_MODULE_DEVICE(mod);											\
																			\
	B0_ASSERT_RETURN_ON_NULL_ARG(dev, samples);								\
	B0_ASSERT_RETURN_ON_FAIL_ARG(dev, count > 0);							\
																			\
	B0_ASSERT_BACKEND_FUNC_NULL(dev, aout, stream.fn_name);				\
	return B0_BACKEND(dev, aout).stream.fn_name(mod, samples, count);

b0_result_code b0_aout_stream_write(b0_aout *mod, void *samples, size_t count)
{
	STREAM_WRITE_ROUTE_GENERIC_CODE(write)
}

b0_result_code b0_aout_stream_write_double(b0_aout *mod, double *samples, size_t count)
{
	STREAM_WRITE_ROUTE_GENERIC_CODE(write_double)
}

b0_result_code b0_aout_stream_write_float(b0_aout *mod, float *samples, size_t count)
{
	STREAM_WRITE_ROUTE_GENERIC_CODE(write_float)
}

static b0_result_code stream_write_ieee(b0_aout *mod,
		unsigned int ieee_binary_size, void *samples, size_t samples_count)
{
	b0_device *dev;
	b0_result_code r;

	B0_ASSERT_RETURN_ON_NULL_ARG(NULL, mod);
	dev = B0_MODULE_DEVICE(mod);

	B0_ASSERT_RETURN_ON_NULL_ARG(dev, samples);
	B0_ASSERT_RETURN_ON_FAIL_ARG(dev, samples_count > 0);

	b0_frontend_aout_data *frontend_data = B0_MODULE_FRONTEND_DATA(mod);
	unsigned int bitsize = frontend_data->cache.bitsize;
	unsigned int bytes_per_sample = B0_BIT2BYTE(bitsize);

	/* allocate */
	void *tmp_buf = B0_TEMP_BUFFER_ALLOC(samples_count * bytes_per_sample);
	B0_ASSERT_RETURN_ON_NULL_ALLOC(dev, tmp_buf);

	/* convert */
	r = b0_math_ieee_to_raw(dev, mod->ref.low, mod->ref.high,
			ieee_binary_size, samples, bitsize,
			tmp_buf, samples_count);
	if (B0_ERROR_RESULT_CODE(r)) {
		goto error;
	}

	/* send */
	r = b0_aout_stream_write(mod, tmp_buf, samples_count);
	if (B0_ERROR_RESULT_CODE(r)) {
		goto error;
	}

	r = B0_OK;

	error:
	B0_TEMP_BUFFER_FREE(tmp_buf);
	return r;
}

b0_result_code b0_generic_aout_stream_write_double(b0_aout *mod,
			double *samples, size_t samples_count)
{
	return stream_write_ieee(mod, 64, samples, samples_count);
}

b0_result_code b0_generic_aout_stream_write_float(b0_aout *mod,
			float *samples, size_t samples_count)
{
	return stream_write_ieee(mod, 32, samples, samples_count);
}

b0_result_code b0_aout_stream_prepare(b0_aout *mod)
{
	b0_device *dev;

	B0_ASSERT_RETURN_ON_NULL_ARG(NULL, mod);
	dev = B0_MODULE_DEVICE(mod);

	b0_frontend_aout_data *frontend_data = B0_MODULE_FRONTEND_DATA(mod);
	frontend_data->cache.bitsize = mod->stream.values[0].bitsize;

	B0_ASSERT_BACKEND_FUNC_NULL(dev, aout, stream.prepare);
	return B0_BACKEND(dev, aout).stream.prepare(mod);
}

b0_result_code b0_aout_stream_start(b0_aout *mod)
{
	b0_device *dev;

	B0_ASSERT_RETURN_ON_NULL_ARG(NULL, mod);
	dev = B0_MODULE_DEVICE(mod);

	B0_ASSERT_BACKEND_FUNC_NULL(dev, aout, stream.start);
	return B0_BACKEND(dev, aout).stream.start(mod);
}

b0_result_code b0_aout_stream_stop(b0_aout *mod)
{
	b0_device *dev;

	B0_ASSERT_RETURN_ON_NULL_ARG(NULL, mod);
	dev = B0_MODULE_DEVICE(mod);

	B0_ASSERT_BACKEND_FUNC_NULL(dev, aout, stream.stop);
	return B0_BACKEND(dev, aout).stream.stop(mod);
}

b0_result_code b0_aout_snapshot_calc(b0_aout *mod, double freq,
		unsigned int bitsize, size_t *count_out, unsigned long *speed_out)
{
	b0_device *dev;
	unsigned long best_speed = 0, best_count = 0, max_count;
	unsigned int bytes_per_sample;
	size_t i, j;

	B0_ASSERT_RETURN_ON_NULL_ARG(NULL, mod);
	dev = B0_MODULE_DEVICE(mod);

	/* verify arguments */
	B0_ASSERT_RETURN_ON_FAIL_ARG(dev, freq > 0);
	B0_ASSERT_RETURN_ON_FAIL_ARG(dev, bitsize > 0);
	B0_ASSERT_RETURN_ON_NULL_ARG(dev, count_out);
	B0_ASSERT_RETURN_ON_NULL_ARG(dev, speed_out);

	/* Maximum number of samples we can hold */
	bytes_per_sample = B0_BIT2BYTE(bitsize);
	max_count = mod->buffer_size / bytes_per_sample;

	for (i = 0; i < mod->snapshot.count; i++) {
		struct b0_aout_bitsize_speeds *value = &mod->snapshot.values[i];

		/* Bitsize not matched */
		if (value->bitsize != bitsize) {
			continue;
		}

		for (j = 0; j < value->speed.count; j++) {
			unsigned long speed = value->speed.values[j];
			size_t count = speed / freq;

			/* Test fisibility */
			if (count > max_count) {
				continue;
			}

			/* Test betterness */
			if (count > best_count) {
				best_count = count;
				best_speed = speed;
			}
		}
	}

	/* we found any result? */
	B0_ASSERT_RETURN_ON_FAIL(dev, best_count, B0_FAIL_CALC);

	/* return back the result */
	*count_out = best_count;
	*speed_out = best_speed;
	return B0_OK;
}

/** @endcond */
