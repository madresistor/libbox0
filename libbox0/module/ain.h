/*
 * This file is part of libbox0.
 * Copyright (C) 2013-2016 Kuldeep Singh Dhaka <kuldeep@madresistor.com>
 *
 * libbox0 is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * libbox0 is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with libbox0.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef LIBBOX0_MODULE_AIN_H
#define LIBBOX0_MODULE_AIN_H

#include <stdint.h>
#include <stddef.h>
#include "../common.h"
#include "module.h"

__BEGIN_DECLS

enum b0_ain_capab {
	B0_AIN_CAPAB_FORMAT_2COMPL = (1 << 0),
	B0_AIN_CAPAB_FORMAT_BINARY = (0 << 0),

	B0_AIN_CAPAB_ALIGN_LSB = (0 << 1),
	B0_AIN_CAPAB_ALIGN_MSB = (1 << 1),

	B0_AIN_CAPAB_ENDIAN_LITTLE = (0 << 2),
	B0_AIN_CAPAB_ENDIAN_BIG = (1 << 2)
};

typedef enum b0_ain_capab b0_ain_capab;

/**
 * Analog In
 * @extends b0_module
 */
struct b0_ain {
	b0_module header;

	unsigned int chan_count;
	size_t buffer_size;
	b0_ain_capab capab;

	struct {
		uint8_t **chan;
	} label;

	struct {
		double high, low;
		b0_ref_type type;
	} ref;

	/* Practically ADC have a relationship between bitsize and speed
	 * As the bitsize increase, the the sampling frequency (speed) decrease
	 */
	struct b0_ain_mode_bitsize_speeds {
		struct b0_ain_bitsize_speeds {
			unsigned int bitsize;
			struct {
				unsigned long *values;
				size_t count;
			} speed;
		} *values;
		size_t count;
	} stream, snapshot;
};

typedef struct b0_ain b0_ain;

/**
 * @class b0_ain
 *
 * @startuml "Analog In States"
 *
 * [*] -d-> Opened : b0_ain_open()
 * Opened -d-> Closed : b0_ain_close()
 * Closed -d-> [*]

 * Opened -r-> Stream
 * Stream -l-> Opened
 *
 * Opened -l-> Snapshot
 * Snapshot -r-> Opened
 *
 * state Stream {
 *  state "Prepared" as StreamStopped
 *  state "Running" as StreamRunning
 *  state "Stopped" as StreamStopped
 *
 *  [*] -d-> StreamStopped : b0_ain_stream_prepare()
 *
 *  StreamStopped -d-> StreamRunning : b0_ain_stream_start()
 *  StreamStopped -u-> StreamRunning : b0_ain_stream_start()
 *  StreamRunning -d-> StreamStopped : b0_ain_stream_stop()
 *  StreamStopped --> [*]
 *
 *  StreamStopped -u-> StreamStopped : b0_ain_{bitsize_speed, chan_seq}_{set, get}()
 *  StreamRunning -u-> StreamRunning : b0_ain_stream_read{, float, double}()
 * }
 *
 * state Snapshot {
 *  state "Running" as SnapshotRunning
 *  state "Stopped" as SnapshotStopped
 *
 *  [*] -d-> SnapshotStopped : b0_ain_snapshot_prepare()
 *  SnapshotStopped -d-> SnapshotRunning : b0_ain_snapshot_start()
 *  SnapshotRunning -u-> SnapshotStopped : b0_ain_snapshot_stop(), \n [Acquisition complete]
 *  SnapshotStopped --> [*]
 *
 *  SnapshotStopped -u-> SnapshotStopped : b0_{speed, bitsize, chan_seq}_{set, get}()
 * }
 *
 * @enduml
 *
 * @section ain_intro Introduction
 * AIN can work into two mode.
 * - \ref ain_snapshot
 * - \ref ain_stream
 * @{
 */

/**
 * @copydoc b0_modimpl_open
 * @see b0_ain_stream_prepare()
 * @see b0_ain_snapshot_prepare()
 * @see b0_ain_close()
 * @memberof b0_ain
 */
B0_API b0_result_code b0_ain_open(b0_device *dev, b0_ain **mod, int index);

/**
 * @copydoc b0_modimpl_close
 * @memberof b0_ain
 */
B0_API b0_result_code b0_ain_close(b0_ain *mod);

/**
 * @name Stream
 * @{
 *
 * @section ain_stream Stream
 *
 * stream mode is designed for continuous data transfer between application and module.
 *
 * how to use:
 *  1. prepare for streaming b0_ain_stream_prepare()
 *  2. apply configuration
 *   - channel sequence b0_ain_chan_seq_set()
 *   - speed and bitsize b0_ain_bitsize_speed()
 *  3. start the module b0_ain_stream_start()
 *
 * when the application want to stop the data transfer,
 * 	it can call stop b0_ain_stream_stop().
 */

/**
 * Set the channel sequence
 * @param mod AIN module
 * @param values Values
 * @param count Number of entries in @a values
 * @return result code
 */
B0_API b0_result_code b0_ain_chan_seq_set(b0_ain *mod,
			unsigned int *values, size_t count);

/**
 * get the channel sequence
 * @param mod AIN module
 * @param[out] values Values
 * @param[in,out] count Number of entries in @a values
 * @return result code
 * @note @a count contain the number of entries @a values can hold
 */
B0_API b0_result_code b0_ain_chan_seq_get(b0_ain *mod,
			unsigned int *values, size_t *count);

/**
 * Set the @a bitsize and @a speed of acquisition
 * @param mod AIN module
 * @param bitsize Bitsize
 * @param speed Speed (sampling frequency)
 * @return result code
 */
B0_API b0_result_code b0_ain_bitsize_speed_set(b0_ain *mod,
			unsigned int bitsize, unsigned long speed);

/**
 * Get the @a bitsize and @a speed of acquisition
 * @param mod AIN module
 * @param[out] bitsize Bitsize
 * @param[out] speed Speed (sampling frequency)
 * @return result code
 */
B0_API b0_result_code b0_ain_bitsize_speed_get(b0_ain *mod,
			unsigned int *bitsize, unsigned long *speed);

/**
 * prepare AIN module for stream data transfer.
 * @param[in] mod AIN module
 * @note all previous config will be cleared
 * @return result code
 * @see b0_ain_stream_start()
 * @see b0_ain_open()
 * @memberof b0_ain
 */
B0_API b0_result_code b0_ain_stream_prepare(b0_ain *mod);

/**
 * start AIN streaming
 * @note after entering in this function, read can be anytime performed.
 * @param[in] mod AIN module
 * @return result code
 * @see b0_ain_stream_prepare()
 * @see b0_ain_stream_stop()
 * @see b0_ain_stream_start()
 * @see b0_ain_stream_start_float()
 * @memberof b0_ain
 */
B0_API b0_result_code b0_ain_stream_start(b0_ain *mod);

/**
 * Read samples from stream data
 * @param[in] mod libbox0 ain module
 * @param[out] samples Buffer
 * @param[in] count number of samples to read
 * @param[out] actual_count number of samples readed (see note)
 * @return result code
 * @note if @a actual_count is NULL,
 *  this function will block till the @a count is readed (blocking mode)
 * @note if @a actual_count is _not_ NULL,
 *  this function will read till @a count is reached.
 *  if no data is available internally, this will return (non-blocking mode)
 * @note <b>Application code has to handle endianness</b>
 * @see b0_ain_stream_read_double()
 * @see b0_ain_stream_read_float()
 * @memberof b0_ain
 */
B0_API b0_result_code b0_ain_stream_read(b0_ain *mod, void *samples,
	size_t count, size_t *actual_count);

/**
 * Read samples from stream data (in floating point) [IEEE 32bit]
 * @param[in] mod libbox0 ain module
 * @param[out] samples Buffer
 * @param[in] count number of samples to read
 * @param[out] actual_count number of samples readed (see note)
 * @return result code
 * @note if @a actual_count is NULL,
 *  this function will block till the @a count is readed (blocking mode)
 * @note if @a actual_count is _not_ NULL,
 *  this function will read till @a count is reached.
 *  if no data is available internally, this will return (non-blocking mode)
 * @see b0_ain_stream_read()
 * @see b0_ain_stream_read_double()
 * @memberof b0_ain
 */
B0_API b0_result_code b0_ain_stream_read_float(b0_ain *mod, float *samples,
	size_t count, size_t *actual_count);

/**
 * Read samples from stream data (in floating point) [IEEE 64bit]
 * @param[in] mod libbox0 ain module
 * @param[out] samples Buffer
 * @param[in] count number of samples to read
 * @param[out] actual_count number of samples readed (see note)
 * @return result code
 * @note if @a actual_count is NULL,
 *  this function will block till the @a count is readed (blocking mode)
 * @note if @a actual_count is _not_ NULL,
 *  this function will read till @a count is reached.
 *  if no data is available internally, this will return (non-blocking mode)
 * @see b0_ain_stream_read_double()
 * @see b0_ain_stream_read()
 * @memberof b0_ain
 */
B0_API b0_result_code b0_ain_stream_read_double(b0_ain *mod, double *samples,
	size_t count, size_t *actual_count);

/**
 * stop streaming
 * @param[in] mod libbox0 ain module
 * @return result code
 * @memberof b0_ain
 */
B0_API b0_result_code b0_ain_stream_stop(b0_ain *mod);

/**
 * @}
 */

/**
 * @name Snapshot
 * @{
 *
 * @section ain_snapshot Snapshot
 *
 * snapshot is meant for application where reading from device is done but without the overhead of stream.
 * snapshot mode is simple,
 *  1. prepare the module by calling b0_ain_snapshot_prepare()
 *  2. set configuration
 *    - b0_ain_chan_seq_set()
 *    - b0_ain_bitsize_speed_set()
 *  3. start the acquisition process b0_ain_snapshot_start()
 *
 * @note 3rd step can be performed multiple time
 *  without having to call step 1st and 2nd
 *
 * @note configuration can be applied anytime, assuming that module is prepared and not running
 *
 * @note their is a function b0_ain_snapshot_stop(). this will send a STOP command to device.
 *  this is meant for sending a stop command and has no significant purpose
 */

/**
 * Prepare the AIN module for snapshot mode
 * @param[in] mod AIN module
 * @return result code
 *
 * @see b0_ain_snapshot_start()
 * @see b0_ain_open()
 * @memberof b0_ain
 */
B0_API b0_result_code b0_ain_snapshot_prepare(b0_ain *mod);

/**
 * read samples from AIN module
 * 		this will convert the raw data to double values
 * @param[in] mod AIN module
 * @param[out] samples pointer to memory to store the samples
 * @param[in] count the number of sample to aquire from device
 * @note count should not overflow the device buffer,
 * 			see b0_buffer property to estimate the
 * 			number of maximum samples that can be aquired
 * @note <b>Application code has to handle endianness</b>
 * @note <b>blocking call</b>
 * @return result code
 * @see b0_ain_snapshot_prepare()
 * @memberof b0_ain
 */
B0_API b0_result_code b0_ain_snapshot_start(b0_ain *mod,
			void *samples, size_t count);

/**
 * read sample from AIN module.
 * 		this will convert the raw data to double values
 * @param[in] mod AIN module
 * @param[out] samples pointer to memory to store the samples
 * @param[in] count the number of sample to aquire from device
 * @note count should not overflow the device buffer,
 * 			see b0_buffer property to estimate the
 * 			number of maximum samples that can be aquired
 * @note <b>blocking call</b>
 * @return result code
 * @see b0_ain_snapshot_prepare()
 * @memberof b0_ain
 */
B0_API b0_result_code b0_ain_snapshot_start_double(b0_ain *mod,
			double *samples, size_t count);

/**
 * read samples from AIN module
 * 		this will convert the raw data to double values
 * @param[in] mod AIN module
 * @param[out] samples pointer to memory to store the samples
 * @param[in] count the number of sample to aquire from device
 * @note count should not overflow the device buffer,
 * 			see b0_buffer property to estimate the
 * 			number of maximum samples that can be aquired
 * @note <b>blocking call</b>
 * @return result code
 * @see b0_ain_snapshot_prepare()
 * @memberof b0_ain
 */
B0_API b0_result_code b0_ain_snapshot_start_float(b0_ain *mod,
			float *samples, size_t count);

/**
 * send a STOP command to AIN module
 * 		this function is pretty much useless and should be ignored.
 * 		this included to make the API complete and pretty.
 * @param[in] mod AIN module
 * @return result code
 *
 * @see b0_ain_snapshot_start()
 * @see b0_ain_snapshot_prepare()
 *
 * @note this function is just to place a similar API to stream (send a stop to device)
 *   but since snapshot_start is blocking, this is useless
 *
 * @note it is mostly useless unless some error occured while running and stop need to be send
 * @memberof b0_ain
 */
B0_API b0_result_code b0_ain_snapshot_stop(b0_ain *mod);

/**
 * @}
 */

/**
 * @}
 */

__END_DECLS

#endif
