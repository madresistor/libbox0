/*
 * This file is part of libbox0.
 * Copyright (C) 2013-2016 Kuldeep Singh Dhaka <kuldeep@madresistor.com>
 *
 * libbox0 is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * libbox0 is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with libbox0.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef LIBBOX0_MODULE_AOUT_H
#define LIBBOX0_MODULE_AOUT_H

#include <stdint.h>
#include <stddef.h>
#include "../common.h"
#include "module.h"

__BEGIN_DECLS

enum b0_aout_capab {
	B0_AOUT_CAPAB_FORMAT_2COMPL = (1 << 0),
	B0_AOUT_CAPAB_FORMAT_BINARY = (0 << 0),

	B0_AOUT_CAPAB_ALIGN_LSB = (0 << 1),
	B0_AOUT_CAPAB_ALIGN_MSB = (1 << 1),

	B0_AOUT_CAPAB_ENDIAN_LITTLE = (0 << 2),
	B0_AOUT_CAPAB_ENDIAN_BIG = (1 << 2),

	/* Repeat supported */
	B0_AOUT_CAPAB_REPEAT = (1 << 3)
};

typedef enum b0_aout_capab b0_aout_capab;

/**
 * Analog Out
 * @extends b0_module
 */
struct b0_aout {
	b0_module header;

	unsigned chan_count;
	size_t buffer_size;
	b0_aout_capab capab;

	struct {
		uint8_t **chan;
	} label;

	struct {
		double high, low;
		b0_ref_type type;
	} ref;

	/* Practically DAC have a relationship between bitsize and speed
	 * As the bitsize increase, the the sampling frequency (speed) decrease
	 */
	struct b0_aout_mode_bitsize_speeds {
		struct b0_aout_bitsize_speeds {
			unsigned int bitsize;
			struct {
				unsigned long *values;
				size_t count;
			} speed;
		} *values;
		size_t count;
	} stream, snapshot;
};

typedef struct b0_aout b0_aout;

/**
 * @class b0_aout
 *
 * @startuml "Analog Out States"
 *
 * [*] -d-> Opened : b0_aout_open()
 * Opened -d-> Closed : b0_aout_close()
 * Closed -d-> [*]

 * Opened -r-> Stream
 * Stream -l-> Opened
 *
 * Opened -l-> Snapshot
 * Snapshot -r-> Opened
 *
 * state Stream {
 *  state "Running" as StreamRunning
 *  state "Stopped" as StreamStopped
 *
 *  [*] -d-> StreamStopped : b0_aout_stream_prepare()
 *
 *  StreamStopped -u-> StreamRunning : b0_aout_stream_start()
 *  StreamRunning -d-> StreamStopped : b0_aout_stream_stop()
 *  StreamStopped --> [*]
 *
 *  StreamStopped -u-> StreamStopped : b0_aout_{bitsize_speed, repeat, chan_seq}_{set, get}()
 *
 *  StreamRunning -u-> StreamRunning: b0_aout_stream_write{, float, double}()
 * }
 *
 * state Snapshot {
 *  state "Running" as SnapshotRunning
 *  state "Stopped" as SnapshotStopped
 *
 *  [*] -d-> SnapshotStopped : b0_aout_snapshot_prepare()
 *  SnapshotStopped -d-> SnapshotRunning : b0_aout_snapshot_start()
 *  SnapshotRunning -u-> SnapshotStopped : b0_aout_snapshot_stop(), \n [Repeat complete]
 *  SnapshotStopped --> [*]
 *
 *  SnapshotStopped -u-> SnapshotStopped : b0_aout_{bitsize_speed, chan_seq, repeat}_{set, get}()
 * }
 *
 * @enduml
 *
 * @section aout_intro Introduction
 *  AOUT can operate in two mode
 *  - \ref aout_snapshot
 *  - \ref aout_stream
 * @{
 */

/**
 * @copydoc b0_modimpl_open
 * @see b0_aout_stream_prepare()
 * @see b0_aout_snapshot_prepare()
 * @see b0_aout_close()
 * @memberof b0_aout
 */
B0_API b0_result_code b0_aout_open(b0_device *dev, b0_aout **mod, int index);

/**
 * @copydoc b0_modimpl_close
 * @see b0_aout_open()
 * @memberof b0_aout
 */
B0_API b0_result_code b0_aout_close(b0_aout *mod);

/**
 * Set the channel sequence
 * @param mod AOUT module
 * @param values Values
 * @param count Number of entries in @a values
 * @return result code
 */
B0_API b0_result_code b0_aout_chan_seq_set(b0_aout *mod, unsigned int *values,
			size_t count);

/**
 * get the channel sequence
 * @param mod AOUT module
 * @param[out] values Values
 * @param[in,out] count Number of entries in @a values
 * @return result code
 * @note @a count contain the number of entries @a values can hold
 */
B0_API b0_result_code b0_aout_chan_seq_get(b0_aout *mod, unsigned int *values,
			size_t *count);

/**
 * Set the @a bitsize and @a speed of acquisition
 * @param mod AOUT module
 * @param bitsize Bitsize
 * @param speed Speed (sampling frequency)
 * @return result code
 */
B0_API b0_result_code b0_aout_bitsize_speed_set(b0_aout *mod,
				unsigned int bitsize, unsigned long speed);

/**
 * Get the @a bitsize and @a speed of acquisition
 * @param mod AOUT module
 * @param[out] bitsize Bitsize
 * @param[out] speed Speed (sampling frequency)
 * @return result code
 */
B0_API b0_result_code b0_aout_bitsize_speed_get(b0_aout *mod,
			unsigned int *bitsize, unsigned long *speed);

/**
 * Set the number of time to repeat
 * @param mod AOUT module
 * @param value Repeat value
 * @return result code
 * @note Only apply to snapshot mode
 * @note If @a value is 0, repeat is disabled
 */
B0_API b0_result_code b0_aout_repeat_set(b0_aout *mod, unsigned long value);

/**
 * Get the number of time to repeat
 * @param mod AOUT module
 * @param[out] value Repeat value
 * @return result code
 * @note Only apply to snapshot mode
 * @note If @a value is 0, repeat is disabled
 */
B0_API b0_result_code b0_aout_repeat_get(b0_aout *mod, unsigned long *value);

/**
 * @name Stream
 * @{
 *
 * @section aout_stream Stream
 * In AOUT stream, a waveform is streamed to device.
 * procedure:
 *  1. Prepare the module b0_aout_stream_prepare()
 *  2. apply configuration
 *   - b0_aout_chan_seq_set()
 *   - b0_aout_speed_bitsize_set()
 *  3. Start the module b0_aout_stream_start()
 *
 * when application want to stop streaming, it should call b0_aout_stream_stop()
 */
/**
 * @memberof b0_aout
 */
B0_API b0_result_code b0_aout_stream_prepare(b0_aout *mod);

/**
 * @memberof b0_aout
 */
B0_API b0_result_code b0_aout_stream_start(b0_aout *mod);

/**
 * @memberof b0_aout
 */
B0_API b0_result_code b0_aout_stream_stop(b0_aout *mod);

/**
 * Write to AOUT Stream
 * @param[in] mod AOUT module
 * @param[in] data Buffer
 * @param[in] len Number of samples
 * @return result code
 * @note **Application code has to handle endianness**
 * @note b0_aout_stream_write_double()
 * @note b0_aout_stream_write_float()
 * @memberof b0_aout
 */
B0_API b0_result_code b0_aout_stream_write(b0_aout *mod, void *data, size_t len);

/**
 * Write to AOUT Stream [IEEE 64bit]
 * @param[in] mod AOUT module
 * @param[in] data Buffer
 * @param[in] len Number of samples
 * @return result code
 * @note b0_aout_stream_write()
 * @note b0_aout_stream_write_float()
 * @memberof b0_aout
 */
B0_API b0_result_code b0_aout_stream_write_double(b0_aout *mod, double *data, size_t len);

/**
 * Write to AOUT Stream [IEEE 32bit]
 * @param[in] mod AOUT module
 * @param[in] data Buffer
 * @param[in] len Number of samples
 * @return result code
 * @note b0_aout_stream_write()
 * @note b0_aout_stream_write_double()
 * @memberof b0_aout
 */
B0_API b0_result_code b0_aout_stream_write_float(b0_aout *mod, float *data, size_t len);

/**
 * @}
 */

/**
 * @name Snapshot
 * @{
 *
 * @section aout_snapshot Snapshot
 * In AOUT snapshot, a waveform is written to device memory and outputed.
 * procedure:
 *  1. Prepare the module b0_aout_snapshot_prepare()
 *  2. apply configuration
 *   - b0_aout_bitsize_speed_set()
 *   - b0_aout_chan_seq_set()
 *  3. start the module b0_aout_start()
 *
 * @note 3rd step can be performed multiple times
 * @note configuration can be applied anytime,
 *  assuming that the module is prepared and not running
 */
/**
 * @memberof b0_aout
 */
B0_API b0_result_code b0_aout_snapshot_prepare(b0_aout *mod);

/**
 * @memberof b0_aout
 */
B0_API b0_result_code b0_aout_snapshot_start(b0_aout *mod, void *data,
				size_t samples_count);

/**
 * @memberof b0_aout
 */
B0_API b0_result_code b0_aout_snapshot_stop(b0_aout *mod);

/**
 * @memberof b0_aout
 */
B0_API b0_result_code b0_aout_snapshot_start_float(b0_aout *mod,
					float *data, size_t len);

/**
 * @memberof b0_aout
 */
B0_API b0_result_code b0_aout_snapshot_start_double(b0_aout *mod,
					double *data, size_t len);

/**
 * Calculate the speed where the sample count is maximum for snapshot mode
 * @param mod AOUT module
 * @param freq Frequency of the signal
 * @param bitsize Bitsize to be used
 * @param[out] count The best value of count
 * @param[out] speed The crossponding speed for count and frequency
 * @return result code
 * @memberof b0_aout
 */
B0_API b0_result_code b0_aout_snapshot_calc(b0_aout *mod, double freq,
			unsigned int bitsize, size_t *count, unsigned long *speed);

/**
 * @}
 */

/**
 * @}
 */

__END_DECLS

#endif
