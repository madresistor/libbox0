/*
 * This file is part of libbox0.
 * Copyright (C) 2013-2016 Kuldeep Singh Dhaka <kuldeep@madresistor.com>
 *
 * libbox0 is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * libbox0 is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with libbox0.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef LIBBOX0_MODULE_H
#define LIBBOX0_MODULE_H

#include "../device.h"
#include "../common.h"

__BEGIN_DECLS

/**
 * @file
 */

enum b0_module_type {
	B0_DIO = 1,
	B0_AOUT = 2,
	B0_AIN = 3,
	B0_SPI = 4,
	B0_I2C = 5,
	B0_PWM = 6,
};

typedef enum b0_module_type b0_module_type;

/**
 * Module
 */
struct b0_module {
	/** Type */
	b0_module_type type;

	/** Index */
	int index;

	/** Name */
	uint8_t *name;

	/** Parent device */
	b0_device *device;

	/**
	 * Backend data
	 * @protected
	 */
	void *backend_data;

	/**
	 * Frontend data
	 * @protected
	 */
	void *frontend_data;
};

/* Already done in device.h */
/* typedef struct b0_module b0_module; */

/**
 * Return B0_OK if the module is openable.
 * If the module is not openable, return B0_ERR_UNAVAIL.
 * (can return other errors too..)
 * @warning if B0_OK is returned, DO NOT assume that module is openable.
 *   It only give you *HINT* if it worth trying to open.
 * @param mod Module
 */
B0_API b0_result_code b0_module_openable(b0_module *mod);

/** search generic module from device
 * @param[in] dev device
 * @param[out] mod search result
 * @param[in] type type of module
 * @param[in] index index of module
 * @return result code
 */
B0_API b0_result_code b0_module_search(b0_device *dev, b0_module **mod,
							b0_module_type type, int index);

#if defined(__DOXYGEN__)
/* common documentation for all modules */
typedef struct b0_modimpl b0_modimpl;

/**
 * @brief close the module
 * @details resources are free'd on closing
 * @param[in] mod module to close
 * @return result code
 */
b0_result_code b0_modimpl_close(b0_modimpl *mod);

/**
 * @brief open an module
 * @param[in] dev device to search for module
 * @param[out] mod pointer to location to store the pointer
 * @param[in] index module index
 * @return result code
 */
b0_result_code b0_modimpl_open(b0_device *dev, b0_modimpl **mod, int index);

#endif

__END_DECLS

#endif
