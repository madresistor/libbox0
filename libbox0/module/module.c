/*
 * This file is part of libbox0.
 * Copyright (C) 2013-2016 Kuldeep Singh Dhaka <kuldeep@madresistor.com>
 *
 * libbox0 is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * libbox0 is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with libbox0.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "../libbox0-private.h"

/** @cond INTERNAL */

/** @endcond */

b0_result_code b0_module_search(b0_device *dev, b0_module **mod,
					b0_module_type type, int index)
{
	B0_ASSERT_RETURN_ON_NULL_ARG(NULL, dev);
	B0_ASSERT_RETURN_ON_NULL_ARG(dev, mod);
	B0_ASSERT_RETURN_ON_FAIL_ARG(dev, index >= 0);
	B0_ASSERT_RETURN_ON_FAIL_ARG(dev, type > 0);

	size_t i;
	for (i = 0; i < dev->modules_len; i++) {
		if (dev->modules[i]->type == type && dev->modules[i]->index == index) {
				*mod = dev->modules[i];
				return B0_OK;
		}
	}

	return B0_ERR;
}


b0_result_code b0_module_openable(b0_module *mod)
{
	b0_device *dev;

	B0_ASSERT_RETURN_ON_NULL_ARG(NULL, mod);
	dev = B0_MODULE_DEVICE(mod);

	B0_ASSERT_BACKEND_FUNC_NULL(dev, module, openable);
	return B0_BACKEND(dev, module).openable(mod);
}
