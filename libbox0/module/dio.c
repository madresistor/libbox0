/*
 * This file is part of libbox0.
 * Copyright (C) 2013-2016 Kuldeep Singh Dhaka <kuldeep@madresistor.com>
 *
 * libbox0 is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * libbox0 is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with libbox0.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "../libbox0-private.h"

/** @copydoc INTERNAL */

b0_result_code b0_dio_open(b0_device *dev, b0_dio **mod, int index)
{
	B0_ASSERT_RETURN_ON_NULL_ARG(NULL, dev);
	B0_ASSERT_RETURN_ON_NULL_ARG(dev, mod);
	B0_ASSERT_RETURN_ON_FAIL_ARG(dev, index >= 0);

	B0_ASSERT_BACKEND_FUNC_NULL(dev, dio, open);
	return B0_BACKEND(dev, dio).open(dev, mod, index);
}

b0_result_code b0_dio_close(b0_dio *mod)
{
	b0_device *dev;

	B0_ASSERT_RETURN_ON_NULL_ARG(NULL, mod);
	dev = B0_MODULE_DEVICE(mod);

	B0_ASSERT_BACKEND_FUNC_NULL(dev, dio, close);
	return B0_BACKEND(dev, dio).close(mod);
}

b0_result_code b0_dio_basic_prepare(b0_dio *mod)
{
	b0_device *dev;

	B0_ASSERT_RETURN_ON_NULL_ARG(NULL, mod);
	dev = B0_MODULE_DEVICE(mod);

	B0_ASSERT_BACKEND_FUNC_NULL(dev, dio, basic.prepare);
	return B0_BACKEND(dev, dio).basic.prepare(mod);
}

b0_result_code b0_dio_basic_start(b0_dio *mod)
{
	b0_device *dev;

	B0_ASSERT_RETURN_ON_NULL_ARG(NULL, mod);
	dev = B0_MODULE_DEVICE(mod);

	B0_ASSERT_BACKEND_FUNC_NULL(dev, dio, basic.start);
	return B0_BACKEND(dev, dio).basic.start(mod);
}

b0_result_code b0_dio_basic_stop(b0_dio *mod)
{
	b0_device *dev;

	B0_ASSERT_RETURN_ON_NULL_ARG(NULL, mod);
	dev = B0_MODULE_DEVICE(mod);

	B0_ASSERT_BACKEND_FUNC_NULL(dev, dio, basic.stop);
	return B0_BACKEND(dev, dio).basic.stop(mod);
}

b0_result_code b0_dio_value_get(b0_dio *mod, unsigned int pin, bool *value)
{
	b0_device *dev;

	B0_ASSERT_RETURN_ON_NULL_ARG(NULL, mod);
	dev = B0_MODULE_DEVICE(mod);

	B0_ASSERT_RETURN_ON_FAIL_ARG(dev, pin < mod->pin_count);
	B0_ASSERT_RETURN_ON_NULL_ARG(dev, value);

	B0_ASSERT_BACKEND_FUNC_NULL(dev, dio, single.value.get);
	return B0_BACKEND(dev, dio).single.value.get(mod, pin, value);
}

b0_result_code b0_dio_value_set(b0_dio *mod, unsigned int pin, bool value)
{
	b0_device *dev;

	B0_ASSERT_RETURN_ON_NULL_ARG(NULL, mod);
	dev = B0_MODULE_DEVICE(mod);

	B0_ASSERT_RETURN_ON_FAIL_ARG(dev, pin < mod->pin_count);

	B0_ASSERT_BACKEND_FUNC_NULL(dev, dio, single.value.set);
	return B0_BACKEND(dev, dio).single.value.set(mod, pin, value);
}

b0_result_code b0_dio_value_toggle(b0_dio *mod, unsigned int pin)
{
	b0_device *dev;

	B0_ASSERT_RETURN_ON_NULL_ARG(NULL, mod);
	dev = B0_MODULE_DEVICE(mod);

	B0_ASSERT_RETURN_ON_FAIL_ARG(dev, pin < mod->pin_count);

	B0_ASSERT_BACKEND_FUNC_NULL(dev, dio, single.value.toggle);
	return B0_BACKEND(dev, dio).single.value.toggle(mod, pin);
}

b0_result_code b0_dio_dir_get(b0_dio *mod, unsigned int pin, bool *value)
{
	b0_device *dev;

	B0_ASSERT_RETURN_ON_NULL_ARG(NULL, mod);
	dev = B0_MODULE_DEVICE(mod);

	B0_ASSERT_RETURN_ON_FAIL_ARG(dev, pin < mod->pin_count);
	B0_ASSERT_RETURN_ON_NULL_ARG(dev, value);

	B0_ASSERT_BACKEND_FUNC_NULL(dev, dio, single.dir.get);
	return B0_BACKEND(dev, dio).single.dir.get(mod, pin, value);
}

b0_result_code b0_dio_dir_set(b0_dio *mod, unsigned int pin, bool value)
{
	b0_device *dev;

	B0_ASSERT_RETURN_ON_NULL_ARG(NULL, mod);
	dev = B0_MODULE_DEVICE(mod);

	B0_ASSERT_RETURN_ON_FAIL_ARG(dev, pin < mod->pin_count);

	B0_ASSERT_BACKEND_FUNC_NULL(dev, dio, single.dir.set);
	return B0_BACKEND(dev, dio).single.dir.set(mod, pin, value);
}

b0_result_code b0_dio_hiz_get(b0_dio *mod, unsigned int pin, bool *value)
{
	b0_device *dev;

	B0_ASSERT_RETURN_ON_NULL_ARG(NULL, mod);
	dev = B0_MODULE_DEVICE(mod);

	B0_ASSERT_RETURN_ON_FAIL_ARG(dev, pin < mod->pin_count);
	B0_ASSERT_RETURN_ON_NULL_ARG(dev, value);

	B0_ASSERT_BACKEND_FUNC_NULL(dev, dio, single.hiz.get);
	return B0_BACKEND(dev, dio).single.hiz.get(mod, pin, value);
}

b0_result_code b0_dio_hiz_set(b0_dio *mod, unsigned int pin, bool value)
{
	b0_device *dev;

	B0_ASSERT_RETURN_ON_NULL_ARG(NULL, mod);
	dev = B0_MODULE_DEVICE(mod);

	B0_ASSERT_RETURN_ON_FAIL_ARG(dev, pin < mod->pin_count);

	B0_ASSERT_BACKEND_FUNC_NULL(dev, dio, single.hiz.set);
	return B0_BACKEND(dev, dio).single.hiz.set(mod, pin, value);
}

static int check_pin_argument(b0_dio *mod, unsigned int *pins, size_t size)
{
	unsigned i;
	b0_device *dev = B0_MODULE_DEVICE(mod);
	B0_ASSERT_RETURN_ON_NULL_ARG(dev, pins);
	B0_ASSERT_RETURN_ON_FAIL(dev, size > 0, B0_ERR_UNDERFLOW);

	for (i = 0; i < size; i++) {
		B0_ASSERT_RETURN_ON_FAIL(dev, (pins[i] < mod->pin_count), B0_ERR_ARG);
	}

	return B0_OK;
}

b0_result_code b0_dio_multiple_value_set(b0_dio *mod, unsigned int *pins,
				size_t size, bool value)
{
	b0_device *dev;

	B0_ASSERT_RETURN_ON_NULL_ARG(NULL, mod);
	dev = B0_MODULE_DEVICE(mod);

	B0_DEBUG_RETURN_ON_FAIL_STMT(dev, check_pin_argument(mod, pins, size));

	B0_ASSERT_BACKEND_FUNC_NULL(dev, dio, multiple.value.set);
	return B0_BACKEND(dev, dio).multiple.value.set(mod, pins, size, value);
}

/**
 * @param[out] values
 */
b0_result_code b0_dio_multiple_value_get(b0_dio *mod, unsigned int *pins,
				bool *values, size_t size)
{
	b0_device *dev;

	B0_ASSERT_RETURN_ON_NULL_ARG(NULL, mod);
	dev = B0_MODULE_DEVICE(mod);

	B0_DEBUG_RETURN_ON_FAIL_STMT(dev, check_pin_argument(mod, pins, size));
	B0_ASSERT_RETURN_ON_NULL_ARG(dev, values);

	B0_ASSERT_BACKEND_FUNC_NULL(dev, dio, multiple.value.get);
	return B0_BACKEND(dev, dio).multiple.value.get(mod, pins, values, size);
}

b0_result_code b0_dio_multiple_value_toggle(b0_dio *mod, unsigned int *pins,
					size_t size)
{
	b0_device *dev;

	B0_ASSERT_RETURN_ON_NULL_ARG(NULL, mod);
	dev = B0_MODULE_DEVICE(mod);

	B0_DEBUG_RETURN_ON_FAIL_STMT(dev, check_pin_argument(mod, pins, size));

	B0_ASSERT_BACKEND_FUNC_NULL(dev, dio, multiple.value.toggle);
	return B0_BACKEND(dev, dio).multiple.value.toggle(mod, pins, size);
}

b0_result_code b0_dio_multiple_dir_set(b0_dio *mod, unsigned int *pins,
				size_t size, bool value)
{
	b0_device *dev;

	B0_ASSERT_RETURN_ON_NULL_ARG(NULL, mod);
	dev = B0_MODULE_DEVICE(mod);

	B0_DEBUG_RETURN_ON_FAIL_STMT(dev, check_pin_argument(mod, pins, size));

	B0_ASSERT_BACKEND_FUNC_NULL(dev, dio, multiple.dir.set);
	return B0_BACKEND(dev, dio).multiple.dir.set(mod, pins, size, value);
}

/**
 * @param[out] values
 */
b0_result_code b0_dio_multiple_dir_get(b0_dio *mod, unsigned int *pins,
				bool *values, size_t size)
{
	b0_device *dev;

	B0_ASSERT_RETURN_ON_NULL_ARG(NULL, mod);
	dev = B0_MODULE_DEVICE(mod);

	B0_DEBUG_RETURN_ON_FAIL_STMT(dev, check_pin_argument(mod, pins, size));
	B0_ASSERT_RETURN_ON_NULL_ARG(dev, values);

	B0_ASSERT_BACKEND_FUNC_NULL(dev, dio, multiple.dir.get);
	return B0_BACKEND(dev, dio).multiple.dir.get(mod, pins, values, size);
}

b0_result_code b0_dio_multiple_hiz_set(b0_dio *mod, unsigned int *pins,
				size_t size, bool value)
{
	b0_device *dev;

	B0_ASSERT_RETURN_ON_NULL_ARG(NULL, mod);
	dev = B0_MODULE_DEVICE(mod);

	B0_DEBUG_RETURN_ON_FAIL_STMT(dev, check_pin_argument(mod, pins, size));

	B0_ASSERT_BACKEND_FUNC_NULL(dev, dio, multiple.hiz.set);
	return B0_BACKEND(dev, dio).multiple.hiz.set(mod, pins, size, value);
}

b0_result_code b0_dio_multiple_hiz_get(b0_dio *mod, unsigned int *pins,
				bool *values, size_t size)
{
	b0_device *dev;

	B0_ASSERT_RETURN_ON_NULL_ARG(NULL, mod);
	dev = B0_MODULE_DEVICE(mod);

	B0_DEBUG_RETURN_ON_FAIL_STMT(dev, check_pin_argument(mod, pins, size));
	B0_ASSERT_RETURN_ON_NULL_ARG(dev, values);

	B0_ASSERT_BACKEND_FUNC_NULL(dev, dio, multiple.hiz.get);
	return B0_BACKEND(dev, dio).multiple.hiz.get(mod, pins, values, size);
}

/**
 * @todo build this function
 */
b0_result_code b0_generic_dio_multiple_value_set(b0_dio *mod,
			unsigned int *pins, size_t size, bool value)
{
	B0_UNUSED(mod);
	B0_UNUSED(pins);
	B0_UNUSED(size);
	B0_UNUSED(value);
	/* TODO: loop and perform using <single> */
	return B0_ERR_SUPP;
}

/**
 * @todo build this function
 */
b0_result_code b0_generic_dio_multiple_value_get(b0_dio *mod,
			unsigned int *pins, bool *values, size_t size)
{
	B0_UNUSED(mod);
	B0_UNUSED(pins);
	B0_UNUSED(values);
	B0_UNUSED(size);
	/* TODO: perform using <single> */
	return B0_ERR_SUPP;
}

/**
 * @todo build this function
 */
b0_result_code b0_generic_dio_multiple_value_toggle(b0_dio *mod,
			unsigned int *pins, size_t size)
{
	B0_UNUSED(mod);
	B0_UNUSED(pins);
	B0_UNUSED(size);
	/* TODO: perform using <single> */
	return B0_ERR_SUPP;
}

/**
 * @todo build this function
 */
b0_result_code b0_generic_dio_multiple_dir_set(b0_dio *mod,
			unsigned int *pins, size_t size, bool value)
{
	B0_UNUSED(mod);
	B0_UNUSED(pins);
	B0_UNUSED(size);
	B0_UNUSED(value);
	/* TODO: perform using <single> */
	return B0_ERR_SUPP;
}

/**
 * @todo build this function
 */
b0_result_code b0_generic_dio_multiple_dir_get(b0_dio *mod,
			unsigned int *pins, bool *values, size_t size)
{
	B0_UNUSED(mod);
	B0_UNUSED(pins);
	B0_UNUSED(values);
	B0_UNUSED(size);
	/* TODO: perform using <single> */
	return B0_ERR_SUPP;
}

/**
 * @todo build this function
 */
b0_result_code b0_generic_dio_multiple_hiz_set(b0_dio *mod,
			unsigned int *pins, size_t size, bool value)
{
	B0_UNUSED(mod);
	B0_UNUSED(pins);
	B0_UNUSED(size);
	B0_UNUSED(value);
	/* TODO: perform using <single> */
	return B0_ERR_SUPP;
}

/**
 * @todo build this function
 */
b0_result_code b0_generic_dio_multiple_hiz_get(b0_dio *mod,
			unsigned int *pins, bool *values, size_t size)
{
	B0_UNUSED(mod);
	B0_UNUSED(pins);
	B0_UNUSED(values);
	B0_UNUSED(size);
	/* TODO: perform using <single> */
	return B0_ERR_SUPP;
}

b0_result_code b0_dio_all_value_set(b0_dio *mod, bool value)
{
	b0_device *dev;

	B0_ASSERT_RETURN_ON_NULL_ARG(NULL, mod);
	dev = B0_MODULE_DEVICE(mod);

	B0_ASSERT_BACKEND_FUNC_NULL(dev, dio, all.value.set);
	return B0_BACKEND(dev, dio).all.value.set(mod, value);
}

b0_result_code b0_dio_all_value_get(b0_dio *mod, bool *values)
{
	b0_device *dev;

	B0_ASSERT_RETURN_ON_NULL_ARG(NULL, mod);
	dev = B0_MODULE_DEVICE(mod);

	B0_ASSERT_RETURN_ON_NULL_ARG(dev, values);

	B0_ASSERT_BACKEND_FUNC_NULL(dev, dio, all.value.get);
	return B0_BACKEND(dev, dio).all.value.get(mod, values);
}

b0_result_code b0_dio_all_value_toggle(b0_dio *mod)
{
	b0_device *dev;

	B0_ASSERT_RETURN_ON_NULL_ARG(NULL, mod);
	dev = B0_MODULE_DEVICE(mod);

	B0_ASSERT_BACKEND_FUNC_NULL(dev, dio, all.value.toggle);
	return B0_BACKEND(dev, dio).all.value.toggle(mod);
}

b0_result_code b0_dio_all_dir_set(b0_dio *mod, bool value)
{
	b0_device *dev;

	B0_ASSERT_RETURN_ON_NULL_ARG(NULL, mod);
	dev = B0_MODULE_DEVICE(mod);

	B0_ASSERT_BACKEND_FUNC_NULL(dev, dio, all.dir.set);
	return B0_BACKEND(dev, dio).all.dir.set(mod, value);
}

b0_result_code b0_dio_all_dir_get(b0_dio *mod, bool *values)
{
	b0_device *dev;

	B0_ASSERT_RETURN_ON_NULL_ARG(NULL, mod);
	dev = B0_MODULE_DEVICE(mod);

	B0_ASSERT_RETURN_ON_NULL_ARG(dev, values);

	B0_ASSERT_BACKEND_FUNC_NULL(dev, dio, all.dir.get);
	return B0_BACKEND(dev, dio).all.dir.get(mod, values);
}

b0_result_code b0_dio_all_hiz_set(b0_dio *mod, bool value)
{
	b0_device *dev;

	B0_ASSERT_RETURN_ON_NULL_ARG(NULL, mod);
	dev = B0_MODULE_DEVICE(mod);

	B0_ASSERT_BACKEND_FUNC_NULL(dev, dio, all.hiz.set);
	return B0_BACKEND(dev, dio).all.hiz.set(mod, value);
}

b0_result_code b0_dio_all_hiz_get(b0_dio *mod, bool *values)
{
	b0_device *dev;

	B0_ASSERT_RETURN_ON_NULL_ARG(NULL, mod);
	dev = B0_MODULE_DEVICE(mod);

	B0_ASSERT_RETURN_ON_NULL_ARG(dev, values);

	B0_ASSERT_BACKEND_FUNC_NULL(dev, dio, all.hiz.get);
	return B0_BACKEND(dev, dio).all.hiz.get(mod, values);
}

/**
 * @todo build this function
 */
b0_result_code b0_generic_dio_all_value_set(b0_dio *mod, bool value)
{
	B0_UNUSED(mod);
	B0_UNUSED(value);
	/* TODO: perform using <single> */
	return B0_ERR_SUPP;
}

/**
 * @todo build this function
 */
b0_result_code b0_generic_dio_all_value_get(b0_dio *mod, bool *values)
{
	B0_UNUSED(mod);
	B0_UNUSED(values);
	/* TODO: perform using <single> */
	return B0_ERR_SUPP;
}

/**
 * @todo build this function
 */
b0_result_code b0_generic_dio_all_value_toggle(b0_dio *mod)
{
	B0_UNUSED(mod);
	/* TODO: perform using <single> */
	return B0_ERR_SUPP;
}

/**
 * @todo build this function
 */
b0_result_code b0_generic_dio_all_dir_set(b0_dio *mod, bool value)
{
	B0_UNUSED(mod);
	B0_UNUSED(value);
	/* TODO: perform using <single> */
	return B0_ERR_SUPP;
}

/**
 * @todo build this function
 */
b0_result_code b0_generic_dio_all_dir_get(b0_dio *mod, bool *values)
{
	B0_UNUSED(mod);
	B0_UNUSED(values);
	/* TODO: perform using <single> */
	return B0_ERR_SUPP;
}

/**
 * @todo build this function
 */
b0_result_code b0_generic_dio_all_hiz_set(b0_dio *mod, bool value)
{
	B0_UNUSED(mod);
	B0_UNUSED(value);
	/* TODO: perform using <single> */
	return B0_ERR_SUPP;
}

/**
 * @todo build this function
 */
b0_result_code b0_generic_dio_all_hiz_get(b0_dio *mod, bool *values)
{
	B0_UNUSED(mod);
	B0_UNUSED(values);
	/* TODO: perform using <single> */
	return B0_ERR_SUPP;
}

/** @endcond */
