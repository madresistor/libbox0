/*
 * This file is part of libbox0.
 * Copyright (C) 2013-2016 Kuldeep Singh Dhaka <kuldeep@madresistor.com>
 *
 * libbox0 is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * libbox0 is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with libbox0.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef LIBBOX0_MODULE_SPI_PRIVATE_H
#define LIBBOX0_MODULE_SPI_PRIVATE_H

#include <stdint.h>
#include <stdbool.h>
#include "../common.h"
#include "spi.h"

__BEGIN_DECLS

B0_PRIV b0_result_code b0_generic_spi_master_fd(b0_spi *mod,
		const b0_spi_sugar_arg *arg, const void *write_data,
		void *read_data, size_t count);

B0_PRIV b0_result_code b0_generic_spi_master_hd_read(b0_spi *mod,
		const b0_spi_sugar_arg *arg, void *data, size_t count);

B0_PRIV b0_result_code b0_generic_spi_master_hd_write(b0_spi *mod,
		const b0_spi_sugar_arg *arg, const void *data, size_t count);

B0_PRIV b0_result_code b0_generic_spi_master_hd_write_read(b0_spi *mod,
		const b0_spi_sugar_arg *arg, const void *write_data,
		size_t write_count, void *read_data, size_t read_count);

__END_DECLS

#endif
