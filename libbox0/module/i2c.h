/*
 * This file is part of libbox0.
 * Copyright (C) 2013-2016 Kuldeep Singh Dhaka <kuldeep@madresistor.com>
 *
 * libbox0 is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * libbox0 is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with libbox0.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef LIBBOX0_MODULE_I2C_H
#define LIBBOX0_MODULE_I2C_H

#include <stdint.h>
#include <stdbool.h>
#include "../common.h"
#include "module.h"

__BEGIN_DECLS

enum b0_i2c_task_flag {
	B0_I2C_TASK_LAST = (1 << 0), /**< Last task to execute */

	B0_I2C_TASK_WRITE = (0 << 1), /**< Perform write */
	B0_I2C_TASK_READ = (1 << 1), /**< Perform read */

	B0_I2C_TASK_DIR_MASK = (1 << 1),
};

typedef enum b0_i2c_task_flag b0_i2c_task_flag;

/* If a version is not supported, the target should automatically fallback
 *  to a support version that can be used for communication */
enum b0_i2c_version {
	B0_I2C_VERSION_SM = 0,
	B0_I2C_VERSION_FM = 1,
	B0_I2C_VERSION_HS = 2,
	B0_I2C_VERSION_HS_CLEANUP1 = 3,
	B0_I2C_VERSION_FMPLUS = 4,
	B0_I2C_VERSION_UFM = 5,
	B0_I2C_VERSION_VER5 = 6,
	B0_I2C_VERSION_VER6 = 7
};

typedef enum b0_i2c_version b0_i2c_version;

struct b0_i2c_task {
	b0_i2c_task_flag flags; /* Transfer flags */
	uint8_t addr; /* Slave address */
	b0_i2c_version version; /* Protocol to use */
	void *data; /* Pointer to data */
	size_t count; /* Number of bytes to transfer */
};

typedef struct b0_i2c_task b0_i2c_task;

/**
 * Inter Integerated Communication
 * @extends b0_module
 */
struct b0_i2c {
	struct b0_module header;

	struct {
		uint8_t *sck, *sda;
	} label;

	struct {
		b0_i2c_version *values;
		size_t count;
	} version;

	struct {
		double high, low;
	} ref;
};

typedef struct b0_i2c b0_i2c;

/**
 * @copydoc b0_modimpl_open
 * @memberof b0_i2c
 */
B0_API b0_result_code b0_i2c_open(b0_device *dev, b0_i2c **mod, int index);

/**
 * @copydoc b0_modimpl_close
 * @memberof b0_i2c
 */
B0_API b0_result_code b0_i2c_close(b0_i2c *mod);

/**
 * Prepare the I2C for master mode
 * @param mod I2C module
 * @return result code
 */
B0_API b0_result_code b0_i2c_master_prepare(b0_i2c *mod);

/*
 * API Operation naming rule
 * =================
 *
 * b0_i2c_<operation>
 *
 * operation:
 * 		exec
 * 			* perform multiple operation (read, write) by passing datastructure
 * 			* see do_out macro if you only want to perform out to device
 *
 * 		read
 * 			* could be more effecient than calling exec()
 * 			* only read
 *
 * 		write
 * 			* could be more effecient than calling exec()
 * 			* only write
 *
 *		write8_read
 * 			* similar to read
 * 			* usecase 10bit addressing
 * 			* could be more effecient than calling exec()
 *
 * TODO: general call
 */

/**
 * Start a transaction
 * @param[in] mod Module
 * @param[in] tasks Task to execute
 * @param[out] failed_task_index Last failed task index (can be NULL)
 * @param[out] failed_task_ack Last failed task acknowledgment count (can be NULL)
 * @return result code
 * @memberof b0_i2c
 * @see b0_i2c_stop()
 * @note failed_task_index = -1 means the value is invalid
 * @note failed_task_ack = -1 means the value is invalid
 */
B0_API b0_result_code b0_i2c_master_start(b0_i2c *mod, const b0_i2c_task *tasks,
	int *failed_task_index, int *failed_task_ack);

/**
 * Stop a transaction
 * @param[in] mod Module
 * @return result code
 * @memberof b0_i2c
 * @see b0_i2c_master_start()
 */
B0_API b0_result_code b0_i2c_master_stop(b0_i2c *mod);

struct b0_i2c_sugar_arg {
	uint8_t addr;
	b0_i2c_version version;
};

typedef struct b0_i2c_sugar_arg b0_i2c_sugar_arg;

/**
 * @memberof b0_i2c
 */
B0_API b0_result_code b0_i2c_master_read(b0_i2c *mod,
		const b0_i2c_sugar_arg *arg, void *data, size_t count);

/**
 * @memberof b0_i2c
 */
B0_API b0_result_code b0_i2c_master_write8_read(b0_i2c *mod,
		const b0_i2c_sugar_arg *arg, uint8_t write, void *read_data,
		size_t read_count);

/**
 * @memberof b0_i2c
 */
B0_API b0_result_code b0_i2c_master_write(b0_i2c *mod,
		const b0_i2c_sugar_arg *arg, const void *data, size_t count);

/**
 * @memberof b0_i2c
 */
B0_API b0_result_code b0_i2c_master_write_read(b0_i2c *mod,
		const b0_i2c_sugar_arg *arg, const void *write_data,
		size_t write_count, void *read_data, size_t read_count);

/**
 * @memberof b0_i2c
 */
B0_API b0_result_code b0_i2c_master_slave_id(b0_i2c *mod,
		const b0_i2c_sugar_arg *arg, uint16_t *manuf, uint16_t *part,
		uint8_t *rev);

/**
 * @memberof b0_i2c
 */
B0_API b0_result_code b0_i2c_master_slave_detect(b0_i2c *mod,
		const b0_i2c_sugar_arg *arg, bool *detected);

/**
 * @memberof b0_i2c
 */
B0_API b0_result_code b0_i2c_master_slaves_detect(b0_i2c *mod,
		b0_i2c_version version, const uint8_t *addresses,
		bool *detected, size_t count, size_t *processed);

__END_DECLS

#endif
