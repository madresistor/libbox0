/*
 * This file is part of libbox0.
 * Copyright (C) 2013-2016 Kuldeep Singh Dhaka <kuldeep@madresistor.com>
 *
 * libbox0 is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * libbox0 is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with libbox0.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <stdint.h>
#include "../libbox0-private.h"

/** @copydoc INTERNAL */

b0_result_code b0_spi_open(b0_device *dev, b0_spi **mod, int index)
{
	B0_ASSERT_RETURN_ON_NULL_ARG(NULL, dev);
	B0_ASSERT_RETURN_ON_NULL_ARG(dev, mod);
	B0_ASSERT_RETURN_ON_FAIL_ARG(dev, index >= 0);

	B0_ASSERT_BACKEND_FUNC_NULL(dev, spi, open);
	return B0_BACKEND(dev, spi).open(dev, mod, index);
}

b0_result_code b0_spi_close(b0_spi *mod)
{
	b0_device *dev;

	B0_ASSERT_RETURN_ON_NULL_ARG(NULL, mod);
	dev = B0_MODULE_DEVICE(mod);

	B0_ASSERT_BACKEND_FUNC_NULL(dev, spi, close);
	return B0_BACKEND(dev, spi).close(mod);
}

b0_result_code b0_spi_speed_set(b0_spi *mod, unsigned long value)
{
	b0_device *dev;

	B0_ASSERT_RETURN_ON_NULL_ARG(NULL, mod);
	dev = B0_MODULE_DEVICE(mod);

	B0_ASSERT_RETURN_ON_FAIL_ARG(dev, value > 0);

	B0_ASSERT_BACKEND_FUNC_NULL(dev, spi, speed.set);
	return B0_BACKEND(dev, spi).speed.set(mod, value);
}

b0_result_code b0_spi_speed_get(b0_spi *mod, unsigned long *value)
{
	b0_device *dev;

	B0_ASSERT_RETURN_ON_NULL_ARG(NULL, mod);
	dev = B0_MODULE_DEVICE(mod);

	B0_ASSERT_RETURN_ON_NULL_ARG(dev, value);

	B0_ASSERT_BACKEND_FUNC_NULL(dev, spi, speed.get);
	return B0_BACKEND(dev, spi).speed.get(mod, value);
}

b0_result_code b0_spi_master_prepare(b0_spi *mod)
{
	b0_device *dev;

	B0_ASSERT_RETURN_ON_NULL_ARG(NULL, mod);
	dev = B0_MODULE_DEVICE(mod);

	B0_ASSERT_BACKEND_FUNC_NULL(dev, spi, master.prepare);
	return B0_BACKEND(dev, spi).master.prepare(mod);
}

b0_result_code b0_spi_master_start(b0_spi *mod, const b0_spi_task *tasks,
	int *failed_task_index, int *failed_task_count)
{
	b0_device *dev;

	B0_ASSERT_RETURN_ON_NULL_ARG(NULL, mod);
	dev = B0_MODULE_DEVICE(mod);

	B0_ASSERT_RETURN_ON_NULL_ARG(dev, tasks);

	if (B0_IS_NOT_NULL(failed_task_index)) {
		*failed_task_index = -1;
	}

	if (B0_IS_NOT_NULL(failed_task_count)) {
		*failed_task_count = -1;
	}

	B0_ASSERT_BACKEND_FUNC_NULL(dev, spi, master.start);
	return B0_BACKEND(dev, spi).master.start(mod, tasks, failed_task_index,
		failed_task_count);
}

b0_result_code b0_spi_active_state_set(b0_spi *mod, unsigned addr,
		bool value)
{
	b0_device *dev;

	B0_ASSERT_RETURN_ON_NULL_ARG(NULL, mod);
	dev = B0_MODULE_DEVICE(mod);

	B0_ASSERT_BACKEND_FUNC_NULL(dev, spi, active_state.set);
	return B0_BACKEND(dev, spi).active_state.set(mod, addr, value);
}

b0_result_code b0_spi_active_state_get(b0_spi *mod, unsigned addr,
		bool *value)
{
	b0_device *dev;

	B0_ASSERT_RETURN_ON_NULL_ARG(NULL, mod);
	dev = B0_MODULE_DEVICE(mod);

	B0_ASSERT_RETURN_ON_NULL_ARG(dev, value);

	B0_ASSERT_BACKEND_FUNC_NULL(dev, spi, active_state.get);
	return B0_BACKEND(dev, spi).active_state.get(mod, addr, value);
}

b0_result_code b0_spi_master_stop(b0_spi *mod)
{
	b0_device *dev;

	B0_ASSERT_RETURN_ON_NULL_ARG(NULL, mod);
	dev = B0_MODULE_DEVICE(mod);

	B0_ASSERT_BACKEND_FUNC_NULL(dev, spi, master.stop);
	return B0_BACKEND(dev, spi).master.stop(mod);
}

b0_result_code b0_spi_master_fd(b0_spi *mod, const b0_spi_sugar_arg *arg,
		const void *write_data, void *read_data, size_t count)
{
	b0_device *dev;
	B0_ASSERT_RETURN_ON_NULL_ARG(NULL, mod);
	dev = B0_MODULE_DEVICE(mod);

	B0_ASSERT_RETURN_ON_NULL_ARG(dev, arg);
	B0_ASSERT_RETURN_ON_NULL_ARG(dev, write_data);
	B0_ASSERT_RETURN_ON_NULL_ARG(dev, read_data);
	B0_ASSERT_RETURN_ON_FAIL_ARG(dev, count > 0);

	B0_ASSERT_BACKEND_FUNC_NULL(dev, spi, master.fd);
	return B0_BACKEND(dev, spi).master.fd(mod, arg, write_data, read_data, count);
}

b0_result_code b0_generic_spi_master_fd(b0_spi *mod, const b0_spi_sugar_arg *arg,
		const void *write_data, void *read_data, size_t count)
{
	b0_spi_task_flags flags = arg->flags &
		~(B0_SPI_TASK_LAST | B0_SPI_TASK_DUPLEX_MASK);

	const b0_spi_task tasks[] = {{
		.flags = flags | B0_SPI_TASK_FD | B0_SPI_TASK_LAST,
		.addr = arg->addr,
		.bitsize = arg->bitsize,
		.wdata = write_data,
		.rdata = read_data,
		.count = count
	}};

	return b0_spi_master_start(mod, tasks, NULL, NULL);
}

b0_result_code b0_spi_master_hd_read(b0_spi *mod, const b0_spi_sugar_arg *arg,
		void *data, size_t count)
{
	b0_device *dev;
	B0_ASSERT_RETURN_ON_NULL_ARG(NULL, mod);
	dev = B0_MODULE_DEVICE(mod);

	B0_ASSERT_RETURN_ON_NULL_ARG(dev, arg);
	B0_ASSERT_RETURN_ON_NULL_ARG(dev, data);
	B0_ASSERT_RETURN_ON_FAIL_ARG(dev, count > 0);

	B0_ASSERT_BACKEND_FUNC_NULL(dev, spi, master.hd_read);
	return B0_BACKEND(dev, spi).master.hd_read(mod, arg, data, count);
}

b0_result_code b0_generic_spi_master_hd_read(b0_spi *mod,
		const b0_spi_sugar_arg *arg, void *data, size_t count)
{
	b0_spi_task_flags flags = arg->flags &
		~(B0_SPI_TASK_LAST | B0_SPI_TASK_DUPLEX_MASK);

	const b0_spi_task tasks[] = {{
		.flags = flags | B0_SPI_TASK_LAST | B0_SPI_TASK_HD_READ,
		.addr = arg->addr,
		.bitsize = arg->bitsize,
		.rdata = data,
		.count = count
	}};

	return b0_spi_master_start(mod, tasks, NULL, NULL);
}

b0_result_code b0_spi_master_hd_write(b0_spi *mod, const b0_spi_sugar_arg *arg,
		const void *data, size_t count)
{
	b0_device *dev;
	B0_ASSERT_RETURN_ON_NULL_ARG(NULL, mod);
	dev = B0_MODULE_DEVICE(mod);

	B0_ASSERT_RETURN_ON_NULL_ARG(dev, arg);
	B0_ASSERT_RETURN_ON_NULL_ARG(dev, data);
	B0_ASSERT_RETURN_ON_FAIL_ARG(dev, count > 0);

	B0_ASSERT_BACKEND_FUNC_NULL(dev, spi, master.hd_write);
	return B0_BACKEND(dev, spi).master.hd_write(mod, arg, data, count);
}

b0_result_code b0_generic_spi_master_hd_write(b0_spi *mod,
		const b0_spi_sugar_arg *arg, const void *data, size_t count)
{
	b0_spi_task_flags flags = arg->flags &
		~(B0_SPI_TASK_LAST | B0_SPI_TASK_DUPLEX_MASK);

	const b0_spi_task tasks[] = {{
		.flags = flags | B0_SPI_TASK_HD_WRITE | B0_SPI_TASK_LAST,
		.addr = arg->addr,
		.bitsize = arg->bitsize,
		.wdata = data,
		.count = count
	}};

	return b0_spi_master_start(mod, tasks, NULL, NULL);
}

b0_result_code b0_spi_master_hd_write_read(b0_spi *mod,
		const b0_spi_sugar_arg *arg,
		const void *write_data, size_t write_count,
		void *read_data, size_t read_count)
{
	b0_device *dev;
	B0_ASSERT_RETURN_ON_NULL_ARG(NULL, mod);
	dev = B0_MODULE_DEVICE(mod);

	B0_ASSERT_RETURN_ON_NULL_ARG(dev, arg);
	B0_ASSERT_RETURN_ON_NULL_ARG(dev, write_data);
	B0_ASSERT_RETURN_ON_FAIL_ARG(dev, write_count > 0)
	B0_ASSERT_RETURN_ON_NULL_ARG(dev, read_data);
	B0_ASSERT_RETURN_ON_FAIL_ARG(dev, read_count > 0);

	B0_ASSERT_BACKEND_FUNC_NULL(dev, spi, master.hd_write_read);
	return B0_BACKEND(dev, spi).master.hd_write_read(mod, arg, write_data,
		write_count, read_data, read_count);
}

b0_result_code b0_generic_spi_master_hd_write_read(b0_spi *mod,
		const b0_spi_sugar_arg *arg,
		const void *write_data, size_t write_count,
		void *read_data, size_t read_count)
{
	b0_spi_task_flags flags = arg->flags &
		~(B0_SPI_TASK_LAST | B0_SPI_TASK_DUPLEX_MASK);

	const b0_spi_task tasks[] = {{
		.flags = flags | B0_SPI_TASK_HD_WRITE,
		.addr = arg->addr,
		.bitsize = arg->bitsize,
		.wdata = write_data,
		.count = write_count
	}, {
		.flags = flags | B0_SPI_TASK_HD_READ | B0_SPI_TASK_LAST,
		.addr = arg->addr,
		.bitsize = arg->bitsize,
		.rdata = read_data,
		.count = read_count
	}};

	return b0_spi_master_start(mod, tasks, NULL, NULL);
}

/** @endcond */
