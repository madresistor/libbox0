/*
 * This file is part of libbox0.
 * Copyright (C) 2013-2016 Kuldeep Singh Dhaka <kuldeep@madresistor.com>
 *
 * libbox0 is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * libbox0 is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with libbox0.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "../libbox0-private.h"

/** @copydoc INTERNAL */

b0_result_code b0_pwm_output_prepare(b0_pwm *mod)
{
	b0_device *dev;

	B0_ASSERT_RETURN_ON_NULL_ARG(NULL, mod);
	dev = B0_MODULE_DEVICE(mod);

	B0_ASSERT_BACKEND_FUNC_NULL(dev, pwm, output.prepare);
	return B0_BACKEND(dev, pwm).output.prepare(mod);
}

b0_result_code b0_pwm_output_start(b0_pwm *mod)
{
	b0_device *dev;

	B0_ASSERT_RETURN_ON_NULL_ARG(NULL, mod);
	dev = B0_MODULE_DEVICE(mod);

	B0_ASSERT_BACKEND_FUNC_NULL(dev, pwm, output.start);
	return B0_BACKEND(dev, pwm).output.start(mod);
}

b0_result_code b0_pwm_output_stop(b0_pwm *mod)
{
	b0_device *dev;

	B0_ASSERT_RETURN_ON_NULL_ARG(NULL, mod);
	dev = B0_MODULE_DEVICE(mod);

	B0_ASSERT_BACKEND_FUNC_NULL(dev, pwm, output.stop);
	return B0_BACKEND(dev, pwm).output.stop(mod);
}

b0_result_code b0_pwm_output_calc(b0_pwm *mod, unsigned int bitsize,
		double freq, unsigned long *speed, b0_pwm_reg *period,
		double max_error, bool best_result)
{
	b0_device *dev;
	size_t i;
	b0_pwm_reg period_max;
	b0_pwm_reg period_best = 0, period_curr;
	double freq_curr;
	double error_best = max_error, error_curr;
	unsigned long speed_best = 0, speed_curr;

	B0_ASSERT_RETURN_ON_NULL_ARG(NULL, mod);
	dev = B0_MODULE_DEVICE(mod);

	B0_ASSERT_RETURN_ON_FAIL_ARG(dev, bitsize > 0);
	B0_ASSERT_RETURN_ON_FAIL_ARG(dev, freq > 0);
	B0_ASSERT_RETURN_ON_FAIL_ARG(dev, max_error >= 0.0);
	B0_ASSERT_RETURN_ON_FAIL_ARG(dev, max_error <= 100.0);
	B0_ASSERT_RETURN_ON_NULL_ARG(dev, speed);
	B0_ASSERT_RETURN_ON_NULL_ARG(dev, period);

	period_max = 1ULL << bitsize;

	for (i = 0; i < mod->speed.count; i++) {
		speed_curr = mod->speed.values[i];
		period_curr = B0_PWM_CAST_REG(speed_curr / freq);

		/* is the period value fisible? */
		if (!(period_curr < period_max && period_curr > 2)) {
			continue;
		}

		freq_curr = B0_PWM_OUTPUT_CALC_FREQ(speed_curr, period_curr);
		error_curr = B0_PWM_OUTPUT_CALC_FREQ_ERR(freq, freq_curr);

		if (error_curr > max_error) {
			continue;
		}

		/*
		 * if both config have same error,
		 *  choose the larger one with heigher period, (ie more accurate)
		 */
		if (error_curr < error_best ||
			(error_curr == error_best && period_curr > period_best)) {
			error_best = error_curr;
			period_best = period_curr;
			speed_best = speed_curr;
		}

		if (best_result) {
			if (error_best == 0) {
				/* we have found the best solution we can find. */
				break;
			}
		} else {
			if (error_best <= max_error) {
				/* found the first best matching solution */
				break;
			}
		}
	}

	/* we found criteria matching results? */
	if (!(error_best <= max_error)) {
		B0_DEBUG(dev, "no valid result found");
		return B0_FAIL_CALC;
	}

	*speed = speed_best;
	*period = period_best;
	return B0_OK;
}

b0_result_code b0_pwm_close(b0_pwm *mod)
{
	b0_device *dev;

	B0_ASSERT_RETURN_ON_NULL_ARG(NULL, mod);
	dev = B0_MODULE_DEVICE(mod);

	B0_ASSERT_BACKEND_FUNC_NULL(dev, pwm, close);
	return B0_BACKEND(dev, pwm).close(mod);
}

b0_result_code b0_pwm_open(b0_device *dev, b0_pwm **mod, int index)
{
	b0_result_code r;

	B0_ASSERT_RETURN_ON_NULL_ARG(NULL, dev);
	B0_ASSERT_RETURN_ON_NULL_ARG(dev, mod);
	B0_ASSERT_RETURN_ON_FAIL_ARG(dev, index >= 0);

	B0_ASSERT_BACKEND_FUNC_NULL(dev, pwm, open);
	r = B0_BACKEND(dev, pwm).open(dev, mod, index);

	if (B0_SUCCESS_RESULT_CODE(r)) {
		/* runtime test if any bitsize is greater than sizeof(b0_pwm_reg)
		 * -1 is to make sure that 2 ^ n fits in a b0_pwm_reg register */
		b0_pwm *_mod = *mod;
		unsigned int max_bytes = sizeof(b0_pwm_reg);
		size_t i;

		for (i = 0; i < _mod->bitsize.count; i++) {
			unsigned int bits = _mod->bitsize.values[i];
			unsigned int byte = B0_BIT2BYTE(bits);
			if (byte > max_bytes) {
				B0_WARN(dev, "%u (a bitsize value) in PWM%i require more "
					"than %u bytes for calculation, will cause calculation "
					"overflow and this would lead to incorrect device IO",
					bits, index, max_bytes);
			}
		}
	}

	return r;
}

b0_result_code b0_pwm_speed_set(b0_pwm *mod, unsigned long value)
{
	b0_device *dev;

	B0_ASSERT_RETURN_ON_NULL_ARG(NULL, mod);
	dev = B0_MODULE_DEVICE(mod);

	B0_ASSERT_RETURN_ON_FAIL_ARG(dev, value > 0);

	B0_ASSERT_BACKEND_FUNC_NULL(dev, pwm, speed.set);
	return B0_BACKEND(dev, pwm).speed.set(mod, value);
}

b0_result_code b0_pwm_speed_get(b0_pwm *mod, unsigned long *value)
{
	b0_device *dev;

	B0_ASSERT_RETURN_ON_NULL_ARG(NULL, mod);
	dev = B0_MODULE_DEVICE(mod);

	B0_ASSERT_RETURN_ON_NULL_ARG(dev, value);

	B0_ASSERT_BACKEND_FUNC_NULL(dev, pwm, speed.get);
	return B0_BACKEND(dev, pwm).speed.get(mod, value);
}

b0_result_code b0_pwm_bitsize_set(b0_pwm *mod, unsigned int value)
{
	b0_device *dev;

	B0_ASSERT_RETURN_ON_NULL_ARG(NULL, mod);
	dev = B0_MODULE_DEVICE(mod);

	B0_ASSERT_RETURN_ON_FAIL_ARG(dev, value > 0);

	B0_ASSERT_BACKEND_FUNC_NULL(dev, pwm, bitsize.set);
	return B0_BACKEND(dev, pwm).bitsize.set(mod, value);
}

b0_result_code b0_pwm_bitsize_get(b0_pwm *mod, unsigned int *value)
{
	b0_device *dev;

	B0_ASSERT_RETURN_ON_NULL_ARG(NULL, mod);
	dev = B0_MODULE_DEVICE(mod);

	B0_ASSERT_RETURN_ON_NULL_ARG(dev, value);

	B0_ASSERT_BACKEND_FUNC_NULL(dev, pwm, bitsize.get);
	return B0_BACKEND(dev, pwm).bitsize.get(mod, value);
}

b0_result_code b0_pwm_width_set(b0_pwm *mod, unsigned int index,
					b0_pwm_reg value)
{
	b0_device *dev;

	B0_ASSERT_RETURN_ON_NULL_ARG(NULL, mod);
	dev = B0_MODULE_DEVICE(mod);

	B0_ASSERT_RETURN_ON_FAIL_ARG(dev, index < mod->pin_count);

	B0_ASSERT_BACKEND_FUNC_NULL(dev, pwm, width.set);
	return B0_BACKEND(dev, pwm).width.set(mod, index, value);
}

b0_result_code b0_pwm_width_get(b0_pwm *mod, unsigned int index,
					b0_pwm_reg *value)
{
	b0_device *dev;

	B0_ASSERT_RETURN_ON_NULL_ARG(NULL, mod);
	dev = B0_MODULE_DEVICE(mod);

	B0_ASSERT_RETURN_ON_NULL_ARG(dev, value);
	B0_ASSERT_RETURN_ON_FAIL_ARG(dev, index < mod->pin_count);

	B0_ASSERT_BACKEND_FUNC_NULL(dev, pwm, width.get);
	return B0_BACKEND(dev, pwm).width.get(mod, index, value);
}

b0_result_code b0_pwm_period_set(b0_pwm *mod, b0_pwm_reg value)
{
	b0_device *dev;

	B0_ASSERT_RETURN_ON_NULL_ARG(NULL, mod);
	dev = B0_MODULE_DEVICE(mod);

	B0_ASSERT_BACKEND_FUNC_NULL(dev, pwm, period.set);
	return B0_BACKEND(dev, pwm).period.set(mod, value);
}

b0_result_code b0_pwm_period_get(b0_pwm *mod, b0_pwm_reg *value)
{
	b0_device *dev;

	B0_ASSERT_RETURN_ON_NULL_ARG(NULL, mod);
	dev = B0_MODULE_DEVICE(mod);

	B0_ASSERT_RETURN_ON_NULL_ARG(dev, value);

	B0_ASSERT_BACKEND_FUNC_NULL(dev, pwm, period.get);
	return B0_BACKEND(dev, pwm).period.get(mod, value);
}

/** @endcond */
