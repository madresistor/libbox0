/*
 * This file is part of libbox0.
 * Copyright (C) 2013-2016 Kuldeep Singh Dhaka <kuldeep@madresistor.com>
 *
 * libbox0 is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * libbox0 is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with libbox0.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef LIBBOX0_MODULE_DIO_H
#define LIBBOX0_MODULE_DIO_H

#include <stdint.h>
#include <stddef.h>
#include <stdbool.h>
#include "../common.h"
#include "module.h"

__BEGIN_DECLS

enum b0_dio_capab {
	B0_DIO_CAPAB_OUTPUT = (1 << 0),
	B0_DIO_CAPAB_INPUT = (1 << 1),
	B0_DIO_CAPAB_HIZ = (1 << 2)
};

typedef enum b0_dio_capab b0_dio_capab;

/**
 * Digital Input/Output
 * @extends b0_module
 */
struct b0_dio {
	b0_module header;

	unsigned int pin_count;

	b0_dio_capab capab;

	struct {
		uint8_t **pin;
	} label;

	struct {
		double high, low;
		b0_ref_type type;
	} ref;
};

typedef struct b0_dio b0_dio;

#define B0_DIO_LOW false
#define B0_DIO_HIGH true

#define B0_DIO_INPUT false
#define B0_DIO_OUTPUT true

#define B0_DIO_DISABLE false
#define B0_DIO_ENABLE true

/**
 * @copydoc b0_modimpl_open
 * @memberof b0_dio
 */
B0_API b0_result_code b0_dio_open(b0_device *dev, b0_dio **mod, int index);

/**
 * @copydoc b0_modimpl_close
 * @memberof b0_dio
 */
B0_API b0_result_code b0_dio_close(b0_dio *mod);

/**
 * @name Basic Mode
 * Operation is performed on one pin
 * @{
 */

/**
 * Prepare module for basic mode
 * @param[in] mod DIO module
 * @return result code
 *
 * @note
 *  currently basic mode is only support. \n
 *  this function is here, beacuse in future streaming mode will be added to dio. \n
 *  in that case b0_dio_stream_prepare() will be added. \n
 * @memberof b0_dio
 */
B0_API b0_result_code b0_dio_basic_prepare(b0_dio *mod);

/**
 * Start module in basic mode
 * @param[in] mod DIO module
 * @return result code
 * @memberof b0_dio
 */
B0_API b0_result_code b0_dio_basic_start(b0_dio *mod);

/**
 * Stop module in basic mode
 * @param[in] mod DIO module
 * @return result code
 * @memberof b0_dio
 */
B0_API b0_result_code b0_dio_basic_stop(b0_dio *mod);

/** @} */

/**
 * @name Single
 * Operation is performed on one pin
 * @{
 */

/**
 * Get pin value
 * @param[in] mod DIO module
 * @param[in] pin Pin
 * @param[out] value Value
 * @return result code
 * @memberof b0_dio
 */
B0_API b0_result_code b0_dio_value_get(b0_dio *mod, unsigned int pin, bool *value);

/**
 * Set pin value
 * @param[in] mod DIO module
 * @param[in] pin Pin
 * @param[in] value Value (use of B0_DIO_HIGH, B0_DIO_LOW recommended)
 * @return result code
 * @memberof b0_dio
 */
B0_API b0_result_code b0_dio_value_set(b0_dio *mod, unsigned int pin, bool value);

/**
 * Toggle pin value
 * @param[in] mod DIO module
 * @param[in] pin Pin
 * @return result code
 * @memberof b0_dio
 */
B0_API b0_result_code b0_dio_value_toggle(b0_dio *mod, unsigned int pin);

/**
 * Get pin direction
 * @param[in] mod DIO module
 * @param[in] pin Pin
 * @param[out] value Value
 * @return result code
 * @memberof b0_dio
 */
B0_API b0_result_code b0_dio_dir_get(b0_dio *mod, unsigned int pin, bool *value);

/**
 * Set pin direction
 * @param[in] mod DIO module
 * @param[in] pin Pin
 * @param[in] value Value (use of B0_DIO_OUTPUT, B0_DIO_INPUT recommended)
 * @return result code
 * @memberof b0_dio
 */
B0_API b0_result_code b0_dio_dir_set(b0_dio *mod, unsigned int pin, bool value);

/**
 * Get pin HiZ
 * @param[in] mod DIO module
 * @param[in] pin Pin
 * @param[out] value Value
 * @return result code
 * @memberof b0_dio
 */
B0_API b0_result_code b0_dio_hiz_get(b0_dio *mod, unsigned int pin, bool *value);

/**
 * Set pin HiZ
 * @param[in] mod DIO module
 * @param[in] pin Pin
 * @param[in] value Value (use of B0_DIO_ENABLE, B0_DIO_DISABLE recommended)
 * @return result code
 * @memberof b0_dio
 */
B0_API b0_result_code b0_dio_hiz_set(b0_dio *mod, unsigned int pin, bool value);

/**
 * Macro to make @a pin input
 * @memberof b0_dio
 */
#define b0_dio_input(mod, pin) b0_dio_dir_set(mod, pin, B0_DIO_INPUT)

/**
 * Macro to make @a pin output
 * @memberof b0_dio
 */
#define b0_dio_output(mod, pin) b0_dio_dir_set(mod, pin, B0_DIO_OUTPUT)

/**
 * Macro to set @a pin value to high
 * @memberof b0_dio
 */
#define b0_dio_high(mod, pin) b0_dio_value_set(mod, pin, B0_DIO_HIGH)

/**
 * Macro to set @a pin value to low
 * @memberof b0_dio
 */
#define b0_dio_low(mod, pin) b0_dio_value_set(mod, pin, B0_DIO_LOW)

/**
 * Macro to toggle @a pin
 * @memberof b0_dio
 */
#define b0_dio_toggle(mod, pin) b0_dio_value_toggle(mod, pin)

/**
 * @}
 */

/**
 * @name Multiple
 * Operation is performed on multiple pins
 * @{
 */

/**
 * Set multiple pins value
 * @param[in] mod DIO module
 * @param[in] pins Pins
 * @param[in] size Length of @a pins
 * @param[in] value Value (use of B0_DIO_HIGH, B0_DIO_LOW recommended)
 * @return result code
 * @memberof b0_dio
 */
B0_API b0_result_code b0_dio_multiple_value_set(b0_dio *mod,
			unsigned int *pins, size_t size, bool value);

/**
 * Get multiple pins value
 * @param[in] mod DIO module
 * @param[in] pins Pins
 * @param[out] values Values
 * @param[in] size Length of @a pins and @a values
 * @return result code
 * @memberof b0_dio
 */
B0_API b0_result_code b0_dio_multiple_value_get(b0_dio *mod,
			unsigned int *pins, bool *values, size_t size);

/**
 * Toggle multiple pins
 * @param[in] mod DIO module
 * @param[in] pins Pins
 * @param[in] size Length of @a pins
 * @return result code
 * @memberof b0_dio
 */
B0_API b0_result_code b0_dio_multiple_value_toggle(b0_dio *mod,
			unsigned int *pins, size_t size);

/**
 * Set multiple pins direction
 * @param[in] mod DIO module
 * @param[in] pins Pins
 * @param[in] size Length of @a pins
 * @param[in] value Value (use of B0_DIO_INPUT, B0_DIO_OUTPUT recommended)
 * @return result code
 * @memberof b0_dio
 */
B0_API b0_result_code b0_dio_multiple_dir_set(b0_dio *mod,
			unsigned int *pins, size_t size, bool value);

/**
 * Get multiple pin direction
 * @param[in] mod DIO module
 * @param[in] pins Pins
 * @param[out] values Values
 * @param[in] size Length of @a pins and @a values
 * @return result code
 * @memberof b0_dio
 */
B0_API b0_result_code b0_dio_multiple_dir_get(b0_dio *mod,
			unsigned int *pins, bool *values, size_t size);

/**
 * Get multiple pins HiZ
 * @param[in] mod DIO module
 * @param[in] pins Pins
 * @param[out] values Values
 * @param[in] size Length of @a pins and @a values
 * @return result code
 * @memberof b0_dio
 */
B0_API b0_result_code b0_dio_multiple_hiz_get(b0_dio *mod,
			unsigned int *pins, bool *values, size_t size);

/**
 * Set multiple pins to HiZ
 * @param[in] mod DIO module
 * @param[in] pins Pins
 * @param[in] size Length of @a pins
 * @param[in] value Value (use of B0_DIO_ENABLE, B0_DIO_DISABLE recommended)
 * @return result code
 * @memberof b0_dio
 */
B0_API b0_result_code b0_dio_multiple_hiz_set(b0_dio *mod,
			unsigned int *pins, size_t size, bool value);

/** @} */

/**
 * @name All
 * Operation is performed on all pins
 * @{
 */

/**
 * Get all pins value
 * @param[in] mod DIO module
 * @param[in] value Value (use of B0_DIO_LOW, B0_DIO_HIGH recommended)
 * @return result code
 * @memberof b0_dio
 */
B0_API b0_result_code b0_dio_all_value_set(b0_dio *mod, bool value);

/**
 * Get all pins value
 * @param[in] mod DIO module
 * @param[out] values Values
 * @return result code
 * @memberof b0_dio
 */
B0_API b0_result_code b0_dio_all_value_get(b0_dio *mod, bool *values);

/**
 * Toggle all pins value
 * @param[in] mod DIO module
 * @return result code
 * @memberof b0_dio
 */
B0_API b0_result_code b0_dio_all_value_toggle(b0_dio *mod);

/**
 * Set all pin direction
 * @param[in] mod DIO module
 * @param value Value (B0_DIO_INPUT, B0_DIO_OUTPUT use recommended)
 * @return result code
 * @memberof b0_dio
 */
B0_API b0_result_code b0_dio_all_dir_set(b0_dio *mod, bool value);

/**
 * Set all pins direction
 * @param[in] mod DIO module
 * @param[out] values Values
 * @return result code
 * @memberof b0_dio
 */
B0_API b0_result_code b0_dio_all_dir_get(b0_dio *mod, bool *values);

/**
 * Get all pin HiZ value
 * @param[in] mod DIO module
 * @param[out] values Values
 * @return result code
 * @memberof b0_dio
 */
B0_API b0_result_code b0_dio_all_hiz_get(b0_dio *mod, bool *values);

/**
 * Set all HiZ value
 * @param[in] mod DIO module
 * @param[in] value Value (B0_DIO_ENABLE, B0_DIO_DISABLE use recommended)
 * @return result code
 * @memberof b0_dio
 */
B0_API b0_result_code b0_dio_all_hiz_set(b0_dio *mod, bool value);

/**
 * @}
 */

__END_DECLS

#endif
