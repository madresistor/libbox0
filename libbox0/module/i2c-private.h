/*
 * This file is part of libbox0.
 * Copyright (C) 2013-2016 Kuldeep Singh Dhaka <kuldeep@madresistor.com>
 *
 * libbox0 is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * libbox0 is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with libbox0.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef LIBBOX0_MODULE_I2C_PRIVATE_H
#define LIBBOX0_MODULE_I2C_PRIVATE_H

#include <stdint.h>
#include <stdbool.h>
#include "../common.h"
#include "i2c.h"

__BEGIN_DECLS

B0_PRIV b0_result_code b0_generic_i2c_master_read(b0_i2c *mod,
		const b0_i2c_sugar_arg *arg, void *data, size_t count);

B0_PRIV b0_result_code b0_generic_i2c_master_write(b0_i2c *mod,
		const b0_i2c_sugar_arg *arg, const void *data, size_t count);

B0_PRIV b0_result_code b0_generic_i2c_master_write8_read(b0_i2c *mod,
		const b0_i2c_sugar_arg *arg, uint8_t write, void *read_data,
		size_t read_count);

B0_PRIV b0_result_code b0_generic_i2c_master_write(b0_i2c *mod,
		const b0_i2c_sugar_arg *arg, const void *data, size_t count);

B0_PRIV b0_result_code b0_generic_i2c_master_write_read(b0_i2c *mod,
			const b0_i2c_sugar_arg *arg, const void *write_data,
			size_t write_count, void *read_data, size_t read_count);

B0_PRIV b0_result_code b0_generic_i2c_master_slave_id(b0_i2c *mod,
			const b0_i2c_sugar_arg *arg, uint16_t *manuf, uint16_t *part,
			uint8_t *rev);

B0_PRIV b0_result_code b0_generic_i2c_master_slave_detect(b0_i2c *mod,
			const b0_i2c_sugar_arg *arg, bool *detected);

B0_PRIV b0_result_code b0_generic_i2c_master_slaves_detect(b0_i2c *mod,
		b0_i2c_version version, const uint8_t *addresses, bool *detected,
		size_t count, size_t *processed);

__END_DECLS

#endif
