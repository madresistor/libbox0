/*
 * This file is part of libbox0.
 * Copyright (C) 2015, 2016 Kuldeep Singh Dhaka <kuldeep@madresistor.com>
 *
 * libbox0 is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * libbox0 is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with libbox0.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef LIBBOX0_BACKEND_H
#define LIBBOX0_BACKEND_H

#include "../common.h"
#include "../libbox0.h"

__BEGIN_DECLS

enum b0_backend_type {
	B0_BACKEND_USB
};

typedef enum b0_backend_type b0_backend_type;

/**
 * @brief backend data structure.
 * @details backend data structure that contain pointer to functions.
 *  the frontend API will redirect calls to backend.
 */
struct b0_backend {
	b0_backend_type type;

	/**
	 * name of the backend.
	 * @note used for debug purpose only.
	 */
	const uint8_t *name;

	/**
	 * backend url.
	 * @note used for debug purpose only.
	 */
	const uint8_t *url;

	struct {
		/** @copydoc b0_device_manuf */
		b0_result_code (* ping)(b0_device *dev);

		/** @copydoc b0_device_manuf */
		b0_result_code (* close)(b0_device *dev);
	} device;

	struct {
		/** @copydoc b0_module_openable */
		b0_result_code (* openable)(b0_module *mod);
	} module;

	struct {
		/** @copydoc b0_ain_open */
		b0_result_code (* open)(b0_device *dev, b0_ain **mod, int index);

		/** @copydoc b0_ain_close */
		b0_result_code (* close)(b0_ain *mod);

		struct {
			/** @copydoc b0_ain_chan_seq_set() */
			b0_result_code (* set)(b0_ain *mod, unsigned *values,
							size_t count);

			/** @copydoc b0_ain_chan_seq_get() */
			b0_result_code (* get)(b0_ain *mod, unsigned *values,
							size_t *count);
		} chan_seq;

		struct {
			/** @copydoc b0_ain_bitsize_speed_set() */
			b0_result_code (* set)(b0_ain *mod, unsigned bitsize,
							unsigned long speed);

			/** @copydoc b0_ain_bitsize_speed_get() */
			b0_result_code (* get)(b0_ain *mod, unsigned *bitsize,
							unsigned long *speed);
		} bitsize_speed;

		struct {
			/** @copydoc b0_ain_snapshot_prepare */
			b0_result_code (* prepare)(b0_ain *mod);

			/** @copydoc b0_ain_snapshot_start */
			b0_result_code (* start)(b0_ain *mod, void *samples, size_t count);

			/** @copydoc b0_ain_snapshot_start_double */
			b0_result_code (* start_double)(b0_ain *mod, double *samples,
						size_t count);

			/** @copydoc b0_ain_snapshot_start_double */
			b0_result_code (* start_float)(b0_ain *mod, float *samples,
						size_t count);

			/** @copydoc b0_ain_snapshot_stop */
			b0_result_code (* stop)(b0_ain *mod);
		} snapshot;

		struct {
			/** @copydoc b0_ain_stream_prepare */
			b0_result_code (* prepare)(b0_ain *mod);

			/** @copydoc b0_ain_stream_start */
			b0_result_code (* start)(b0_ain *mod);

			/** @copydoc b0_ain_stream_stop */
			b0_result_code (* stop)(b0_ain *mod);

			/** @copydoc b0_ain_stream_read */
			b0_result_code (* read)(b0_ain *mod, void *samples, size_t count,
				size_t *actual_count);

			/** @copydoc b0_ain_stream_read_double */
			b0_result_code (* read_double)(b0_ain *mod, double *samples,
				size_t count, size_t *actual_count);

			/** @copydoc b0_ain_stream_read_float */
			b0_result_code (* read_float)(b0_ain *mod, float *samples,
				size_t count, size_t *actual_count);
		} stream;
	} ain;

	struct {
		/** @copydoc b0_aout_open */
		b0_result_code (* open)(b0_device *dev, b0_aout **mod, int index);

		/** @copydoc b0_aout_close */
		b0_result_code (* close)(b0_aout *mod);

		struct {
			/** @copydoc b0_aout_chan_seq_set() */
			b0_result_code (* set)(b0_aout *mod, unsigned *values,
								size_t count);

			/** @copydoc b0_aout_chan_seq_get() */
			b0_result_code (* get)(b0_aout *mod, unsigned *values,
								size_t *count);
		} chan_seq;

		struct {
			/** @copydoc b0_aout_bitsize_speed_set() */
			b0_result_code (* set)(b0_aout *mod, unsigned bitsize,
								unsigned long speed);

			/** @copydoc b0_aout_bitsize_speed_get() */
			b0_result_code (* get)(b0_aout *mod, unsigned *bitsize,
								unsigned long *speed);
		} bitsize_speed;

		struct {
			/** @copydoc b0_aout_repeat_set() */
			b0_result_code (* set)(b0_aout *mod, unsigned long value);

			/** @copydoc b0_aout_repeat_get() */
			b0_result_code (* get)(b0_aout *mod, unsigned long *value);
		} repeat;

		struct {
			/** @copydoc b0_aout_snapshotprepare */
			b0_result_code (* prepare)(b0_aout *mod);

			/** @copydoc b0_aout_snapshotstart */
			b0_result_code (* start)(b0_aout *mod, void *data, size_t count);

			/** @copydoc b0_aout_snapshotstart_double */
			b0_result_code (* start_double)(b0_aout *mod, double *data, size_t count);

			/** @copydoc b0_aout_snapshotstart_float */
			b0_result_code (* start_float)(b0_aout *mod, float *data, size_t count);

			/** @copydoc b0_aout_snapshotstop */
			b0_result_code (* stop)(b0_aout *mod);
		} snapshot;

		struct {
			b0_result_code (* prepare)(b0_aout *mod);
			b0_result_code (* start)(b0_aout *mod);
			b0_result_code (* stop)(b0_aout *mod);
			b0_result_code (* write)(b0_aout *mod, void *buf, size_t len);
			b0_result_code (* write_float)(b0_aout *mod, float *buf, size_t len);
			b0_result_code (* write_double)(b0_aout *mod, double *buf, size_t len);
		} stream;
	} aout;

	struct {
		b0_result_code (* open)(b0_device *dev, b0_dio **mod, int index);
		b0_result_code (* close)(b0_dio *mod);

		struct {
			struct {
				b0_result_code (* get)(b0_dio *mod, unsigned int pin, bool *value);
				b0_result_code (* set)(b0_dio *mod, unsigned int pin, bool value);
				b0_result_code (* toggle)(b0_dio *mod, unsigned int pin);
			} value;

			struct {
				b0_result_code (* get)(b0_dio *mod, unsigned int pin, bool *value);
				b0_result_code (* set)(b0_dio *mod, unsigned int pin, bool value);
			} dir;

			struct {
				b0_result_code (* get)(b0_dio *mod, unsigned int pin, bool *value);
				b0_result_code (* set)(b0_dio *mod, unsigned int pin, bool value);
			} hiz;
		} single;

		struct {
			struct {
				b0_result_code (* set)(b0_dio *mod, unsigned int *pins,
					size_t size, bool value);

				b0_result_code (* get)(b0_dio *mod, unsigned int *pins,
					bool *values, size_t size);

				b0_result_code (* toggle)(b0_dio *mod, unsigned int *pins,
					size_t size);
			} value;

			struct {
				b0_result_code (* set)(b0_dio *mod, unsigned int *pins,
					size_t size, bool value);
				b0_result_code (* get)(b0_dio *mod, unsigned int *pins,
					bool *value, size_t size);
			} dir;

			struct {
				b0_result_code (* get)(b0_dio *mod, unsigned int *pins,
					bool *value, size_t size);
				b0_result_code (* set)(b0_dio *mod, unsigned int *pins,
					size_t size, bool value);
			} hiz;
		} multiple;

		struct {
			struct {
				b0_result_code (* set)(b0_dio *mod, bool value);
				b0_result_code (* get)(b0_dio *mod, bool *pins);
				b0_result_code (* toggle)(b0_dio *mod);
			} value;

			struct {
				b0_result_code (* set)(b0_dio *mod, bool value);
				b0_result_code (* get)(b0_dio *mod, bool *pins);
			} dir;

			struct {
				b0_result_code (* get)(b0_dio *mod, bool *pins);
				b0_result_code (* set)(b0_dio *mod, bool value);
			} hiz;
		} all;

		struct {
			b0_result_code (* prepare)(b0_dio *mod);
			b0_result_code (* start)(b0_dio *mod);
			b0_result_code (* stop)(b0_dio *mod);
		} basic;
	} dio;

	struct {
		b0_result_code (* open)(b0_device *dev, b0_i2c **mod, int index);
		b0_result_code (* close)(b0_i2c *mod);

		struct {
			b0_result_code (* prepare)(b0_i2c *mod);
			b0_result_code (* start)(b0_i2c *mod, const b0_i2c_task *tasks,
				int *failed_task_index, int *failed_task_ack);
			b0_result_code (* stop)(b0_i2c *mod);

			b0_result_code (* read)(b0_i2c *mod,
				const b0_i2c_sugar_arg *arg, void *data, size_t count);

			b0_result_code (* write)(b0_i2c *mod,
				const b0_i2c_sugar_arg *arg, const void *data, size_t bWrite);

			b0_result_code (* write8_read)(b0_i2c *mod,
				const b0_i2c_sugar_arg *arg, uint8_t write, void *read_data,
				size_t read_count);

			b0_result_code (* write_read)(b0_i2c *mod,
				const b0_i2c_sugar_arg *arg, const void *write_data,
				size_t write_count, void *read_data, size_t read_count);

			b0_result_code (* slave_id)(b0_i2c *mod,
				const b0_i2c_sugar_arg *arg, uint16_t *manuf,
				uint16_t *part, uint8_t *rev);

			b0_result_code (* slave_detect)(b0_i2c *mod,
				const b0_i2c_sugar_arg *arg, bool *detected);

			b0_result_code (* slaves_detect)(b0_i2c *mod,
				b0_i2c_version version, const uint8_t *addresses,
				bool *detected, size_t count, size_t *processed);
		} master;
	} i2c;

	struct {
		b0_result_code (* open)(b0_device *dev, b0_spi **mod, int index);
		b0_result_code (* close)(b0_spi *mod);

		struct {
			/** @copydoc b0_spi_speed_set() */
			b0_result_code (* set)(b0_spi *mod, unsigned long value);

			/** @copydoc b0_spi_speed_get() */
			b0_result_code (* get)(b0_spi *mod, unsigned long *value);
		} speed;

		struct {
			/** @copydoc b0_spi_active_state_set() */
			b0_result_code (* set)(b0_spi *mod, unsigned addr, bool value);

			/** @copydoc b0_spi_active_state_get() */
			b0_result_code (* get)(b0_spi *mod, unsigned addr, bool *value);
		} active_state;

		struct {
			b0_result_code (* prepare)(b0_spi *mod);
			b0_result_code (* start)(b0_spi *mod, const b0_spi_task *tasks,
				int *failed_task_index, int *failed_task_count);
			b0_result_code (* stop)(b0_spi *mod);

			b0_result_code (*fd)(b0_spi *mod, const b0_spi_sugar_arg *arg,
				const void *write_data, void *read_data, size_t read_count);

			b0_result_code (*hd_read)(b0_spi *mod, const b0_spi_sugar_arg *arg,
				void *data, size_t count);

			b0_result_code (*hd_write)(b0_spi *mod, const b0_spi_sugar_arg *arg,
				const void *data, size_t count);

			b0_result_code (*hd_write_read)(b0_spi *mod,
				const b0_spi_sugar_arg *arg, const void *write_data,
				size_t write_count, void *read_data, size_t read_count);
		} master;
	} spi;

	struct {
		/** @copydoc b0_pwm_open() */
		b0_result_code (* open)(b0_device *dev, b0_pwm **mod, int index);

		/** @copydoc b0_pwm_close() */
		b0_result_code (* close)(b0_pwm *mod);

		struct {
			/** @copydoc b0_pwm_speed_set() */
			b0_result_code (* set)(b0_pwm *mod, unsigned long value);

			/** @copydoc b0_pwm_speed_get() */
			b0_result_code (* get)(b0_pwm *mod, unsigned long *value);
		} speed;

		struct {
			/** @copydoc b0_pwm_bitsize_set() */
			b0_result_code (* set)(b0_pwm *mod, unsigned int value);

			/** @copydoc b0_pwm_bitsize_get() */
			b0_result_code (* get)(b0_pwm *mod, unsigned int *value);
		} bitsize;

		struct {
			b0_result_code (* set)(b0_pwm *mod, uint8_t width_index, b0_pwm_reg width);
			b0_result_code (* get)(b0_pwm *mod, uint8_t width_index, b0_pwm_reg *width);
		} width;

		struct {
			b0_result_code (* set)(b0_pwm *mod, b0_pwm_reg width);
			b0_result_code (* get)(b0_pwm *mod, b0_pwm_reg *width);
		} period;

		struct {
			b0_result_code (* prepare)(b0_pwm *mod);
			b0_result_code (* start)(b0_pwm *mod);
			b0_result_code (* stop)(b0_pwm *mod);
		} output;
	} pwm;
};

__END_DECLS

#endif
