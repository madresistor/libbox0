/*
 * This file is part of libbox0.
 * Copyright (C) 2013-2016 Kuldeep Singh Dhaka <kuldeep@madresistor.com>
 *
 * libbox0 is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * libbox0 is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with libbox0.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "usb-private.h"

/** @cond INTERNAL */

const b0_backend usb_backend = {
	.type = B0_BACKEND_USB,

	.name = (const uint8_t *) "Box0",
	.url = (const uint8_t *) "https://www.madresistor.com/box0",

	.device = {
		.ping = b0_usb_device_ping,
		.close = b0_usb_device_close
	},

	.module = {
		.openable = b0_usb_module_openable
	},

	.ain = {
		.open = b0_usb_ain_open,
		.close = b0_usb_ain_close,

		.bitsize_speed = {
			.set = b0_usb_ain_bitsize_speed_set,
			.get = b0_usb_ain_bitsize_speed_get
		},

		.chan_seq = {
			.set = b0_usb_ain_chan_seq_set,
			.get = b0_usb_ain_chan_seq_get
		},

		.snapshot = {
			.prepare = b0_usb_ain_snapshot_prepare,
			.start = b0_usb_ain_snapshot_start,
			.start_double = b0_generic_ain_snapshot_start_double,
			.start_float = b0_generic_ain_snapshot_start_float,
			.stop = b0_usb_ain_snapshot_stop
		},

		.stream = {
			.prepare = b0_usb_ain_stream_prepare,
			.start = b0_usb_ain_stream_start,
			.stop = b0_usb_ain_stream_stop,
			.read = b0_usb_ain_stream_read,
			.read_double = b0_generic_ain_stream_read_double,
			.read_float = b0_generic_ain_stream_read_float
		}
	},

	.aout = {
		.open = b0_usb_aout_open,
		.close = b0_usb_aout_close,

		.bitsize_speed = {
			.set = b0_usb_aout_bitsize_speed_set,
			.get = b0_usb_aout_bitsize_speed_get
		},

		.chan_seq = {
			.set = b0_usb_aout_chan_seq_set,
			.get = b0_usb_aout_chan_seq_get
		},

		.repeat = {
			.set = b0_usb_aout_repeat_set,
			.get = b0_usb_aout_repeat_get
		},

		.snapshot = {
			.prepare = b0_usb_aout_snapshot_prepare,
			.start = b0_usb_aout_snapshot_start,
			.start_float = b0_generic_aout_snapshot_start_float,
			.start_double = b0_generic_aout_snapshot_start_double,
			.stop = b0_usb_aout_snapshot_stop
		},

		.stream = {
			.prepare = b0_usb_aout_stream_prepare,
			.start = b0_usb_aout_stream_start,
			.stop = b0_usb_aout_stream_stop,
			.write = b0_usb_aout_stream_write,
			.write_double = b0_generic_aout_stream_write_double,
			.write_float = b0_generic_aout_stream_write_float
		}
	},

	.dio = {
		.open = b0_usb_dio_open,
		.close = b0_usb_dio_close,

		.single = {
			.value = {
				.get = b0_usb_dio_value_get,
				.set = b0_usb_dio_value_set,
				.toggle = b0_usb_dio_value_toggle,
			},

			.dir = {
				.get = b0_usb_dio_dir_get,
				.set = b0_usb_dio_dir_set,
			},

			.hiz = {
				.get = b0_usb_dio_hiz_get,
				.set = b0_usb_dio_hiz_set,
			}
		},

		.multiple = {
			.value = {
				.get = b0_usb_dio_multiple_value_get,
				.set = b0_usb_dio_multiple_value_set,
				.toggle = b0_usb_dio_multiple_value_toggle,
			},

			.dir = {
				.get = b0_usb_dio_multiple_dir_get,
				.set = b0_usb_dio_multiple_dir_set,
			},

			.hiz = {
				.get = b0_usb_dio_multiple_hiz_get,
				.set = b0_usb_dio_multiple_hiz_set,
			}
		},

		.all = {
			.value = {
				.get = b0_usb_dio_all_value_get,
				.set = b0_usb_dio_all_value_set,
				.toggle = b0_usb_dio_all_value_toggle,
			},

			.dir = {
				.get = b0_usb_dio_all_dir_get,
				.set = b0_usb_dio_all_dir_set,
			},

			.hiz = {
				.get = b0_usb_dio_all_hiz_get,
				.set = b0_usb_dio_all_hiz_set,
			}
		},

		.basic = {
			.prepare = b0_usb_dio_basic_prepare,
			.start = b0_usb_dio_basic_start,
			.stop = b0_usb_dio_basic_stop
		}
	},

	.spi = {
		.open = b0_usb_spi_open,
		.close = b0_usb_spi_close,

		.master = {
			.prepare = b0_usb_spi_master_prepare,
			.start = b0_usb_spi_master_start,
			.stop = b0_usb_spi_master_stop,
			.fd = b0_generic_spi_master_fd,
			.hd_read = b0_generic_spi_master_hd_read,
			.hd_write = b0_generic_spi_master_hd_write,
			.hd_write_read = b0_generic_spi_master_hd_write_read,
		},

		.speed = {
			.set = b0_usb_spi_speed_set,
			.get = b0_usb_spi_speed_get
		},

		.active_state = {
			.set = b0_usb_spi_active_state_set,
			.get = b0_usb_spi_active_state_get
		},
	},

	.i2c = {
		.open = b0_usb_i2c_open,
		.close = b0_usb_i2c_close,

		.master = {
			.prepare = b0_usb_i2c_master_prepare,
			.start = b0_usb_i2c_master_start,
			.stop = b0_usb_i2c_master_stop,

			.read = b0_generic_i2c_master_read,
			.write = b0_generic_i2c_master_write,
			.write8_read = b0_generic_i2c_master_write8_read,
			.write_read = b0_generic_i2c_master_write_read,

			.slave_id = b0_generic_i2c_master_slave_id,
			.slave_detect = b0_usb_i2c_master_slave_detect,
			.slaves_detect = b0_generic_i2c_master_slaves_detect
		}
	},

	.pwm = {
		.open = b0_usb_pwm_open,
		.close = b0_usb_pwm_close,

		.width = {
			.set = b0_usb_pwm_width_set,
			.get = b0_usb_pwm_width_get
		},

		.period = {
			.set = b0_usb_pwm_period_set,
			.get = b0_usb_pwm_period_get
		},

		.speed = {
			.set = b0_usb_pwm_speed_set,
			.get = b0_usb_pwm_speed_get
		},

		.output = {
			.prepare = b0_usb_pwm_output_prepare,
			.start = b0_usb_pwm_output_start,
			.stop = b0_usb_pwm_output_stop,
		}
	}
};

/* it is assumed that utf8 contain enough bytes for
 * 	containing the equivalent utf16 (including the null byte)
 * one suggestion is to allocate: (utf16_len * 3) byte in utf8 buffer
 *
 * TODO: check for realloc return NULL
 */
/**
 * Convert UTF-16 Little Endian string to UTF-8
 * @param utf16 UTF-16 string
 * @param utf8_len Number of UTF-16 character in @a utf16
 * @return UTF-8 String
 */
static uint8_t* le_utf16_to_utf8(uint16_t *utf16, size_t utf16_len)
{
	size_t i, j = 0;
	uint8_t *utf8 = NULL;

	for (i = 0; i < utf16_len; i++) {
		uint16_t c = B0_LE2H16(utf16[i]);

		/* as per USB Specs,
		 *  string descriptor is not NULL terminated.
		 * but still placing a check for future */
		if (c == 0x0000) {
			break;
		}

		if ((c >= 0x0001) && (c <= 0x007F)) {
			utf8 = realloc(utf8, j + 1);
			utf8[j++] = c;
		} else if (c > 0x07FF) {
			utf8 = realloc(utf8, j + 6);
			utf8[j++] = 0xE0; utf8[j++] = (c >> 12) & 0x0F;
			utf8[j++] = 0x80; utf8[j++] = (c >>  6) & 0x3F;
			utf8[j++] = 0x80; utf8[j++] = (c >>  0) & 0x3F;
		} else {
			utf8 = realloc(utf8, j + 4);
			utf8[j++] = 0xC0; utf8[j++] = (c >>  6) & 0x1F;
			utf8[j++] = 0x80; utf8[j++] = (c >>  0) & 0x3F;
		}
	}

	/* NULL */
	utf8 = realloc(utf8, j + 1);

	if (B0_IS_NOT_NULL(utf8)) {
		utf8[j++] = '\0';
	}

	return utf8;
}

/**
 * Fetch string descriptor and convert it UTF-8
 * @param dev Device
 * @param desc_index Descriptor Index
 * @return UTF-8 String on success
 * @return NULL on failure
 * @return NULL on no content
 */
uint8_t *b0_usb_string_desc_as_utf8(b0_device *dev, uint8_t desc_index)
{
	int r;
	uint16_t tmp_u16str[127];

	if (!desc_index) {
		/* Invalid index */
		return NULL;
	}

	/* TODO: support other language than English */
	r = libusb_get_string_descriptor(B0_USB_DEVICE_USBDH(dev), desc_index,
			0x0409, (unsigned char *) tmp_u16str, sizeof(tmp_u16str));

	if (r < 0) {
		B0_WARN(dev, "unable to fetch string descriptor %"PRIu8, desc_index);

		/* still need to place it in list */
		r = 0;
	}

	/* decrease two byte (remove the descriptor header) */
	r = (r - 2) / 2;
	if (r > 0) {
		return le_utf16_to_utf8(&tmp_u16str[1], (size_t) r);
	}

	return NULL;
}

/**
 * Append the @a vendor iLabel to the @a _values
 * @param dev Device
 * @param vendor Box0 USB Label
 * @param used Number of entries already consumed out of @a vendor
 * @param _values Pointer to pointer where the final label to be kept
 * @param _count Number of entries already in @a _values
 * @return result code
 */
b0_result_code b0_usb_label_append(b0_device *dev,
			const struct b0_usb_label *vendor, unsigned used,
			uint8_t ***_values, size_t *_count)
{
	size_t i, limit = vendor->header.bLength - sizeof(struct b0_usb_property);

	if (used >= limit) {
		/* Nothing to process */
		return B0_OK;
	}

	uint8_t **values = *_values;
	size_t count = *_count;

	values = realloc(values, sizeof(*values) * (count + limit - used));
	B0_ASSERT_RETURN_ON_NULL_ALLOC(dev, values);

	for (i = used; i < limit; i++) {
		/* copy the new descriptor */
		values[count++] = b0_usb_string_desc_as_utf8(dev, vendor->iValues[i]);
	}

	*_values = values;
	*_count = count;

	return B0_OK;
}

struct vendor_speed_raw {
	uint32_t dValues[0];
} B0_PACKED;

static int parse_raw(b0_device *dev, const struct vendor_speed_raw *vendor,
		size_t vendor_size, unsigned long **_values, size_t *_count)
{
	size_t i, len = vendor_size / 4;
	unsigned long *values = *_values;
	size_t count = *_count;

	B0_ASSERT_RETURN_ON_FAIL(dev, len > 0, B0_ERR_BOGUS);
	values = realloc(values, sizeof(*values) * (count + len));
	B0_ASSERT_RETURN_ON_NULL_ALLOC(dev, values);
	*_values = values;

	for (i = 0; i < len; i++) {
		values[count++] = B0_LE2H32(vendor->dValues[i]);
	}

	*_count = count;
	return B0_OK;
}

struct vendor_speed_inc {
	uint32_t dCount;
	uint32_t dStart;
	uint32_t dIncrement;
} B0_PACKED;

static int parse_inc(b0_device *dev, const struct vendor_speed_inc *vendor,
		size_t vendor_size, unsigned long **_values, size_t *_count)
{
	B0_UNUSED(vendor_size);

	uint32_t i, value = B0_LE2H32(vendor->dStart);
	uint32_t len = B0_LE2H32(vendor->dCount);
	unsigned long *values = *_values;
	size_t count = *_count;

	B0_ASSERT_RETURN_ON_FAIL(dev, len, B0_ERR_BOGUS);
	values = realloc(values, sizeof(*values) * (count + len));
	B0_ASSERT_RETURN_ON_NULL_ALLOC(dev, values);
	*_values = values;

	for (i = 0; i < len; i++) {
		values[count++] = value;
		value += B0_LE2H32(vendor->dIncrement);
	}

	*_count = count;

	return B0_OK;
}

struct vendor_speed_dec {
	uint32_t dCount;
	uint32_t dStart;
	uint32_t dDecrement;
} B0_PACKED;

static int parse_dec(b0_device *dev, const struct vendor_speed_dec *vendor,
		size_t vendor_size, unsigned long **_values, size_t *_count)
{
	B0_UNUSED(vendor_size);

	uint32_t i, value = B0_LE2H32(vendor->dStart);
	uint32_t len = B0_LE2H32(vendor->dCount);
	unsigned long *values = *_values;
	size_t count = *_count;

	B0_ASSERT_RETURN_ON_FAIL(dev, len, B0_ERR_BOGUS);
	values = realloc(values, sizeof(*values) * (count + len));
	B0_ASSERT_RETURN_ON_NULL_ALLOC(dev, values);
	*_values = values;

	for (i = 0; i < len; i++) {
		values[count++] = value;
		value -= B0_LE2H32(vendor->dDecrement);
		if (value == 0) {
			break;
		}
	}

	*_count = count;
	values = realloc(values, sizeof(*values) * count);
	B0_ASSERT_RETURN_ON_NULL_ALLOC(dev, values);
	*_values = values;

	return B0_OK;
}

struct vendor_speed_div {
	uint32_t dCount;
	uint32_t dSpeed;
} B0_PACKED;

static int parse_div(b0_device *dev, const struct vendor_speed_div *vendor,
		size_t vendor_size, unsigned long **_values, size_t *_count)
{
	B0_UNUSED(vendor_size);

	uint32_t i, len = B0_LE2H32(vendor->dCount);
	uint32_t current_value, last_value;
	unsigned long *values = *_values;
	unsigned count = *_count;

	B0_ASSERT_RETURN_ON_FAIL(dev, len, B0_ERR_BOGUS);
	values = realloc(values, sizeof(*values) * (count + len));
	B0_ASSERT_RETURN_ON_NULL_ALLOC(dev, values);
	*_values = values;

	last_value = 0;
	for (i = 0; i < len; i++) {
		current_value = B0_LE2H32(vendor->dSpeed) / (i + 1);
		if (current_value != last_value) {
			values[count++] = last_value = current_value;
		}
	}

	*_count = count;
	values = realloc(values, sizeof(*values) * count);
	B0_ASSERT_RETURN_ON_NULL_ALLOC(dev, values);
	*_values = values;

	return B0_OK;
}

static b0_result_code b0_usb_speed_parse_by_operator(b0_device *dev,
		uint8_t bOperator, const void *vendor, size_t vendor_size, unsigned long **values,
		size_t *count)
{
	switch(bOperator) {
	case B0_USB_SPEED_OPER_RAW:
		B0_DEBUG_RETURN_ON_FAIL_STMT(dev,
			parse_raw(dev, vendor, vendor_size, values, count));
	break;
	case B0_USB_SPEED_OPER_INC:
		B0_DEBUG_RETURN_ON_FAIL_STMT(dev,
			parse_inc(dev, vendor, vendor_size, values, count));
	break;
	case B0_USB_SPEED_OPER_DIV:
		B0_DEBUG_RETURN_ON_FAIL_STMT(dev,
			parse_div(dev, vendor, vendor_size, values, count));
	break;
	case B0_USB_SPEED_OPER_DEC:
		B0_DEBUG_RETURN_ON_FAIL_STMT(dev,
			parse_dec(dev, vendor, vendor_size, values, count));
	break;
	default:
		B0_WARN(dev, "speed operator not supported (0x%"PRIx8")", bOperator);
	return B0_ERR_SUPP;
	}

	return B0_OK;
}

b0_result_code b0_usb_speed_parse(b0_device *dev,
		const struct b0_usb_speed *vendor, unsigned long **values, size_t *count)
{
	const void *arg = vendor; arg += sizeof(*vendor);
	size_t arg_size = vendor->header.bLength - sizeof(*vendor);
	return b0_usb_speed_parse_by_operator(dev, vendor->bOperator, arg, arg_size, values, count);
}

b0_result_code b0_usb_bitsize_speed_parse(b0_device *dev,
		const struct b0_usb_bitsize_speed *vendor,
		struct _b0_usb_mode_bitsize_speeds *snapshot,
		struct _b0_usb_mode_bitsize_speeds *stream)
{
	size_t i, j;

	struct _b0_usb_mode_bitsize_speeds *mode = NULL;

	switch (vendor->bUse) {
	case B0_USB_BITSIZE_SPEED_SNAPSHOT_USE:
		mode = snapshot;
	break;
	case B0_USB_BITSIZE_SPEED_STREAM_USE:
		mode = stream;
	break;
	default:
	return B0_ERR_BOGUS;
	}

	for (i = 0; i < 3; i++) {
		unsigned int bitsize = vendor->bBitsize[i];
		if (!bitsize) {
			break;
		}

		/* Search for existing value */
		struct _b0_usb_bitsize_speeds *value = NULL;
		for (j = 0; j < mode->count; j++) {
			if (mode->values[j].bitsize == bitsize) {
				value = &mode->values[j];
				break;
			}
		}

		/* Allocate if not already existing */
		if (B0_IS_NULL(value)) {
			struct _b0_usb_bitsize_speeds *values = mode->values;
			values = realloc(values, sizeof(*values) * (mode->count + 1));
			B0_ASSERT_RETURN_ON_NULL_ALLOC(dev, values);

			value = &values[mode->count];
			value->bitsize = bitsize;
			value->speed.values = NULL;
			value->speed.count = 0;

			mode->values = values;
			mode->count++;
		}

		const void *arg = vendor; arg += sizeof(*vendor);
		size_t arg_size = vendor->header.bLength - sizeof(*vendor);

		B0_DEBUG_RETURN_ON_FAIL_STMT(dev, b0_usb_speed_parse_by_operator(dev,
				vendor->bSpeedOperator, arg, arg_size, &value->speed.values,
				&value->speed.count));
	}

	return B0_OK;
}

/**
 * Search for endpoints for module
 * @param mod Module
 * @param in_required IN endpoint must be successfully found
 * @param[out] in IN endpoint result store
 * @param out_required OUT endpoint must be successfully found
 * @param[out] out OUT endpoint result store
 */
B0_PRIV b0_result_code b0_usb_search_endpoint(b0_module *mod,
		bool in_required, struct b0_usb_ep *in,
		bool out_required, struct b0_usb_ep *out)
{
	unsigned int i;
	b0_result_code r;
	int j, k;

	const struct libusb_interface_descriptor *altinterface;
	const struct libusb_endpoint_descriptor *endpoint;
	const struct libusb_interface *interface;

	const struct libusb_endpoint_descriptor *fav_in_endpoint = NULL;
	const struct libusb_endpoint_descriptor *fav_out_endpoint = NULL;
	const struct libusb_interface_descriptor *fav_altinterface = NULL;

	b0_device *dev = B0_MODULE_DEVICE(mod);
	b0_usb_module_data *module_backend_data = B0_MODULE_BACKEND_DATA(mod);
	b0_usb_device_data *device_backend_data = B0_DEVICE_BACKEND_DATA(dev);

	/* fetch descriptor */
	struct libusb_config_descriptor *config;
	if (libusb_get_active_config_descriptor(device_backend_data->usbd, &config) < 0) {
		B0_WARN(dev, "libusb_get_active_config_descriptor failed");
		return B0_ERR;
	}

	/* find interface */
	for (i = 0; i < config->bNumInterfaces; i++) {
		interface = &config->interface[i];

		/* search for alternate setting in interface */
		for (j = 0; j < interface->num_altsetting; j++) {
			altinterface = &interface->altsetting[j];

			if (altinterface->bInterfaceNumber != module_backend_data->bInterfaceNumber) {
				/* do not belong to interface we are looking for */
				continue;
			}

			if (altinterface->bAlternateSetting != module_backend_data->bAlternateSetting) {
				/* do not belong to interface we are looking for */
				continue;
			}

			/* search for favourable endpoints */
			for (k = 0; k < altinterface->bNumEndpoints; k++) {
				endpoint = &altinterface->endpoint[k];

				/* only bulk are accepted */
				uint8_t ep_type = endpoint->bmAttributes & 0x3;
				if (ep_type == LIBUSB_TRANSFER_TYPE_BULK) {
					/* first entry only accepted */
					if (endpoint->bEndpointAddress & 0x80) {
						if (B0_IS_NULL(fav_in_endpoint)) {
							fav_in_endpoint = endpoint;
						}
					} else {
						if (B0_IS_NULL(fav_out_endpoint)) {
							fav_out_endpoint = endpoint;
						}
					}
				}
			}

			/* found! */
			fav_altinterface = altinterface;
			goto break_all;
		}
	}

	break_all:
	if (B0_IS_NULL(fav_altinterface)) {
		/* no favourable config found: not supported */
		B0_WARN(dev, "module do not contain any bulk interface");
		r = B0_ERR_SUPP;
		goto done;
	}

	/* save out endpoint */
	if (B0_IS_NOT_NULL(out)) {
		if (B0_IS_NOT_NULL(fav_out_endpoint)) {
			out->bEndpointAddress = fav_out_endpoint->bEndpointAddress;
			out->wMaxPacketSize = fav_out_endpoint->wMaxPacketSize;
		} else if (out_required) {
			r = B0_ERR_BOGUS;
			goto done;
		}
	}

	/* save in endpoint */
	if (B0_IS_NOT_NULL(in)) {
		if (B0_IS_NOT_NULL(fav_in_endpoint)) {
			in->bEndpointAddress = fav_in_endpoint->bEndpointAddress;
			in->wMaxPacketSize = fav_in_endpoint->wMaxPacketSize;
		} else if (in_required) {
			r = B0_ERR_BOGUS;
			goto done;
		}
	}

	r = B0_OK;

	/* free descriptor */
	done:
	libusb_free_config_descriptor(config);
	return r;
}

/**
 * Free up the 2 dimentional array.
 * It will free internal entries too (if not NULL)
 * @param arr Array
 * @param size Number of entries in @a arr
 */
void b0_usb_free_2d_dynamic_array(void **arr, size_t size)
{
	size_t i;

	if (B0_IS_NULL(arr) || !size) {
		return;
	}

	/* First free up internal arrays */
	for (i = 0; i < size; i++) {
		B0_FREE_IF_NOT_NULL(arr[i]);
	}

	free(arr);
}

/**
 * Reallocate a 2 dimentional array
 * It will free the extra entries if (size_old > size_new)
 * It will set NULL the new entries if (size_old < size_new)
 * @param arr Array
 * @param size_old Old size
 * @param size_new New size
 * @return Pointer to the array
 * @return NULL on failure (Old array is intact and valid)
 */
void **b0_usb_realloc_2d_array(void **arr, size_t size_old, size_t size_new)
{
	size_t i;

	/* Nothing to do */
	if (size_old == size_new) {
		return arr;
	}

	if (size_old > size_new) {
		/* Free up the extra items before realloc */
		for (i = size_new; i < size_old; i++) {
			B0_FREE_IF_NOT_NULL(arr[i]);
		}
	}

	arr = realloc(arr, sizeof(*arr) * size_new);

	if (B0_IS_NOT_NULL(arr)) {
		/* NULL out the newly allocated memory */
		for (i = size_old; i < size_new; i++) {
			arr[i] = NULL;
		}
	}

	return arr;
}

b0_result_code b0_usb_bitsize_parse(b0_device *dev,
		const struct b0_usb_bitsize *vendor,
		unsigned int **_values, size_t *_count)
{
	size_t len = vendor->header.bLength - sizeof(struct b0_usb_property);
	size_t i, count = *_count;
	unsigned int *values = *_values;

	/* Allocate memory */
	values = realloc(values, sizeof(*values) * (count + len));
	B0_ASSERT_RETURN_ON_NULL_ALLOC(dev, values);

	/* Copy the values */
	for (i = 0; i < len; i++) {
		values[count + i] = vendor->bValues[i];
	}

	/* keep back results */
	*_values = values;
	*_count = count + len;

	return B0_OK;
}

b0_result_code b0_usb_ref_parse(b0_device *dev,
		const struct b0_usb_ref *vendor, double *low, double *high,
		b0_ref_type *type)
{
	B0_UNUSED(dev);

	if (B0_IS_NOT_NULL(type)) {
		*type = vendor->bType;
	}

	if (B0_IS_NOT_NULL(low)) {
		*low = vendor->spLow;
	}

	if (B0_IS_NOT_NULL(high)) {
		*high = vendor->spHigh;
	}

	return B0_OK;
}

b0_result_code b0_usb_count_parse(b0_device *dev,
		const struct b0_usb_count *vendor, unsigned int *value)
{
	B0_UNUSED(dev);

	if (!vendor->bCount) {
		return B0_ERR_BOGUS;
	}

	*value = vendor->bCount;
	return B0_OK;
}

b0_result_code b0_usb_buffer_parse(b0_device *dev,
		const struct b0_usb_buffer *vendor, size_t *value)
{
	B0_UNUSED(dev);

	if (!vendor->dValue) {
		return B0_ERR_BOGUS;
	}

	*value = vendor->dValue;
	return B0_OK;
}

/** @endcond */
