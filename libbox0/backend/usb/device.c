/*
 * This file is part of libbox0.
 * Copyright (C) 2013-2016 Kuldeep Singh Dhaka <kuldeep@madresistor.com>
 *
 * libbox0 is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * libbox0 is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with libbox0.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "usb-private.h"

/** @cond INTERNAL */

static b0_result_code favourable_config(b0_device *dev, int *conf);
static void *polling_thread(void *arg);

static b0_result_code open_handle(libusb_device_handle *handle,
			libusb_context *usbc, b0_device **dev, int internal_resource);
static b0_result_code open_device(libusb_device *usbd, libusb_context *usbc,
			b0_device **dev, int internal_resource);

static const b0_usb supported_devices[] = {{
		.backend = &usb_backend,
		.vid = 0x1d50,
		.pid = 0x8085
	},
	{NULL, 0, 0}
};

enum {
	INTERNAL_LIBUSB_CONTEXT = 1,
	INTERNAL_LIBUSB_DEVICE_HANDLE = 2,
	INTERNAL_LIBUSB_DEVICE = 4
};

static b0_result_code open_device(libusb_device *usbd, libusb_context *usbc,
		b0_device **dev, int internal_resource)
{
	libusb_device_handle *usbdh;

	if (libusb_open(usbd, &usbdh) < 0) {
		B0_WARN(NULL, "libusb_open failed");
		return B0_ERR;
	}

	internal_resource |= INTERNAL_LIBUSB_DEVICE_HANDLE;
	return open_handle(usbdh, usbc, dev, internal_resource);
}

/* determine the type of fixes required for this specific device */
static b0_result_code learn_fix(b0_device *dev)
{
	b0_usb_device_data *backend_data = B0_DEVICE_BACKEND_DATA(dev);

	struct libusb_device_descriptor dev_desc;
	if (libusb_get_device_descriptor(backend_data->usbd, &dev_desc) < 0) {
		B0_WARN(dev, "unable to fetch device descriptor");
		return B0_ERR_IO;
	}

	backend_data->fix.i2c_0byte = false;

	/* STM32F0 series uC specific */
	if (dev_desc.idVendor == 0x1d50 && dev_desc.idProduct == 0x8085) {
		if ((dev_desc.bcdDevice & 0xFF00) == 0x0500 /* 05.xx */) {
			B0_DEBUG(dev, "box0-v5 device detected, "
				"configuring fixes: i2c0_byte");
			backend_data->fix.i2c_0byte = true;
		}
	}

	return B0_OK;
}

static b0_result_code learn_config(b0_device *dev)
{
	b0_usb_device_data *backend_data = B0_DEVICE_BACKEND_DATA(dev);

	backend_data->config.bulk_timeout = B0_USB_BULK_TIMEOUT;
	backend_data->config.ctrlreq_timeout = B0_USB_CTRLREQ_TIMEOUT;

	return B0_OK;
}

/**
 * Read name,manuf,serial string descriptor from device
 * @param[in] dev Device
 * @return result code
 */
static b0_result_code read_string_name_manuf_serial(b0_device *dev)
{
	b0_usb_device_data *backend_data = B0_DEVICE_BACKEND_DATA(dev);
	struct libusb_device_descriptor dev_desc;
	uint8_t *name = NULL, *manuf = NULL, *serial = NULL;
	b0_result_code r;

	/* get device descriptor */
	if (libusb_get_device_descriptor(backend_data->usbd, &dev_desc) < 0) {
		B0_WARN(NULL, "unable to fetch device descriptor");
		r = B0_ERR;
		goto error;
	}

	/* TODO: support other languages */

	/* get name string */
	if (dev_desc.iProduct > 0) {
		unsigned char buf[100];
		if (libusb_get_string_descriptor_ascii(backend_data->usbdh,
								dev_desc.iProduct, buf, sizeof(buf)) < 0) {
			B0_WARN(NULL, "unable to fetch string descriptor index: iProduct");
			r = B0_ERR;
			goto error;
		}

		name = (uint8_t *) strdup((const char *) buf);
		B0_ASSERT_RETURN_ON_NULL_ALLOC(dev, name);
	}

	/* get manuf string */
	if (dev_desc.iManufacturer > 0) {
		unsigned char buf[100];
		if (libusb_get_string_descriptor_ascii(backend_data->usbdh,
								dev_desc.iManufacturer, buf, sizeof(buf)) < 0) {
			B0_WARN(NULL, "unable to fetch string descriptor index: iManufacturer");
			r = B0_ERR;
			goto error;
		}

		manuf = (uint8_t *) strdup((const char *) buf);
		B0_ASSERT_RETURN_ON_NULL_ALLOC(dev, manuf);
	}

	/* get serial string */
	if (dev_desc.iSerialNumber > 0) {
		unsigned char buf[100];
		if (libusb_get_string_descriptor_ascii(backend_data->usbdh,
								dev_desc.iSerialNumber, buf, sizeof(buf)) < 0) {
			B0_WARN(dev, "unable to fetch string descriptor index: iSerialNumber");
			r = B0_ERR;
			goto error;
		}

		serial = (uint8_t *) strdup((const char *) buf);
		B0_ASSERT_RETURN_ON_NULL_ALLOC(dev, serial);
	}

	dev->name = name;
	dev->manuf = manuf;
	dev->serial = serial;
	return B0_OK;

	error:
	B0_FREE_IF_NOT_NULL(name);
	B0_FREE_IF_NOT_NULL(manuf);
	B0_FREE_IF_NOT_NULL(serial);

	return r;
}

/**
 * Initalize device configuration
 * @param[in] dev Device
 * @return result code
 */
static b0_result_code init_conf(b0_device *dev)
{
	b0_result_code r;
	int conf;

	if (libusb_get_configuration(B0_USB_DEVICE_USBDH(dev), &conf) < 0) {
		B0_WARN(dev, "libusb_get_configuration failed");
		r = B0_ERR;
		goto error;
	}

	/* device is in address state? */
	if (conf == 0) {
		B0_DEBUG(dev, "device is in address state");

		r = favourable_config(dev, &conf);

		if (B0_ERROR_RESULT_CODE(r)) {
			B0_DEBUG(dev, "No favourable config found");
			goto error;
		}

		if (libusb_set_configuration(B0_USB_DEVICE_USBDH(dev), conf) < 0) {
			B0_WARN(dev, "unable to set config");
			r = B0_ERR;
			goto error;
		}
	}

	return B0_OK;

	error:
	return r;
}

static b0_result_code open_handle(libusb_device_handle *usbdh,
		libusb_context *usbc, b0_device **dev, int internal_resource)
{
	b0_result_code r = B0_ERR;
	int e;
	b0_device *dev_alloc = NULL;
	b0_usb_device_data *backend_data = NULL;
	const b0_backend *backend = NULL;

	/* get usb device */
	libusb_device *usbd = libusb_get_device(usbdh);
	if (B0_IS_NULL(usbd)) {
		B0_WARN(NULL, "libusb_get_device failed");
		goto error;
	}

	/* search backend */
	r = b0_usb_search_backend(usbd, &backend);
	if (B0_ERROR_RESULT_CODE(r)) {
		B0_WARN(NULL, "backend not found");
		goto error;
	}

	backend_data = calloc(1, sizeof(*backend_data));
	if (B0_IS_NULL(backend_data)) {
		r = B0_ERR_ALLOC;
		goto error;
	}

	backend_data->usbdh = usbdh;
	backend_data->usbd = usbd;
	backend_data->usbc = usbc;
	backend_data->internal_resource = internal_resource;

	/* allocate device */
	r = b0_device_alloc(&dev_alloc);
	if (B0_ERROR_RESULT_CODE(r)) {
		B0_WARN(NULL, "device frontend failed");
		goto error;
	}

	dev_alloc->backend = backend;
	dev_alloc->backend_data = backend_data;

	/* learn the type of device specific fixes required */
	r = learn_fix(dev_alloc);
	if (B0_ERROR_RESULT_CODE(r)) {
		B0_WARN(dev_alloc, "unable to learn fix");
		goto error;
	}

	/* learn the type of device specific fixes required */
	r = learn_config(dev_alloc);
	if (B0_ERROR_RESULT_CODE(r)) {
		B0_WARN(dev_alloc, "unable to learn config");
		goto error;
	}

	/* read strings */
	r = read_string_name_manuf_serial(dev_alloc);
	if (B0_ERROR_RESULT_CODE(r)) {
		goto error;
	}

	/* upto this point all member of b0_device are initalized. */

	e = libusb_set_auto_detach_kernel_driver(usbdh, 1);
	if (e < 0 && e != LIBUSB_ERROR_NOT_SUPPORTED) {
		B0_WARN(dev_alloc, "unable to enable auto-detach-kernel-driver");
		r = B0_ERR;
		goto error;
	}

	/* initalize device configuration */
	r = init_conf(dev_alloc);
	if (B0_ERROR_RESULT_CODE(r)) {
		goto error;
	}

	/* build modules list */
	r = b0_usb_build_modules_list(dev_alloc);
	if (B0_ERROR_RESULT_CODE(r)) {
		goto error;
	}

	/* start a polling thread */
	backend_data->async.exit = 0;
	if (pthread_create(&backend_data->async.thread_id, NULL, polling_thread, dev_alloc)) {
		B0_ERROR(dev_alloc, "Unable to start thread");
		r = B0_ERR;
		goto error;
	}

	*dev = dev_alloc;
	return B0_OK;

	error:
	B0_FREE_IF_NOT_NULL(backend_data);
	B0_FREE_IF_NOT_NULL(dev_alloc);

	return r;
}

static b0_result_code favourable_config(b0_device *dev, int *config_out)
{
	uint8_t i;
	char name[20], *_name;
	struct libusb_device_descriptor device_desc;
	b0_usb_device_data *backend_data = B0_DEVICE_BACKEND_DATA(dev);

	if (libusb_get_device_descriptor(backend_data->usbd, &device_desc) < 0) {
		B0_WARN(dev, "unable to fetch device descriptor");
		return B0_ERR_IO;
	}

	for (i = 0; i < device_desc.bNumConfigurations; i++) {
		struct libusb_config_descriptor *conf;
		if (libusb_get_config_descriptor(backend_data->usbd, i, &conf) != 0) {
			B0_WARN(dev, "unable to fetch config descriptor");
			continue;
		}

		if (libusb_get_string_descriptor_ascii(backend_data->usbdh,
			conf->iConfiguration, (unsigned char *)name,
				sizeof(name)) < 0) {
			B0_WARN(dev, "unable to fetch string descriptor");
			libusb_free_config_descriptor(conf);
			continue;
		}

		/* convert string to lowercase */
		_name = name;
		while (*_name) {
			*_name = tolower(*_name);
			_name++;
		}

		/* TODO: for language other than English, need to compare at utf16 */
		if (B0_IS_NOT_NULL(strstr(name, "box0"))) {
			/* assuming, if this contain the box0 substring, this contain
			 * some descriptor related to box0
			 */
			libusb_free_config_descriptor(conf);
			*config_out = conf->bConfigurationValue;
			return B0_OK;
		}
	}

	return B0_ERR;
}

/* sometimes the program hangs when exiting.
 * is it due to this thead? (seen in AIN)
 */
static void *polling_thread(void *arg)
{
	b0_device *dev = (b0_device *)arg;
	b0_usb_device_data *backend_data = B0_DEVICE_BACKEND_DATA(dev);

	struct timeval tv;
	tv.tv_sec = 1;
	tv.tv_usec = 0;

	while (backend_data->async.exit == 0) {
		int r = libusb_handle_events_timeout_completed(backend_data->usbc, &tv,
			&backend_data->async.exit);
		if (r < 0) {
			B0_WARN(dev, "libusb_handle_events_timeout_completed() failed"
				" (returned %s [%i])", libusb_error_name(r), r);
		}
	}

	return (void *)B0_OK;
}

b0_result_code b0_usb_search_backend(libusb_device *usbd, const b0_backend **backend)
{
	const b0_usb *i;
	struct libusb_device_descriptor dev_desc;

	B0_ASSERT_RETURN_ON_NULL_ARG(NULL, usbd);
	B0_ASSERT_RETURN_ON_NULL_ARG(NULL, backend);

	if (libusb_get_device_descriptor(usbd, &dev_desc) < 0) {
		/* unable to fetch device desceriptor */
		return B0_ERR;
	}

	for (i = supported_devices; i->backend; i++) {
		if (dev_desc.idVendor == i->vid && dev_desc.idProduct == i->pid) {
			*backend = i->backend;
			return B0_OK;
		}
	}

	/* Not a compatible device */
	return B0_ERR_SUPP;
}

b0_result_code b0_usb_open_supported(b0_device **dev)
{
	libusb_device **list_usbd;
	ssize_t len, i;
	b0_device *dev_alloc = NULL;
	libusb_context *usbc;

	int internal_resource = 0;
	internal_resource |= INTERNAL_LIBUSB_CONTEXT;
	internal_resource |= INTERNAL_LIBUSB_DEVICE_HANDLE;

	B0_ASSERT_RETURN_ON_NULL_ARG(NULL, dev);

	/* generate a context */
	if (libusb_init(&usbc)) {
		/* unable to initalize libusb context */
		B0_WARN(NULL, "libusb_init failed");
		return B0_ERR;
	}

	len = libusb_get_device_list(usbc, &list_usbd);
	if (len < 0) {
		B0_WARN(NULL, "libusb_get_device_list returned %i", len);
		return B0_ERR_UNAVAIL;
	}

	for (i = 0; i < len; i++) {
		libusb_device *usbd = list_usbd[i];
		const b0_backend *backend;

		if (B0_ERROR_RESULT_CODE(b0_usb_search_backend(usbd, &backend))) {
			/* error occured or not supported */
			continue;
		}

		b0_device *dev_tmp;
		if (B0_ERROR_RESULT_CODE(open_device(usbd, usbc, &dev_tmp, internal_resource))) {
			/* unable to prepare device */
			continue;
		}

		dev_alloc = dev_tmp;
		break;
	}

	libusb_free_device_list(list_usbd, 1);

	if (B0_IS_NOT_NULL(dev_alloc)) {
		*dev = dev_alloc;
		return B0_OK;
	}

	libusb_exit(usbc);
	return B0_ERR_UNAVAIL;
}

/** @copydoc b0_device_ping */
b0_result_code b0_usb_device_ping(b0_device *dev)
{
	int r = B0_USB_DEVICE_CTRLREQ_OUT(dev, B0_USB_NOP, 0x0000, 0x0000, NULL, 0);

	switch(r) {
	case LIBUSB_ERROR_TIMEOUT:
		/* timeout */
		return B0_ERR_TIMEOUT;
	break;
	case LIBUSB_ERROR_PIPE:
		/* device dont support this wtf? */
		return B0_ERR_SUPP;
	break;
	case LIBUSB_ERROR_NO_DEVICE:
		/* disconnected */
		return B0_ERR_UNAVAIL;
	break;
	default:
		/* unknown */
		return (r < 0) ? B0_ERR : B0_OK;
	}
}

/** @endcond */

b0_result_code b0_usb_open_vid_pid(b0_device **dev, uint16_t vid, uint16_t pid)
{
	libusb_context *usbc;
	b0_result_code r;

	if (libusb_init(&usbc)) {
		B0_WARN(NULL, "libusb_init failed");
		return B0_ERR;
	}

	libusb_device_handle *usbdh = libusb_open_device_with_vid_pid(usbc, vid, pid);
	if (B0_IS_NULL(usbdh)) {
		B0_WARN(NULL, "libusb_open_device_with_vid_pid failed");
		r = B0_ERR_UNAVAIL;
		goto error;
	}

	int internal_resource = 0;
	internal_resource |= INTERNAL_LIBUSB_CONTEXT;
	internal_resource |= INTERNAL_LIBUSB_DEVICE_HANDLE;

	r = open_handle(usbdh, usbc, dev, internal_resource);
	if (B0_SUCCESS_RESULT_CODE(r)) {
		return r;
	}

	/* close the handle as it is not require anymore */
	libusb_close(usbdh);

	error:
	libusb_exit(usbc);
	return r;
}

b0_result_code b0_usb_open(libusb_device *usbd, libusb_context *usbc,
							b0_device **dev)
{
	B0_ASSERT_RETURN_ON_NULL_ARG(NULL, usbd);
	B0_ASSERT_RETURN_ON_NULL_ARG(NULL, dev);

	return open_device(usbd, usbc, dev, 0);
}

b0_result_code b0_usb_open_handle(libusb_device_handle *usbdh,
			libusb_context *usbc, b0_device **dev)
{
	B0_ASSERT_RETURN_ON_NULL_ARG(NULL, usbdh);
	B0_ASSERT_RETURN_ON_NULL_ARG(NULL, dev);

	return open_handle(usbdh, usbc, dev, 0);
}

/**
 * @copydoc b0_device_close
 * @note internally generated resources will be freed
 * @see b0_usb_open_handle()
 * @see b0_usb_open_vid_pid()
 * @see b0_usb_open()
 * @see b0_usb_open_supported()
 */
b0_result_code b0_usb_device_close(b0_device *dev)
{
	b0_usb_device_data *backend_data = B0_DEVICE_BACKEND_DATA(dev);

	/* end polling thread */
	backend_data->async.exit = 1;

	B0_DEBUG_STMT(dev, b0_usb_free_modules(dev));

	B0_DEBUG(dev, "Waiting for polling thread to exit");
	if (pthread_join(backend_data->async.thread_id, NULL)) {
		B0_WARN(dev, "Unable to join thread");
	}

	/* if not user supplied, free the device handle */
	if (backend_data->internal_resource & INTERNAL_LIBUSB_DEVICE_HANDLE) {
		libusb_close(backend_data->usbdh);
	}

	/* if not user supplied, free the device */
	if (backend_data->internal_resource & INTERNAL_LIBUSB_DEVICE) {
		/* _future_ */
	}

	/* if not user supplied, free the context */
	if (backend_data->internal_resource & INTERNAL_LIBUSB_CONTEXT) {
		libusb_exit(backend_data->usbc);
	}

	/* free backend */
	free(backend_data);

	/* free string */
	B0_FREE_IF_NOT_NULL(dev->name);
	B0_FREE_IF_NOT_NULL(dev->manuf);
	B0_FREE_IF_NOT_NULL(dev->serial);

	return b0_device_free(dev);
}

b0_result_code b0_usb_libusb_device(b0_device *dev, libusb_device **usbd)
{
	B0_ASSERT_RETURN_ON_NULL_ARG(NULL, dev);
	B0_ASSERT_RETURN_ON_NULL_ARG(dev, usbd);

	b0_usb_device_data *backend_data = B0_DEVICE_BACKEND_DATA(dev);
	*usbd = backend_data->usbd;
	return B0_OK;
}

b0_result_code b0_usb_libusb_context(b0_device *dev, libusb_context **usbc)
{
	B0_ASSERT_RETURN_ON_NULL_ARG(NULL, dev);
	B0_ASSERT_RETURN_ON_NULL_ARG(dev, usbc);

	b0_usb_device_data *backend_data = B0_DEVICE_BACKEND_DATA(dev);
	*usbc = backend_data->usbc;
	return B0_OK;
}

b0_result_code b0_usb_libusb_device_handle(b0_device *dev,
					libusb_device_handle **usbdh)
{
	B0_ASSERT_RETURN_ON_NULL_ARG(NULL, dev);
	B0_ASSERT_RETURN_ON_NULL_ARG(dev, usbdh);

	b0_usb_device_data *backend_data = B0_DEVICE_BACKEND_DATA(dev);
	*usbdh = backend_data->usbdh;
	return B0_OK;
}

b0_result_code b0_usb_device_bulk_timeout(b0_device *dev, unsigned long timeout)
{
	B0_ASSERT_RETURN_ON_NULL_ARG(NULL, dev);

	b0_usb_device_data *backend_data = B0_DEVICE_BACKEND_DATA(dev);
	backend_data->config.bulk_timeout = timeout;
	return B0_OK;
}

b0_result_code b0_usb_device_ctrlreq_timeout(b0_device *dev, unsigned long timeout)
{
	B0_ASSERT_RETURN_ON_NULL_ARG(NULL, dev);

	b0_usb_device_data *backend_data = B0_DEVICE_BACKEND_DATA(dev);
	backend_data->config.ctrlreq_timeout = timeout;
	return B0_OK;
}

b0_result_code b0_usb_ain_stream_delay(b0_ain *mod, unsigned long delay)
{
	B0_ASSERT_RETURN_ON_NULL_ARG(NULL, mod);

	b0_device *dev = B0_MODULE_DEVICE(mod);
	B0_ASSERT_RETURN_ON_FAIL_ARG(dev, delay > 0);

	b0_usb_ain_data *backend_data = B0_MODULE_BACKEND_DATA(mod);
	backend_data->config.delay = delay;
	return B0_OK;
}

b0_result_code b0_usb_aout_stream_pending(b0_aout *mod, unsigned long pending)
{
	B0_ASSERT_RETURN_ON_NULL_ARG(NULL, mod);

	b0_device *dev = B0_MODULE_DEVICE(mod);
	B0_ASSERT_RETURN_ON_FAIL_ARG(dev, pending > 0);

	b0_usb_aout_data *backend_data = B0_MODULE_BACKEND_DATA(mod);
	backend_data->config.pending = pending;
	return B0_OK;
}

/** @endcond */
