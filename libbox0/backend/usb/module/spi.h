/*
 * This file is part of libbox0.
 * Copyright (C) 2013-2016 Kuldeep Singh Dhaka <kuldeep@madresistor.com>
 *
 * libbox0 is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * libbox0 is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with libbox0.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef LIBBOX0_BACKEND_USB_MODULE_SPI_H
#define LIBBOX0_BACKEND_USB_MODULE_SPI_H

#include "../usb-private.h"

__BEGIN_DECLS

/** @cond INTERNAL */

struct b0_usb_spi_data {
	b0_usb_module_data header;

	/* Number of task processed. */
	uint32_t task_tag;

	struct {
		enum {
			B0_USB_SPI_LABEL_NEXT_SCLK,
			B0_USB_SPI_LABEL_NEXT_MOSI,
			B0_USB_SPI_LABEL_NEXT_MISO,
			B0_USB_SPI_LABEL_NEXT_SS
		} state;
		size_t ss_count;
	} label;

	size_t buffer_size;

	struct b0_usb_ep ep_in, ep_out;

	struct {
		unsigned long speed;
	} cache;

	struct {
		pthread_mutex_t in_progress;
	} run;
};

typedef struct b0_usb_spi_data b0_usb_spi_data;

B0_PRIV b0_result_code b0_usb_spi_open(b0_device *dev, b0_spi **mod, int index);
B0_PRIV b0_result_code b0_usb_spi_close(b0_spi *mod);

B0_PRIV b0_result_code b0_usb_spi_speed_set(b0_spi *mod, unsigned long value);
B0_PRIV b0_result_code b0_usb_spi_speed_get(b0_spi *mod, unsigned long *value);

B0_PRIV b0_result_code b0_usb_spi_active_state_set(b0_spi *mod,
	unsigned addr, bool value);

B0_PRIV b0_result_code b0_usb_spi_active_state_get(b0_spi *mod,
	unsigned addr, bool *value);

B0_PRIV b0_result_code b0_usb_spi_master_start(b0_spi *mod,
	const b0_spi_task *tasks, int *failed_task_index, int *failed_task_count);

B0_PRIV b0_result_code b0_usb_spi_master_stop(b0_spi *mod);

B0_PRIV b0_result_code b0_usb_spi_master_prepare(b0_spi *mod);

/* TODO: mono READ or WRITE operation support */

/** @endcond */

__END_DECLS

#endif
