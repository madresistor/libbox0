/*
 * This file is part of libbox0.
 * Copyright (C) 2013-2016 Kuldeep Singh Dhaka <kuldeep@madresistor.com>
 *
 * libbox0 is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * libbox0 is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with libbox0.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "../usb-private.h"

/** @cond INTERNAL */

static b0_result_code open_after_alloc(b0_ain *mod)
{
	b0_device *dev = B0_MODULE_DEVICE(mod);
	b0_usb_ain_data *backend_data = B0_MODULE_BACKEND_DATA(mod);

	/* initalize all properties */
	mod->snapshot.count = mod->stream.count = 0;
	mod->snapshot.values = mod->stream.values = NULL;
	mod->capab = 0;
	mod->ref.high = mod->ref.low = 0;
	mod->ref.type = 0;
	mod->chan_count = 0;
	mod->label.chan = NULL;
	mod->buffer_size = 0;
	backend_data->label.chan_count = 0;
	backend_data->cache.bitsize = 0;
	backend_data->cache.speed = 0;

	/* Initalize in_progress */
	if (pthread_mutex_init(&backend_data->run.in_progress, NULL)) {
		B0_ERROR(dev, "Failed to initalize mutex `in_progress`");
		return B0_ERR;
	}

	/* Initalize stream.data.lock */
	if (pthread_mutex_init(&backend_data->run.stream.data.lock, NULL)) {
		B0_ERROR(dev, "Failed to initalize mutex `stream.data.lock`");
		return B0_ERR;
	}

	/* Initalize stream.transfer.lock */
	if (pthread_mutex_init(&backend_data->run.stream.transfer.lock, NULL)) {
		B0_ERROR(dev, "Failed to initalize mutex `stream.transfer.lock`");
		return B0_ERR;
	}

	backend_data->config.delay = B0_USB_AIN_STREAM_DELAY;

	return B0_OK;
}

static b0_result_code capab_parse(b0_ain *mod,
			const struct b0_usb_capab *vendor)
{
	mod->capab = vendor->bmCapab;
	return B0_OK;
}

/**
 * @brief extract property/ value from vendor property descriptor
 */
static b0_result_code open_parse_callback(b0_ain *mod,
			const struct b0_usb_property *vendor)
{
	b0_device *dev = B0_MODULE_DEVICE(mod);

	switch(vendor->bProperty) {
	case B0_USB_BITSIZE_SPEED:
		B0_DEBUG_RETURN_ON_FAIL_STMT(dev, b0_usb_bitsize_speed_parse(dev,
			(const struct b0_usb_bitsize_speed *) vendor,
			(struct _b0_usb_mode_bitsize_speeds *) &mod->snapshot,
			(struct _b0_usb_mode_bitsize_speeds *) &mod->stream));
	break;

	case B0_USB_CAPAB:
		B0_DEBUG_RETURN_ON_FAIL_STMT(dev, capab_parse(mod,
			(const struct b0_usb_capab *) vendor));
	break;

	case B0_USB_REF:
		B0_DEBUG_RETURN_ON_FAIL_STMT(dev, b0_usb_ref_parse(dev,
			(const struct b0_usb_ref *) vendor, &mod->ref.low, &mod->ref.high,
			&mod->ref.type));
	break;

	case B0_USB_COUNT:
		B0_DEBUG_RETURN_ON_FAIL_STMT(dev, b0_usb_count_parse(dev,
			(const struct b0_usb_count *) vendor, &mod->chan_count));
	break;

	case B0_USB_LABEL: {
		b0_usb_ain_data *backend_data = B0_MODULE_BACKEND_DATA(mod);
		B0_DEBUG_RETURN_ON_FAIL_STMT(dev, b0_usb_label_append(dev,
			(const struct b0_usb_label *) vendor, 0, &mod->label.chan,
			&backend_data->label.chan_count));
	} break;

	case B0_USB_BUFFER:
		B0_DEBUG_RETURN_ON_FAIL_STMT(dev, b0_usb_buffer_parse(dev,
			(const struct b0_usb_buffer *) vendor, &mod->buffer_size));
	break;

	default:
		B0_WARN(dev, "Unknown property");
	return B0_ERR_SUPP;
	}

	return B0_OK;
}

static b0_result_code open_after_parse(b0_ain *mod)
{
	b0_device *dev = B0_MODULE_DEVICE(mod);
	b0_usb_ain_data *backend_data = B0_MODULE_BACKEND_DATA(mod);

	if (!mod->chan_count) {
		B0_WARN(dev, "AIN has no channels");
		return B0_ERR_BOGUS;
	}

	if (!mod->stream.count && !mod->snapshot.count) {
		B0_WARN(dev, "AIN do not support SNAPSHOT nor STREAM");
		return B0_ERR_BOGUS;
	}

	if (mod->snapshot.count && !mod->buffer_size) {
		B0_WARN(dev, "AIN do not have any buffer for SNAPSHOT");
		return B0_ERR_BOGUS;
	}

	void **label_chan = b0_usb_realloc_2d_array((void **) mod->label.chan,
							backend_data->label.chan_count, mod->chan_count);
	B0_ASSERT_RETURN_ON_NULL_ALLOC(dev, label_chan);
	mod->label.chan = (uint8_t **) label_chan;

	/* Find the endpoint we need to read from */
	B0_DEBUG_RETURN_ON_FAIL_STMT(dev,
		b0_usb_search_endpoint( B0_GENERIC_MODULE(mod), true,
			&backend_data->ep_in, false, NULL));

	return B0_OK;
}

/**
 * @brief open an AIN module
 * @details
 * 		this will take control over the USB interface
 * 		stream or snapshot mode need to be selected in order to further continue
 * @param[in] dev device to search for module
 * @param[out] mod pointer to location to store the pointer
 * @param[in] index module index
 * @return result code
 *
 * @see b0_usb_ain_stream_prepare()
 * @see b0_usb_ain_snapshot_prepare()
 * @see b0_usb_ain_close()
 */
b0_result_code b0_usb_ain_open(b0_device *dev, b0_ain **mod, int index)
{
	return b0_usb_derive_module_open(dev, B0_AIN, index,
		(void *) mod, sizeof(b0_ain), sizeof(b0_usb_ain_data),
		(b0_usb_derive_module_open_after_alloc) open_after_alloc,
		(b0_usb_derive_module_open_parse_callback) open_parse_callback,
		(b0_usb_derive_module_open_after_parse) open_after_parse);
}

static void free_mode(struct b0_ain_mode_bitsize_speeds *mode)
{
	size_t i;

	for (i = 0; i < mode->count; i++) {
		B0_FREE_IF_NOT_NULL(mode->values[i].speed.values);
	}

	B0_FREE_IF_NOT_NULL(mode->values);
}

static b0_result_code close_before_unload(b0_ain *mod)
{
	b0_device *dev = B0_MODULE_DEVICE(mod);
	b0_usb_ain_data *backend_data = B0_MODULE_BACKEND_DATA(mod);

	b0_usb_free_2d_dynamic_array((void **) mod->label.chan, mod->chan_count);

	free_mode(&mod->snapshot);
	free_mode(&mod->stream);

	/* Destroy in_progress */
	if (pthread_mutex_destroy(&backend_data->run.in_progress)) {
		B0_ERROR(dev, "Failed to destroy mutex `in_progress`");
	}

	/* Destroy stream.data.lock */
	if (pthread_mutex_destroy(&backend_data->run.stream.data.lock)) {
		B0_ERROR(dev, "Failed to destroy mutex `stream.data.lock`");
	}

	/* Destroy stream.transfer.lock */
	if (pthread_mutex_destroy(&backend_data->run.stream.transfer.lock)) {
		B0_ERROR(dev, "Failed to destroy mutex `stream.transfer.lock`");
	}

	return B0_OK;
}

/**
 * @brief close the AIN module
 * @details resources are free'd. \n
 *  this will release the USB interface and
 *  after this point, no communication be
 *  performed with module without openining again
 * @param[in] mod AIN module to close
 * @return result code
 */
b0_result_code b0_usb_ain_close(b0_ain *mod)
{
	return b0_usb_derive_module_close(mod,
		(b0_usb_derive_module_close_before_unload) close_before_unload);
}

b0_result_code b0_usb_ain_chan_seq_set(b0_ain *mod, unsigned int *values,
					size_t count)
{
	return b0_usb_module_chan_seq_set(B0_GENERIC_MODULE(mod), values, count);
}

b0_result_code b0_usb_ain_chan_seq_get(b0_ain *mod, unsigned int *values,
					size_t *count)
{
	return b0_usb_module_chan_seq_get(B0_GENERIC_MODULE(mod), values, count);
}

b0_result_code b0_usb_ain_bitsize_speed_set(b0_ain *mod,
				unsigned int bitsize, unsigned long speed)
{
	b0_result_code r;
	b0_usb_ain_data *backend_data =  B0_MODULE_BACKEND_DATA(mod);

	r = b0_usb_module_bitsize_speed_set(B0_GENERIC_MODULE(mod), bitsize, speed);

	if (B0_SUCCESS_RESULT_CODE(r)) {
		backend_data->cache.bitsize = bitsize;
		backend_data->cache.speed = speed;
	}

	return r;
}

B0_PRIV b0_result_code b0_usb_ain_bitsize_speed_get(b0_ain *mod,
			unsigned int *bitsize, unsigned long *speed)
{
	b0_result_code r;
	b0_usb_ain_data *backend_data =  B0_MODULE_BACKEND_DATA(mod);

	r = b0_usb_module_bitsize_speed_get(B0_GENERIC_MODULE(mod), bitsize, speed);

	if (B0_SUCCESS_RESULT_CODE(r)) {
		if (B0_IS_NOT_NULL(bitsize)) {
			backend_data->cache.bitsize = *bitsize;
		}

		if (B0_IS_NOT_NULL(speed)) {
			backend_data->cache.speed = *speed;
		}
	}

	return r;
}

/**
 * @brief Prepare the AIN module for snapshot mode
 * @details
 * 		1) search for alternate setting that have bulk endpoint.
 * 		2) take control over the interface
 * 		3) set the alternate setting on device
 *
 * @param[in] mod AIN module
 * @return result code
 *
 * @see b0_usb_ain_snapshot_start()
 * @see b0_usb_ain_open()
 */
b0_result_code b0_usb_ain_snapshot_prepare(b0_ain *mod)
{
	b0_result_code r;

	if (!mod->snapshot.count || !mod->buffer_size) {
		/* SNAPSHOT isnt supported */
		return B0_ERR_SUPP;
	}

	r = b0_usb_module_mode_set(B0_GENERIC_MODULE(mod), B0_USB_MODE_SNAPSHOT);

	if (B0_SUCCESS_RESULT_CODE(r)) {
		b0_usb_ain_data *backend_data = B0_MODULE_BACKEND_DATA(mod);
		backend_data->cache.bitsize = mod->snapshot.values[0].bitsize;
		backend_data->cache.speed = mod->snapshot.values[0].speed.values[0];
	}

	return r;
}

/**
 * @brief read samples from AIN module
 * @details
 * 		this will convert the raw data to double values
 * @param[in] mod AIN module
 * @param[out] samples memory to acquire samples
 * @param[in] count the number of samples to aquire from device
 * @param[in] actual_count The number of samples actually acquired
 * @note blocking call
 * @return result code
 * @see b0_usb_ain_snapshot_prepare()
 */
b0_result_code b0_usb_ain_snapshot_start(b0_ain *mod, void *samples,
		size_t count)
{
	b0_result_code r;
	b0_device *dev = B0_MODULE_DEVICE(mod);
	b0_usb_ain_data *backend_data = B0_MODULE_BACKEND_DATA(mod);
	unsigned int bitsize = backend_data->cache.bitsize;
	unsigned long speed = backend_data->cache.speed;
	uint8_t bEndpointAddress = backend_data->ep_in.bEndpointAddress;
	unsigned int bytes_per_sample = B0_BIT2BYTE(bitsize);
	int binary_len = bytes_per_sample * count;

	/* Acquisition time */
	unsigned int acq_ms = (unsigned int) ceil((count * 1000.0) / speed);

	/* prevent simultaneous request in multiple threads */
	if (pthread_mutex_trylock(&backend_data->run.in_progress)) {
		return B0_ERR_BUSY;
	}

	/* start acquisition */
	uint32_t _cnt = B0_H2LE32(count);
	r = b0_usb_module_state_start(B0_GENERIC_MODULE(mod), (uint8_t *)&_cnt,
						sizeof(_cnt));
	if (B0_ERROR_RESULT_CODE(r)) {
		B0_ERROR(dev, "unable to start AIN in snapshot mode");
		goto done;
	}

	/* we are ready to read from device */
	int transferred;
	int status = libusb_bulk_transfer(B0_USB_MODULE_USBDH(mod),
			bEndpointAddress, samples, binary_len, &transferred,
			B0_USB_MODULE_BULK_TIMEOUT(mod) + acq_ms);

	if (status < 0) {
		r = B0_ERR;
	} else if (transferred < binary_len) {
		r = B0_ERR_UNDERFLOW;
	} else {
		r = B0_OK;
	}

	done:
	/* free the lock */
	if (pthread_mutex_unlock(&backend_data->run.in_progress)) {
		B0_WARN(dev, "unlocking frontend `in_progress' failed");
	}

	return r;
}

/**
 * @brief send a STOP command to AIN module
 * @details
 * 		this function is pretty much useless and should be ignored.
 * 		this included to make the API complete and pretty.
 * @param[in] mod AIN module
 * @return result code
 *
 * @see b0_usb_ain_snapshot_start()
 * @see b0_usb_ain_snapshot_prepare()
 *
 * @note this function is just to place a similar API to
 *   stream (send a stop to device) but since snapshot_start is blocking,
 *   this is useless.
 *
 * @note it is mostly useless unless some error occured
 *   while running and stop need to be send
 */
b0_result_code b0_usb_ain_snapshot_stop(b0_ain *mod)
{
	return b0_usb_module_state_stop(B0_GENERIC_MODULE(mod), NULL, 0);
}

static void stream_callback(libusb_transfer *transfer);

static void stream_transfer_clean(b0_ain *mod);
static b0_result_code stream_transfer_push(b0_ain *mod,
		libusb_transfer *payload);
static b0_result_code stream_transfer_submit(b0_ain *mod);
static b0_result_code stream_transfer_build(b0_ain *mod);

static b0_result_code stream_data_push(b0_ain *mod,
		b0_usb_ain_stream_data_item *item, size_t total, size_t used);
static b0_result_code stream_data_pop(b0_ain *mod,
		b0_usb_ain_stream_data_item **item);
static b0_result_code stream_data_pop_reverse(b0_ain *mod,
		b0_usb_ain_stream_data_item *item);
static void stream_data_clean(b0_ain *mod);

#define ITEM_FROM_PAYLOAD(ptr) \
	(((void *) ptr) - offsetof(b0_usb_ain_stream_data_item, payload))

/**
 * @brief prepare AIN module for stream data transfer.
 * @details
 * 		- take control over the interface
 * 		- calculate various parameter for usb
 * 		- allocate buffer for value conver and cache required parameters
 *
 * @note  while AIN is running,
 *   calling [b0_usb_ain_stream_start() or b0_usb_ain_stream_prepare()]
 *   will *for-sure* result in *crash*
 *   so do not do that [learn from my mistake ;p]
 *
 * @param[in] mod AIN module
 * @return result code
 *
 * @see b0_usb_ain_stream_start()
 * @see b0_usb_ain_open()
 */
b0_result_code b0_usb_ain_stream_prepare(b0_ain *mod)
{
	b0_result_code r;
	b0_device *dev = B0_MODULE_DEVICE(mod);
	b0_usb_ain_data *backend_data = B0_MODULE_BACKEND_DATA(mod);

	/* stop not called after start */
	B0_ASSERT_RETURN_ON_FAIL(dev,
		B0_IS_NULL(backend_data->run.stream.transfer.head), B0_ERR_STATE);

	r = b0_usb_module_mode_set(B0_GENERIC_MODULE(mod), B0_USB_MODE_STREAM);

	if (B0_SUCCESS_RESULT_CODE(r)) {
		backend_data->cache.bitsize = mod->stream.values[0].bitsize;
		backend_data->cache.speed = mod->stream.values[0].speed.values[0];
	}

	return r;
}

static void stream_transfer_clean(b0_ain *mod)
{
	b0_device *dev = B0_MODULE_DEVICE(mod);
	b0_usb_ain_data *backend_data = B0_MODULE_BACKEND_DATA(mod);
	b0_usb_ain_stream_transfer *queue = &backend_data->run.stream.transfer;
	b0_usb_ain_stream_transfer_item *item = queue->head;
	queue->head = NULL;
	queue->tail = NULL;

	while (B0_IS_NOT_NULL(item)) {
		libusb_transfer *t = item->payload;

		/* free container */
		b0_usb_ain_stream_transfer_item *tmp = item;
		item = item->next;
		free(tmp);

		/* free transfer */
		int r = libusb_cancel_transfer(t);
		if (r == LIBUSB_ERROR_NOT_FOUND) {
			/* transfer not pending. */
			free(ITEM_FROM_PAYLOAD(t->buffer));
			libusb_free_transfer(t);
		} else if (r < 0) {
			B0_WARN(dev, "Error occured while transfer: %p", t);
		}
	}
}

static b0_result_code stream_transfer_push(b0_ain *mod, libusb_transfer *payload)
{
	b0_device *dev = B0_MODULE_DEVICE(mod);
	b0_usb_ain_data *backend_data = B0_MODULE_BACKEND_DATA(mod);

	b0_usb_ain_stream_transfer_item *item = malloc(sizeof(*item));
	B0_ASSERT_RETURN_ON_NULL_ALLOC(dev, item);

	item->payload = payload;
	item->next = NULL;

	b0_usb_ain_stream_transfer *queue = &backend_data->run.stream.transfer;

	if (B0_IS_NULL(queue->head)) {
		queue->head = item;
	} else {
		queue->tail->next = item;
	}
	queue->tail = item;

	return B0_OK;
}

/**
 * submit transfer to kernel
 */
static b0_result_code stream_transfer_submit(b0_ain *mod)
{
	b0_device *dev = B0_MODULE_DEVICE(mod);
	b0_usb_ain_data *backend_data = B0_MODULE_BACKEND_DATA(mod);
	b0_usb_ain_stream_transfer *queue = &backend_data->run.stream.transfer;
	b0_usb_ain_stream_transfer_item *i;

	for (i = queue->head; B0_IS_NOT_NULL(i); i = i->next) {
		int r = libusb_submit_transfer(i->payload);
		if (r < 0) {
			B0_WARN(dev, "Unable to submit %p transfer (%s)",
				i->payload, libusb_strerror(r));
			return B0_ERR;
		}
	}

	return B0_OK;
}

/**
 * build transfer.
 * fill in maximum number of packets that are allowed
 */
static b0_result_code stream_transfer_build(b0_ain *mod)
{
	b0_device *dev = B0_MODULE_DEVICE(mod);
	b0_usb_ain_data *backend_data = B0_MODULE_BACKEND_DATA(mod);
	size_t i, transfer_count = 10; /* TODO: variable */
	unsigned int timeout = 0; /* FIXME: make it variable */
	uint16_t max_pkt_size = backend_data->ep_in.wMaxPacketSize;
	uint8_t ep_addr = backend_data->ep_in.bEndpointAddress;
	unsigned long delay = backend_data->config.delay;
	unsigned long speed = backend_data->cache.speed;
	unsigned int bitsize = backend_data->cache.bitsize;
	unsigned int bytes_per_sample = B0_BIT2BYTE(bitsize);

	/* Number of packets in transfer */
	int pkt_cnt = (int) ceil(delay * speed * bytes_per_sample /
								(1000.0 * max_pkt_size));

	/* Number of bytes to read in 1 transfer */
	int tlen = pkt_cnt * max_pkt_size;

	/* prepare transfer objects
	 * so that one can be processed while
	 * anothers are being processed by kernel
	 */
	for (i = 0; i < transfer_count; i++) {
		libusb_transfer *transfer = libusb_alloc_transfer(0);
		B0_ASSERT_RETURN_ON_NULL(dev, transfer, B0_ERR_ALLOC);

		struct b0_usb_ain_stream_data_item *item;
		item = malloc(sizeof(*item) + tlen);
		if (B0_IS_NULL(item)) {
			libusb_free_transfer(transfer);
		}
		B0_ASSERT_RETURN_ON_NULL(dev, item, B0_ERR_ALLOC);

		libusb_transfer_cb_fn cb = (libusb_transfer_cb_fn) stream_callback;
		libusb_fill_bulk_transfer(transfer, B0_USB_MODULE_USBDH(mod), ep_addr,
								item->payload, tlen, cb, mod, timeout);

		B0_DEBUG_RETURN_ON_FAIL_STMT(dev, stream_transfer_push(mod, transfer));
	}

	return B0_OK;
}

/**
 * @brief start AIN streaming
 * @note after entering in this function, callback can be anytime performed.
 * @param[in] mod AIN module
 * @return result code
 *
 * @see b0_usb_ain_stream_prepare()
 * @see b0_usb_ain_stream_stop()
 */

b0_result_code b0_usb_ain_stream_start(b0_ain *mod)
{
	b0_result_code r;
	b0_device *dev = B0_MODULE_DEVICE(mod);
	b0_usb_ain_data *backend_data = B0_MODULE_BACKEND_DATA(mod);

	/* build transfer */
	r = stream_transfer_build(mod);
	if (B0_ERROR_RESULT_CODE(r)) {
		goto error;
	}

	/* send the start command */
	r = b0_usb_module_state_start(B0_GENERIC_MODULE(mod), NULL, 0);
	if (B0_ERROR_RESULT_CODE(r)) {
		B0_ERROR(dev, "unable to start AIN for stream");
		goto error;
	}

	backend_data->run.stream.running = true;

	/* submit the transfer */
	r = stream_transfer_submit(mod);
	if (B0_ERROR_RESULT_CODE(r)) {
		goto error;
	}

	return r;

	error:
	stream_transfer_clean(mod);
	return r;
}

/**
 * @brief stop streaming
 * @details
 *  after this function has returned, no further callback will be receivied.
 * @param[in] mod libbox0 ain module
 * @note this cannot be called from callback thread,
 *     because the internal mutex is already locked by callback.
 * @return B0_ERR_STATE if b0_usb_ain_stream_start() has not been called
 * @return B0_BUG on internal bug
 * @return B0_ERR is failed to lock/unlock internal mutex
 * @return B0_OK on success
 */
b0_result_code b0_usb_ain_stream_stop(b0_ain *mod)
{
	b0_result_code r;
	b0_usb_ain_data *backend_data = B0_MODULE_BACKEND_DATA(mod);
	b0_device *dev = B0_MODULE_DEVICE(mod);

	if (pthread_mutex_lock(&backend_data->run.in_progress)) {
		B0_WARN(dev, "failed to lock mutex in_progress");
		r = B0_ERR;
		goto error;
	}

	backend_data->run.stream.running = false;

	B0_DEBUG(dev, "Cancelling AIN%"PRIu8" transfers", backend_data->header.bIndex);
	stream_transfer_clean(mod);
	stream_data_clean(mod);

	if (pthread_mutex_unlock(&backend_data->run.in_progress)) {
		B0_WARN(dev, "failed to unlock mutex in_progress");
		r = B0_ERR;
		goto error;
	}

	/* send the stop command */
	r = b0_usb_module_state_stop(B0_GENERIC_MODULE(mod), NULL, 0);
	if (B0_ERROR_RESULT_CODE(r)) {
		B0_ERROR(dev, "unable to stop AIN");
		goto error;
	}

	error:
	return r;
}

b0_result_code b0_usb_ain_stream_read(b0_ain *mod, void *samples,
		size_t count, size_t *actual_count)
{
	b0_usb_ain_data *backend_data = B0_MODULE_BACKEND_DATA(mod);
	b0_result_code r = B0_OK;

	if (!backend_data->run.stream.running) {
		r = B0_ERR_STATE;
		goto done;
	}

	/* some calculations */
	unsigned int bitsize = backend_data->cache.bitsize;
	unsigned bytes_per_sample = B0_BIT2BYTE(bitsize);
	/*
	 * if actual_count is NULL, perform copying in blocking mode.
	 *   block till full data is not received
	 *
	 * if actual_count is not NULL, perform as much as copy possible.
	 *   if no more data available, return to caller
	 */

	size_t count_copied = 0;
	b0_usb_ain_stream_data_item *item = NULL;

	while (count > 0 && backend_data->run.stream.running) {
		/* make sure we have data */
		if (B0_IS_NULL(item)) {
			r = stream_data_pop(mod, &item);
			if (B0_ERROR_RESULT_CODE(r)) {
				break;
			}
		}

		/* we have data? */
		/* FIXME: use pthread_cond to signal that data is available */
		if (B0_IS_NULL(item)) {
			if (B0_IS_NULL(actual_count)) {
				B0_SLEEP_MILLISECOND(1);
				continue;
			} else {
				break;
			}
		}

		/* count the number of maximum samples we can copy */
		size_t to_copy = (item->total - item->used) / bytes_per_sample;

		/* limit the number of samples to copy since user buffer has limit */
		if (to_copy > count) {
			to_copy = count;
		}

		/* copy the data */
		void *buf = item->payload + item->used;
		size_t bytes = to_copy * bytes_per_sample;
		memcpy(samples, buf, bytes);

		/* update information */
		count_copied += to_copy;
		item->used += bytes;
		count -= to_copy;
		samples += bytes;

		/* free remain if we used all the data from it */
		if (item->used >= item->total) {
			free(item);
			item = NULL;
		}
	}

	if (!backend_data->run.stream.running) {
		r = B0_ERR_STATE;

		/* Free up the item as it is unsafe to reverse the pop */
		B0_FREE_IF_NOT_NULL(item);
	} else if (B0_IS_NOT_NULL(item)) {
		stream_data_pop_reverse(mod, item);
	}

	if (B0_SUCCESS_RESULT_CODE(r) && B0_IS_NOT_NULL(actual_count)) {
		*actual_count = count_copied;
	}

	done:
	return r;
}

static void stream_data_clean(b0_ain *mod)
{
	b0_usb_ain_data *backend_data = B0_MODULE_BACKEND_DATA(mod);

	/* queue */
	b0_usb_ain_stream_data *queue = &backend_data->run.stream.data;
	b0_usb_ain_stream_data_item *head = queue->head;
	queue->head = queue->tail = NULL;
	while (B0_IS_NOT_NULL(head)) {
		b0_usb_ain_stream_data_item *item = head;
		head = head->next;
		free(item);
	}
}

static b0_result_code stream_data_pop(b0_ain *mod,
			b0_usb_ain_stream_data_item **item)
{
	b0_device *dev = B0_MODULE_DEVICE(mod);
	b0_usb_ain_data *backend_data = B0_MODULE_BACKEND_DATA(mod);
	b0_usb_ain_stream_data *queue = &backend_data->run.stream.data;

	if (pthread_mutex_lock(&queue->lock)) {
		B0_ERROR(dev, "unable to lock mutex stream.data.lock");
		return B0_ERR;
	}

	if (B0_IS_NULL(queue->head)) {
		*item = NULL;
	} else {
		*item = queue->head;
		queue->head = queue->head->next;

		/* if we used up all data, tail should also be NULL */
		if (B0_IS_NULL(queue->head)) {
			queue->tail = NULL;
		}

		B0_DEBUG(dev, "poped %p (total: %"PRIuS", used: %"PRIuS")",
					*item, (*item)->total, (*item)->used);
	}

	if (pthread_mutex_unlock(&queue->lock)) {
		B0_ERROR(dev, "unable to unlock mutex stream.data.lock");
	}

	return B0_OK;
}

static b0_result_code stream_data_pop_reverse(b0_ain *mod,
				b0_usb_ain_stream_data_item *item)
{
	b0_device *dev = B0_MODULE_DEVICE(mod);
	b0_usb_ain_data *backend_data = B0_MODULE_BACKEND_DATA(mod);
	b0_usb_ain_stream_data *queue = &backend_data->run.stream.data;

	if (pthread_mutex_lock(&queue->lock)) {
		B0_WARN(dev, "unable to hold lock over stream");
		return B0_ERR;
	}

	if (B0_IS_NULL(queue->head)) {
		queue->head = queue->tail = item;
	} else {
		item->next = queue->head;
		queue->head = item;
	}

	B0_DEBUG(dev, "pop-revered %p", item);

	if (pthread_mutex_unlock(&queue->lock)) {
		B0_WARN(dev, "unable to release lock over stream");
	}

	return B0_OK;
}

static b0_result_code stream_data_push(b0_ain *mod,
			b0_usb_ain_stream_data_item *item, size_t total, size_t used)
{
	b0_device *dev = B0_MODULE_DEVICE(mod);
	b0_usb_ain_data *backend_data = B0_MODULE_BACKEND_DATA(mod);
	b0_usb_ain_stream_data *queue = &backend_data->run.stream.data;

	item->next = NULL;
	item->total = total;
	item->used = used;

	if (pthread_mutex_lock(&queue->lock)) {
		B0_ERROR(dev, "unable to lock stream data mutex");
		goto error;
	}

	if (B0_IS_NULL(queue->head)) {
		queue->head = item;
	} else {
		queue->tail->next = item;
	}
	queue->tail = item;

	B0_DEBUG(dev, "pushed %p (total: %"PRIuS", used: %"PRIuS")",
					item, total, used);

	if (pthread_mutex_unlock(&queue->lock)) {
		B0_ERROR(dev, "unable to unlock stream data mutex");
	}

	return B0_OK;

	error:
	free(item);
	return B0_ERR;
}

/**
 * this is the callback that libusb will call from polling thread
 * @param[in] transfer libusb transfer
 */
static void stream_callback(libusb_transfer *transfer)
{
	b0_ain *mod = transfer->user_data;
	b0_device *dev = B0_MODULE_DEVICE(mod);

	switch(transfer->status) {
	case LIBUSB_TRANSFER_COMPLETED: {
		b0_usb_ain_data *backend_data = B0_MODULE_BACKEND_DATA(mod);

		if (pthread_mutex_lock(&backend_data->run.in_progress)) {
			B0_ERROR(dev, "unable to lock mutex in_progress");
			break;
		}

		if (backend_data->run.stream.running) {
			b0_usb_ain_stream_data_item *old_item, *new_item;

			old_item = ITEM_FROM_PAYLOAD(transfer->buffer);
			transfer->buffer = NULL;

			/* Allocate a new buffer for the transfer */
			new_item = malloc(sizeof(*new_item) + transfer->length);

			if (B0_IS_NULL(new_item)) {
				B0_WARN(dev, "unable to allocate replacement buffer");
			} else {
				transfer->buffer = new_item->payload;
				if (libusb_submit_transfer(transfer) < 0) {
					B0_WARN(dev, "unable to re-submit transfer %p", transfer);
				}
			}

			B0_DEBUG_STMT(dev, stream_data_push(mod, old_item, transfer->length, 0));
		}

		if (pthread_mutex_unlock(&backend_data->run.in_progress)) {
			B0_ERROR(dev, "unable to unlock mutex in_progress");
		}
	} break;

	case LIBUSB_TRANSFER_TIMED_OUT:
		B0_WARN(dev, "Transfer timeout");
		if (libusb_submit_transfer(transfer) < 0) {
			B0_WARN(dev, "unable to re-submit transfer");
		}
	break;

	case LIBUSB_TRANSFER_CANCELLED:
		/* cancelled by stop. [freeing myself] */
		free(ITEM_FROM_PAYLOAD(transfer->buffer));
		libusb_free_transfer(transfer);
	break;

	case LIBUSB_TRANSFER_ERROR:
	case LIBUSB_TRANSFER_STALL:
	case LIBUSB_TRANSFER_NO_DEVICE:
	case LIBUSB_TRANSFER_OVERFLOW:
	default:
		B0_DEBUG(dev, "error occured: %s", libusb_error_name(transfer->status));
	break;
	}
}

/** @endcond */
