/*
 * This file is part of libbox0.
 * Copyright (C) 2013-2016 Kuldeep Singh Dhaka <kuldeep@madresistor.com>
 *
 * libbox0 is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * libbox0 is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with libbox0.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "../usb-private.h"

/** @cond INTERNAL */

/**
 * note the difference fini and init instead of free and alloc.
 * alloc (initalize and allocate memory), free(deinitialize and free memory)
 * init (initalize) and fini (deinitalize)
 */

static const char* module_type_stringify(uint8_t bModule)
{
	switch(bModule) {
	case B0_AIN: return "AIN";
	case B0_AOUT: return "AOUT";
	case B0_SPI: return "SPI";
	case B0_I2C: return "I2C";
	case B0_DIO: return "DIO";
	case B0_PWM: return "PWM";
	default: return NULL;
	}
}

b0_result_code b0_usb_free_modules(b0_device *dev)
{
	b0_module *mod;
	b0_usb_module_data *backend_data;
	size_t i;

	for (i = 0; i < dev->modules_len; i++) {
		mod = dev->modules[i];

		B0_FREE_IF_NOT_NULL(mod->name);

		backend_data = B0_MODULE_BACKEND_DATA(mod);

		/* if the module isnt unloaded, warn the user */
		if (backend_data->loaded) {
			B0_WARN(dev, "application left module loaded %u:%u",
				backend_data->bModule, backend_data->bIndex);
		}

		free(backend_data);

		mod->name = NULL;
		mod->backend_data = NULL;
	}

	return B0_OK;
}

b0_result_code b0_usb_build_modules_list(b0_device *dev)
{
	b0_result_code r;
	unsigned i;
	struct b0_usb_device box0_dev;
	struct b0_usb_module box0_mod;
	struct b0_module *module;
	struct b0_usb_module_data *backend_data;
	const char *type_name;

	/* Fetch Box0 device */
	if (libusb_get_descriptor(B0_USB_DEVICE_USBDH(dev), B0_USB_DESC_DEVICE, 0,
			(uint8_t *)&box0_dev, sizeof(box0_dev)) < (int) sizeof(box0_dev)) {
		B0_WARN(dev, "unable to fetch box0 device descriptor");
		return B0_ERR;
	}

	/* verify if we support the protocol version */
	if (B0_LE2H16(box0_dev.bcdBOX0) != 0x0020) {
		B0_WARN(dev, "version not supported");
		return B0_ERR_SUPP;
	}

	/* store the bcdBOX0 in device */
	B0_USB_DEVICE_BACKEND_DATA(dev)->bcdBOX0 = B0_LE2H16(box0_dev.bcdBOX0);

	for (i = 0; i < box0_dev.bNumModules; i++) {
		/* Fetch Box0 module descriptor */
		if (libusb_get_descriptor(B0_USB_DEVICE_USBDH(dev),
				B0_USB_DESC_MODULE, i, (uint8_t *)&box0_mod,
				sizeof(box0_mod)) < (int) sizeof(box0_mod)) {
			B0_WARN(dev, "unable to fetch box0 module descriptor");
			return B0_ERR;
		}

		/* Ignore special numbers */
		if (box0_mod.bModule == 0xFF || box0_mod.bModule == 0x00) {
			B0_DEBUG(dev, "Ignoring descriptor (0x%"PRIx8")", box0_mod.bModule);
			continue;
		}

		type_name = module_type_stringify(box0_mod.bModule);
		if (B0_IS_NOT_NULL(type_name)) {
			B0_DEBUG(dev, "Found module: %s (%"PRIu8") @ interface %"PRIu8,
				type_name, box0_mod.bIndex, box0_mod.bInterfaceNumber);
		} else {
			B0_DEBUG(dev, "Found module: %"PRIu8" (%"PRIu8") @ interface %"PRIu8,
				box0_mod.bModule, box0_mod.bIndex, box0_mod.bInterfaceNumber);
		}

		/* module private data */
		backend_data = calloc(1, sizeof(struct b0_usb_module_data));
		if (B0_IS_NULL(backend_data)) {
			return B0_ERR_ALLOC;
		}

		backend_data->bModule = box0_mod.bModule;
		backend_data->bIndex = box0_mod.bIndex;
		backend_data->bInterfaceNumber = box0_mod.bInterfaceNumber;
		backend_data->desc_index = (uint8_t) i;

		/* keep track so that the same application
		 *  do not open the same module again */
		backend_data->loaded = false;

		r = b0_module_alloc(dev, &module);
		if (B0_ERR_RC(r)) {
			free(backend_data);
			return r;
		}

		/* initalize module */
		module->type = box0_mod.bModule;
		module->index = box0_mod.bIndex;
		module->device = dev;
		module->name = NULL;
		module->backend_data = backend_data;

		/* Note: A failure here is not consider a big problem. */
		module->name = b0_usb_string_desc_as_utf8(dev, box0_mod.iName);
	}

	return B0_OK;
}

static b0_result_code search_and_set_alt_setting(b0_module *mod)
{
	b0_result_code r;
	int i, j;
	b0_usb_module_data *backend_data = B0_MODULE_BACKEND_DATA(mod);
	b0_device *dev = B0_MODULE_DEVICE(mod);
	b0_usb_device_data *device_backend_data = B0_DEVICE_BACKEND_DATA(dev);
	uint8_t bInterfaceNumber = backend_data->bInterfaceNumber;
	uint8_t bAlternateSetting;
	struct libusb_config_descriptor *config;

	/* get the active descriptor so that we can search for the interface */
	if (libusb_get_active_config_descriptor(device_backend_data->usbd,
					&config) < 0) {
		B0_WARN(dev, "unable to get active config descriptor");
		return B0_ERR;
	}

	/* search for interface */
	for (i = 0; i < config->bNumInterfaces; i++) {
		const struct libusb_interface *iface = &config->interface[i];
		for (j = 0; j < iface->num_altsetting; j++) {
			const struct libusb_interface_descriptor *altset;
			altset = &iface->altsetting[j];
			if (altset->bInterfaceNumber == bInterfaceNumber &&
				altset->bInterfaceClass == 0xFF &&
				altset->bInterfaceSubClass == backend_data->bModule) {
				/* found a match */
				bAlternateSetting = altset->bAlternateSetting;
				goto matched;
			}
		}
	}

	/* nothing matched, goto done */
	B0_WARN(dev, "no valid alternate interface found");
	r = B0_ERR_BOGUS;
	goto done;

	matched:
	/* set the module alternate setting */
	if (libusb_set_interface_alt_setting(device_backend_data->usbdh,
					bInterfaceNumber, bAlternateSetting) < 0) {
		B0_WARN(dev, "Unable to set-interface %i,  alternate-setting: %i",
			bInterfaceNumber, bAlternateSetting);
		r = B0_ERR;
		goto done;
	}

	backend_data->bAlternateSetting = bAlternateSetting;
	r = B0_OK;

	done:
	libusb_free_config_descriptor(config);
	return r;
}

b0_result_code b0_usb_module_load(b0_module *mod)
{
	b0_usb_module_data *backend_data = B0_MODULE_BACKEND_DATA(mod);
	b0_device *dev = B0_MODULE_DEVICE(mod);
	libusb_device_handle *usbdh = B0_USB_MODULE_USBDH(mod);

	/* verify that the module is already not loaded */
	B0_ASSERT_RETURN_ON_FAIL(dev, !backend_data->loaded, B0_ERR_UNAVAIL);

	/* take control over interface */
	if (libusb_claim_interface(usbdh, backend_data->bInterfaceNumber) < 0) {
		B0_WARN(dev, "Unable to claim interface");
		return B0_ERR_UNAVAIL;
	}

	/* search the first alternate setting and set it! */
	B0_DEBUG_RETURN_ON_FAIL_STMT(dev, search_and_set_alt_setting(mod));

	/* mark the module as loaded */
	backend_data->loaded = true;
	return B0_OK;
}

b0_result_code b0_usb_module_unload(b0_module *mod)
{
	b0_usb_module_data *backend_data = B0_MODULE_BACKEND_DATA(mod);
	libusb_device_handle *usbdh = B0_USB_MODULE_USBDH(mod);
	b0_device *dev = B0_MODULE_DEVICE(mod);

	/* verify that the module is loaded */
	B0_ASSERT_RETURN_ON_FAIL(dev, backend_data->loaded, B0_ERR_STATE);

	/* release the interface */
	if (libusb_release_interface(usbdh, backend_data->bInterfaceNumber) < 0) {
		B0_WARN(dev, "Unable to release interface");
		return B0_ERR;
	}

	backend_data->loaded = false;
	return B0_OK;
}

b0_result_code b0_usb_module_openable(b0_module *mod)
{
	b0_device *dev = B0_MODULE_DEVICE(mod);
	b0_usb_module_data *backend_data = B0_MODULE_BACKEND_DATA(mod);
	uint8_t bIfaceNum = backend_data->bInterfaceNumber;

	/* Module is already loaded by some other code */
	if (backend_data->loaded) {
		return B0_ERR_UNAVAIL;
	}

	/* try to take control over the interface to assure that other
	 *  program is not having control over the interface */
	int r = libusb_claim_interface(B0_USB_DEVICE_USBDH(dev), bIfaceNum);

	if (r == LIBUSB_ERROR_BUSY) {
		return B0_ERR_UNAVAIL;
	} else if (r < 0) {
		/* some other problem */
		return B0_ERR;
	}

	/* release the interface, since we were just testing. */
	libusb_release_interface(B0_USB_DEVICE_USBDH(dev), bIfaceNum);

	return B0_OK;
}

b0_result_code b0_usb_module_mode_set(b0_module *mod, uint8_t value)
{
	b0_device *dev = B0_MODULE_DEVICE(mod);

	/* request to device */
	if (B0_USB_MODULE_CTRLREQ_OUT(mod, B0_USB_MODE_SET, value, NULL, 0) < 0) {
		B0_WARN(dev, "unable to set mode");
		return B0_ERR_IO;
	}

	return B0_OK;
}

b0_result_code b0_usb_module_state_set(b0_module *mod, uint8_t state,
					uint8_t *data, uint16_t len)
{
	b0_device *dev = B0_MODULE_DEVICE(mod);
	if (B0_USB_MODULE_CTRLREQ_OUT(mod, B0_USB_STATE_SET, state, data, len) < 0) {
		const char *state_name = NULL;
		switch(state) {
		case B0_USB_STATE_START:
			state_name = "START";
		break;
		case B0_USB_STATE_STOP:
			state_name = "STOP";
		break;
		}

		if (B0_IS_NOT_NULL(state_name)) {
			B0_WARN(dev, "unable to set state to %s", state_name);
		} else {
			B0_WARN(dev, "unable to set state (%"PRIu8")", state);
		}

		return B0_ERR_IO;
	}

	return B0_OK;
}

b0_result_code b0_usb_module_state_get(b0_module *mod, uint8_t *data, uint16_t len)
{
	b0_device *dev = B0_MODULE_DEVICE(mod);
	if (B0_USB_MODULE_CTRLREQ_IN(mod, B0_USB_STATE_GET, 0, data, len) < 0) {
		B0_WARN(dev, "unable to get state");
		return B0_ERR_IO;
	}

	return B0_OK;
}

b0_result_code b0_usb_module_state_running(b0_module *mod, bool *running)
{
	uint8_t state;
	b0_device *dev = B0_MODULE_DEVICE(mod);

	B0_DEBUG_RETURN_ON_FAIL_STMT(dev, b0_usb_module_state_get(mod, &state, 1));

	switch(state) {
	case B0_USB_STATE_RUNNING:
		*running = true;
	break;
	case B0_USB_STATE_STOPPED:
		*running = false;
	break;
	default:
		/* this function assume that device
		 *  will return RUNNING or STOPPED. (nothing else!) */
		B0_WARN(dev,
			"state is neither B0_USB_STATE_RUNNING or B0_USB_STATE_STOPPED");
		return B0_ERR_BOGUS;
	}

	return B0_OK;
}

b0_result_code b0_usb_module_chan_seq_set(b0_module *mod,
				unsigned int *values, size_t count)
{
	b0_device *dev = B0_MODULE_DEVICE(mod);
	size_t i;
	int status;

	uint8_t *u8 = malloc(count);
	B0_ASSERT_RETURN_ON_NULL_ALLOC(dev, u8);

	for (i = 0; i < count; i++) {
		u8[i] = values[i];
	}

	/* request to device */
	status = B0_USB_MODULE_CTRLREQ_OUT(mod, B0_USB_CHAN_SEQ_SET, 0, u8, count);

	free(u8);

	if (status < 0) {
		B0_WARN(dev, "unable to set channel sequence");
		return B0_ERR_IO;
	}

	return B0_OK;
}

b0_result_code b0_usb_module_chan_seq_get(b0_module *mod,
				unsigned int *values, size_t *count)
{
	b0_device *dev = B0_MODULE_DEVICE(mod);
	b0_result_code r;
	int i, status;
	uint8_t *u8 = malloc(*count);
	B0_ASSERT_RETURN_ON_NULL_ALLOC(dev, u8);

	/* request from device */
	status = B0_USB_MODULE_CTRLREQ_IN(mod, B0_USB_CHAN_SEQ_GET, 0, u8, *count);

	if (status < 0) {
		B0_WARN(dev, "unable to set channel sequence");
		r = B0_ERR_IO;
		free(u8);
	} else if (status < 1) {
		B0_WARN(dev, "device returned 0 length channel sequence");
		r = B0_ERR_BOGUS;
		free(u8);
	} else {
		*count = status;
		for (i = 0; i < status; i++) {
			values[i] = u8[i];
		}

		r = B0_OK;
	}

	free(u8);
	return r;
}

b0_result_code b0_usb_module_bitsize_speed_set(b0_module *mod,
				unsigned int bitsize, unsigned long speed)
{
	b0_device *dev = B0_MODULE_DEVICE(mod);

	struct b0_usb_bitsize_speed_arg arg = {
		.dSpeed = B0_H2LE32(speed),
		.bBitsize = bitsize
	};

	/* request to device */
	if (B0_USB_MODULE_CTRLREQ_OUT(mod, B0_USB_BITSIZE_SPEED_SET, 0,
				(void *) &arg, sizeof(arg)) < 0) {
		B0_WARN(dev, "unable to set bitsize and speed");
		return B0_ERR_IO;
	}

	return B0_OK;
}

b0_result_code b0_usb_module_bitsize_speed_get(b0_module *mod,
				unsigned int *bitsize, unsigned long *speed)
{
	b0_device *dev = B0_MODULE_DEVICE(mod);

	struct b0_usb_bitsize_speed_arg arg;

	/* request to device */
	if (B0_USB_MODULE_CTRLREQ_IN(mod, B0_USB_BITSIZE_SPEED_GET, 0,
				(void *) &arg, sizeof(arg)) < 0) {
		B0_WARN(dev, "unable to get bitsize and speed");
		return B0_ERR_IO;
	}

	if (B0_IS_NOT_NULL(bitsize)) {
		*bitsize = arg.bBitsize;
	}

	if (B0_IS_NOT_NULL(speed)) {
		*speed = B0_LE2H32(arg.dSpeed);
	}

	return B0_OK;
}

b0_result_code b0_usb_module_repeat_set(b0_module *mod, unsigned long value)
{
	b0_device *dev = B0_MODULE_DEVICE(mod);
	uint32_t u32 = B0_H2LE32(value);

	/* request to device */
	if (B0_USB_MODULE_CTRLREQ_OUT(mod, B0_USB_REPEAT_SET, 0,
				(void *) &u32, sizeof(u32)) < 0) {
		B0_WARN(dev, "unable to set repeat value");
		return B0_ERR_IO;
	}

	return B0_OK;
}

b0_result_code b0_usb_module_repeat_get(b0_module *mod, unsigned long *value)
{
	b0_device *dev = B0_MODULE_DEVICE(mod);
	uint32_t u32;

	/* request from device */
	if (B0_USB_MODULE_CTRLREQ_IN(mod, B0_USB_REPEAT_GET, 0,
				(void *) &u32, sizeof(u32)) < 0) {
		B0_WARN(dev, "unable to get repeat value");
		return B0_ERR_IO;
	}

	*value = B0_LE2H32(u32);

	return B0_OK;
}

b0_result_code b0_usb_module_speed_set(b0_module *mod, unsigned long value)
{
	b0_device *dev = B0_MODULE_DEVICE(mod);
	uint32_t u32 = B0_H2LE32(value);

	/* request to device */
	if (B0_USB_MODULE_CTRLREQ_OUT(mod, B0_USB_SPEED_SET, 0,
				(void *) &u32, sizeof(u32)) < 0) {
		B0_WARN(dev, "unable to set speed");
		return B0_ERR_IO;
	}

	return B0_OK;
}

b0_result_code b0_usb_module_speed_get(b0_module *mod, unsigned long *value)
{
	b0_device *dev = B0_MODULE_DEVICE(mod);
	uint32_t u32;

	/* request from device */
	if (B0_USB_MODULE_CTRLREQ_IN(mod, B0_USB_SPEED_GET, 0,
				(void *) &u32, sizeof(u32)) < 0) {
		B0_WARN(dev, "unable to get speed");
		return B0_ERR_IO;
	}

	*value = B0_LE2H32(u32);

	return B0_OK;
}

b0_result_code b0_usb_module_bitsize_set(b0_module *mod, unsigned int value)
{
	b0_device *dev = B0_MODULE_DEVICE(mod);

	/* request to device */
	if (B0_USB_MODULE_CTRLREQ_OUT(mod, B0_USB_BITSIZE_SET, value & 0xFF,
						NULL, 0) < 0) {
		B0_WARN(dev, "unable to set bitsize");
		return B0_ERR_IO;
	}

	return B0_OK;
}

b0_result_code b0_usb_module_bitsize_get(b0_module *mod, unsigned int *value)
{
	b0_device *dev = B0_MODULE_DEVICE(mod);
	uint8_t u8;

	/* request from device */
	if (B0_USB_MODULE_CTRLREQ_IN(mod, B0_USB_BITSIZE_GET, 0,
				(void *) &u8, sizeof(u8)) < 0) {
		B0_WARN(dev, "unable to get bitsize");
		return B0_ERR_IO;
	}

	*value = u8;

	return B0_OK;
}

/** @endcond */
