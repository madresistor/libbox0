/*
 * This file is part of libbox0.
 * Copyright (C) 2013-2016 Kuldeep Singh Dhaka <kuldeep@madresistor.com>
 *
 * libbox0 is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * libbox0 is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with libbox0.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef LIBBOX0_BACKEND_MODULE_H
#define LIBBOX0_BACKEND_MODULE_H

#include "../usb-private.h"

__BEGIN_DECLS

/** @cond INTERNAL */

struct b0_usb_module_data {
	uint8_t bModule;
	uint8_t bIndex;
	uint8_t bInterfaceNumber, bAlternateSetting;
	uint8_t desc_index;
	bool loaded;
};

typedef struct b0_usb_module_data b0_usb_module_data;

B0_PRIV b0_result_code b0_usb_build_modules_list(b0_device *dev);

B0_PRIV b0_result_code b0_usb_free_modules(b0_device *dev);

B0_PRIV b0_result_code b0_usb_module_load(b0_module *mod);
B0_PRIV b0_result_code b0_usb_module_unload(b0_module *mod);

B0_PRIV b0_result_code b0_usb_module_openable(b0_module *mod);

B0_PRIV b0_result_code b0_usb_module_mode_set(b0_module *mod, uint8_t value);

B0_PRIV b0_result_code b0_usb_module_state_set(b0_module *mod, uint8_t state,
					uint8_t *data, uint16_t len);
B0_PRIV b0_result_code b0_usb_module_state_get(b0_module *mod, uint8_t *data,
					uint16_t len);
B0_PRIV b0_result_code b0_usb_module_state_running(b0_module *mod,
					bool *running);

#define b0_usb_module_state_start(mod, data, len) \
	b0_usb_module_state_set(mod, B0_USB_STATE_START, data, len)

#define b0_usb_module_state_stop(mod, data, len) \
	b0_usb_module_state_set(mod, B0_USB_STATE_STOP, data, len)

B0_PRIV b0_result_code b0_usb_module_chan_seq_set(b0_module *mod,
				unsigned int *values, size_t count);

B0_PRIV b0_result_code b0_usb_module_chan_seq_get(b0_module *mod,
				unsigned int *values, size_t *count);

B0_PRIV b0_result_code b0_usb_module_bitsize_speed_set(b0_module *mod,
				unsigned int bitsize, unsigned long speed);

B0_PRIV b0_result_code b0_usb_module_bitsize_speed_get(b0_module *mod,
				unsigned int *bitsize, unsigned long *speed);

B0_PRIV b0_result_code b0_usb_module_repeat_set(b0_module *mod,
				unsigned long value);

B0_PRIV b0_result_code b0_usb_module_repeat_get(b0_module *mod,
				unsigned long *value);

B0_PRIV b0_result_code b0_usb_module_bitsize_set(b0_module *mod,
				unsigned int value);

B0_PRIV b0_result_code b0_usb_module_bitsize_get(b0_module *mod,
				unsigned int *value);

B0_PRIV b0_result_code b0_usb_module_speed_set(b0_module *mod,
				unsigned long value);

B0_PRIV b0_result_code b0_usb_module_speed_get(b0_module *mod,
				unsigned long *value);

/** @endcond */

__END_DECLS

#endif
