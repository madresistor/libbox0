/*
 * This file is part of libbox0.
 * Copyright (C) 2013-2016 Kuldeep Singh Dhaka <kuldeep@madresistor.com>
 *
 * libbox0 is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * libbox0 is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with libbox0.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "../usb-private.h"

/** @cond INTERNAL */

static b0_result_code open_after_alloc(b0_pwm *mod)
{
	b0_usb_pwm_data *backend_data = B0_MODULE_BACKEND_DATA(mod);

	mod->bitsize.count = 0;
	mod->bitsize.values = NULL;
	mod->ref.high = mod->ref.low = 0;
	mod->ref.type = 0;
	mod->speed.count = 0;
	mod->speed.values = NULL;
	mod->label.pin = NULL;
	backend_data->label.pin_count = 0;

	return B0_OK;
}

static b0_result_code open_parse_callback(b0_pwm *mod,
						const struct b0_usb_property *vendor)
{
	b0_device *dev = B0_MODULE_DEVICE(mod);

	switch(vendor->bProperty) {
	case B0_USB_BITSIZE:
		B0_DEBUG_RETURN_ON_FAIL_STMT(dev, b0_usb_bitsize_parse(dev,
			(const struct b0_usb_bitsize *)vendor,
			&mod->bitsize.values, &mod->bitsize.count));
	break;

	case B0_USB_REF:
		B0_DEBUG_RETURN_ON_FAIL_STMT(dev, b0_usb_ref_parse(dev,
			(const struct b0_usb_ref *) vendor, &mod->ref.low, &mod->ref.high,
			&mod->ref.type));
	break;

	case B0_USB_SPEED:
		B0_DEBUG_RETURN_ON_FAIL_STMT(dev, b0_usb_speed_parse(dev,
			(const struct b0_usb_speed *) vendor,
			&mod->speed.values, &mod->speed.count));
	break;

	case B0_USB_LABEL: {
		b0_usb_pwm_data *backend_data = B0_MODULE_BACKEND_DATA(mod);
		B0_DEBUG_RETURN_ON_FAIL_STMT(dev, b0_usb_label_append(dev,
			(const struct b0_usb_label *) vendor, 0, &mod->label.pin,
			&backend_data->label.pin_count));
	} break;

	case B0_USB_COUNT:
		B0_DEBUG_RETURN_ON_FAIL_STMT(dev, b0_usb_count_parse(dev,
			(const struct b0_usb_count *) vendor, &mod->pin_count));
	break;

	default:
		B0_WARN(dev, "Unknown property");
	return B0_ERR_SUPP;
	}

	return B0_OK;
}

static b0_result_code open_after_parse(b0_pwm *mod)
{
	b0_device *dev = B0_MODULE_DEVICE(mod);
	b0_usb_pwm_data *backend_data = B0_MODULE_BACKEND_DATA(mod);

	if (!mod->pin_count) {
		B0_WARN(dev, "PWM has no channels");
		return B0_ERR_BOGUS;
	}

	if (!mod->speed.count) {
		B0_WARN(dev, "PWM has speed values");
		return B0_ERR_BOGUS;
	}

	if (!mod->bitsize.count) {
		B0_WARN(dev, "PWM has bitsize values");
		return B0_ERR_BOGUS;
	}

	/* Make labels of pins consistant with pin count */
	void **label_pin = b0_usb_realloc_2d_array((void **) mod->label.pin,
							backend_data->label.pin_count, mod->pin_count);
	B0_ASSERT_RETURN_ON_NULL_ALLOC(dev, label_pin);
	mod->label.pin = (uint8_t **) label_pin;

	/* allocate cache(width) */
	backend_data->cache.width =
		malloc(mod->pin_count * sizeof(*backend_data->cache.width));
	B0_ASSERT_RETURN_ON_NULL_ALLOC(dev, backend_data->cache.width);

	/* allocate cache(width_valid) */
	backend_data->cache.width_valid =
		calloc(mod->pin_count, sizeof(*backend_data->cache.width_valid));
	B0_ASSERT_RETURN_ON_NULL_ALLOC(dev, backend_data->cache.width_valid);

	/* mark period as invalid */
	backend_data->cache.period_valid = false;

	return B0_OK;
}

b0_result_code b0_usb_pwm_open(b0_device *dev, b0_pwm **mod, int index)
{
	return b0_usb_derive_module_open(dev, B0_PWM, index,
		(void *) mod, sizeof(b0_pwm), sizeof(b0_usb_pwm_data),
		(b0_usb_derive_module_open_after_alloc) open_after_alloc,
		(b0_usb_derive_module_open_parse_callback) open_parse_callback,
		(b0_usb_derive_module_open_after_parse) open_after_parse);
}

static b0_result_code close_before_unload(b0_pwm *mod)
{
	b0_usb_pwm_data *backend_data = B0_MODULE_BACKEND_DATA(mod);

	B0_FREE_IF_NOT_NULL(mod->label.pin);
	B0_FREE_IF_NOT_NULL(mod->speed.values);
	B0_FREE_IF_NOT_NULL(mod->bitsize.values);
	B0_FREE_IF_NOT_NULL(backend_data->cache.width);
	B0_FREE_IF_NOT_NULL(backend_data->cache.width_valid);

	return B0_OK;
}

b0_result_code b0_usb_pwm_close(b0_pwm *mod)
{
	return b0_usb_derive_module_close(mod,
		(b0_usb_derive_module_close_before_unload) close_before_unload);
}

b0_result_code b0_usb_pwm_speed_set(b0_pwm *mod, unsigned long value)
{
	return b0_usb_module_speed_set(B0_GENERIC_MODULE(mod), value);
}

b0_result_code b0_usb_pwm_speed_get(b0_pwm *mod, unsigned long *value)
{
	return b0_usb_module_speed_get(B0_GENERIC_MODULE(mod), value);
}

b0_result_code b0_usb_pwm_bitsize_set(b0_pwm *mod, unsigned int value)
{
	b0_result_code r;

	r = b0_usb_module_bitsize_set(B0_GENERIC_MODULE(mod), value);

	if (B0_SUCCESS_RESULT_CODE(r)) {
		b0_usb_pwm_data *backend_data = B0_MODULE_BACKEND_DATA(mod);
		backend_data->cache.bitsize = value;
	}

	return r;
}

b0_result_code b0_usb_pwm_bitsize_get(b0_pwm *mod, unsigned int *value)
{
	b0_result_code r;

	r = b0_usb_module_bitsize_get(B0_GENERIC_MODULE(mod), value);

	if (B0_SUCCESS_RESULT_CODE(r)) {
		b0_usb_pwm_data *backend_data = B0_MODULE_BACKEND_DATA(mod);
		backend_data->cache.bitsize = *value;
	}

	return r;
}

/*
 *this utilizes maxium size but care should be taken in case
 * the machine has sizeof(unsigned int) < {x | x belongs-to mod->bitsize->values}
 */
static int set(b0_pwm *mod, b0_pwm_reg value, uint8_t cmd, uint16_t wValue)
{
	b0_usb_pwm_data *backend_data = B0_MODULE_BACKEND_DATA(mod);
	unsigned int bitsize = backend_data->cache.bitsize;
	uint8_t len = B0_BIT2BYTE(bitsize);
	value = B0_PWM_REG_H2LE(value);

	if (B0_USB_MODULE_CTRLREQ_OUT(mod,
				cmd, wValue, (uint8_t *)&value, len) < len) {
		return B0_ERR_IO;
	}

	return B0_OK;
}

b0_result_code b0_usb_pwm_width_set(b0_pwm *mod, uint8_t index, b0_pwm_reg value)
{
	b0_result_code r;
	b0_device *dev = B0_MODULE_DEVICE(mod);

	b0_usb_pwm_data *backend_data = B0_MODULE_BACKEND_DATA(mod);

	if (backend_data->cache.width_valid[index] &&
		backend_data->cache.width[index] == value) {
		B0_DEBUG(dev, "cache matched for PWM%i WIDTH",
							backend_data->header.bIndex);
		r = B0_OK;
	} else {
		r = set(mod, value, B0_USB_PWM_WIDTH_SET, index);
		if (B0_SUCCESS_RESULT_CODE(r)) {
			backend_data->cache.width[index] = value;
			backend_data->cache.width_valid[index] = true;
		}
	}

	return r;
}

b0_result_code b0_usb_pwm_period_set(b0_pwm *mod, b0_pwm_reg period)
{
	b0_result_code r;
	b0_device *dev = B0_MODULE_DEVICE(mod);
	b0_usb_pwm_data *backend_data = B0_MODULE_BACKEND_DATA(mod);

	if (backend_data->cache.period_valid &&
		backend_data->cache.period == period) {
		B0_DEBUG(dev, "cache matched for PWM%i PERIOD",
							backend_data->header.bIndex);
		r = B0_OK;
	} else {
		r = set(mod, period, B0_USB_PWM_PERIOD_SET, 0);
		if (B0_SUCCESS_RESULT_CODE(r)) {
			backend_data->cache.period_valid = true;
			backend_data->cache.period = period;
		}
	}

	return r;
}

static int get(b0_pwm *mod, b0_pwm_reg *value, uint8_t cmd, uint16_t wValue)
{
	int r;
	b0_pwm_reg le_value = 0;
	b0_device *dev = B0_MODULE_DEVICE(mod);
	b0_usb_pwm_data *backend_data = B0_MODULE_BACKEND_DATA(mod);
	unsigned int bitsize = backend_data->cache.bitsize;
	int len = B0_BIT2BYTE(bitsize);

	r = B0_USB_MODULE_CTRLREQ_IN(mod, cmd, wValue, (uint8_t *)&le_value, len);
	if (r < 0) {
		return B0_ERR_IO;
	}

	if (r != len) {
		B0_WARN(dev, "read length not equal bytes expected");
	}

	*value = B0_PWM_REG_LE2H(le_value);

	return B0_OK;
}

b0_result_code b0_usb_pwm_width_get(b0_pwm *mod, uint8_t index, b0_pwm_reg *value)
{
	b0_device *dev = B0_MODULE_DEVICE(mod);
	b0_usb_pwm_data *backend_data = B0_MODULE_BACKEND_DATA(mod);

	if (backend_data->cache.width_valid[index]) {
		B0_DEBUG(dev, "utilizing cache for PWM%i WIDTH",
							backend_data->header.bIndex);
		*value = backend_data->cache.width[index];
		return B0_OK;
	}

	return get(mod, value, B0_USB_PWM_WIDTH_GET, index);
}

b0_result_code b0_usb_pwm_period_get(b0_pwm *mod, b0_pwm_reg *period)
{
	b0_device *dev = B0_MODULE_DEVICE(mod);
	b0_usb_pwm_data *backend_data = B0_MODULE_BACKEND_DATA(mod);

	if (backend_data->cache.period_valid) {
		B0_DEBUG(dev, "utilizing cache for PWM%i PERIOD",
							backend_data->header.bIndex);
		*period = backend_data->cache.period;
		return B0_OK;
	}

	return get(mod, period, B0_USB_PWM_PERIOD_GET, 0);
}

b0_result_code b0_usb_pwm_output_prepare(b0_pwm *mod)
{
	b0_result_code r;
	r = b0_usb_module_mode_set(B0_GENERIC_MODULE(mod), B0_USB_MODE_OUTPUT);

	if (B0_SUCCESS_RESULT_CODE(r)) {
		b0_usb_pwm_data *backend_data = B0_MODULE_BACKEND_DATA(mod);
		backend_data->cache.bitsize = mod->bitsize.values[0];
	}

	return r;
}

b0_result_code b0_usb_pwm_output_start(b0_pwm *mod)
{
	b0_result_code r;
	b0_device *dev = B0_MODULE_DEVICE(mod);

	r = b0_usb_module_state_start(B0_GENERIC_MODULE(mod), NULL, 0);
	if (B0_ERROR_RESULT_CODE(r)) {
		B0_ERROR(dev, "request failure (already in running state?)");
		return B0_ERR_IO;
	}

	return r;
}

b0_result_code b0_usb_pwm_output_stop(b0_pwm *mod)
{
	b0_result_code r;
	b0_device *dev = B0_MODULE_DEVICE(mod);

	r = b0_usb_module_state_stop(B0_GENERIC_MODULE(mod), NULL, 0);
	if (B0_ERROR_RESULT_CODE(r)) {
		B0_ERROR(dev, "request failure (already in stop state?)");
	}

	return r;
}

/** @endcond */
