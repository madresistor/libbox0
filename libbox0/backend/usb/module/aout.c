/*
 * This file is part of libbox0.
 * Copyright (C) 2013-2016 Kuldeep Singh Dhaka <kuldeep@madresistor.com>
 *
 * libbox0 is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * libbox0 is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with libbox0.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "../usb-private.h"

/** @cond INTERNAL */

static b0_result_code open_after_alloc(b0_aout *mod)
{
	b0_device *dev = B0_MODULE_DEVICE(mod);
	b0_usb_aout_data *backend_data = B0_MODULE_BACKEND_DATA(mod);

	/* initalize all properties */
	mod->snapshot.count = mod->stream.count = 0;
	mod->snapshot.values = mod->stream.values = NULL;
	mod->capab = 0;
	mod->ref.high = mod->ref.low = 0;
	mod->ref.type = 0;
	mod->chan_count = 0;
	mod->label.chan = NULL;
	mod->buffer_size = 0;
	backend_data->label.chan_count = 0;
	backend_data->cache.bitsize = 0;

	/* Initalize in_progress. */
	if (pthread_mutex_init(&backend_data->run.in_progress, NULL)) {
		B0_WARN(dev, "Failed to initalize in_progress");
		return B0_ERR;
	}

	/* Initalize stream queue lock. */
	if (pthread_mutex_init(&backend_data->run.stream.queue.lock, NULL)) {
		B0_WARN(dev, "Failed to initalize stream.queue.lock");
		return B0_ERR;
	}

	/* Initalize kernel lock. */
	if (pthread_mutex_init(&backend_data->run.stream.kernel.lock, NULL)) {
		B0_WARN(dev, "Failed to initalize stream.kernel.lock");
		return B0_ERR;
	}

	backend_data->config.pending = B0_USB_AOUT_STREAM_PENDING;

	return B0_OK;
}

static b0_result_code capab_parse(b0_aout *mod,
			const struct b0_usb_capab *vendor)
{
	mod->capab = vendor->bmCapab;
	return B0_OK;
}

static b0_result_code open_parse_callback(b0_aout *mod,
				const struct b0_usb_property *vendor)
{
	b0_device *dev = B0_MODULE_DEVICE(mod);

	switch(vendor->bProperty) {
	case B0_USB_BITSIZE_SPEED:
		B0_DEBUG_RETURN_ON_FAIL_STMT(dev, b0_usb_bitsize_speed_parse(dev,
			(const struct b0_usb_bitsize_speed *) vendor,
			(struct _b0_usb_mode_bitsize_speeds *) &mod->snapshot,
			(struct _b0_usb_mode_bitsize_speeds *) &mod->stream));
	break;

	case B0_USB_CAPAB:
		B0_DEBUG_RETURN_ON_FAIL_STMT(dev, capab_parse(mod,
			(const struct b0_usb_capab *) vendor));
	break;

	case B0_USB_REF:
		B0_DEBUG_RETURN_ON_FAIL_STMT(dev, b0_usb_ref_parse(dev,
			(const struct b0_usb_ref *) vendor, &mod->ref.low,
			&mod->ref.high, &mod->ref.type));
	break;

	case B0_USB_COUNT:
		B0_DEBUG_RETURN_ON_FAIL_STMT(dev, b0_usb_count_parse(dev,
			(const struct b0_usb_count *) vendor, &mod->chan_count));
	break;

	case B0_USB_LABEL: {
		b0_usb_aout_data *backend_data = B0_MODULE_BACKEND_DATA(mod);
		B0_DEBUG_RETURN_ON_FAIL_STMT(dev, b0_usb_label_append(dev,
			(const struct b0_usb_label *) vendor, 0, &mod->label.chan,
			&backend_data->label.chan_count));
	} break;

	case B0_USB_BUFFER:
		B0_DEBUG_RETURN_ON_FAIL_STMT(dev, b0_usb_buffer_parse(dev,
			(const struct b0_usb_buffer *) vendor, &mod->buffer_size));
	break;

	default:
		B0_WARN(dev, "Unknown property");
	return B0_ERR_SUPP;
	}

	return B0_OK;

	return B0_OK;
}

static b0_result_code open_after_parse(b0_aout *mod)
{
	b0_device *dev = B0_MODULE_DEVICE(mod);
	b0_usb_aout_data *backend_data = B0_MODULE_BACKEND_DATA(mod);

	if (!mod->chan_count) {
		B0_WARN(dev, "AOUT has no channels");
		return B0_ERR_BOGUS;
	}

	if (!mod->stream.count && !mod->snapshot.count) {
		B0_WARN(dev, "AOUT do not support SNAPSHOT nor STREAM");
		return B0_ERR_BOGUS;
	}

	if (mod->snapshot.count && !mod->buffer_size) {
		B0_WARN(dev, "AOUT do not have any buffer for SNAPSHOT");
		return B0_ERR_BOGUS;
	}

	void **label_chan = b0_usb_realloc_2d_array((void **) mod->label.chan,
							backend_data->label.chan_count, mod->chan_count);
	B0_ASSERT_RETURN_ON_NULL_ALLOC(dev, label_chan);
	mod->label.chan = (uint8_t **) label_chan;

	/* Find the endpoint we need to read from */
	B0_DEBUG_RETURN_ON_FAIL_STMT(dev,
		b0_usb_search_endpoint( B0_GENERIC_MODULE(mod), false, NULL,
				true, &backend_data->ep_out));

	return B0_OK;
}

/**
 * @brief open an AOUT module
 * @details
 * 		this will take control over the USB interface
 * 		stream or snapshot mode need to be selected in order to further continue
 * @param[in] dev device to search for module
 * @param[out] mod pointer to location to store the pointer
 * @param[in] index module index
 * @return result code
 *
 * @see b0_usb_aout_stream_prepare()
 * @see b0_usb_aout_snapshot_prepare()
 * @see b0_usb_aout_close()
 */
b0_result_code b0_usb_aout_open(b0_device *dev, b0_aout **mod, int index)
{
	return b0_usb_derive_module_open(dev, B0_AOUT, index,
		(void *) mod, sizeof(b0_aout), sizeof(b0_usb_aout_data),
		(b0_usb_derive_module_open_after_alloc) open_after_alloc,
		(b0_usb_derive_module_open_parse_callback) open_parse_callback,
		(b0_usb_derive_module_open_after_parse) open_after_parse);
}

static void free_mode(struct b0_aout_mode_bitsize_speeds *mode)
{
	size_t i;

	for (i = 0; i < mode->count; i++) {
		B0_FREE_IF_NOT_NULL(mode->values[i].speed.values);
	}

	B0_FREE_IF_NOT_NULL(mode->values);
}

static b0_result_code close_before_unload(b0_aout *mod)
{
	b0_device *dev = B0_MODULE_DEVICE(mod);
	b0_usb_aout_data *backend_data = B0_MODULE_BACKEND_DATA(mod);

	b0_usb_free_2d_dynamic_array((void **) mod->label.chan, mod->chan_count);

	free_mode(&mod->snapshot);
	free_mode(&mod->stream);

	/* Destory in_progress lock. */
	if (pthread_mutex_destroy(&backend_data->run.in_progress)) {
		B0_WARN(dev, "Failed to destroy in_progress");
	}

	/* Destory stream queue lock. */
	if (pthread_mutex_destroy(&backend_data->run.stream.queue.lock)) {
		B0_WARN(dev, "Failed to destroy stream.queue.lock");
	}

	/* Initalize kernel lock. */
	if (pthread_mutex_destroy(&backend_data->run.stream.kernel.lock)) {
		B0_WARN(dev, "Failed to destroy stream.kernel.lock");
	}

	return B0_OK;
}

/**
 * @brief close the AOUT module
 * @details on closing, resources are freed.
 * 		this will take release the USB interface and
 * 		after this point, no communication be performed
 * 		with module without openining
 * @param[in] mod AOUT module to close
 * @return result code
 */
b0_result_code b0_usb_aout_close(b0_aout *mod)
{
	return b0_usb_derive_module_close(mod,
		(b0_usb_derive_module_close_before_unload) close_before_unload);
}

b0_result_code b0_usb_aout_chan_seq_set(b0_aout *mod, unsigned int *values,
					size_t count)
{
	return b0_usb_module_chan_seq_set(B0_GENERIC_MODULE(mod), values, count);
}

b0_result_code b0_usb_aout_chan_seq_get(b0_aout *mod, unsigned int *values,
					size_t *count)
{
	return b0_usb_module_chan_seq_get(B0_GENERIC_MODULE(mod), values, count);
}

b0_result_code b0_usb_aout_bitsize_speed_set(b0_aout *mod,
				unsigned int bitsize, unsigned long speed)
{
	b0_result_code r;

	r = b0_usb_module_bitsize_speed_set(B0_GENERIC_MODULE(mod), bitsize,
				speed);

	if (B0_SUCCESS_RESULT_CODE(r)) {
		b0_usb_aout_data *backend_data = B0_MODULE_BACKEND_DATA(mod);
		backend_data->cache.bitsize = bitsize;
	}

	return r;
}

B0_PRIV b0_result_code b0_usb_aout_bitsize_speed_get(b0_aout *mod,
			unsigned int *bitsize, unsigned long *speed)
{
	b0_result_code r;
	b0_usb_aout_data *backend_data =  B0_MODULE_BACKEND_DATA(mod);

	r = b0_usb_module_bitsize_speed_get(B0_GENERIC_MODULE(mod), bitsize, speed);

	if (B0_SUCCESS_RESULT_CODE(r)) {
		if (B0_IS_NOT_NULL(bitsize)) {
			backend_data->cache.bitsize = *bitsize;
		}
	}

	return r;
}

b0_result_code b0_usb_aout_repeat_set(b0_aout *mod, unsigned long value)
{
	return b0_usb_module_repeat_set(B0_GENERIC_MODULE(mod), value);
}

b0_result_code b0_usb_aout_repeat_get(b0_aout *mod, unsigned long *value)
{
	return b0_usb_module_repeat_get(B0_GENERIC_MODULE(mod), value);
}

b0_result_code b0_usb_aout_snapshot_start(b0_aout *mod, void *samples, size_t count)
{
	b0_result_code r;
	b0_usb_aout_data *backend_data = B0_MODULE_BACKEND_DATA(mod);
	uint8_t bEndpointAddress = backend_data->ep_out.bEndpointAddress;
	unsigned bitsize = backend_data->cache.bitsize;
	unsigned bytes_per_sample = B0_BIT2BYTE(bitsize);
	int binary_len = count * bytes_per_sample;

	/* send START */
	uint32_t _cnt = B0_H2LE32(count);
	r = b0_usb_module_state_start(B0_GENERIC_MODULE(mod),
		(uint8_t *)&_cnt, sizeof(_cnt));

	if (B0_ERROR_RESULT_CODE(r)) {
		goto done;
	}

	/* Write to device */
	int transferred;
	int status = libusb_bulk_transfer(B0_USB_MODULE_USBDH(mod),
			bEndpointAddress, samples, binary_len, &transferred,
			B0_USB_MODULE_BULK_TIMEOUT(mod));

	if (status < 0) {
		r = B0_ERR;
	} else if (transferred < binary_len) {
		r = B0_ERR_UNDERFLOW;
	} else {
		r = B0_OK;
	}

	done:
	return r;
}

b0_result_code b0_usb_aout_snapshot_prepare(b0_aout *mod)
{
	b0_result_code r;

	if (!mod->snapshot.count || !mod->buffer_size) {
		/* SNAPSHOT isnt supported */
		return B0_ERR_SUPP;
	}

	r = b0_usb_module_mode_set(B0_GENERIC_MODULE(mod), B0_USB_MODE_SNAPSHOT);

	if (B0_SUCCESS_RESULT_CODE(r)) {
		b0_usb_aout_data *backend_data = B0_MODULE_BACKEND_DATA(mod);
		backend_data->cache.bitsize = mod->snapshot.values[0].bitsize;
	}

	return r;
}

b0_result_code b0_usb_aout_snapshot_stop(b0_aout *mod)
{
	return b0_usb_module_state_stop(B0_GENERIC_MODULE(mod), NULL, 0);
}

static b0_result_code stream_data_pop(b0_aout *mod,
					b0_usb_aout_stream_data_item **item);
static b0_result_code stream_data_push(b0_aout *mod,
					void *buf, size_t len);
static b0_result_code stream_schedule(b0_aout *mod);
static b0_result_code stream_data_clean(b0_aout *mod);
static void stream_callback(libusb_transfer *transfer);
static b0_result_code kernel_atomic_add(b0_aout *mod, int value);

b0_result_code b0_usb_aout_stream_start(b0_aout *mod)
{
	b0_device *dev = B0_MODULE_DEVICE(mod);
	b0_usb_aout_data *backend_data = B0_MODULE_BACKEND_DATA(mod);
	b0_usb_aout_stream *stream = &backend_data->run.stream;

	B0_DEBUG_RETURN_ON_FAIL_STMT(dev,
		b0_usb_module_state_start(B0_GENERIC_MODULE(mod), NULL, 0));
	stream->running = true;
	stream->queue.remain.samples = NULL;
	stream->queue.remain.count = 0;

	B0_DEBUG_RETURN_STMT(dev, stream_schedule(mod));
}

static b0_result_code stream_data_clean(b0_aout *mod)
{
	b0_device *dev = B0_MODULE_DEVICE(mod);
	b0_usb_aout_stream_data_item *item;
	b0_usb_aout_data *backend_data = B0_MODULE_BACKEND_DATA(mod);
	b0_usb_aout_stream_data *queue = &backend_data->run.stream.queue;

	if (pthread_mutex_lock(&queue->lock)) {
		B0_WARN(dev, "unable to hold lock over stream");
		return B0_ERR;
	}

	item = queue->head;

	/* update head */
	queue->head = NULL;
	queue->tail = NULL;

	if (pthread_mutex_unlock(&queue->lock)) {
		B0_WARN(dev, "unable to release lock over stream");
		return B0_ERR;
	}

	/* free up all items */
	while (B0_IS_NOT_NULL(item)) {
		b0_usb_aout_stream_data_item *tmp = item;
		item = item->next;
		libusb_cancel_transfer(tmp->payload); /* Callback will free transfer */
		free(tmp);
	}

	return B0_OK;
}

/**
 * schedule buffer
 */
static b0_result_code stream_data_push(b0_aout *mod, void *buf, size_t bytes)
{
	/* try to schedule first packet
	 * if not successful, leave the packet intact and try again later
	 */
	b0_result_code r;
	libusb_transfer *transfer;
	b0_usb_aout_stream_data_item *item;
	b0_usb_aout_data *backend_data = B0_MODULE_BACKEND_DATA(mod);
	b0_device *dev = B0_MODULE_DEVICE(mod);
	b0_usb_aout_stream *stream = &backend_data->run.stream;
	uint8_t ep_addr = backend_data->ep_out.bEndpointAddress;
	int timeout;

	item = malloc(sizeof(*item));
	B0_ASSERT_RETURN_ON_NULL_ALLOC(dev, item);

	/* FIXME: use bulk timeout + expected time of output generation
									(via speed and pending length) */
	timeout = 0;

	transfer = libusb_alloc_transfer(0);
	if (B0_IS_NULL(transfer)) {
		B0_DEBUG(dev, "unable to allocate libusb transfer");
		r = B0_ERR_ALLOC;
		goto free_item;
	}

	/* automatically free buffer when transfer is free'd */
	/* TODO: apply some method to reuse transfer object and its memory
	 * maybe, create two different queues to push unused containers and
	 *  transfer so that in future they can be used */
	transfer->flags |= LIBUSB_TRANSFER_FREE_BUFFER;
	transfer->flags |= LIBUSB_TRANSFER_FREE_TRANSFER;

	libusb_transfer_cb_fn cb = (libusb_transfer_cb_fn) stream_callback;
	libusb_fill_bulk_transfer(transfer, B0_USB_MODULE_USBDH(mod), ep_addr,
							buf, bytes, cb, mod, timeout);

	/* prepare item */
	item->next = NULL;
	item->payload = transfer;

	/* lock */
	if (pthread_mutex_lock(&stream->queue.lock)) {
		B0_WARN(dev, "unable to hold lock over stream");
		r = B0_ERR;
		goto free_transfer;
	}

	/* ref */
	if (B0_IS_NULL(stream->queue.head)) {
		stream->queue.head = item;
	} else {
		stream->queue.tail->next = item;
	}
	stream->queue.tail = item;

	B0_DEBUG(dev, "pushed %p", item);

	/* unlock */
	if (pthread_mutex_unlock(&stream->queue.lock)) {
		B0_WARN(dev, "unable to release lock over stream");
		r = B0_ERR;
		goto free_transfer;
	}

	return B0_OK;

	/* error */
	free_transfer:
	libusb_free_transfer(transfer);

	free_item:
	free(item);

	return r;
}

static b0_result_code stream_data_pop(b0_aout *mod,
				b0_usb_aout_stream_data_item **item)
{
	b0_device *dev = B0_MODULE_DEVICE(mod);
	b0_usb_aout_data *backend_data = B0_MODULE_BACKEND_DATA(mod);
	b0_usb_aout_stream_data *queue = &backend_data->run.stream.queue;

	if (pthread_mutex_lock(&queue->lock)) {
		B0_WARN(dev, "unable to hold lock over stream");
		return B0_ERR;
	}

	b0_usb_aout_stream_data_item *head = queue->head;

	if (B0_IS_NULL(head)) {
		*item = NULL;
	} else {
		queue->head = head->next;
		if (B0_IS_NULL(queue->head)) {
			queue->tail = NULL;
		}

		*item = head;

		B0_DEBUG(dev, "poped %p", *item);
	}

	if (pthread_mutex_unlock(&queue->lock)) {
		B0_WARN(dev, "unable to release lock over stream");
	}

	return B0_OK;
}

static b0_result_code stream_data_pop_reverse(b0_aout *mod,
				b0_usb_aout_stream_data_item *item)
{
	b0_device *dev = B0_MODULE_DEVICE(mod);
	b0_usb_aout_data *backend_data = B0_MODULE_BACKEND_DATA(mod);
	b0_usb_aout_stream_data *queue = &backend_data->run.stream.queue;

	if (pthread_mutex_lock(&queue->lock)) {
		B0_WARN(dev, "unable to hold lock over stream");
		return B0_ERR;
	}

	if (B0_IS_NULL(queue->head)) {
		queue->head = queue->tail = item;
	} else {
		item->next = queue->head;
		queue->head = item;
	}

	B0_DEBUG(dev, "pop-revered %p", item);

	if (pthread_mutex_unlock(&queue->lock)) {
		B0_WARN(dev, "unable to release lock over stream");
	}

	return B0_OK;
}

/**
 * atomic value change to kernel current.
 */
static b0_result_code kernel_atomic_add(b0_aout *mod, int value)
{
	b0_device *dev = B0_MODULE_DEVICE(mod);
	b0_usb_aout_data *backend_data = B0_MODULE_BACKEND_DATA(mod);
	b0_usb_aout_stream_kernel *kernel = &backend_data->run.stream.kernel;

	if (pthread_mutex_lock(&kernel->lock)) {
		B0_WARN(dev, "unable to hold lock over kernel");
		return B0_ERR;
	}

	kernel->current += value;

	if (pthread_mutex_unlock(&kernel->lock)) {
		B0_WARN(dev, "unable to release lock over kernel");
	}

	return B0_OK;
}

/**
 * this function create new isochronous requests if their is space available.
 *
 * also, it assumes that it has exclusive access to b0_usb_aout_data::run::stream members
 */
static b0_result_code stream_schedule(b0_aout *mod)
{
	b0_device *dev = B0_MODULE_DEVICE(mod);
	b0_usb_aout_data *backend_data = B0_MODULE_BACKEND_DATA(mod);
	b0_usb_aout_stream_kernel *kernel = &backend_data->run.stream.kernel;

	if (pthread_mutex_lock(&backend_data->run.in_progress)) {
		B0_WARN(dev, "unable to hold lock over in_progress");
		return B0_ERR;
	}

	/* only one thread can perform scheduling at a time */

	if (!backend_data->run.stream.running) {
		B0_DEBUG(dev, "stream not running");
		return B0_OK;
	}

	while (1) {
		b0_usb_aout_stream_data_item *item;
		if (B0_ERROR_RESULT_CODE(stream_data_pop(mod, &item))) {
			break;
		}

		if (B0_IS_NULL(item)) {
			B0_DEBUG(dev, "no more item to schedule");
			break;
		}

		libusb_transfer *transfer = item->payload;
		size_t length = transfer->length;

		/* reading without lock because only this thread increase the value.
		 *   since other thread only decrease the value, and we need to check
		 *   if we exceed kernel limit or not. their will never be any problem */
		if ((kernel->current + length) > backend_data->config.pending) {
			B0_DEBUG(dev, "kernel limit approched");
			B0_DEBUG_STMT(dev, stream_data_pop_reverse(mod, item));
			break;
		}

		if (libusb_submit_transfer(transfer) < 0) {
			B0_WARN(dev, "unable to submit transfer %p, "
				"maybe try again later", transfer);
			B0_DEBUG_STMT(dev, stream_data_pop_reverse(mod, item));
			break;
		}

		if (B0_ERROR_RESULT_CODE(kernel_atomic_add(mod, length))) {
			break;
		}

		/* free the container of transfer */
		B0_DEBUG(dev, "scheduled %p", item);
		b0_usb_aout_stream_data_item *tmp = item;
		item = item->next;
		free(tmp);
	}

	B0_DEBUG(dev, "kernel limits {configured = %i, current = %i}",
		backend_data->config.pending, kernel->current);

	if (pthread_mutex_unlock(&backend_data->run.in_progress)) {
		B0_WARN(dev, "unable to release lock over in_progress");
	}

	return B0_OK;
}

b0_result_code b0_usb_aout_stream_write(b0_aout *mod, void *samples, size_t count)
{
	b0_device *dev = B0_MODULE_DEVICE(mod);
	b0_usb_aout_data *backend_data = B0_MODULE_BACKEND_DATA(mod);
	b0_usb_aout_stream *stream = &backend_data->run.stream;
	unsigned int bitsize = backend_data->cache.bitsize;
	unsigned int bytes_per_sample = B0_BIT2BYTE(bitsize);
	const uint16_t wMaxPacketSize = backend_data->ep_out.wMaxPacketSize;

	/* lock */
	if (pthread_mutex_lock(&stream->queue.lock)) {
		B0_WARN(dev, "unable to hold lock over stream");
		return B0_ERR_BUSY;
	}

	/* cache */
	void *remain_buf = stream->queue.remain.samples;
	size_t remain_count = stream->queue.remain.count;

	/* used up */
	stream->queue.remain.samples = NULL;
	stream->queue.remain.count = 0;

	/* unlock */
	if (pthread_mutex_unlock(&stream->queue.lock)) {
		B0_WARN(dev, "unable to free lock over stream");
	}

	/* calculate the number of bytes need/can be copied
	 *  (in order to build a transfer) */
	/* FIXME: the "4" is arbitary. to make smaller tranfer
	 *  rather than a big transfer that eat everything */
	size_t packet_max_length = backend_data->config.pending / 4;
	packet_max_length -= packet_max_length % bytes_per_sample;
	packet_max_length -= packet_max_length % wMaxPacketSize;

	B0_DEBUG(dev, "TOTAL %zu", count);

	/* repeat till we have samples */
	while (count > 0) {
		/* calculate the number of packets we can prepare in this iterations */
		size_t total_bytes = (count + remain_count) * bytes_per_sample;
		size_t transfer_bytes = (total_bytes / wMaxPacketSize) * wMaxPacketSize;

		/* prevent large transfer */
		if (transfer_bytes > packet_max_length) {
			transfer_bytes = packet_max_length;
		}

		if (transfer_bytes) {
			/* allocate memory for holding binary convertion */
			remain_buf = realloc(remain_buf, transfer_bytes);
			B0_ASSERT_RETURN_ON_NULL_ALLOC(dev, remain_buf);

			/* copy the buffer */
			size_t remain_bytes = remain_count * bytes_per_sample;
			size_t bytes_copy = transfer_bytes - remain_bytes;
			memcpy(remain_buf + remain_bytes, samples, bytes_copy);

			/* we can prepare this transfer */
			B0_DEBUG_RETURN_ON_FAIL_STMT(dev,
				stream_data_push(mod, remain_buf, transfer_bytes));

			/* mark as used up */
			remain_buf = NULL;
			remain_count = 0;

			/* update stats */
			size_t consumed_bytes = transfer_bytes - remain_bytes;
			count -= consumed_bytes / bytes_per_sample;
			samples += consumed_bytes;

			/* Atleast try to maintain a buffer with 2/3 full.
			 * This will also make sure that buffer are
			 * scheduled as early as possible. */
			if (stream->kernel.current < ((backend_data->config.pending * 2) / 3)) {
				B0_DEBUG_STMT(dev, stream_schedule(mod));
			}
		} else {
			/* we do not have enough bytes to build a transfer.
			 *  instead copy the data and place it in remaining */

			/* allocate memory for holding binary convertion */
			remain_buf = realloc(remain_buf, total_bytes);
			B0_ASSERT_RETURN_ON_NULL_ALLOC(dev, remain_buf);

			/* copy the buffer */
			size_t remain_bytes = remain_count * bytes_per_sample;
			size_t bytes_copy = total_bytes - remain_bytes;
			memcpy(remain_buf + remain_bytes, samples, bytes_copy);

			/* update status */
			remain_count = total_bytes / bytes_per_sample;
			count = 0;
		}
	}

	/* lock */
	if (pthread_mutex_lock(&stream->queue.lock)) {
		B0_WARN(dev, "unable to hold lock over stream");
		/* Cannot halt here, its better to do unsafe access instead
		 *  of throwing away samples */
	}

	/* in case of left over samples */
	stream->queue.remain.samples = remain_buf;
	stream->queue.remain.count = remain_count;

	/* unlock */
	if (pthread_mutex_unlock(&stream->queue.lock)) {
		B0_WARN(dev, "unable to free lock over stream");
	}

	/* start the scheduler */
	B0_DEBUG_RETURN_ON_FAIL_STMT(dev, stream_schedule(mod));

	/* note: the transfer object will be freed by the callback */
	return B0_OK;
}

/**
 * @note caller will *_set() after prepare() since, they are cleared by SET_INTERFACE
 */
b0_result_code b0_usb_aout_stream_prepare(b0_aout *mod)
{
	b0_result_code r;
	b0_device *dev = B0_MODULE_DEVICE(mod);
	b0_usb_aout_data *backend_data = B0_MODULE_BACKEND_DATA(mod);
	b0_usb_aout_stream *stream = &backend_data->run.stream;

	/* clear any previous transfer */
	B0_DEBUG_STMT(dev, stream_data_clean(mod));

	stream->queue.remain.samples = NULL;
	stream->queue.remain.count = 0;
	stream->queue.head = NULL;
	stream->queue.tail = NULL;

	/* kernel memory track */
	stream->kernel.current = 0;

	r = b0_usb_module_mode_set(B0_GENERIC_MODULE(mod), B0_USB_MODE_STREAM);

	if (B0_SUCCESS_RESULT_CODE(r)) {
		backend_data->cache.bitsize = mod->stream.values[0].bitsize;
	}

	return r;
}

static void stream_callback(libusb_transfer *transfer)
{
	b0_aout *mod = transfer->user_data;
	b0_device *dev = B0_MODULE_DEVICE(mod);

	switch(transfer->status) {
	case LIBUSB_TRANSFER_COMPLETED:
		/* Nothing */
	break;
	case LIBUSB_TRANSFER_TIMED_OUT:
		B0_WARN(dev, "Transfer timeout");
	break;
	case LIBUSB_TRANSFER_CANCELLED:
		/* cancelled by stop */
		/* Note will free automatically. */
	break;
	case LIBUSB_TRANSFER_STALL:
	case LIBUSB_TRANSFER_ERROR:
	case LIBUSB_TRANSFER_NO_DEVICE:
	case LIBUSB_TRANSFER_OVERFLOW:
	default:
		B0_DEBUG(dev, "error occured: %s", libusb_error_name(transfer->status));
	return;
	}

	B0_DEBUG_STMT(dev,
		kernel_atomic_add(mod, -1 * transfer->length));

	/**
	 * if streaming has not stopped, schedule more packets
	 */
	if (transfer->status == LIBUSB_TRANSFER_COMPLETED) {
		B0_DEBUG_STMT(dev, stream_schedule(mod));
	}
}

b0_result_code b0_usb_aout_stream_stop(b0_aout *mod)
{
	b0_device *dev = B0_MODULE_DEVICE(mod);
	b0_usb_aout_data *backend_data = B0_MODULE_BACKEND_DATA(mod);

	/* free up any buffer */
	B0_DEBUG_STMT(dev,
		b0_usb_module_state_stop(B0_GENERIC_MODULE(mod), NULL, 0));

	/* set running false */
	backend_data->run.stream.running = false;

	/* FIXME: cancel scheduled transfer. */

	B0_DEBUG_RETURN_STMT(dev, stream_data_clean(mod));
	return B0_OK;
}

/** @endcond */
