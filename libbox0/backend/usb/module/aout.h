/*
 * This file is part of libbox0.
 * Copyright (C) 2013-2016 Kuldeep Singh Dhaka <kuldeep@madresistor.com>
 *
 * libbox0 is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * libbox0 is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with libbox0.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef LIBBOX0_BACKEND_USB_MODULE_AOUT_H
#define LIBBOX0_BACKEND_USB_MODULE_AOUT_H

#include "../usb-private.h"
#include <stdbool.h>

__BEGIN_DECLS

/** @cond INTERNAL */

typedef struct b0_usb_aout_stream_data b0_usb_aout_stream_data;
typedef struct b0_usb_aout_stream_data_item b0_usb_aout_stream_data_item;

typedef struct b0_usb_aout_stream_kernel b0_usb_aout_stream_kernel;

typedef struct b0_usb_aout_stream b0_usb_aout_stream;

struct b0_usb_aout_data {
	b0_usb_module_data header;

	struct {
		unsigned int bitsize;
	} cache;

	struct {
		size_t chan_count;
	} label;

	struct b0_usb_ep ep_out;

	struct {
		pthread_mutex_t in_progress;

		/**
		 * perform temporary calculation (convert double to binary)
		 */
		struct b0_usb_aout_stream {
			/**
			 * queue to hold stream packets that have not been scheduled
			 */
			struct b0_usb_aout_stream_data {
				/**
				 * mutex to prevent multiple thread modify the queue
				 */
				pthread_mutex_t lock;

				/**
				 * queue
				 */
				struct b0_usb_aout_stream_data_item {
					struct b0_usb_aout_stream_data_item *next;
					libusb_transfer *payload;
				} *head, *tail;

				/** left over buffer from last write.
				 * leftover should always be less than wMaxPacket size
				 *  that means: when ever possible, packets should be scheduled.
				 */
				struct {
					size_t count;
					void *samples;
				} remain;
			} queue;

			/**
			 * keep track of memory that is
			 *  pending and maximum that can be send to kernel
			 */
			struct b0_usb_aout_stream_kernel {
				pthread_mutex_t lock;
				unsigned int current; /* current consumption of kernel memory */
			} kernel;

			/**
			 * keep track of module is running
			 *
			 * use to signal polling thread to stop sending new packets
			 */
			volatile bool running;
		} stream;
	} run;

	struct {
		unsigned long pending;
	} config;
};

typedef struct b0_usb_aout_data b0_usb_aout_data;

B0_PRIV b0_result_code b0_usb_aout_open(b0_device *dev, b0_aout **mod, int index);
B0_PRIV b0_result_code b0_usb_aout_close(b0_aout *mod);

B0_PRIV b0_result_code b0_usb_aout_bitsize_speed_set(b0_aout *mod,
			unsigned int bitsize, unsigned long speed);
B0_PRIV b0_result_code b0_usb_aout_bitsize_speed_get(b0_aout *mod,
			unsigned int *bitsize, unsigned long *speed);

B0_PRIV b0_result_code b0_usb_aout_chan_seq_set(b0_aout *mod,
			unsigned int *values, size_t count);
B0_PRIV b0_result_code b0_usb_aout_chan_seq_get(b0_aout *mod,
			unsigned int *values, size_t *count);

B0_PRIV b0_result_code b0_usb_aout_repeat_set(b0_aout *mod, unsigned long value);
B0_PRIV b0_result_code b0_usb_aout_repeat_get(b0_aout *mod, unsigned long *value);

B0_PRIV b0_result_code b0_usb_aout_stream_prepare(b0_aout *mod);
B0_PRIV b0_result_code b0_usb_aout_stream_start(b0_aout *mod);
B0_PRIV b0_result_code b0_usb_aout_stream_stop(b0_aout *mod);
B0_PRIV b0_result_code b0_usb_aout_stream_write(b0_aout *mod, void *samples, size_t count);

B0_PRIV b0_result_code b0_usb_aout_snapshot_prepare(b0_aout *mod);
B0_PRIV b0_result_code b0_usb_aout_snapshot_start(b0_aout *mod, void *samples, size_t count);
B0_PRIV b0_result_code b0_usb_aout_snapshot_stop(b0_aout *mod);

/** @endcond */

__END_DECLS

#endif
