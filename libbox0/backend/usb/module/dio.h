/*
 * This file is part of libbox0.
 * Copyright (C) 2013-2016 Kuldeep Singh Dhaka <kuldeep@madresistor.com>
 *
 * libbox0 is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * libbox0 is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with libbox0.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef LIBBOX0_BACKEND_USB_MODULE_DIO_H
#define LIBBOX0_BACKEND_USB_MODULE_DIO_H

#include "../usb-private.h"

__BEGIN_DECLS

typedef struct b0_usb_dio_data b0_usb_dio_data;

struct b0_usb_dio_data {
	b0_usb_module_data header;

	struct {
		/* This is used while reading descriptor to keep count of
		 *  number of item in mod->label.pin */
		size_t pin_count;
	} label;
};

B0_PRIV b0_result_code b0_usb_dio_open(b0_device *dev, b0_dio **mod, int index);
B0_PRIV b0_result_code b0_usb_dio_close(b0_dio *mod);

B0_PRIV b0_result_code b0_usb_dio_basic_prepare(b0_dio *mod);
B0_PRIV b0_result_code b0_usb_dio_basic_start(b0_dio *mod);
B0_PRIV b0_result_code b0_usb_dio_basic_stop(b0_dio *mod);

/* <single> */
B0_PRIV b0_result_code b0_usb_dio_value_get(b0_dio *mod, unsigned int pin, bool *value);
B0_PRIV b0_result_code b0_usb_dio_value_set(b0_dio *mod, unsigned int pin, bool value);
B0_PRIV b0_result_code b0_usb_dio_value_toggle(b0_dio *mod, unsigned int pin);

B0_PRIV b0_result_code b0_usb_dio_dir_get(b0_dio *mod, unsigned int pin, bool *value);
B0_PRIV b0_result_code b0_usb_dio_dir_set(b0_dio *mod, unsigned int pin, bool value);

B0_PRIV b0_result_code b0_usb_dio_hiz_get(b0_dio *mod, unsigned int pin, bool *value);
B0_PRIV b0_result_code b0_usb_dio_hiz_set(b0_dio *mod, unsigned int pin, bool value);

/* multiple */
B0_PRIV b0_result_code b0_usb_dio_multiple_value_set(b0_dio *mod,
				unsigned int *pins, size_t size, bool value);

B0_PRIV b0_result_code b0_usb_dio_multiple_value_get(b0_dio *mod,
				unsigned int *pins, bool *value, size_t size);

B0_PRIV b0_result_code b0_usb_dio_multiple_value_toggle(b0_dio *mod,
				unsigned int *pins, size_t size);

B0_PRIV b0_result_code b0_usb_dio_multiple_dir_set(b0_dio *mod,
				unsigned int *pins, size_t size, bool value);

B0_PRIV b0_result_code b0_usb_dio_multiple_dir_get(b0_dio *mod,
				unsigned int *pins, bool *value, size_t size);

B0_PRIV b0_result_code b0_usb_dio_multiple_hiz_get(b0_dio *mod,
				unsigned int *pins, bool *value, size_t size);

B0_PRIV b0_result_code b0_usb_dio_multiple_hiz_set(b0_dio *mod,
				unsigned int *pins, size_t size, bool value);

/* all */
B0_PRIV b0_result_code b0_usb_dio_all_value_set(b0_dio *mod, bool value);
B0_PRIV b0_result_code b0_usb_dio_all_value_get(b0_dio *mod, bool *values);
B0_PRIV b0_result_code b0_usb_dio_all_value_toggle(b0_dio *mod);

B0_PRIV b0_result_code b0_usb_dio_all_dir_set(b0_dio *mod, bool value);
B0_PRIV b0_result_code b0_usb_dio_all_dir_get(b0_dio *mod, bool *values);

B0_PRIV b0_result_code b0_usb_dio_all_hiz_get(b0_dio *mod, bool *values);
B0_PRIV b0_result_code b0_usb_dio_all_hiz_set(b0_dio *mod, bool value);

__END_DECLS

#endif
