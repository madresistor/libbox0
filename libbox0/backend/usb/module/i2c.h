/*
 * This file is part of libbox0.
 * Copyright (C) 2013-2016 Kuldeep Singh Dhaka <kuldeep@madresistor.com>
 *
 * libbox0 is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * libbox0 is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with libbox0.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef LIBBOX0_BACKEND_USB_MODULE_I2C_H
#define LIBBOX0_BACKEND_USB_MODULE_I2C_H

#include "../usb-private.h"

__BEGIN_DECLS

/** @cond INTERNAL */

struct b0_usb_i2c_data {
	b0_usb_module_data header;

	uint32_t task_tag;

	struct {
		enum {
			B0_USB_I2C_LABEL_NEXT_SCL,
			B0_USB_I2C_LABEL_NEXT_SDA,
			B0_USB_I2C_LABEL_IGNORE,
		} state;
	} label;

	size_t buffer_size;

	struct b0_usb_ep ep_in, ep_out;

	struct {
		pthread_mutex_t in_progress;
	} run;
};

typedef struct b0_usb_i2c_data b0_usb_i2c_data;

B0_PRIV b0_result_code b0_usb_i2c_open(b0_device *dev, b0_i2c **mod, int index);
B0_PRIV b0_result_code b0_usb_i2c_close(b0_i2c *mod);

B0_PRIV b0_result_code b0_usb_i2c_master_slave_detect(b0_i2c *mod,
		const b0_i2c_sugar_arg *arg, bool *detected);

B0_PRIV b0_result_code b0_usb_i2c_master_prepare(b0_i2c *mod);

B0_PRIV b0_result_code b0_usb_i2c_master_start(b0_i2c *mod,
		const b0_i2c_task *tasks, int *failed_task_index,
		int *failed_task_ack);

B0_PRIV b0_result_code b0_usb_i2c_master_stop(b0_i2c *mod);

/** @endcond */

__END_DECLS

#endif
