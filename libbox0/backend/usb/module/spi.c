/*
 * This file is part of libbox0.
 * Copyright (C) 2013-2016 Kuldeep Singh Dhaka <kuldeep@madresistor.com>
 *
 * libbox0 is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * libbox0 is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with libbox0.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <stdint.h>
#include "../usb-private.h"

/** @cond INTERNAL */

static b0_result_code open_after_alloc(b0_spi *mod)
{
	b0_device *dev = B0_MODULE_DEVICE(mod);
	b0_usb_spi_data *backend_data = B0_MODULE_BACKEND_DATA(mod);

	mod->ss_count = 0;
	mod->label.miso = mod->label.mosi = mod->label.sclk = NULL;
	mod->label.ss = NULL;
	backend_data->label.ss_count = 0;
	backend_data->label.state = B0_USB_SPI_LABEL_NEXT_SCLK;
	mod->bitsize.values = NULL;
	mod->bitsize.count = 0;
	mod->speed.values = NULL;
	mod->speed.count = 0;
	mod->ref.high = mod->ref.low = 0;
	mod->ref.type = 0;
	backend_data->task_tag = 0;
	backend_data->cache.speed = 0;

	/* initalize lock */
	if (pthread_mutex_init(&backend_data->run.in_progress, NULL)) {
		B0_ERROR(dev, "Failed to initalize mutex `in_progress`");
		return B0_ERR;
	}

	return B0_OK;
}

static b0_result_code label_parse(b0_spi *mod,
			const struct b0_usb_label *vendor)
{
	b0_device *dev = B0_MODULE_DEVICE(mod);
	b0_usb_spi_data *backend_data = B0_MODULE_BACKEND_DATA(mod);
	unsigned i, limit = vendor->header.bLength - sizeof(struct b0_usb_property);

	/* iLabel[0] = SCLK
	 * iLabel[1] = MOSI
	 * iLabel[2] = MISO
	 * iLabel[3 + i] = SS<i>
	 */

	for (i = 0; i < limit; i++) {
		switch (backend_data->label.state) {
		case B0_USB_SPI_LABEL_NEXT_SCLK:
			mod->label.sclk = b0_usb_string_desc_as_utf8(dev, vendor->iValues[i]);
			backend_data->label.state = B0_USB_SPI_LABEL_NEXT_MOSI;
		break;
		case B0_USB_SPI_LABEL_NEXT_MOSI:
			mod->label.mosi = b0_usb_string_desc_as_utf8(dev, vendor->iValues[i]);
			backend_data->label.state = B0_USB_SPI_LABEL_NEXT_MISO;
		break;
		case B0_USB_SPI_LABEL_NEXT_MISO:
			mod->label.miso = b0_usb_string_desc_as_utf8(dev, vendor->iValues[i]);
			backend_data->label.state = B0_USB_SPI_LABEL_NEXT_SS;
		break;
		case B0_USB_SPI_LABEL_NEXT_SS:
		B0_DEBUG_RETURN_STMT(dev, b0_usb_label_append(dev,
				(struct b0_usb_label *) vendor, i, &mod->label.ss,
				&backend_data->label.ss_count));
		break; /* Nothing to process more */
		}
	}

	return B0_OK;
}

static b0_result_code open_parse_callback(b0_spi *mod,
						const struct b0_usb_property *vendor)
{
	b0_device *dev = B0_MODULE_DEVICE(mod);
	b0_usb_spi_data *backend_data = B0_MODULE_BACKEND_DATA(mod);

	switch(vendor->bProperty) {
	case B0_USB_REF:
		B0_DEBUG_RETURN_ON_FAIL_STMT(dev, b0_usb_ref_parse(dev,
				(const struct b0_usb_ref *) vendor,
				&mod->ref.low, &mod->ref.high, &mod->ref.type));
	break;

	case B0_USB_LABEL:
		B0_DEBUG_RETURN_ON_FAIL_STMT(dev, label_parse(mod,
				(const struct b0_usb_label *) vendor));
	break;

	case B0_USB_BUFFER:
		B0_DEBUG_RETURN_ON_FAIL_STMT(dev, b0_usb_buffer_parse(dev,
			(const struct b0_usb_buffer *) vendor, &backend_data->buffer_size));
	break;

	case B0_USB_SPEED:
		B0_DEBUG_RETURN_ON_FAIL_STMT(dev, b0_usb_speed_parse(dev,
			(const struct b0_usb_speed *) vendor,
			&mod->speed.values, &mod->speed.count));
	break;

	case B0_USB_COUNT:
		B0_DEBUG_RETURN_ON_FAIL_STMT(dev, b0_usb_count_parse(dev,
			(const struct b0_usb_count *) vendor, &mod->ss_count));
	break;

	case B0_USB_BITSIZE:
		B0_DEBUG_RETURN_ON_FAIL_STMT(dev, b0_usb_bitsize_parse(dev,
			(const struct b0_usb_bitsize *)vendor,
			&mod->bitsize.values, &mod->bitsize.count));
	break;

	default:
		B0_WARN(dev, "Unknown property");
	return B0_ERR_SUPP;
	}

	return B0_OK;
}

static b0_result_code open_after_parse(b0_spi *mod)
{
	b0_device *dev = B0_MODULE_DEVICE(mod);
	b0_usb_spi_data *backend_data = B0_MODULE_BACKEND_DATA(mod);

	if (!mod->ss_count) {
		B0_WARN(dev, "SPI has no channels");
		return B0_ERR_BOGUS;
	}

	if (!mod->speed.count) {
		B0_WARN(dev, "SPI has speed values");
		return B0_ERR_BOGUS;
	}

	if (!mod->bitsize.count) {
		B0_WARN(dev, "SPI has bitsize values");
		return B0_ERR_BOGUS;
	}

	if (!backend_data->buffer_size) {
		B0_WARN(dev, "SPI has no buffer for master mode");
		return B0_ERR_BOGUS;
	}

	void **label_ss = b0_usb_realloc_2d_array((void **) mod->label.ss,
							backend_data->label.ss_count, mod->ss_count);
	B0_ASSERT_RETURN_ON_NULL_ALLOC(dev, label_ss);
	mod->label.ss = (uint8_t **) label_ss;

	B0_DEBUG_RETURN_STMT(dev, b0_usb_search_endpoint(B0_GENERIC_MODULE(mod),
		true, &backend_data->ep_in, true, &backend_data->ep_out));

	/* Default speed */
	backend_data->cache.speed = mod->speed.values[0];

	return B0_OK;
}

b0_result_code b0_usb_spi_open(b0_device *dev, b0_spi **mod, int index)
{
	return b0_usb_derive_module_open(dev, B0_SPI, index,
		(void *) mod, sizeof(b0_spi), sizeof(b0_usb_spi_data),
		(b0_usb_derive_module_open_after_alloc) open_after_alloc,
		(b0_usb_derive_module_open_parse_callback) open_parse_callback,
		(b0_usb_derive_module_open_after_parse) open_after_parse);
}

static b0_result_code close_before_unload(b0_spi *mod)
{
	b0_device *dev = B0_MODULE_DEVICE(mod);
	b0_usb_spi_data *backend_data = B0_MODULE_BACKEND_DATA(mod);

	B0_FREE_IF_NOT_NULL(mod->label.sclk);
	B0_FREE_IF_NOT_NULL(mod->label.mosi);
	B0_FREE_IF_NOT_NULL(mod->label.miso);
	b0_usb_free_2d_dynamic_array((void **) mod->label.ss, mod->ss_count);
	B0_FREE_IF_NOT_NULL(mod->speed.values);
	B0_FREE_IF_NOT_NULL(mod->bitsize.values);

	if (pthread_mutex_destroy(&backend_data->run.in_progress)) {
		B0_WARN(dev, "`in_progress' failed to destory'");
	}

	return B0_OK;
}

b0_result_code b0_usb_spi_close(b0_spi *mod)
{
	return b0_usb_derive_module_close(mod,
		(b0_usb_derive_module_close_before_unload) close_before_unload);
}

b0_result_code b0_usb_spi_active_state_set(b0_spi *mod, unsigned addr,
		bool value)
{
	uint16_t wValue = (addr << 8) | (value ? 0x01 : 0x00);
	if (B0_USB_MODULE_CTRLREQ_OUT(mod,
			B0_USB_SPI_ACTIVE_STATE_SET, wValue, NULL, 0) < 0) {
		return B0_ERR_IO;
	}

	return B0_OK;
}

b0_result_code b0_usb_spi_active_state_get(b0_spi *mod, unsigned addr,
		bool *value)
{
	uint16_t wValue = (addr << 8);
	uint8_t _value;

	if (B0_USB_MODULE_CTRLREQ_IN(mod,
			B0_USB_SPI_ACTIVE_STATE_GET, wValue, &_value, 1) < 0) {
		return B0_ERR_IO;
	}

	*value = _value == 0x00 ? false : true;
	return B0_OK;
}

b0_result_code b0_usb_spi_speed_set(b0_spi *mod, unsigned long value)
{
	b0_result_code r = b0_usb_module_speed_set(B0_GENERIC_MODULE(mod), value);

	if (B0_SUCCESS_RESULT_CODE(r)) {
		b0_usb_spi_data *backend_data = B0_MODULE_BACKEND_DATA(mod);
		backend_data->cache.speed = value;
	}

	return r;
}

b0_result_code b0_usb_spi_speed_get(b0_spi *mod, unsigned long *value)
{
	b0_result_code r = b0_usb_module_speed_get(B0_GENERIC_MODULE(mod), value);

	if (B0_SUCCESS_RESULT_CODE(r)) {
		b0_usb_spi_data *backend_data = B0_MODULE_BACKEND_DATA(mod);
		backend_data->cache.speed = *value;
	}

	return r;
}

/**
 * Convert @a tasks to device binary format.
 * @param tasks Tasks
 * @param[out] _out_data Pointer to store out data pointer
 * @param[out] _out_size Pointer to store out data size
 * @param[out] _in_data Pointer to store in data pointer
 * @param[out] _in_size Pointer to store in data size
 * @param[out] failed_task_index Pointer to store the failed task index
 * @return result code
 * @note @a _out_data or @a _out_size or @a _in_data or @a _in_size
 *  cannot be NULL.
 */
static b0_result_code tasks_to_device_format(b0_spi *mod,
	const b0_spi_task *tasks, void **_out_data, size_t *_out_size,
	void **_in_data, size_t *_in_size, int *failed_task_index)
{
	b0_result_code r;
	void *out_data = NULL, *in_data = NULL;
	size_t out_size = 0, in_size = 0;
	unsigned current_task_index = 0;
	b0_device *dev = B0_MODULE_DEVICE(mod);

	for (;;) {
		const b0_spi_task *task = &tasks[current_task_index];

		B0_DEBUG(dev, "TASK[%u] flags=0x%x addr=%u bitsize=%u wdata=%p "
			"rdata=%p count=%"PRIuS, current_task_index, task->flags,
			task->addr, task->bitsize, task->wdata, task->rdata, task->count);

		if (task->count >= 256) {
			B0_WARN(dev, "TASK[%u] require more than 255 count, not possible",
					current_task_index);
			r = B0_ERR_OVERFLOW;
			goto problem_with_task;
		}

		if (task->addr >= mod->ss_count) {
			B0_WARN(dev, "TASK[%u] slave address out of range",
				current_task_index);
			r = B0_ERR_ARG;
			goto problem_with_task;
		}

		uint8_t config = 0;

		if (task->flags & B0_SPI_TASK_CPHA) {
			config |= B0_USB_SPI_CONFIG_CPHA;
		}

		if (task->flags & B0_SPI_TASK_CPOL) {
			config |= B0_USB_SPI_CONFIG_CPOL;
		}

		if (task->flags & B0_SPI_TASK_LSB_FIRST) {
			config |= B0_USB_SPI_CONFIG_LSB_FIRST;
		}

		uint32_t bytes_transfer = task->count *
					B0_DIVIDE_AND_CEIL(task->bitsize, 8);

		switch (task->flags & B0_SPI_TASK_DUPLEX_MASK) {
		case B0_SPI_TASK_FD: {
			/* Calculate the sizes */
			struct b0_usb_spi_out_fd *out;
			size_t new_out_size = out_size + sizeof(*out) + task->count;
			size_t new_in_size = in_size + sizeof(struct b0_usb_spi_in_fd) +
									task->count;

			/* Allocate memory */
			void *new_out_data = realloc(out_data, new_out_size);
			if (B0_IS_NULL(new_out_data)) {
				r = B0_ERR_ALLOC;
				goto problem;
			}

			/* Build the transaction */
			out = new_out_data + out_size;
			out->bSS = task->addr;
			out->bmConfig = config | B0_USB_SPI_CONFIG_FD;
			out->bBitsize = task->bitsize;
			out->bCount = task->count;
			out->dSpeed = task->speed;
			if (task->count) {
				memcpy(out->xData, task->wdata, bytes_transfer);
			}

			/* Update the values */
			out_data = new_out_data;
			out_size = new_out_size;
			in_size = new_in_size;
		} break;
		case B0_SPI_TASK_HD_READ: {
			/* Calculate the sizes */
			struct b0_usb_spi_out_hd_read *out;
			size_t new_out_size = out_size + sizeof(*out);
			size_t new_in_size = in_size + task->count;

			/* Allocate memory */
			void *new_out_data = realloc(out_data, new_out_size);
			if (B0_IS_NULL(new_out_data)) {
				r = B0_ERR_ALLOC;
				goto problem;
			}

			/* Build the transaction */
			out = new_out_data + out_size;
			out->bSS = task->addr;
			out->bmConfig = config | B0_USB_SPI_CONFIG_HD_READ;
			out->bBitsize = task->bitsize;
			out->bRead = task->count;
			out->dSpeed = task->speed;

			/* Update the values */
			out_data = new_out_data;
			out_size = new_out_size;
			in_size = new_in_size;
		} break;
		case B0_SPI_TASK_HD_WRITE: {
			/* Calculate the sizes */
			struct b0_usb_spi_out_hd_write *out;
			size_t new_out_size = out_size + sizeof(*out) + task->count;
			size_t new_in_size = in_size +
								sizeof(struct b0_usb_spi_in_hd_write);

			/* Allocate memory */
			void *new_out_data = realloc(out_data, new_out_size);
			if (B0_IS_NULL(new_out_data)) {
				r = B0_ERR_ALLOC;
				goto problem;
			}

			/* Build the transaction */
			out = new_out_data + out_size;
			out->bSS = task->addr;
			out->bmConfig = config | B0_USB_SPI_CONFIG_HD_WRITE;
			out->bBitsize = task->bitsize;
			out->bWrite = task->count;
			out->dSpeed = task->speed;
			if (task->count) {
				memcpy(out->xData, task->wdata, bytes_transfer);
			}

			/* Update the values */
			out_data = new_out_data;
			out_size = new_out_size;
			in_size = new_in_size;
		} break;
		default:
			/* this is not a valid argument */
			r = B0_ERR_ARG;
			goto problem;
		}

		if (task->flags & B0_SPI_TASK_LAST) {
			break;
		}

		current_task_index++;
	}

	in_data = malloc(in_size);
	if (B0_IS_NULL(in_data)) {
		r = B0_ERR_ALLOC;
		goto problem;
	}

	*_out_data = out_data;
	*_out_size = out_size;
	*_in_data = in_data;
	*_in_size = in_size;

	return B0_OK;

	problem_with_task:

	if (B0_IS_NOT_NULL(failed_task_index)) {
		*failed_task_index = current_task_index;
	}

	problem:
	B0_FREE_IF_NOT_NULL(out_data);

	return r;
}

/**
 * Calculate the (expected) time required on device side to execute the tasks
 * Assuming the @a tasks is fully valid.
 * @param tasks Tasks
 * @param fallback_speed Speed to use as fallback (when task->speed = 0)
 * @return duration in milliseconds
 */
static unsigned tasks_expected_exec_time(b0_spi *mod,
			const b0_spi_task *tasks)
{
	b0_usb_spi_data *backend_data = B0_MODULE_BACKEND_DATA(mod);
	unsigned long fallback_speed = backend_data->cache.speed;
	double value = 0;

	for (;;) {
		double speed = tasks->speed ? tasks->speed : fallback_speed;
		/* "+ 2" compensate for rising/falling slave select line */
		value += ((tasks->count * tasks->bitsize) + 2) / speed;

		if (tasks->flags & B0_SPI_TASK_LAST) {
			break;
		}

		tasks++;
	}

	return (unsigned) ceil(value * 1000); /* Convert to milliseconds */
}

/**
 * Execute the transaction on device
 * @param mod SPI module
 * @param send Send data
 * @param send_len @a send length
 * @param[out] recv Receive data
 * @param recv_len @a recv length
 * @param actual_recv_len The number of bytes actually recived
 * @param expected_exec_time Expected execution time (milliseconds)
 * @return result code
 */
static b0_result_code exec_on_dev(b0_spi *mod, void *send,
	size_t send_len, void *recv, size_t recv_len, size_t *actual_recv_len,
	unsigned expected_exec_time)
{
	b0_result_code r;
	b0_device *dev = B0_MODULE_DEVICE(mod);
	b0_usb_spi_data *backend_data = B0_MODULE_BACKEND_DATA(mod);
	uint32_t tag = backend_data->task_tag++;
	uint8_t bEndpointAddressOut = backend_data->ep_out.bEndpointAddress;
	uint8_t bEndpointAddressIn = backend_data->ep_in.bEndpointAddress;
	int status, transferred;

	/* prevent simultaneous request in multiple threads */
	if (pthread_mutex_trylock(&backend_data->run.in_progress)) {
		return B0_ERR_BUSY;
	}

	struct b0_usb_spi_command command = {
		.dSignature = B0_H2LE32(B0_USB_SPI_COMMAND_SIGNATURE),
		.dTag = B0_H2LE32(tag),
		.dLength = B0_H2LE32(send_len),
		.bCommand = B0_USB_SPI_COMMAND_TRANSACTION
	};

	/* Write command */
	status = libusb_bulk_transfer(B0_USB_MODULE_USBDH(mod),
			bEndpointAddressOut, (void *) &command, sizeof(command),
			&transferred, B0_USB_MODULE_BULK_TIMEOUT(mod));

	if (status < 0) {
		B0_WARN(dev, "Unable to write command to device");
		r = B0_ERR_IO;
		goto done;
	} else if (transferred < (int) sizeof(command)) {
		B0_WARN(dev, "Unable to write full command to device");
		r = B0_ERR_IO;
		goto done;
	}

	/* Write transaction data */
	status = libusb_bulk_transfer(B0_USB_MODULE_USBDH(mod),
			bEndpointAddressOut, send, send_len, &transferred,
			B0_USB_MODULE_BULK_TIMEOUT(mod));

	if (status < 0) {
		B0_WARN(dev, "Unable to write transaction data to device");
		r = B0_ERR_IO;
		goto done;
	} else if (transferred < (int) send_len) {
		B0_WARN(dev, "Unable to write full transaction data to device");
		r = B0_ERR_IO;
		goto done;
	}

	/* Read status */
	struct b0_usb_spi_status _status;
	status = libusb_bulk_transfer(B0_USB_MODULE_USBDH(mod),
			bEndpointAddressIn, (void *) &_status, sizeof(_status),
			&transferred,
			B0_USB_MODULE_BULK_TIMEOUT(mod) + expected_exec_time);

	if (status < 0) {
		B0_WARN(dev, "Unable to read status from device");
		r = B0_ERR_IO;
		goto done;
	} else if (transferred < (int) sizeof(command)) {
		B0_WARN(dev, "Unable to read full status device");
		r = B0_ERR_IO;
		goto done;
	}

	if (B0_LE2H32(_status.dSignature) != B0_USB_SPI_STATUS_SIGNATURE) {
		B0_WARN(dev, "SPI status signature invalid");
		r = B0_ERR_BOGUS;
		goto done;
	}

	if (B0_LE2H32(_status.dTag) != tag) {
		B0_WARN(dev, "SPI status tag mismatch");
		r = B0_ERR_BOGUS;
		goto done;
	}

	if (_status.bStatus != B0_USB_SPI_STATUS_SUCCESS) {
		B0_DEBUG(dev, "SPI transaction failed");
		r = B0_ERR;
		goto done;
	}

	size_t status_recv_len = B0_LE2H32(_status.dLength);

	if (status_recv_len > recv_len) {
		B0_WARN(dev, "Buffer overflow because device reporting more data than expected");
		r = B0_ERR_OVERFLOW;
		goto done;
		/* Not reading data from device, will be lost! */
	} else if (status_recv_len < recv_len) {
		B0_DEBUG(dev, "Device status stage reported less data than expected");
	}

	if (status_recv_len) {
		/* Read transaction data */
		status = libusb_bulk_transfer(B0_USB_MODULE_USBDH(mod),
				bEndpointAddressIn, recv, status_recv_len, &transferred,
				B0_USB_MODULE_BULK_TIMEOUT(mod));

		if (status < 0) {
			B0_WARN(dev, "Problem while reading transaction data from device");
			r = B0_ERR_IO;
			goto done;
		} else if (transferred < (int) status_recv_len) {
			B0_WARN(dev, "Unable to read full tranaction data from device");
			r = B0_ERR_IO;
			goto done;
		}
	}

	/* Number of bytes readed */
	*actual_recv_len = status_recv_len;
	r = B0_OK;

	done:
	/* free the lock */
	if (pthread_mutex_unlock(&backend_data->run.in_progress)) {
		B0_WARN(dev, "unlocking `in_progress' failed");
	}

	return r;
}

/**
 * Convert the readed data from device to task
 * @param mod Module
 * @param in_data Readed data
 * @param readed Number of bytes readed
 * @param tasks Tasks
 * @param failed_task_index Pointer to store the failed task index
 * @param failed_task_count Pointer to store the failed task count
 */
static b0_result_code device_format_to_task(b0_spi *mod, void *in_data,
			size_t readed, const b0_spi_task *tasks,
			int *failed_task_index, int *failed_task_count)
{
	b0_result_code r;
	int current_task_index = 0, current_task_count = -1;
	size_t processed = 0;
	b0_device *dev = B0_MODULE_DEVICE(mod);

	for (;;) {
		const b0_spi_task *task = &tasks[current_task_index];
		unsigned int count;

		if ((task->flags & B0_SPI_TASK_DUPLEX_MASK) == B0_SPI_TASK_HD_WRITE) {
			if ((processed + 1) > readed) {
				/* Enough data has not been readed ?! */
				B0_DEBUG(dev, "TASK[%u] Full data was not retreive from device",
					current_task_index);
				current_task_count = -1;
				r = B0_ERR_UNDERFLOW;
				goto problem;
			}

			count = *((uint8_t *) in_data + processed++);

			if (count != task->count) {
				/* problem, writing/reading payload was a problem */
				B0_DEBUG(dev, "TASK[%u] Full data was not transferred on bus",
					current_task_index);
				current_task_count = count;
				r = B0_ERR_IO;
				goto problem;
			}
		} else {
			if (!task->count) {
				goto next;
			}

			/* Copy what ever we have got */
			count = readed - processed;
			count = B0_MIN(count, task->count);
			memcpy(task->rdata, in_data + processed, count);
			processed += count;

			/* Check if we got the full data */
			if (count < task->count) {
				/* Enough data has not been readed ?! */
				B0_DEBUG(dev, "TASK[%u] Full data was not retreive from device",
					current_task_index);
				r = B0_ERR_UNDERFLOW;
				current_task_count = count;
				goto problem;
			}
		}

		next:
		if (task->flags & B0_SPI_TASK_LAST) {
			current_task_count = count;
			break;
		}

		current_task_index++;
	}

	return B0_OK;

	problem:
	if (B0_IS_NOT_NULL(failed_task_index)) {
		*failed_task_index = current_task_index;
	}

	if (B0_IS_NOT_NULL(failed_task_count)) {
		*failed_task_count = current_task_count;
	}

	return r;
}

b0_result_code b0_usb_spi_master_start(b0_spi *mod, const b0_spi_task *tasks,
	int *failed_task_index, int *failed_task_count)
{
	void *send, *recv;
	size_t send_len, recv_len;
	size_t actual_recv_len;

	b0_device *dev = B0_MODULE_DEVICE(mod);
	b0_result_code r;

	/* Convert to device format */
	r = tasks_to_device_format(mod, tasks, &send, &send_len,
					&recv, &recv_len, failed_task_index);
	if (B0_ERROR_RESULT_CODE(r)) {
		B0_WARN(dev, "failed to convert tasks to device format");
		return r;
	}

	unsigned exec_time = tasks_expected_exec_time(mod, tasks);

	/* Execute on device */
	r = exec_on_dev(mod, send, send_len, recv, recv_len, &actual_recv_len, exec_time);
	if (B0_ERROR_RESULT_CODE(r)) {
		B0_WARN(dev, "Unable to execute tasks on device");
		goto free_buf;
	}

	/* Convert from device format to task */
	r = device_format_to_task(mod, recv, actual_recv_len, tasks,
							failed_task_index, failed_task_count);
	if (B0_ERROR_RESULT_CODE(r)) {
		B0_WARN(dev, "conversion from device format to task failed");
		goto free_buf;
	}

	r = B0_OK;

	free_buf:

	B0_FREE_IF_NOT_NULL(send);
	B0_FREE_IF_NOT_NULL(recv);

	return r;

}

b0_result_code b0_usb_spi_master_stop(b0_spi *mod)
{
	return b0_usb_module_state_stop(B0_GENERIC_MODULE(mod), NULL, 0);
}

b0_result_code b0_usb_spi_master_prepare(b0_spi *mod)
{
	b0_result_code r;
	r = b0_usb_module_mode_set(B0_GENERIC_MODULE(mod), B0_USB_MODE_MASTER);

	if (B0_SUCCESS_RESULT_CODE(r)) {
		b0_usb_spi_data *backend_data = B0_MODULE_BACKEND_DATA(mod);
		backend_data->cache.speed = mod->speed.values[0];
	}

	return r;
}


/** @endcond */

/*
 * SPI:
 *  A SPI Transaction is a collection of query which contain the information
 *   abstract level to read/write from slave.
 *
 * The design of SPI Bus is such that:
 *  - There are 4 modes in SPI. (see below)
 *  - Slave Select (Active low or Active High)
 *  - Variant which have slave addressing using first 8bits (send to slave)
 *  - Full duplex (MISO, MOSI, CLK) and Half duplex (MISO, CLK)
 *  - Active multiple slave (not supported. ie. only one slave can be active-(low/high))
 *  - Multi master
 *  - Multiple bytes can be read/write
 *  - Direction of data.
 *      Full duplex: not required
 *      Half duplex: device specific (nothing defined on SPI protocol level)
 *  - LSB transfer or MSB Transfer (TO BE REMOVED.)
 *  - TODO: add capability to tell host which line is used for half-duplex.
 *     currently: MOSI in stm32f0 is used.
 *
 * Visualize an out-transaction as a request going out of the system
 *  and an in-transaction as request result coming into the system
 *
 * ____
 * out-query:
 *   first byte is bSS:
 *   8bit value of which slave (slave select line to active high) to select
 *
 *  then the second byte is bmConfig:
 *   This contain all the configuration of the bus communication:
 *   bit0 - (CPHA = Clock Phase)
 *   bit1 - (Clock Polarity)
 *   bit2 - full-duplex(0) or half-duplex (1)
 *   bit3 - valid only if half-duplex, read(1) or write(0)
 *   bit4 - msb-first(0) or lsb-first(1)
 *
 *  then comes the bitsize:
 *   This contain the bitsize value at which the communication will be performed
 *   maximum: 255
 *   minimum: 1
 *
 *  data-word: the data that is send in one go.
 *    sizeof(data-word) = bBitsize
 *
 *  if out-query is full-duplex-out-query:
 *    bCount = number of data-word to read|write
 *    xData = array of data-word
 *    (data is written to MOSI and readed from MISO line simultaneously)
 *
 *  if out-query is read-half-duplex-out-query
 *    bRead = the number of data-word to read
 *
 *  if out-query is write-half-duplex-out-query
 *    bWrite = number of data-word to write
 *    xData = array of data-word
 *    (data is written to MOSI)
 *
 * ___
 * in-query
 *  since the application code aready knows the structure of the out-query, and
 *   and in-query transaction is halted on first error
 *   (and the residue is end to application),
 *  the application can detect the length of data that is readed.
 *
 * if in-query is full-duplex-in-query
 *   xData = data-word readed
 *
 * if in-query is read-half-duplex-in-query
 *   xData = data-word readed
 *
 * if in-query is write-half-duplex-in-query
 *   bWrite = number of bytes written to slave
 *
 * ___
 * In C,
 * Application code can create a transaction
 *  by aggregading multiple query in a struct.
 *  query are themselves struct.
 *
 * ___
 * Mode  CPOL   CPHA
 *  0     0      0
 *  1     0      1
 *  2     1      0
 *  3     1      1
 *
 * CPOL = Clock Polarity
 * CPHA = Clock Phase
 */
