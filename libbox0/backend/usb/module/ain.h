/*
 * This file is part of libbox0.
 * Copyright (C) 2013-2016 Kuldeep Singh Dhaka <kuldeep@madresistor.com>
 *
 * libbox0 is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * libbox0 is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with libbox0.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef LIBBOX0_BACKEND_USB_MODULE_AIN_H
#define LIBBOX0_BACKEND_USB_MODULE_AIN_H

#include "../usb-private.h"
#include "common.h"
#include <stdbool.h>

__BEGIN_DECLS

/** @cond INTERNAL */

typedef struct b0_usb_ain_data b0_usb_ain_data;

typedef struct b0_usb_ain_stream_transfer b0_usb_ain_stream_transfer;
typedef struct b0_usb_ain_stream_transfer_item b0_usb_ain_stream_transfer_item;

typedef struct b0_usb_ain_stream_data b0_usb_ain_stream_data;
typedef struct b0_usb_ain_stream_data_item b0_usb_ain_stream_data_item;

typedef struct libusb_transfer libusb_transfer;

struct b0_usb_ain_data {
	b0_usb_module_data header;

	struct {
		/* This is used while reading descriptor to keep count of
		 *  number of item in mod->label.chan */
		size_t chan_count;
	} label;

	struct b0_usb_ep ep_in;

	struct {
		unsigned int bitsize;
		unsigned long speed;
	} cache;

	struct {
		/**
		 * @brief critical section mutex
		 * @details lock held while critical operation in
		 *  which this members will be modified
		 */
		pthread_mutex_t in_progress;

		struct {
			/**
			 * libusb transfer object queue
			 */
			struct b0_usb_ain_stream_transfer {
				pthread_mutex_t lock;

				struct b0_usb_ain_stream_transfer_item {
					libusb_transfer *payload;
					struct b0_usb_ain_stream_transfer_item *next;
				} *head, *tail;
			} transfer;

			/** data that is received but have not been readed queue */
			struct b0_usb_ain_stream_data {
				pthread_mutex_t lock;

				struct b0_usb_ain_stream_data_item {
					/* Meta */
					size_t total, used; /* bytes */
					struct b0_usb_ain_stream_data_item *next;

					/* Payload */
					unsigned char payload[0];
				} *head, *tail;
			} data;

			volatile bool running;
		} stream;
	} run;

	struct {
		unsigned long delay; /* unit: millisecond */
	} config;
};

B0_PRIV b0_result_code b0_usb_ain_open(b0_device *dev, b0_ain **mod, int index);
B0_PRIV b0_result_code b0_usb_ain_close(b0_ain *mod);

B0_PRIV b0_result_code b0_usb_ain_bitsize_speed_set(b0_ain *mod,
			unsigned int bitsize, unsigned long speed);
B0_PRIV b0_result_code b0_usb_ain_bitsize_speed_get(b0_ain *mod,
			unsigned int *bitsize, unsigned long *speed);

B0_PRIV b0_result_code b0_usb_ain_chan_seq_set(b0_ain *mod,
			unsigned int *values, size_t count);
B0_PRIV b0_result_code b0_usb_ain_chan_seq_get(b0_ain *mod,
			unsigned int *values, size_t *count);

B0_PRIV b0_result_code b0_usb_ain_stream_prepare(b0_ain *mod);
B0_PRIV b0_result_code b0_usb_ain_stream_start(b0_ain *mod);
B0_PRIV b0_result_code b0_usb_ain_stream_stop(b0_ain *mod);
B0_PRIV b0_result_code b0_usb_ain_stream_read(b0_ain *mod, void *samples,
					size_t count, size_t *actual_count);

B0_PRIV b0_result_code b0_usb_ain_snapshot_prepare(b0_ain *mod);
B0_PRIV b0_result_code b0_usb_ain_snapshot_start(b0_ain *mod, void *samples,
					size_t count);
B0_PRIV b0_result_code b0_usb_ain_snapshot_stop(b0_ain *mod);

/** @endcond */

__END_DECLS

#endif
