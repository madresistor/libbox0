/*
 * This file is part of libbox0.
 * Copyright (C) 2013-2016 Kuldeep Singh Dhaka <kuldeep@madresistor.com>
 *
 * libbox0 is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * libbox0 is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with libbox0.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "../usb-private.h"

/** @cond INTERNAL */

static b0_result_code open_after_alloc(b0_i2c *mod)
{
	b0_usb_i2c_data *backend_data = B0_MODULE_BACKEND_DATA(mod);
	b0_device *dev = B0_MODULE_DEVICE(mod);

	mod->ref.high = mod->ref.low = 0;
	mod->version.count = 0;
	mod->version.values = NULL;
	mod->label.sck = mod->label.sda = NULL;
	backend_data->label.state = B0_USB_I2C_LABEL_NEXT_SCL;
	backend_data->buffer_size = 0;
	backend_data->task_tag = 0;

	/* initalize lock */
	if (pthread_mutex_init(&backend_data->run.in_progress, NULL)) {
		B0_ERROR(dev, "Failed to initalize mutex `in_progress`");
		return B0_ERR;
	}

	return B0_OK;
}

static b0_result_code label_parse(b0_i2c *mod,
					const struct b0_usb_label *vendor)
{
	b0_device *dev = B0_MODULE_DEVICE(mod);
	b0_usb_i2c_data *backend_data = B0_MODULE_BACKEND_DATA(mod);
	unsigned i, limit = vendor->header.bLength - sizeof(struct b0_usb_property);

	/* iLabel[0] = SCL | iLabel[1] = SDA */
	for (i = 0; i < limit; i++) {
		switch (backend_data->label.state) {
		case B0_USB_I2C_LABEL_NEXT_SCL:
			mod->label.sck = b0_usb_string_desc_as_utf8(dev, vendor->iValues[i]);
			backend_data->label.state = B0_USB_I2C_LABEL_NEXT_SDA;
		break;
		case B0_USB_I2C_LABEL_NEXT_SDA:
			mod->label.sda = b0_usb_string_desc_as_utf8(dev, vendor->iValues[i]);
			backend_data->label.state = B0_USB_I2C_LABEL_IGNORE;
		break;
		default:
		break;
		}
	}

	return B0_OK;
}

static b0_result_code i2c_version_parse(b0_i2c *mod,
					const struct b0_usb_i2c_version *vendor)
{
	size_t len = vendor->header.bLength - sizeof(struct b0_usb_property);
	size_t i, count = mod->version.count;
	b0_i2c_version *values = mod->version.values;
	b0_device *dev = B0_MODULE_DEVICE(mod);

	/* Allocate memory */
	values = realloc(values, sizeof(*values) * (count + len));
	B0_ASSERT_RETURN_ON_NULL_ALLOC(dev, values);

	/* Copy the values */
	for (i = 0; i < len; i++) {
		values[count + i] = vendor->bValues[i];
	}

	/* keep back results */
	mod->version.values = values;
	mod->version.count = count + len;

	return B0_OK;
}

static b0_result_code open_parse_callback(b0_i2c *mod,
					const struct b0_usb_property *vendor)
{
	b0_device *dev = B0_MODULE_DEVICE(mod);
	b0_usb_i2c_data *backend_data = B0_MODULE_BACKEND_DATA(mod);

	switch(vendor->bProperty) {
	case B0_USB_REF:
		B0_DEBUG_RETURN_ON_FAIL_STMT(dev, b0_usb_ref_parse(dev,
			(const struct b0_usb_ref *) vendor,
			&mod->ref.low, &mod->ref.high, NULL));
	break;

	case B0_USB_I2C_VERSION:
		B0_DEBUG_RETURN_ON_FAIL_STMT(dev, i2c_version_parse(mod,
			(const struct b0_usb_i2c_version *) vendor));
	break;

	case B0_USB_LABEL:
		B0_DEBUG_RETURN_ON_FAIL_STMT(dev, label_parse(mod,
			(const struct b0_usb_label *) vendor));
	break;

	case B0_USB_BUFFER:
		B0_DEBUG_RETURN_ON_FAIL_STMT(dev, b0_usb_buffer_parse(dev,
			(const struct b0_usb_buffer *) vendor,
			&backend_data->buffer_size));
	break;

	default:
		B0_WARN(dev, "Unknown property");
	return B0_ERR_SUPP;
	}

	return B0_OK;
}

static b0_result_code open_after_parse(b0_i2c *mod)
{
	b0_device *dev = B0_MODULE_DEVICE(mod);
	b0_usb_i2c_data *backend_data = B0_MODULE_BACKEND_DATA(mod);

	if (!mod->version.count) {
		/* If device did not provide any, use standard mode as default */
		b0_i2c_version *values = malloc(sizeof(*values));
		B0_ASSERT_RETURN_ON_NULL_ALLOC(dev, values);
		values[0] = B0_I2C_VERSION_SM;
		mod->version.values = values;
		mod->version.count = 1;
	}

	if (!backend_data->buffer_size) {
		B0_WARN(dev, "I2C has no buffer for master mode");
		return B0_ERR_BOGUS;
	}

	B0_DEBUG_RETURN_STMT(dev, b0_usb_search_endpoint(B0_GENERIC_MODULE(mod),
		true, &backend_data->ep_in, true, &backend_data->ep_out));

	return B0_OK;
}

b0_result_code b0_usb_i2c_open(b0_device *dev, b0_i2c **mod, int index)
{
	return b0_usb_derive_module_open(dev, B0_I2C, index,
		(void *) mod, sizeof(b0_i2c), sizeof(b0_usb_i2c_data),
		(b0_usb_derive_module_open_after_alloc) open_after_alloc,
		(b0_usb_derive_module_open_parse_callback) open_parse_callback,
		(b0_usb_derive_module_open_after_parse) open_after_parse);
}

static b0_result_code close_before_unload(b0_i2c *mod)
{
	b0_device *dev = B0_MODULE_DEVICE(mod);
	b0_usb_i2c_data *backend_data = B0_MODULE_BACKEND_DATA(mod);

	B0_FREE_IF_NOT_NULL(mod->label.sck);
	B0_FREE_IF_NOT_NULL(mod->label.sda);
	B0_FREE_IF_NOT_NULL(mod->version.values);

	if (pthread_mutex_destroy(&backend_data->run.in_progress)) {
		B0_WARN(dev, "`in_progress' failed to destory'");
	}

	return B0_OK;
}

b0_result_code b0_usb_i2c_close(b0_i2c *mod)
{
	return b0_usb_derive_module_close(mod,
		(b0_usb_derive_module_close_before_unload) close_before_unload);
}

b0_result_code b0_usb_i2c_master_stop(b0_i2c *mod)
{
	return b0_usb_module_state_stop(B0_GENERIC_MODULE(mod), NULL, 0);
}

/**
 * Convert @a tasks to device binary format.
 * @param tasks Tasks
 * @param[out] _out_data Pointer to store out data pointer
 * @param[out] _out_size Pointer to store out data size
 * @param[out] _in_data Pointer to store in data pointer
 * @param[out] _in_size Pointer to store in data size
 * @param[out] failed_task_index Pointer to store the failed task index
 * @return result code
 * @note @a _out_data or @a _out_size or @a _in_data or @a _in_size
 *  cannot be NULL.
 */
static b0_result_code tasks_to_device_format(b0_i2c *mod,
	const b0_i2c_task *tasks, void **_out_data, size_t *_out_size,
	void **_in_data, size_t *_in_size, int *failed_task_index)
{
	int current_task_index = 0;
	b0_result_code r;
	void *out_data = NULL, *in_data = NULL;
	size_t out_size = 0, in_size = 0;
	b0_device *dev = B0_MODULE_DEVICE(mod);
	b0_usb_i2c_data *backend_data = B0_MODULE_BACKEND_DATA(mod);
	size_t buffer_size = backend_data->buffer_size;

	for (;;) {
		const b0_i2c_task *task = &tasks[current_task_index];
		B0_DEBUG(dev, "TASK[%u] flags=0x%x addr=0x%"PRIx8" data=%p, "
			"count=%"PRIuS, current_task_index, task->flags, task->addr,
			task->data, task->count);

		if (task->count > 254) {
			B0_WARN(dev, "TASK[%u] require more than 254 bytes, not possible",
								current_task_index);
			r = B0_ERR_OVERFLOW;
			goto problem_with_task;
		}

		if ((task->flags & B0_I2C_TASK_DIR_MASK) == B0_I2C_TASK_READ) {
			/* Allocate space for the new task in OUT data */
			struct b0_usb_i2c_out_read *out_read;
			size_t new_out_size = out_size + sizeof(*out_read);
			void *new_out_data = realloc(out_data, new_out_size);
			if (B0_IS_NULL(new_out_data)) {
				r = B0_ERR_ALLOC;
				goto problem_with_task;
			}

			/* prepare the OUT data */
			out_read = new_out_data + out_size;
			out_read->bAddress = (task->addr << 1) | 0x01;
			out_read->bRead = task->count;
			out_read->bVersion = task->version;

			/* Increment the values */
			out_data = new_out_data;
			out_size = new_out_size;
			in_size += sizeof(struct b0_usb_i2c_in_read) + task->count;
		} else {
			/* Allocate space for the new task in OUT data */
			struct b0_usb_i2c_out_write *out_write;
			size_t new_out_size = out_size + sizeof(*out_write) + task->count;
			void *new_out_data = realloc(out_data, new_out_size);
			if (B0_IS_NULL(new_out_data)) {
				r = B0_ERR_ALLOC;
				goto problem_with_task;
			}

			/* prepare the OUT data */
			out_write = new_out_data + out_size;
			out_write->bAddress = (task->addr << 1) | 0x00;
			out_write->bWrite = task->count;
			out_write->bVersion = task->version;

			if (task->count) {
				if (B0_IS_NULL(task->data)) {
					r = B0_ERR_ARG;
					goto problem_with_task;
				}

				memcpy(out_write->bData, task->data, task->count);
			}

			/* Increment the values */
			out_data = new_out_data;
			out_size = new_out_size;
			in_size += sizeof(struct b0_usb_i2c_in_write);
		}

		if (out_size > buffer_size) {
			B0_WARN(dev, "buffer overflow for OUT");
			r = B0_ERR_OVERFLOW;
			goto problem_with_task;
		} else if (in_size > buffer_size) {
			B0_WARN(dev, "buffer overflow for IN");
			r = B0_ERR_OVERFLOW;
			goto problem_with_task;
		}

		if (task->flags & B0_I2C_TASK_LAST) {
			break;
		}

		current_task_index++;
	}

	/* allocate memory for IN */
	in_data = malloc(in_size);
	if (B0_IS_NULL(in_data)) {
		r = B0_ERR_ALLOC;
		goto problem;
	}

	/* without problem! */
	*_out_data = out_data;
	*_out_size = out_size;
	*_in_data = in_data;
	*_in_size = in_size;
	return B0_OK;

	/* problem with task, so need to update failed_task_index */
	problem_with_task:
	if (B0_IS_NOT_NULL(failed_task_index)) {
		*failed_task_index = current_task_index;
	}

	problem:
	B0_FREE_IF_NOT_NULL(out_data);

	return r;
}

/**
 * Convert the readed data from device to task
 * @param mod Module
 * @param in_data Readed data
 * @param readed Number of bytes readed
 * @param tasks Tasks
 * @param failed_task_index Pointer to store the failed task index
 * @param failed_task_ack Pointer to store the failed task acknowledgment count
 */
static b0_result_code device_format_to_task(b0_i2c *mod, void *in_data,
			size_t readed, const b0_i2c_task *tasks,
			int *failed_task_index, int *failed_task_ack)
{
	b0_result_code r;
	int current_task_index = 0, current_task_ack = -1;
	size_t processed = 0;
	b0_device *dev = B0_MODULE_DEVICE(mod);

	for (;;) {
		const b0_i2c_task *task = &tasks[current_task_index];
		unsigned ack;

		if ((processed + 1) > readed) {
			/* We DONOT have enough in data to read bACK */
			current_task_ack = -1;
			r = B0_ERR_UNDERFLOW;
			goto problem;
		}

		ack = *((uint8_t *) in_data + processed++);

		if (!ack) {
			/* problem, problem, slave didnt ack for addressing */
			B0_DEBUG(dev, "TASK[%u] Slave didn't ack for address stage",
				current_task_index);
			current_task_ack = ack;
			r = B0_ERR_IO;
			goto problem;
		}

		/* If this was a read task, copy back the data */
		if ((task->flags & B0_I2C_TASK_DIR_MASK) == B0_I2C_TASK_READ) {
			if (!task->count) {
				/* There is nothing to read */
				goto next;
			} else if (task->count != ack) { /* master send NACK at last */
				/* problem, writing/reading payload was a problem */
				B0_DEBUG(dev, "TASK[%u] Full data was not transferred on bus",
						current_task_index);
				r = B0_ERR_IO;
				current_task_ack = ack;
				/* FIXME: copy what ever we have got */
				goto problem;
			}

			if ((processed + task->count) > readed) {
				/* Enough data has not been readed ?! */
				B0_DEBUG(dev, "TASK[%u] Full data was not retreive from device",
					current_task_index);
				current_task_ack = ack;
				r = B0_ERR_UNDERFLOW;
				/* FIXME: copy what ever we have got! */
				goto problem;
			}

			/* Copy the data! (after all the booby traps) */
			memcpy(task->data, in_data + processed, task->count);
			processed += task->count;
		} else {
			if ((task->count + 1) != ack) {
				B0_DEBUG(dev, "TASK[%u] Full data was not transferred on bus",
					current_task_index);
				current_task_ack = ack;
				r = B0_ERR_IO;
				goto problem;
			}
		}

		next:
		if (task->flags & B0_I2C_TASK_LAST) {
			current_task_ack = ack;
			break;
		}

		current_task_index++;
	}

	return B0_OK;

	problem:
	if (B0_IS_NOT_NULL(failed_task_index)) {
		*failed_task_index = current_task_index;
	}

	if (B0_IS_NOT_NULL(failed_task_ack)) {
		*failed_task_ack = current_task_ack;
	}

	return r;
}

/**
 * Calculate the time require to execute the @a tasks
 * Assuming the @a tasks is fully valid.
 * @param mod I2C module
 * @param tasks Tasks
 * @return duration in milliseconds
 */
static unsigned tasks_expected_exec_time(const b0_i2c_task *tasks)
{
	float value = 0;
	unsigned other = 0;
	float speed = 10000; /* FIXME: Hard coded 10Khz */

	for (;;) {
		value += (tasks->count + 1) * 9 / speed;

		if (tasks->flags & B0_I2C_TASK_LAST) {
			break;
		}

		other++;
		tasks++;
	}

	/* compensate for START + RESTART + STOP */
	value += (other * 2) / speed;

	return (unsigned) ceil(value * 1000); /* to milliseconds */
}

/**
 * Execute the transaction on device
 * @param mod I2C module
 * @param send Send data
 * @param send_len @a send length
 * @param[out] recv Receive data
 * @param recv_len @a recv length
 * @param actual_recv_len The number of bytes actually recived
 * @param expected_exec_time Expected execution time (milliseconds)
 * @return result code
 */
static b0_result_code exec_on_dev(b0_i2c *mod, void *send,
	size_t send_len, void *recv, size_t recv_len, size_t *actual_recv_len,
	unsigned expected_exec_time)
{
	b0_result_code r;
	b0_device *dev = B0_MODULE_DEVICE(mod);
	b0_usb_i2c_data *backend_data = B0_MODULE_BACKEND_DATA(mod);
	uint32_t tag = backend_data->task_tag++;
	uint8_t bEndpointAddressOut = backend_data->ep_out.bEndpointAddress;
	uint8_t bEndpointAddressIn = backend_data->ep_in.bEndpointAddress;
	int status, transferred;

	/* prevent simultaneous request in multiple threads */
	if (pthread_mutex_trylock(&backend_data->run.in_progress)) {
		return B0_ERR_BUSY;
	}

	struct b0_usb_i2c_command command = {
		.dSignature = B0_H2LE32(B0_USB_I2C_COMMAND_SIGNATURE),
		.dTag = B0_H2LE32(tag),
		.dLength = B0_H2LE32(send_len),
		.bCommand = B0_USB_I2C_COMMAND_TRANSACTION
	};

	/* Write command */
	status = libusb_bulk_transfer(B0_USB_MODULE_USBDH(mod),
			bEndpointAddressOut, (void *) &command, sizeof(command),
			&transferred, B0_USB_MODULE_BULK_TIMEOUT(mod));

	if (status < 0) {
		B0_WARN(dev, "Unable to write command to device");
		r = B0_ERR_IO;
		goto done;
	} else if (transferred < (int) sizeof(command)) {
		B0_WARN(dev, "Unable to write full command to device");
		r = B0_ERR_IO;
		goto done;
	}

	/* Write transaction data */
	status = libusb_bulk_transfer(B0_USB_MODULE_USBDH(mod),
			bEndpointAddressOut, send, send_len, &transferred,
			B0_USB_MODULE_BULK_TIMEOUT(mod));

	if (status < 0) {
		B0_WARN(dev, "Unable to write transaction data to device");
		r = B0_ERR_IO;
		goto done;
	} else if (transferred < (int) send_len) {
		B0_WARN(dev, "Unable to write full transaction to device");
		r = B0_ERR_IO;
		goto done;
	}

	/* Read status */
	struct b0_usb_i2c_status _status;
	status = libusb_bulk_transfer(B0_USB_MODULE_USBDH(mod),
			bEndpointAddressIn, (void *) &_status, sizeof(_status),
			&transferred,
			B0_USB_MODULE_BULK_TIMEOUT(mod) + expected_exec_time);

	if (status < 0) {
		B0_WARN(dev, "Unable to read status from device");
		r = B0_ERR_IO;
		goto done;
	} else if (transferred < (int) sizeof(_status)) {
		B0_WARN(dev, "Unable to write full command to device");
		r = B0_ERR_IO;
		goto done;
	}

	if (B0_LE2H32(_status.dSignature) != B0_USB_I2C_STATUS_SIGNATURE) {
		B0_WARN(dev, "I2C status signature invalid");
		r = B0_ERR_BOGUS;
		goto done;
	}

	if (B0_LE2H32(_status.dTag) != tag) {
		B0_WARN(dev, "I2C status tag mismatch");
		r = B0_ERR_BOGUS;
		goto done;
	}

	if (_status.bStatus != B0_USB_I2C_STATUS_SUCCESS) {
		B0_DEBUG(dev, "I2C transaction failed");
	}

	size_t status_recv_len = B0_LE2H32(_status.dLength);

	if (status_recv_len > recv_len) {
		B0_WARN(dev, "Buffer overflow because device reporting more data than expected");
		r = B0_ERR_OVERFLOW;
		goto done;
		/* Not reading data from device, will be lost! */
	} else if (status_recv_len < recv_len) {
		B0_DEBUG(dev, "Device status stage reported less data than expected");
	}

	if (status_recv_len) {
		/* Read transaction data */
		status = libusb_bulk_transfer(B0_USB_MODULE_USBDH(mod),
				bEndpointAddressIn, recv, status_recv_len, &transferred,
				B0_USB_MODULE_BULK_TIMEOUT(mod));

		if (status < 0) {
			B0_WARN(dev, "Problem while reading transaction data from device");
			r = B0_ERR_IO;
			goto done;
		} else if (transferred < (int) status_recv_len) {
			B0_WARN(dev, "Unable to write full transaction data from device");
			r = B0_ERR_IO;
			goto done;
		}
	}

	/* Number of bytes readed */
	*actual_recv_len = status_recv_len;
	r = B0_OK;

	done:
	/* free the lock */
	if (pthread_mutex_unlock(&backend_data->run.in_progress)) {
		B0_WARN(dev, "unlocking `in_progress' failed");
	}

	return r;
}

b0_result_code b0_usb_i2c_master_start(b0_i2c *mod, const b0_i2c_task *tasks,
	int *failed_task_index, int *failed_task_ack)
{
	void *send, *recv;
	size_t send_len, recv_len;
	size_t actual_recv_len;

	b0_device *dev = B0_MODULE_DEVICE(mod);
	b0_result_code r;

	/* Convert to device format */
	r = tasks_to_device_format(mod, tasks, &send, &send_len,
					&recv, &recv_len, failed_task_index);
	if (B0_ERROR_RESULT_CODE(r)) {
		B0_WARN(dev, "failed to convert tasks to device format");
		return r;
	}

	/* Execute on device */
	r = exec_on_dev(mod, send, send_len, recv, recv_len, &actual_recv_len,
				tasks_expected_exec_time(tasks));
	if (B0_ERROR_RESULT_CODE(r)) {
		B0_WARN(dev, "Unable to execute tasks on device");
		goto free_buf;
	}

	/* Convert from device format to task */
	r = device_format_to_task(mod, recv, actual_recv_len, tasks,
							failed_task_index, failed_task_ack);
	if (B0_ERROR_RESULT_CODE(r)) {
		B0_WARN(dev, "conversion from device format to task failed");
		goto free_buf;
	}

	r = B0_OK;

	free_buf:

	B0_FREE_IF_NOT_NULL(send);
	B0_FREE_IF_NOT_NULL(recv);

	return r;
}

/**
 * @note Read 1 byte from the slave and check it presence.
 * @warning Will not work for slaves that only have write only support.
 */
static b0_result_code master_slave_detect_by_reading_1byte(b0_i2c *mod,
				const b0_i2c_sugar_arg *arg, bool *detected)
{
	b0_device *dev = B0_MODULE_DEVICE(mod);

	struct b0_usb_i2c_out_read out = {
		.bVersion = arg->version,
		.bAddress = arg->addr << 1 | 0x1,
		.bRead = 1
	};
	uint8_t in[2];
	size_t actual_len;

	B0_DEBUG_RETURN_ON_FAIL_STMT(dev, exec_on_dev(mod,
		&out, sizeof(out), in, sizeof(in), &actual_len, 2));

	if (actual_len < 1) {
		B0_WARN(dev, "unable to read bAck data");
		return B0_ERR_UNDERFLOW;
	}

	*detected = in[0] ? true : false;
	return B0_OK;
}

b0_result_code b0_usb_i2c_master_slave_detect(b0_i2c *mod,
				const b0_i2c_sugar_arg *arg, bool *detected)
{
	b0_device *dev = B0_MODULE_DEVICE(mod);
	b0_usb_device_data *device_backend_data = B0_DEVICE_BACKEND_DATA(dev);

	if (device_backend_data->fix.i2c_0byte) {
		B0_DEBUG(dev, "applying i2c_0byte fix for slave detect");
		return master_slave_detect_by_reading_1byte(mod, arg, detected);
	}

	/* Just use the generic version */
	return b0_generic_i2c_master_slave_detect(mod, arg, detected);
}

b0_result_code b0_usb_i2c_master_prepare(b0_i2c *mod)
{
	return b0_usb_module_mode_set(B0_GENERIC_MODULE(mod), B0_USB_MODE_MASTER);
}

/** @endcond */

/*
 * I2C:
 *  An I2C Transaction is a collection of query which contain the information
 *   abstract level to read/write from slave.
 *
 * The design of the I2C Bus is such that:
 *  - Multi master
 *  - Master can read-from/write-to (while communicating with slave)
 *  - Direction of the data (read or write) is send in the 8bit
 *     [7bit slave address] [1bit direction]
 *  - A start/stop need to be send in order to start/stop the communication
 *  - A restart (same as start) need to be issued to hold bus control.
 *  - Acknowledgement is send by slave.
 *  - Multiple bytes can be read-from/write-to (while communicating with slave).
 *  - The direction of the communication is same until the master send a restart/stop.
 *
 * Visualize an out-transaction as a request going out of the system
 *  and an in-transaction as request result coming into the system
 *
 * ___
 * So, an out-transaction to the device is send.
 *   an out-transaction is composed of multiple out-query.
 *
 *  the byte of the a query is composed of:
 *   bAddress:
 *     bit[1:7] - Slave Address (7 bit slave address)
 *     bit0 - Direction. [0 = write, 1: read]
 *
 *  if Write (bAddress.bit0 is 0) [write-out-query]
 *   bWrite : Number of bytes to write
 *   bData[] : Length = bWrite (data to write,  payload)
 *
 *  if Read (bAddress.bit0 is 1)  [read-out-query]
 *   bRead : number of bytes to read.
 *
 * ___
 * and an in-transaction to device is receive in response to the out-transaction.
 *  Since the sender already knows the structure of the query.
 *   i.e. which query is read and which query is write.
 *  the in-transaction generator do not include this redundent information in query.
 *
 *  the first bytes is bACK
 *  bACK = number of acknowledment on the bus by slave.
 *  ^^ common for in-read-query and in-write-query
 * in case there was problem in communicating with slave,
 *    the bACK will contain values less than expected.
 *
 *  if the crossponding out-query was a read-out-query
 *  then the number next bytes are data that was readed from slave.
 *   the number of bytes readed depends on the number of bACK.
 *   length of data readed = bACK - 1
 *    the -1 is to remove the acknowledgment of the addressing stage.
 *
 *  If the crossponding out-query was a write-out-query
 *   the (bACK - 1) contains the number of bytes that was written to slave.
 *
 *  In case there was a problem on the bus while communicating.
 *  a shorter in-transaction will be readed and
 *   the device will halt further execution of the transaction.
 *
 * All queries are executed in one go.
 *  i.e. a restart (instead of stop+start) is issued when next query to be executed.
 */
