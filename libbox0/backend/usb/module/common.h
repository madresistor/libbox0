/*
 * This file is part of libbox0.
 * Copyright (C) 2015 Kuldeep Singh Dhaka <kuldeep@madresistor.com>
 *
 * libbox0 is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * libbox0 is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with libbox0.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef LIBBOX0_BACKEND_MODULE_COMMON_H
#define LIBBOX0_BACKEND_MODULE_COMMON_H

#include "../usb-private.h"

__BEGIN_DECLS

typedef b0_result_code (*b0_usb_derive_module_open_after_alloc)(void *);
typedef b0_result_code (*b0_usb_derive_module_open_parse_callback)(void *,
		const struct b0_usb_property *);
typedef b0_result_code (*b0_usb_derive_module_open_after_parse)(void *);

B0_PRIV b0_result_code b0_usb_derive_module_open(b0_device *dev,
		b0_module_type type, int index,
		void **mod, size_t body_size, size_t backend_size,
		b0_usb_derive_module_open_after_alloc after_alloc,
		b0_usb_derive_module_open_parse_callback parse_callback,
		b0_usb_derive_module_open_after_parse after_parse);

typedef b0_result_code (*b0_usb_derive_module_close_before_unload)(void *);

B0_PRIV b0_result_code b0_usb_derive_module_close(void *mod,
	b0_usb_derive_module_close_before_unload before_unload);

__END_DECLS

#endif
