/*
 * This file is part of libbox0.
 * Copyright (C) 2013-2016 Kuldeep Singh Dhaka <kuldeep@madresistor.com>
 *
 * libbox0 is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * libbox0 is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with libbox0.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef LIBBOX0_BACKEND_USB_MODULE_PWM_H
#define LIBBOX0_BACKEND_USB_MODULE_PWM_H

#include "../usb-private.h"

__BEGIN_DECLS

/** @cond INTERNAL */

struct b0_usb_pwm_data {
	b0_usb_module_data header;

	struct {
		/* This is used while reading descriptor to keep count of
		 *  number of item in mod->label.pin */
		size_t pin_count;
	} label;

	struct {
		b0_pwm_reg *width;
		b0_pwm_reg period;
		bool *width_valid;
		bool period_valid;

		unsigned int bitsize;
	} cache;
};

/** @endcond */

typedef struct b0_usb_pwm_data b0_usb_pwm_data;

B0_PRIV b0_result_code b0_usb_pwm_open(b0_device *dev, b0_pwm **mod, int index);
B0_PRIV b0_result_code b0_usb_pwm_close(b0_pwm *mod);

B0_PRIV b0_result_code b0_usb_pwm_bitsize_set(b0_pwm *mod, unsigned int value);
B0_PRIV b0_result_code b0_usb_pwm_bitsize_get(b0_pwm *mod, unsigned int *value);
B0_PRIV b0_result_code b0_usb_pwm_speed_set(b0_pwm *mod, unsigned long value);
B0_PRIV b0_result_code b0_usb_pwm_speed_get(b0_pwm *mod, unsigned long *value);

B0_PRIV b0_result_code b0_usb_pwm_width_set(b0_pwm *mod, uint8_t width_index, b0_pwm_reg width);
B0_PRIV b0_result_code b0_usb_pwm_width_get(b0_pwm *mod, uint8_t width_index, b0_pwm_reg *width);
B0_PRIV b0_result_code b0_usb_pwm_period_set(b0_pwm *mod, b0_pwm_reg period);
B0_PRIV b0_result_code b0_usb_pwm_period_get(b0_pwm *mod, b0_pwm_reg *period);

B0_PRIV b0_result_code b0_usb_pwm_output_prepare(b0_pwm *mod);
B0_PRIV b0_result_code b0_usb_pwm_output_start(b0_pwm *mod);
B0_PRIV b0_result_code b0_usb_pwm_output_stop(b0_pwm *mod);

/** @endcond */

__END_DECLS

#endif
