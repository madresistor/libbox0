/*
 * This file is part of libbox0.
 * Copyright (C) 2013-2016 Kuldeep Singh Dhaka <kuldeep@madresistor.com>
 *
 * libbox0 is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * libbox0 is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with libbox0.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "../usb-private.h"

/** @cond INTERNAL */

#define B0_USB_DIO_WVALUE(pin, value) (((pin) << 8) | (value))

static b0_result_code open_after_alloc(b0_dio *mod)
{
	b0_usb_dio_data *backend_data = B0_MODULE_BACKEND_DATA(mod);

	mod->pin_count = 0;
	mod->ref.high = mod->ref.low = 0;
	mod->ref.type = 0;
	mod->capab = 0;
	mod->label.pin = NULL;
	backend_data->label.pin_count = 0;

	return B0_OK;
}

static b0_result_code capab_parse(b0_dio *mod,
			const struct b0_usb_capab *vendor)
{
	mod->capab = vendor->bmCapab;
	return B0_OK;
}

static b0_result_code open_parse_callback(b0_dio *mod,
				const struct b0_usb_property *vendor)
{
	b0_device *dev = B0_MODULE_DEVICE(mod);

	switch(vendor->bProperty) {
	case B0_USB_COUNT:
		B0_DEBUG_RETURN_ON_FAIL_STMT(dev, b0_usb_count_parse(dev,
			(const struct b0_usb_count *) vendor, &mod->pin_count));
	break;

	case B0_USB_CAPAB:
		B0_DEBUG_RETURN_ON_FAIL_STMT(dev, capab_parse(mod,
			(const struct b0_usb_capab *) vendor));
	break;

	case B0_USB_REF:
		B0_DEBUG_RETURN_ON_FAIL_STMT(dev, b0_usb_ref_parse(dev,
			(const struct b0_usb_ref *) vendor, &mod->ref.low, &mod->ref.high,
			&mod->ref.type));
	break;

	case B0_USB_LABEL: {
		b0_usb_dio_data *backend_data = B0_MODULE_BACKEND_DATA(mod);
		B0_DEBUG_RETURN_ON_FAIL_STMT(dev, b0_usb_label_append(dev,
			(const struct b0_usb_label *) vendor, 0, &mod->label.pin,
			&backend_data->label.pin_count));
	} break;

	default:
		B0_WARN(dev, "Unknown property");
	return B0_ERR_SUPP;
	}

	return B0_OK;
}

static b0_result_code open_after_parse(b0_dio *mod)
{
	b0_device *dev = B0_MODULE_DEVICE(mod);
	b0_usb_dio_data *backend_data = B0_MODULE_BACKEND_DATA(mod);

	if (!mod->pin_count) {
		B0_WARN(dev, "DIO has no pins");
		return B0_ERR_BOGUS;
	}

	void **label_pin = b0_usb_realloc_2d_array((void **) mod->label.pin,
								backend_data->label.pin_count, mod->pin_count);
	B0_ASSERT_RETURN_ON_NULL_ALLOC(dev, label_pin);
	mod->label.pin = (uint8_t **) label_pin;

	return B0_OK;
}

b0_result_code b0_usb_dio_open(b0_device *dev, b0_dio **mod, int index)
{
	return b0_usb_derive_module_open(dev, B0_DIO, index,
		(void *) mod, sizeof(b0_dio), sizeof(b0_usb_dio_data),
		(b0_usb_derive_module_open_after_alloc) open_after_alloc,
		(b0_usb_derive_module_open_parse_callback) open_parse_callback,
		(b0_usb_derive_module_open_after_parse) open_after_parse);
}

static b0_result_code close_before_unload(b0_dio *mod)
{
	b0_usb_free_2d_dynamic_array((void **) mod->label.pin, mod->pin_count);
	return B0_OK;
}

b0_result_code b0_usb_dio_close(b0_dio *mod)
{
	return b0_usb_derive_module_close(mod,
		(b0_usb_derive_module_close_before_unload) close_before_unload);
}

b0_result_code b0_usb_dio_basic_prepare(b0_dio *mod)
{
	return b0_usb_module_mode_set(B0_GENERIC_MODULE(mod),
					B0_USB_MODE_BASIC);
}

b0_result_code b0_usb_dio_basic_start(b0_dio *mod)
{
	b0_result_code r;
	b0_device *dev = B0_MODULE_DEVICE(mod);

	r = b0_usb_module_state_start(B0_GENERIC_MODULE(mod), NULL, 0);
	if (B0_ERROR_RESULT_CODE(r)) {
		B0_ERROR(dev, "request failure (already in running state?)");
		return B0_ERR_IO;
	}

	return r;
}

b0_result_code b0_usb_dio_basic_stop(b0_dio *mod)
{
	b0_result_code r;
	b0_device *dev = B0_MODULE_DEVICE(mod);

	r = b0_usb_module_state_stop(B0_GENERIC_MODULE(mod), NULL, 0);
	if (B0_ERROR_RESULT_CODE(r)) {
		B0_ERROR(dev, "request failure (already in stop state?)");
	}

	return r;
}

/* single */
static int get(b0_dio *mod, uint8_t CMD, unsigned int pin, bool *value)
{
	uint16_t wValue;

	wValue = B0_USB_DIO_WVALUE(pin, 0);

	uint8_t _value;
	if (B0_USB_MODULE_CTRLREQ_IN(mod, CMD, wValue, &_value, 1) < 0) {
		return B0_ERR_IO;
	}

	*value = (_value & 0x01) ? true : false;
	return B0_OK;
}

static int set(b0_dio *mod, uint8_t CMD, unsigned int pin, bool value)
{
	uint16_t wValue;

	uint8_t _value = value ? 0x01 : 0x00;
	wValue = B0_USB_DIO_WVALUE(pin, _value);

	if (B0_USB_MODULE_CTRLREQ_OUT(mod, CMD, wValue, NULL, 0) < 0) {
		return B0_ERR_IO;
	}

	return B0_OK;
}

b0_result_code b0_usb_dio_value_get(b0_dio *mod, unsigned int pin, bool *value)
{
	return get(mod, B0_USB_DIO_VALUE_GET, pin, value);
}

b0_result_code b0_usb_dio_value_set(b0_dio *mod, unsigned int pin, bool value)
{
	return set(mod, B0_USB_DIO_VALUE_SET, pin, value);
}

b0_result_code b0_usb_dio_value_toggle(b0_dio *mod, unsigned int pin)
{
	return set(mod, B0_USB_DIO_VALUE_TOGGLE, pin, false);
}

b0_result_code b0_usb_dio_dir_get(b0_dio *mod, unsigned int pin, bool *value)
{
	return get(mod, B0_USB_DIO_DIR_GET, pin, value);
}

b0_result_code b0_usb_dio_dir_set(b0_dio *mod, unsigned int pin, bool value)
{
	return set(mod, B0_USB_DIO_DIR_SET, pin, value);
}

b0_result_code b0_usb_dio_hiz_get(b0_dio *mod, unsigned int pin, bool *value)
{
	return get(mod, B0_USB_DIO_HIZ_GET, pin, value);
}

b0_result_code b0_usb_dio_hiz_set(b0_dio *mod, unsigned int pin, bool value)
{
	return set(mod, B0_USB_DIO_HIZ_SET, pin, value);
}

/* multiple, all helpers */

/**
 * Calculate the range of pins and bitmask memory length
 * @param[in] pins Pins array
 * @param[in] size length of @a pins
 * @param[out] start_pin Start pin
 * @return the number of bytes requred to store the pins in bitmask
 * @warning @a size cannot be 0
 */
static size_t pin_range(unsigned int *pins, size_t size, uint8_t *start_pin)
{
	uint8_t min, max;
	size_t i, bytes;

	min = max = pins[0];

	for (i = 1; i < size; i++) {
		if (max < pins[i]) {
			max = pins[i];
		}

		if (min > pins[i]) {
			min = pins[i];
		}
	}

	*start_pin = min;

	bytes = (max - min) / 8;
	if (bytes == 0) {
		bytes = 1;
	}

	return bytes;
}

/* multiple */

static int multiple_set(b0_dio *mod, uint8_t bRequest, unsigned int *pins,
								size_t size, bool value)
{
	uint8_t start_pin;
	size_t bytes = pin_range(pins, size, &start_pin);
	uint8_t _value = value ? 0x01 : 0x00;
	uint16_t wValue = B0_USB_DIO_WVALUE(start_pin, _value);
	uint8_t bitmask[32];
	size_t i;

	/* clear the memory */
	memset(bitmask, 0, bytes);

	/* convert pins to mask */
	for (i = 0; i < size; i++) {
		unsigned bit_num = pins[i] - start_pin;
		unsigned off = bit_num / 8;
		unsigned shift = bit_num - (off * 8);
		bitmask[off] |= 1 << shift;
	}

	/* off to the device */
	if (B0_USB_MODULE_CTRLREQ_OUT(mod, bRequest, wValue, bitmask, bytes) < 0) {
		return B0_ERR_IO;
	}

	return B0_OK;
}

static int multiple_get(b0_dio *mod, uint8_t bRequest, unsigned int *pins,
							bool *values, size_t size)
{
	uint8_t start_pin;
	size_t bytes = pin_range(pins, size, &start_pin);
	uint16_t wValue = B0_USB_DIO_WVALUE(start_pin, 0x00);
	uint8_t bitmask[32];
	size_t i;

	/* read from device */
	if (B0_USB_MODULE_CTRLREQ_IN(mod, bRequest, wValue, bitmask, bytes) < 0) {
		return B0_ERR_IO;
	}

	/* convert bitmask to values (using pins) */
	for (i = 0; i < size; i++) {
		unsigned bit_num = pins[i] - start_pin;
		unsigned off = bit_num / 8;
		unsigned shift = bit_num - (off * 8);
		values[i] = !!(bitmask[off] & (1 << shift));
	}

	return B0_OK;
}

b0_result_code b0_usb_dio_multiple_value_set(b0_dio *mod,
						unsigned int *pins, size_t size, bool value)
{
	return multiple_set(mod, B0_USB_DIO_MULTIPLE_VALUE_SET, pins, size, value);
}

b0_result_code b0_usb_dio_multiple_value_get(b0_dio *mod,
						unsigned int *pins, bool *values, size_t size)
{
	return multiple_get(mod, B0_USB_DIO_MULTIPLE_VALUE_GET, pins, values, size);
}

b0_result_code b0_usb_dio_multiple_value_toggle(b0_dio *mod,
						unsigned int *pins, size_t size)
{
	return multiple_set(mod, B0_USB_DIO_MULTIPLE_VALUE_TOGGLE, pins, size, 0);
}

b0_result_code b0_usb_dio_multiple_dir_set(b0_dio *mod, unsigned int *pins,
						size_t size, bool value)
{
	return multiple_set(mod, B0_USB_DIO_MULTIPLE_DIR_SET, pins, size, value);
}

b0_result_code b0_usb_dio_multiple_dir_get(b0_dio *mod, unsigned int *pins,
						bool *values, size_t size)
{
	return multiple_get(mod, B0_USB_DIO_MULTIPLE_DIR_GET, pins, values, size);
}

b0_result_code b0_usb_dio_multiple_hiz_set(b0_dio *mod, unsigned int *pins,
						size_t size, bool value)
{
	return multiple_set(mod, B0_USB_DIO_MULTIPLE_HIZ_SET, pins, size, value);
}

b0_result_code b0_usb_dio_multiple_hiz_get(b0_dio *mod, unsigned int *pins,
						bool *values, size_t size)
{
	return multiple_get(mod, B0_USB_DIO_MULTIPLE_HIZ_GET, pins, values, size);
}

/* all */

static int all_set(b0_dio *mod, uint8_t bRequest, bool value)
{
	uint8_t _value = value ? 0x01 : 0x00;
	uint16_t wValue = B0_USB_DIO_WVALUE(0, _value);
	if (B0_USB_MODULE_CTRLREQ_OUT(mod, bRequest, wValue, NULL, 0) < 0) {
		return B0_ERR_IO;
	}

	return B0_OK;
}

static int all_get(b0_dio *mod, uint8_t bRequest, bool *values)
{
	uint8_t bitmask[32];
	size_t bytes = B0_DIVIDE_AND_CEIL(mod->pin_count, 8);
	size_t i;

	if (B0_USB_MODULE_CTRLREQ_IN(mod, bRequest, 0x00, bitmask, bytes) < 0) {
		return B0_ERR_IO;
	}

	/* convert bitmask to values */
	for (i = 0; i < mod->pin_count; i++) {
		unsigned off = i / 8;
		unsigned shift = i - (off * 8);
		values[i] = !!(bitmask[off] & (1 << shift));
	}

	return B0_OK;
}

b0_result_code b0_usb_dio_all_value_set(b0_dio *mod, bool value)
{
	return all_set(mod, B0_USB_DIO_ALL_VALUE_SET, value);
}

b0_result_code b0_usb_dio_all_value_get(b0_dio *mod, bool *values)
{
	return all_get(mod, B0_USB_DIO_ALL_VALUE_GET, values);
}

b0_result_code b0_usb_dio_all_value_toggle(b0_dio *mod)
{
	return all_set(mod, B0_USB_DIO_ALL_VALUE_TOGGLE, false);
}

b0_result_code b0_usb_dio_all_dir_set(b0_dio *mod, bool value)
{
	return all_set(mod, B0_USB_DIO_ALL_DIR_SET, value);
}

b0_result_code b0_usb_dio_all_dir_get(b0_dio *mod, bool *values)
{
	return all_get(mod, B0_USB_DIO_ALL_DIR_GET, values);
}

b0_result_code b0_usb_dio_all_hiz_set(b0_dio *mod, bool value)
{
	return all_set(mod, B0_USB_DIO_ALL_HIZ_SET, value);
}

b0_result_code b0_usb_dio_all_hiz_get(b0_dio *mod, bool *values)
{
	return all_get(mod, B0_USB_DIO_ALL_HIZ_GET, values);
}

/** @endcond */
