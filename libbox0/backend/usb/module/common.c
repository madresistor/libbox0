/*
 * This file is part of libbox0.
 * Copyright (C) 2015 Kuldeep Singh Dhaka <kuldeep@madresistor.com>
 *
 * libbox0 is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * libbox0 is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with libbox0.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "../usb-private.h"

/**
 * Copy the module for derive module.
 * @param[in] base_mod Base module
 * @param[out] derive_mod_out Derive module
 * @param body_size Size of the module (including sizeof(b0_module))
 * @param backend_size Size of the module backend (including sizeof(b0_usb_module_data)
 * @return result code
 * @warning body_size >= sizeof(b0_module)
 * @warning backend_size >= sizeof(b0_usb_module_data)
 */
static b0_result_code alloc_derive(b0_module *base_mod,
			void **derive_mod_out, size_t body_size, size_t backend_size)
{
	b0_device *dev = B0_MODULE_DEVICE(base_mod);
	void *derive_mod;
	b0_usb_module_data *backend_data;

	/* create a copy of base module memory for derived module */
	derive_mod = calloc(1, body_size);
	B0_ASSERT_RETURN_ON_NULL_ALLOC(dev, derive_mod);
	memcpy(derive_mod, base_mod, sizeof(*base_mod));

	/* create a copy of base module backend data from for derived module */
	backend_data = calloc(1, backend_size);
	if (B0_IS_NULL(backend_data)) {
		free(derive_mod);
	}
	B0_ASSERT_RETURN_ON_NULL_ALLOC(dev, backend_data);
	memcpy(backend_data, base_mod->backend_data, sizeof(*backend_data));
	((b0_module *)derive_mod)->backend_data = backend_data;

	*derive_mod_out = derive_mod;
	return B0_OK;
}

static b0_result_code free_derive(b0_module *mod)
{
	free(mod->backend_data);
	free(mod);
	return B0_OK;
}

b0_result_code b0_usb_derive_module_close(void *mod,
	b0_usb_derive_module_close_before_unload before_unload)
{
	b0_module *base_mod;
	b0_device *dev = B0_MODULE_DEVICE(mod);

	/* deinit backend */
	B0_DEBUG_STMT(dev, before_unload(mod));

	/* search for module (will not fail) */
	B0_DEBUG_STMT(dev, b0_module_search(dev, &base_mod,
		B0_GENERIC_MODULE(mod)->type, B0_GENERIC_MODULE(mod)->index));

	/* unload the base module (ignore error) */
	B0_DEBUG_STMT(dev, b0_usb_module_unload(base_mod));

	/* free memory for derived module */
	B0_DEBUG_STMT(dev, free_derive(mod));

	return B0_OK;
}

static b0_result_code parse_properties(void *mod,
			b0_usb_derive_module_open_parse_callback cb)
{
	b0_result_code r;
	b0_device *dev = B0_MODULE_DEVICE(mod);
	struct b0_usb_module *desc_mod;
	struct b0_usb_property *desc_prop;
	b0_usb_module_data *backend_data = B0_MODULE_BACKEND_DATA(mod);
	void *desc;
	/* assuming 1KB can allocate the target descriptor (in first go) */
	size_t desc_size = 1024;
	int32_t wTotalLength;

	begin:
	desc = malloc(desc_size);

	/* Fetch Box0 module descriptor */
	if (libusb_get_descriptor(B0_USB_DEVICE_USBDH(dev), B0_USB_DESC_MODULE,
			backend_data->desc_index, desc, desc_size) <
				(int) sizeof(struct b0_usb_module)) {
		B0_WARN(dev, "unable to fetch box0 module descriptor");
		return B0_ERR;
	}

	desc_mod = (struct b0_usb_module *) desc;
	if (desc_size < desc_mod->wTotalLength) {
		/* we need to fetch again. (since we have not readed the full desc) */
		desc_size = desc_mod->wTotalLength;
		goto begin;
	}

	/* start parsing property */
	wTotalLength = desc_mod->wTotalLength - desc_mod->bLength;
	desc_prop = desc + desc_mod->bLength;

	while (wTotalLength > 0) {
		if (desc_prop->bDescriptorType != B0_USB_DESC_PROPERTY) {
			B0_WARN(dev, "index mismatch or descriptor has problem");
			goto next_property;
		}

		if (desc_prop->bProperty == 0xFF || desc_prop->bProperty == 0x00) {
			B0_DEBUG(dev, "Ignoring descriptor (0x%"PRIx8")", desc_prop->bProperty);
			goto next_property;
		}

		r = cb(mod, desc_prop);
		if (r == B0_ERR_SUPP) {
			B0_WARN(dev, "module: %"PRIu8" has undefined property "
				"0x%"PRIx8, desc_mod->bModule, desc_prop->bProperty);
		} else if (B0_ERR_RC(r)) {
			B0_WARN(dev, "some problem occured while parsing "
				"property (0x%"PRIx8")", desc_prop->bProperty);
		} else {
			B0_DEBUG(dev, "property successfully parsed "
				"(0x%"PRIx8")", desc_prop->bProperty);
		}

		next_property:
		wTotalLength -= desc_prop->bLength;
		desc_prop = (void *)desc_prop + desc_prop->bLength;
	}

	free(desc);

	if (wTotalLength < 0) {
		B0_WARN(dev, "module total length exceed, "
			"most likely some descriptor mistake");
		return B0_ERR_BOGUS;
	}

	return B0_OK;
}

b0_result_code b0_usb_derive_module_open(b0_device *dev,
		b0_module_type type, int index,
		void **mod, size_t body_size, size_t backend_size,
		b0_usb_derive_module_open_after_alloc after_alloc,
		b0_usb_derive_module_open_parse_callback parse_callback,
		b0_usb_derive_module_open_after_parse after_parse)
{
	void *derive_mod;
	b0_module *base_mod;

	/* search the base module */
	B0_DEBUG_RETURN_ON_FAIL_STMT(dev,
		b0_module_search(dev, &base_mod, type, index));

	/* load the module */
	B0_DEBUG_RETURN_ON_FAIL_STMT(dev, b0_usb_module_load(base_mod));

	/* allocate memory for derived module */
	B0_DEBUG_RETURN_ON_FAIL_STMT(dev,
		alloc_derive(base_mod, &derive_mod, body_size, backend_size));

	/* after alloc */
	B0_DEBUG_RETURN_ON_FAIL_STMT(dev, after_alloc(derive_mod));

	/* on property parse */
	B0_DEBUG_RETURN_ON_FAIL_STMT(dev,
		parse_properties(derive_mod, parse_callback));

	/* after property parsed */
	B0_DEBUG_RETURN_ON_FAIL_STMT(dev, after_parse(derive_mod));

	*mod = derive_mod;
	return B0_OK;
}
