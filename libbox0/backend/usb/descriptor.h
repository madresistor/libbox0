/*
 * This file is part of libbox0.
 * Copyright (C) 2013-2016 Kuldeep Singh Dhaka <kuldeep@madresistor.com>
 *
 * libbox0 is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * libbox0 is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with libbox0.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef B0_BACKEND_USB_DESCRIPTOR_H
#define B0_BACKEND_USB_DESCRIPTOR_H

#include <stdint.h>
#include "common.h"

__BEGIN_DECLS

/** @cond INTERNAL */

#define B0_USB_DESC_TYPE(id) ((0x2 << 5) | ((id) & 0x1F))
#define B0_USB_DESC_DEVICE  B0_USB_DESC_TYPE(1)
#define B0_USB_DESC_MODULE  B0_USB_DESC_TYPE(2)
#define B0_USB_DESC_PROPERTY  B0_USB_DESC_TYPE(3)

#define B0_USB_DIO 1
#define B0_USB_AOUT 2
#define B0_USB_AIN 3
#define B0_USB_SPI 4
#define B0_USB_I2C 5
#define B0_USB_PWM 6

#define B0_USB_REF 1
#define B0_USB_SPEED 2
#define B0_USB_BITSIZE 3
#define B0_USB_CAPAB 4
#define B0_USB_COUNT 5
#define B0_USB_LABEL 6
#define B0_USB_BUFFER 7
#define B0_USB_BITSIZE_SPEED 10

#define B0_USB_SPEED_SET 1
#define B0_USB_SPEED_GET 2

/* device type command */
#define B0_USB_NOP 1

/* states of module */
#define B0_USB_STATE_GET 3
#define B0_USB_STATE_SET 4

#define B0_USB_STATE_STOP  0x00
#define B0_USB_STATE_START 0x01
#define B0_USB_STATE_STOPPED 0x00
#define B0_USB_STATE_RUNNING 0x01

//defined on the states implemented by module
// [BOX0STATUS_STOPPED] ----(BOX0_STATE_START)-----> [BOX0STATUS_STARTED]
// [BOX0STATUS_STARTED] ----(BOX0_STATE_STOP)-----> [BOX0STATUS_STOP]
//bits other than 2 LSB's can be module specific
#define B0_USB_STATE_STATUS_STOPPED 0x00
#define B0_USB_STATE_STATUS_RUNNING 0x01

/* chan list */
#define B0_USB_CHAN_SEQ_GET 7
#define B0_USB_CHAN_SEQ_SET 8

/* bitsize */
#define B0_USB_BITSIZE_GET 9
#define B0_USB_BITSIZE_SET 10

/* output */
#define B0_USB_REPEAT_GET 11
#define B0_USB_REPEAT_SET 12

/* bitsize and speed -- for AIN and AOUT */
#define B0_USB_BITSIZE_SPEED_SET 13
#define B0_USB_BITSIZE_SPEED_GET 14

/* Control request argument (data stage) */
struct b0_usb_bitsize_speed_arg {
	uint32_t dSpeed;
	uint8_t bBitsize;
} B0_PACKED;

#define B0_USB_MODE_SET 15
#define B0_USB_MODE_GET 16

#define B0_USB_MODE_SNAPSHOT 0x0 /* AIN, AOUT */
#define B0_USB_MODE_BASIC 0x0 /* DIO */
#define B0_USB_MODE_OUTPUT 0x0 /* PWM */
#define B0_USB_MODE_MASTER 0x0 /* SPI, I2C */
#define B0_USB_MODE_STREAM 0x1 /* AIN, AOUT */

/**
 * Box0 USB Versioning:
 * TODO: place the vering scheme here
 */

/**
 * @brief used as header marker of start of a module
 * @param bLength size of the descriptor
 * @param bDescriptorType should be B0_USB_DESC_MODULE
 * @param bcdBOX0 box0 ver of the module confirm to. (in BCD format)
 * @param wTotalLength total length the module including property/
 * @param bModule type of module
 * @param bIndex value that will be used to ref the module
 * @param bInterface module attached to which interface
 *
 * when control reguest is performed:
 * wIndex paramter of control transfer is:
 * 		B0_WINDEX_FROM_MODULE(module) = HIGH8(module.bModule) | LOW8(module.bIndex)
 *
 * HIGH8(x) = (x << 8)
 * LOW8(x) = (x << 0)
 */
struct b0_usb_module {
	uint8_t bLength;
	uint8_t bDescriptorType;
	uint16_t wTotalLength;
	uint8_t bModule;
	uint8_t bIndex;
	uint8_t bInterfaceNumber;
	uint8_t iName;
} B0_PACKED;

/**
 * @brief used as header for other property.
 * @param bLength size of the descriptor
 * @param bDescriptorType should be B0_USB_DESC_PROPERTY
 * @param bProperty name of property
 */
struct b0_usb_property {
	uint8_t bLength;
	uint8_t bDescriptorType;
	uint8_t bProperty;
} B0_PACKED;

#define B0_USB_REF_TYPE_VOLTAGE 0
#define B0_USB_REF_TYPE_CURRENT 1

/**
 * @brief reference property
 * @param header header of type struct b0_usb_property
 * @param bType Reference Type
 * @param spLow ref low
 * @param spHigh ref high
 */
struct b0_usb_ref {
	struct b0_usb_property header;
	uint8_t bType;
	float spLow;
	float spHigh;
} B0_PACKED;

#define B0_USB_SPEED_OPER_RAW 0
#define B0_USB_SPEED_OPER_INC 1
#define B0_USB_SPEED_OPER_DIV 2
#define B0_USB_SPEED_OPER_DEC 3

#define B0_USB_SPEED_KHZ(val) (val * 1000)
#define B0_USB_SPEED_MHZ(val) (val * 1000000)

/**
 * @brief speed header
 * @param header header of type struct b0_usb_property
 * @param bOperator operator type
 */
struct b0_usb_speed {
	struct b0_usb_property header;
	uint8_t bOperator;
} B0_PACKED;

/**
 * @brief speed (incremental)
 * @param header header of type struct b0_usb_speed
 * @param dCount number of values to generate
 * @param dStart number to start calculation from
 * @param dIncrement value to increment in each iteration
 *
 * @note dCount = 0 is not a valid value
 * @note dStart = 0 is a valid value
 * @note dIncrement = 0 is not a valid value
 */
struct b0_usb_speed_inc {
	struct b0_usb_speed header;
	uint32_t dCount;
	uint32_t dStart;
	uint32_t dIncrement;
} B0_PACKED;

/**
 * @brief generate speed using division
 * @param header header of type struct b0_usb_speed
 * @param dCount number of values to generate
 * @param dSpeed value to start division from
 * @note dCount = 0 is not a valid value
 * @note dCount = 1 is not a valid value
 * @note dSpeed = 0 is not a valid value
 */
struct b0_usb_speed_div {
	struct b0_usb_speed header;
	uint32_t dCount;
	uint32_t dSpeed;
} B0_PACKED;

/**
 * @brief generate speed from raw values
 * @param dSpeed[] speed values
 */
struct b0_usb_speed_raw {
	struct b0_usb_speed header;
	uint32_t dValues[0];
} B0_PACKED;

/**
 * @brief speed (decement)
 * @param header header of type b0_speed
 * @param dCount number of values to generate
 * @param dStart number to start calculation from
 * @param dDecrement value to decrement in each iteration
 *
 * @note dCount = 0 is not a valid value
 * @note dStart = 0 is a valid value
 * @note dDecrement = 0 is not a valid value
 */
struct b0_usb_speed_dec {
	struct b0_usb_speed header;
	uint32_t dCount;
	uint32_t dStart;
	uint32_t dDecrement;
} B0_PACKED;

/**
 * @brief bitsize values
 * @param header header of type b0_usb_property
 * @param bValues[] array of bitsizes
 */
struct b0_usb_bitsize {
	struct b0_usb_property header;
	uint8_t bValues[0];
} B0_PACKED;

#define B0_USB_BITSIZE_SPEED_SNAPSHOT_USE 0
#define B0_USB_BITSIZE_SPEED_STREAM_USE 1

struct b0_usb_bitsize_speed {
	struct b0_usb_property header;

	uint8_t bUse;

	/* Rule:
	 *  - unused are 0
	 *  - atleast value is provided
	 *  - valid at top
	 */
	uint8_t bBitsize[3];

	uint8_t bSpeedOperator;
} B0_PACKED;

struct b0_usb_bitsize_speed_inc {
	struct b0_usb_bitsize_speed header;
	uint32_t dSpeedCount;
	uint32_t dSpeedStart;
	uint32_t dSpeedIncrement;
} B0_PACKED;

struct b0_usb_bitsize_speed_div {
	struct b0_usb_bitsize_speed header;
	uint32_t dSpeedCount;
	uint32_t dSpeedSpeed;
} B0_PACKED;

struct b0_usb_bitsize_speed_raw {
	struct b0_usb_bitsize_speed header;
	uint32_t dSpeedValues[0];
} B0_PACKED;

struct b0_usb_bitsize_speed_dec {
	struct b0_usb_bitsize_speed header;
	uint32_t dSpeedCount;
	uint32_t dSpeedStart;
	uint32_t dSpeedDecrement;
} B0_PACKED;

/**
 * @brief capab of a module (module specific)
 * @param header header of type struct b0_usb_property
 * @param bmCapab0 bitmap of capab
 * @note bmCapabX can be added as per module
 */
struct b0_usb_capab {
	struct b0_usb_property header;
	uint8_t bmCapab;
} B0_PACKED;

/**
 * @brief channel/pin count
 * @param header header of type b0_usb_property
 * @param bCount number if channels/pins
 *
 */
struct b0_usb_count {
	struct b0_usb_property header;
	uint8_t bCount;
} B0_PACKED;

/**
 * @param iLabel[] is a list of string descriptor that will be shown to user
 * @param iLabel[] is a seq that is interpretter by the module finally
 * Example:
 * 		iLabel[0] is
 * 			channel0 name for AIN/AOUT
 * 			pin name for DIO
 * 			clock pin name for I2C
 * if the list is shorter than the exepcted list is assumed
 * if property missing values is assumed
 * if any value is missing, string is as per host choice
 */
struct b0_usb_label {
	struct b0_usb_property header;
	uint8_t iValues[0];
} B0_PACKED;

struct b0_usb_buffer {
	struct b0_usb_property header;
	uint8_t bUnused; /* for alignment only */
	uint32_t dValue;
} B0_PACKED;

/* ================================================================== */

/* BIT0 */
#define B0_USB_AIN_CAPAB_FORMAT_BINARY 0x00
#define B0_USB_AIN_CAPAB_FORMAT_2COMPL 0x01
/* BIT1 */
#define B0_USB_AIN_CAPAB_ALIGN_LSB 0x00
#define B0_USB_AIN_CAPAB_ALIGN_MSB 0x02
/* BIT2 */
#define B0_USB_AIN_CAPAB_ENDIAN_LITTLE 0x00
#define B0_USB_AIN_CAPAB_ENDIAN_BIG 0x04

/* ================================================================== */

/* BIT0 */
#define B0_USB_AOUT_CAPAB_FORMAT_BINARY 0x00
#define B0_USB_AOUT_CAPAB_FORMAT_2COMPL 0x01
/* BIT1 */
#define B0_USB_AOUT_CAPAB_ALIGN_LSB 0x00
#define B0_USB_AOUT_CAPAB_ALIGN_MSB 0x02
/* BIT2 */
#define B0_USB_AOUT_CAPAB_ENDIAN_LITTLE 0x00
#define B0_USB_AOUT_CAPAB_ENDIAN_BIG 0x04

/*====================================================================*/

/* output/input mode */
#define B0_USB_DIO_CAPAB_OUTPUT (0x1 << 0)
#define B0_USB_DIO_CAPAB_INPUT (0x1 << 1)
#define B0_USB_DIO_CAPAB_HIZ (0x1 << 2)

#define B0_USB_DIO_ALL_HIZ_GET 147
#define B0_USB_DIO_ALL_HIZ_SET 146
#define B0_USB_DIO_ALL_DIR_GET 145
#define B0_USB_DIO_ALL_DIR_SET 144
#define B0_USB_DIO_ALL_VALUE_TOGGLE 143
#define B0_USB_DIO_ALL_VALUE_GET 142
#define B0_USB_DIO_ALL_VALUE_SET 141

#define B0_USB_DIO_MULTIPLE_HIZ_GET 127
#define B0_USB_DIO_MULTIPLE_HIZ_SET 126
#define B0_USB_DIO_MULTIPLE_DIR_GET 125
#define B0_USB_DIO_MULTIPLE_DIR_SET 124
#define B0_USB_DIO_MULTIPLE_VALUE_TOGGLE 123
#define B0_USB_DIO_MULTIPLE_VALUE_GET 122
#define B0_USB_DIO_MULTIPLE_VALUE_SET 121

#define B0_USB_DIO_HIZ_GET 107
#define B0_USB_DIO_HIZ_SET 106
#define B0_USB_DIO_DIR_GET 105
#define B0_USB_DIO_DIR_SET 104
#define B0_USB_DIO_VALUE_TOGGLE 103
#define B0_USB_DIO_VALUE_GET 102
#define B0_USB_DIO_VALUE_SET 101

/* Pin config */
#define B0_USB_DIO_OUTPUT 0x01
#define B0_USB_DIO_INPUT 0x00

/* Pin Values http://en.wikipedia.org/wiki/Four-valued_logic
 * Toggle is applied on OUTPUT with HIGH or LOW value
 * if pin is in HiZ state, then behaviour is unknown
 */
#define B0_USB_DIO_HIGH 1
#define B0_USB_DIO_LOW 0

/* ================================================================== */

#define B0_USB_I2C_VERSION 101

#define B0_USB_I2C_VERSION_GET 101
#define B0_USB_I2C_VERSION_SET 102

#define B0_USB_I2C_VERSION_SM 0
#define B0_USB_I2C_VERSION_FM 1
#define B0_USB_I2C_VERSION_HS 2
//#define B0_USB_I2C_VERSION_HS_CLEANUP1 3
#define B0_USB_I2C_VERSION_FMPLUS 4
#define B0_USB_I2C_VERSION_UFM 5
//#define B0_USB_I2C_VERSION_VER5 6
//#define B0_USB_I2C_VERSION_VER6 7

struct b0_usb_i2c_version {
	struct b0_usb_property header;
	uint8_t bValues[0];
} B0_PACKED;

#define B0_USB_I2C_COMMAND_SIGNATURE 0xA4C60277
#define B0_USB_I2C_STATUS_SIGNATURE 0x836B2A7C

/* bCommand */
#define B0_USB_I2C_COMMAND_TRANSACTION 0

struct b0_usb_i2c_command {
	uint32_t dSignature;
	uint32_t dTag;
	uint32_t dLength;
	uint8_t bCommand;
} B0_PACKED;

#define B0_USB_I2C_STATUS_SUCCESS 0 /* Success */
/* Non-zero values are error */
#define B0_USB_I2C_STATUS_ERR_INVALID 1 /* Invalid Transaction */
#define B0_USB_I2C_STATUS_ERR_UNDEFINED 2 /* Unknown error */
#define B0_USB_I2C_STATUS_ERR_PREMATURE 3 /* Premature transfer */
#define B0_USB_I2C_STATUS_ERR_ARBITRATION 4 /* Arbitration lost */
#define B0_USB_I2C_STATUS_ERR_ELECTRICAL 5 /* Bus/Electrical problem */

struct b0_usb_i2c_status {
	uint32_t dSignature;
	uint32_t dTag;
	uint32_t dLength;
	uint8_t bStatus;
} B0_PACKED;

struct b0_usb_i2c_out_read {
	uint8_t bAddress;
	uint8_t bVersion;
	uint8_t bReserved;
	uint8_t bRead;
} B0_PACKED;

struct b0_usb_i2c_out_write {
	uint8_t bAddress;
	uint8_t bVersion;
	uint8_t bReserved;
	uint8_t bWrite;
	uint8_t bData[0];
} B0_PACKED;

struct b0_usb_i2c_in_read {
	uint8_t bACK;
	uint8_t bData[0];
} B0_PACKED;

struct b0_usb_i2c_in_write {
	uint8_t bACK;
} B0_PACKED;

/* ================================================================== */

/* property/ */
#define B0_USB_PWM_INITAL 101
#define B0_USB_PWM_SYMMETRIC 102

/* commands */
#define B0_USB_PWM_WIDTH_GET 101
#define B0_USB_PWM_WIDTH_SET 102
#define B0_USB_PWM_PERIOD_GET 103
#define B0_USB_PWM_PERIOD_SET 104

/*
 * state of output (LOW or HIGH) when (Width < Period)
 * if not provided assumed LOW only
 */
struct b0_usb_pwm_inital {
	struct b0_usb_property header;
	uint8_t bValues[0];
} B0_PACKED;

#define B0_USB_PWM_INITAL_LOW 0x00
#define B0_USB_PWM_INITAL_HIGH 0x01

/* symetricity in output
 * if not provided, symmetric = B0_USB_PWM_SYMMETRIC_NO is assumed
 */
struct b0_usb_pwm_symmetric {
	struct b0_usb_property header;
	uint8_t bValues[0];
} B0_PACKED;

#define B0_USB_PWM_SYMMETRIC_NO 0x00
#define B0_USB_PWM_SYMMETRIC_YES 0x01

/*
 * get active state value of slave select pin
 * bmRequestType: device->host, vendor, interface
 * bRequest: B0_SPI_ACTIVE_STATE_GET
 * wValue: HIGH8(<slave-select>) LOW8(0)
 * wIndex: B0_WINDEX_FROM_MODULE(module)
 * wLength: 1
 * data = <value>
 *
 * value = 0:LOW, 1:HIGH
 *
 * STALL if slave-select out of range
 *
 * by default SS is low
 */
#define B0_USB_SPI_ACTIVE_STATE_GET 101

/*
 * set active state value of slave select pin
 * bmRequestType: host->device, vendor, interface
 * bRequest: B0_SPI_ACTIVE_STATE_SET
 * wValue: HIGH8(<pin-index>) LOW8(value)
 * wIndex: B0_WINDEX_FROM_MODULE(module)
 * wLength: 0
 *
 * value: 0x0 = LOW, 0x1 = HIGH
 *
 * STALL if pin-index out of range
 */
#define B0_USB_SPI_ACTIVE_STATE_SET 102

#define B0_USB_SPI_COMMAND_SIGNATURE 0x9F7AB274
#define B0_USB_SPI_STATUS_SIGNATURE  0xCAB93829

/* bCommand */
#define B0_USB_SPI_COMMAND_TRANSACTION 0

struct b0_usb_spi_command {
	uint32_t dSignature;
	uint32_t dTag;
	uint32_t dLength;
	uint8_t bCommand;
} B0_PACKED;

#define B0_USB_SPI_STATUS_SUCCESS 0
/* Non-zero values are error */
#define B0_USB_SPI_STATUS_ERR_INVALID 1 /* Invalid Transaction */

struct b0_usb_spi_status {
	uint32_t dSignature;
	uint32_t dTag;
	uint32_t dLength;
	uint8_t bStatus;
} B0_PACKED;

#define B0_USB_SPI_CONFIG_CPHA 				(0x1 << 0)
#define B0_USB_SPI_CONFIG_CPOL 				(0x1 << 1)
#define B0_USB_SPI_CONFIG_FD		 		(0x0 << 2)
#define B0_USB_SPI_CONFIG_HD_READ 			(0x3 << 2)
#define B0_USB_SPI_CONFIG_HD_WRITE			(0x1 << 2)
#define B0_USB_SPI_CONFIG_MSB_FIRST		(0x0 << 4)
#define B0_USB_SPI_CONFIG_LSB_FIRST		(0x1 << 4)

/* OUT Full Duplex */
struct b0_usb_spi_out_fd {
	uint8_t bSS;
	uint8_t bmConfig;
	uint8_t bBitsize;
	uint8_t bCount;
	uint32_t dSpeed;
	uint8_t xData[0];
} B0_PACKED;

/* OUT Half Duplex Read */
struct b0_usb_spi_out_hd_read {
	uint8_t bSS;
	uint8_t bmConfig;
	uint8_t bBitsize;
	uint8_t bRead;
	uint32_t dSpeed;
} B0_PACKED;

/* OUT Half Duplex Write */
struct b0_usb_spi_out_hd_write {
	uint8_t bSS;
	uint8_t bmConfig;
	uint8_t bBitsize;
	uint8_t bWrite;
	uint32_t dSpeed;
	uint8_t xData[0];
} B0_PACKED;

/* IN Full Duplex */
struct b0_usb_spi_in_fd {
	uint8_t xData[0];
} B0_PACKED;

/* IN Half Duplex Read */
struct b0_usb_spi_in_hd_read {
	uint8_t xData[0];
} B0_PACKED;

/* IN Half Duplex Write */
struct b0_usb_spi_in_hd_write {
	uint8_t bWrite;
} B0_PACKED;

/* Device */
struct b0_usb_device {
	uint8_t bLength;
	uint8_t bDescriptorType; /* Should be B0_USB_DESC_DEVICE */
	uint16_t bcdBOX0;
	uint8_t bNumModules;
} B0_PACKED;

/** @endcond */

__END_DECLS

#endif
