/*
 * This file is part of libbox0.
 * Copyright (C) 2013-2016 Kuldeep Singh Dhaka <kuldeep@madresistor.com>
 *
 * libbox0 is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * libbox0 is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with libbox0.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef LIBBOX0_BACKEND_USB_PRIVATE_H
#define LIBBOX0_BACKEND_USB_PRIVATE_H

/* libusb-1.0 */
#include <libusb.h>

#include "../backend.h"
#include "../../libbox0-private.h"
#include "../../common.h"
#include "usb.h"
#include "descriptor.h"
#include "device.h"
#include "extra.h"
#include "backend/usb/config.h"

#include "module/module.h"
#include "module/common.h"
#include "module/ain.h"
#include "module/dio.h"
#include "module/pwm.h"
#include "module/aout.h"
#include "module/i2c.h"
#include "module/spi.h"

__BEGIN_DECLS

/** @cond INTERNAL */

struct b0_usb {
	const b0_backend *backend;

	/**
	 * USB device VID (Vendor ID)
	 */
	uint16_t vid;

	/**
	 * USB device PID (Product ID)
	 */
	uint16_t pid;
};

typedef struct b0_usb b0_usb;

#define B0_USB_DEVICE_BACKEND_DATA(dev) 									\
	((b0_usb_device_data *) B0_DEVICE_BACKEND_DATA(dev))

#define B0_USB_MODULE_BACKEND_DATA(mod) 									\
	((b0_usb_module_data *) B0_MODULE_BACKEND_DATA(mod))

#define B0_USB_DEVICE_USBDH(dev)											\
	B0_USB_DEVICE_BACKEND_DATA(dev)->usbdh

#define B0_USB_DEVICE_CTRLREQ_TIMEOUT(dev)									\
	B0_USB_DEVICE_BACKEND_DATA(dev)->config.ctrlreq_timeout

#define B0_USB_DEVICE_BULK_TIMEOUT(dev)									\
	B0_USB_DEVICE_BACKEND_DATA(dev)->config.bulk_timeout

#define B0_USB_CTRLREQ(dev, bmReq, bRequest, wValue, wIndex, data, len)	\
	libusb_control_transfer(B0_USB_DEVICE_USBDH(dev),						\
		bmReq, bRequest, wValue, wIndex, data, len,							\
		B0_USB_DEVICE_CTRLREQ_TIMEOUT(dev))

#define B0_USB_DEVICE_CTRLREQ_OUT(dev, bRequest, wValue, wIndex, data, len)\
	B0_USB_CTRLREQ(dev, 0x40, bRequest, wValue, wIndex, data, len)

#define B0_USB_DEVICE_CTRLREQ_IN(dev, bRequest, wValue, wIndex, data, len)	\
	B0_USB_CTRLREQ(dev, 0xC0, bRequest, wValue, wIndex, data, len)

#define B0_USB_MODULE_WINDEX(mod) (											\
	(B0_USB_MODULE_BACKEND_DATA(mod)->bModule << 8) |						\
	(B0_USB_MODULE_BACKEND_DATA(mod)->bIndex)								\
)

#define B0_USB_MODULE_USBDH(mod)											\
	B0_USB_DEVICE_USBDH(B0_MODULE_DEVICE(mod))

#define B0_USB_MODULE_CTRLREQ_TIMEOUT(mod)									\
	B0_USB_DEVICE_CTRLREQ_TIMEOUT(B0_MODULE_DEVICE(mod))

#define B0_USB_MODULE_BULK_TIMEOUT(mod)									\
	B0_USB_DEVICE_BULK_TIMEOUT(B0_MODULE_DEVICE(mod))

#define B0_USB_MODULE_CTRLREQ_OUT(mod, bRequest, wValue, data, len)		\
	B0_USB_CTRLREQ(B0_MODULE_DEVICE(mod), 0x41, bRequest, wValue,			\
		B0_USB_MODULE_WINDEX(mod), data, len)

#define B0_USB_MODULE_CTRLREQ_IN(mod, bRequest, wValue, data, len)			\
	B0_USB_CTRLREQ(B0_MODULE_DEVICE(mod), 0xC1, bRequest, wValue,			\
		B0_USB_MODULE_WINDEX(mod), data, len)

/** @endcond */

__END_DECLS

#endif
