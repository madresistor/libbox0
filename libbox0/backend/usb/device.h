/*
 * This file is part of libbox0.
 * Copyright (C) 2013-2016 Kuldeep Singh Dhaka <kuldeep@madresistor.com>
 *
 * libbox0 is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * libbox0 is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with libbox0.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef LIBBOX0_BACKEND_USB_DEVICE_H
#define LIBBOX0_BACKEND_USB_DEVICE_H

#include "../../common.h"

__BEGIN_DECLS

/** @cond INTERNAL */

struct b0_usb_device_data {
	/** libusb ds */
	libusb_context *usbc;
	libusb_device *usbd;
	libusb_device_handle *usbdh;

	uint16_t bcdBOX0;

	/** mantain which resource are generated internally,
	 * and they will only be freed
	 */
	int internal_resource;

	/**
	 * polling thread for device
	 */
	struct {
		int exit;
		pthread_t thread_id;
	} async;

	struct {
		/** STM32F0 do not support transaction of 0byte read/write.
		 * this is due to the DMA 0byte read/write
		 */
		bool i2c_0byte;
	} fix;

	/**
	 * Configurable part by user
	 */
	struct {
		unsigned long ctrlreq_timeout;
		unsigned long bulk_timeout;
	} config;
};

typedef struct b0_usb_device_data b0_usb_device_data;

B0_PRIV b0_result_code b0_usb_device_ping(b0_device *dev);
B0_PRIV b0_result_code b0_usb_device_close(b0_device *dev);

/** @endcond */

__END_DECLS

#endif
