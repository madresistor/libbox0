/*
 * This file is part of libbox0.
 * Copyright (C) 2013-2016 Kuldeep Singh Dhaka <kuldeep@madresistor.com>
 *
 * libbox0 is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * libbox0 is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with libbox0.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef LIBBOX0_BACKEND_EXTRA_H
#define LIBBOX0_BACKEND_EXTRA_H

#include "usb-private.h"

__BEGIN_DECLS

B0_PRIV extern const b0_backend usb_backend;

B0_PRIV uint8_t *b0_usb_string_desc_as_utf8(b0_device *dev, uint8_t desc_index);

B0_PRIV b0_result_code b0_usb_label_append(b0_device *dev,
			const struct b0_usb_label *vendor, unsigned used,
			uint8_t ***_values, size_t *_count);

B0_PRIV b0_result_code b0_usb_speed_parse(b0_device *dev,
		const struct b0_usb_speed *vendor, unsigned long **values, size_t *count);

/* Genericized AIN AOUT struct */
struct _b0_usb_mode_bitsize_speeds {
	struct _b0_usb_bitsize_speeds {
		unsigned int bitsize;
		struct {
			unsigned long *values;
			size_t count;
		} speed;
	} *values;
	size_t count;
};

B0_PRIV b0_result_code b0_usb_bitsize_speed_parse(b0_device *dev,
		const struct b0_usb_bitsize_speed *vendor,
		struct _b0_usb_mode_bitsize_speeds *snapshot,
		struct _b0_usb_mode_bitsize_speeds *stream);

struct b0_usb_ep {
	uint8_t bEndpointAddress;
	uint16_t wMaxPacketSize;
};

B0_PRIV b0_result_code b0_usb_search_endpoint(b0_module *mod,
		bool in_required, struct b0_usb_ep *in,
		bool out_required, struct b0_usb_ep *out);

B0_PRIV void b0_usb_free_2d_dynamic_array(void **arr, size_t size);

B0_PRIV void **b0_usb_realloc_2d_array(void **arr, size_t size_old, size_t size_new);

B0_PRIV b0_result_code b0_usb_bitsize_parse(b0_device *dev,
		const struct b0_usb_bitsize *vendor,
		unsigned int **_values, size_t *_count);

B0_PRIV b0_result_code b0_usb_ref_parse(b0_device *dev,
		const struct b0_usb_ref *vendor, double *low, double *high,
		b0_ref_type *type);

B0_PRIV b0_result_code b0_usb_count_parse(b0_device *dev,
		const struct b0_usb_count *vendor, unsigned int *value);

B0_PRIV b0_result_code b0_usb_buffer_parse(b0_device *dev,
		const struct b0_usb_buffer *vendor, size_t *value);

__END_DECLS

#endif
