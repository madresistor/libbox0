/*
 * This file is part of libbox0.
 * Copyright (C) 2013-2016 Kuldeep Singh Dhaka <kuldeep@madresistor.com>
 *
 * libbox0 is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * libbox0 is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with libbox0.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef LIBBOX0_BACKEND_USB_H
#define LIBBOX0_BACKEND_USB_H

#include "../../common.h"
#include "../../libbox0.h"

__BEGIN_DECLS

/** @cond INTERNAL */

#if !defined(LIBUSB_H)
/* libusb not present */
typedef struct libusb_device libusb_device;
typedef struct libusb_device_handle libusb_device_handle;
typedef struct libusb_context libusb_context;
#endif

/** @endcond */

/**
 * @addtogroup backend_usb Backend USB
 * @{
 */

/**
 * @brief open a usb device using USB {VID, PID} pair
 * @param[out] dev pointer to memory location where b0_device will be placed
 * @param[in] vid Vendor ID
 * @param[in] pid Product ID
 * @note open the first device as returned by libusb_open_device_with_vid_pid
 * @return result code
 * @see b0_device_close()
 * @see b0_usb_open_handle()
 * @see b0_usb_open()
 */
B0_API b0_result_code b0_usb_open_vid_pid(b0_device **dev, uint16_t vid, uint16_t pid);

/**
 * @brief build a libbox0 device based on libusb_device
 * @param[in] usbd libusb device
 * @param[out] dev pointer to memory location to store b0_device
 * @return result code
 * @note libusb device context is not owned by libbox0 and will *not* be freed on close
 * @see b0_device_close()
 * @see b0_usb_open_vid_pid()
 * @see b0_usb_open_handle()
 */
B0_API b0_result_code b0_usb_open(libusb_device *usbd, libusb_context *usbc,
		b0_device **dev);

/**
 * @brief build a libbox0 device based on libusb_device_handle
 * @param[in] usbdh libusb device handle
 * @param[in] usbc libusb context
 * @param[out] dev memory location to store libbox0 device
 * @return result code
 * @note libusb context and libusb device handle is not owned by libbox
 *   and will not be freed on close
 * @see b0_device_close()
 * @see b0_usb_open_vid_pid()
 * @see b0_usb_open()
 */
B0_API b0_result_code b0_usb_open_handle(libusb_device_handle *usbdh,
				libusb_context *usbc, b0_device **dev);

/**
 * @brief open any connected and supported libbox0 device.
 * @details seach for any connected and supported libbox0 device. \n
 *  if multiple devices are connected, then this will return \n
 *  the first successfully opened usb device
 * @param[in] dev pointer to memory where to resultant device
 * @return result code
 * @note libusb context is owned by library and will be freed on close
 * @note libusb device handle is owned by library and will be freed on close
 * @see b0_device_close()
 * @see b0_usb_open_vid_pid()
 * @see b0_usb_open_handle()
 * @see b0_usb_open()
 */
B0_API b0_result_code b0_usb_open_supported(b0_device **dev);

/**
 * @brief search for backend information about libusb device
 * @details search is based on VID/PID match
 * @param[in] usbd libusb device
 * param[out] backend backend information
 * @return B0_ERR_SUPP if device not support
 * @return B0_ERR_ARG if @a usbd or @a supp is NULL
 * @return B0_OK if device supported
 * @see b0_usb_open()
 * @see b0_usb_open_supported()
 */
B0_API b0_result_code b0_usb_search_backend(libusb_device *usbd,
			const b0_backend **backend);

/**
 * @brief extract libusb_device from b0_device
 * @param[in] dev device
 * @param[out] usbd memory location to store libusb device
 * @return result code
 */
B0_API b0_result_code b0_usb_libusb_device(b0_device *dev, libusb_device **usbd);

/**
 * @brief extract libusb_context from b0_device
 * @param[in] dev device
 * @param[out] usbc memory location to store libusb context
 * @return result code
 */
B0_API b0_result_code b0_usb_libusb_context(b0_device *dev, libusb_context **usbc);

/**
 * @brief extract libusb_device_handle from b0_device
 * @param[in] dev device
 * @param[out] usbdh memory location to store libusb device handle
 * @return result code
 */
B0_API b0_result_code b0_usb_libusb_device_handle(b0_device *dev,
	libusb_device_handle **usbdh);

/**
 * Set USB Bulk timeout
 * @param dev Device
 * @param timeout Timeout (unit: milliseconds)
 * @return result code
 */
B0_API b0_result_code b0_usb_device_bulk_timeout(b0_device *dev,
			unsigned long timeout);

/**
 * Set USB Control request timeout
 * @param dev Device
 * @param timeout Timeout (unit: milliseconds)
 * @return result code
 */
B0_API b0_result_code b0_usb_device_ctrlreq_timeout(b0_device *dev,
			unsigned long timeout);

/**
 * Set AIN callback delay.
 * Decreasing the value will increase the update rate.
 *
 * @warning the already set value may be platform dependent. \n
 *   Modify at your own risk.
 *
 * @param mod AIN module
 * @param ms_delay Delay (in milliseconds)
 * @return result code
 */
B0_API b0_result_code b0_usb_ain_stream_delay(b0_ain *mod,
			unsigned long ms_delay);

/**
 * Set AOUT maximum data (in bytes) that can be pending at kernel.
 *
 * @warning the already set value may be platform dependent. \n
 *   Modify at your own risk.
 *
 * @param mod AOUT module
 * @param pending pending memory (bytes)
 * @return result code
 */
B0_API b0_result_code b0_usb_aout_stream_pending(b0_aout *mod,
			unsigned long pending);

/**
 * @}
 */

__END_DECLS

#endif
