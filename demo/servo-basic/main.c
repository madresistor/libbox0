/*
 * This file is part of libbox0.
 * Copyright (C) 2013-2016 Kuldeep Singh Dhaka <kuldeep@madresistor.com>
 *
 * libbox0 is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * libbox0 is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with libbox0.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <stdio.h>
#include <stdlib.h>
#include <libbox0/libbox0.h>
#include <libbox0/backend/usb/usb.h>
#include <libbox0/driver/servo/servo.h>
#include "../common-code/sigkill.c"
#include "../common-code/sleep.c"
#include <math.h>

#define VALUE_CHANGE_DELAY_MS 10

int main(int argc, char *argv[])
{
	/* my findings on VIGOR VS-2 servo */
	b0_servo_config config = {
		.min = {
			//some old config: .angle = -110 degree,
			.angle = -M_PI_2,
			.duty_cycle = 3
		},
		.max = {
			//some old config: .angle = 50 degree,
			.angle = M_PI_2,
			.duty_cycle = 13
		},

		.period = 20.0 / 1000,
		.calc_error = B0_SERVO_CALC_ERROR
	};

	b0_device *dev = NULL;
	double i;
	b0_pwm *pwm0 = NULL;
	b0_servo *servo = NULL;

	if (B0_ERR_RC(b0_usb_open_supported(&dev))) {
		fprintf(stderr, "unable to open device\n");
		return EXIT_FAILURE;
	}

	if (B0_ERR_RC(b0_pwm_open(dev, &pwm0, 0))) {
		fprintf(stderr, "unable to open module\n");
		goto done_device;
	}

	if (B0_ERR_RC(b0_pwm_output_prepare(pwm0))) {
		fprintf(stderr, "unable to prepare module\n");
		goto done_module;
	}

	unsigned int bitsize = pwm0->bitsize.values[0];

	if (B0_ERR_RC(b0_servo_open(pwm0, &servo, &config, 0, bitsize))) {
		fprintf(stderr, "Unable to allocate servo\n");
		goto done_device;
	}

	sigkill_init();
	while (!sigkill_served()) {
		for (i = config.min.angle; i < config.max.angle; i++) {
			if (!!sigkill_served()) {
				break;
			}
			if (B0_ERR_RC(b0_servo_goto(servo, i))) {
				fprintf(stderr, "\nunable to set angle (%f)\n", i);
				fflush(stderr);
				sigkill_fini();
			}

			sleep_milliseconds(VALUE_CHANGE_DELAY_MS);
		}

		for (i = config.max.angle; i > config.min.angle && !sigkill_served(); i--) {
			if (B0_ERR_RC(b0_servo_goto(servo, i))) {
				fprintf(stderr, "\nunable to set angle (%f)\n", i);
				fflush(stderr);
				sigkill_fini();
			}

			sleep_milliseconds(VALUE_CHANGE_DELAY_MS);
		}
	}

	done_servo:
	if (B0_ERR_RC(b0_servo_close(servo))) {
		fprintf(stderr, "unable to free servo\n");
	}

	done_module:
	if (B0_ERR_RC(b0_pwm_close(pwm0))) {
		fprintf(stderr, "unable to close module\n");
	}

	done_device:
	b0_device_close(dev);

	return EXIT_SUCCESS;
}
