/*
 * This file is part of libbox0.
 * Copyright (C) 2016 Kuldeep Singh Dhaka <kuldeep@madresistor.com>
 *
 * libbox0 is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * libbox0 is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with libbox0.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <stdio.h>
#include <stdlib.h>
#include <inttypes.h>
#include <libbox0/libbox0.h>
#include <libbox0/backend/usb/usb.h>
#include <libbox0/driver/dac8564/dac8564.h>
#include "../common-code/sigkill.c"

int main(int argc, char *argv[])
{
	b0_device *dev = NULL;
	b0_spi *spi0;
	b0_dac8564 *dac8564;

	if (B0_ERR_RC(b0_usb_open_supported(&dev))) {
		fprintf(stderr, "unable to open device\n");
		return EXIT_FAILURE;
	}

	if (B0_ERR_RC(b0_spi_open(dev, &spi0, 0))) {
		fprintf(stderr, "unable to open module\n");
		goto done_device;
	}

	if (B0_ERR_RC(b0_spi_master_prepare(spi0))) {
		fprintf(stderr, "unable to prepare module\n");
		goto done_module;
	}

	if (B0_ERR_RC(b0_dac8564_open(spi0, &dac8564, 0, false, false))) {
		goto done_module;
	}

	uint16_t value = (argc > 1) ? strtol(argv[1], NULL, 0) : rand();
	printf("writing 0x%"PRIx16" to channel 0\n", value);

	if (B0_ERR_RC(b0_dac8564_write(dac8564, 0x00, value))) {
		fprintf(stderr, "unable to write data");
		goto done_driver;
	}

	block_till_sigkill();

	done_driver:
	if (B0_ERR_RC(b0_dac8564_close(dac8564))) {
		fprintf(stderr, "error while freeing driver\n");
	}

	done_module:
	if (B0_ERR_RC(b0_spi_close(spi0))) {
		fprintf(stderr, "unable to close module\n");
	}

	done_device:
	b0_device_close(dev);

	return EXIT_SUCCESS;
}
