/*
 * This file is part of libbox0.
 * Copyright (C) 2013-2016 Kuldeep Singh Dhaka <kuldeep@madresistor.com>
 *
 * libbox0 is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * libbox0 is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with libbox0.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <stdio.h>
#include <stdint.h>
#include <stdlib.h>
#include <libbox0/libbox0.h>
#include <libbox0/backend/usb/usb.h>
#include <math.h>
#include "../common-code/sigkill.c"

#define SINE_WAVE_POINTS (6000)
#define SINE_WAVE_FREQ 1000

double sine_wave[SINE_WAVE_POINTS];

int main(int argc, char *argv[])
{
	b0_device *dev = NULL;
	b0_aout *aout0 = NULL;
	size_t i;

	if (B0_ERR_RC(b0_usb_open_supported(&dev))) {
		fprintf(stderr, "unable to open device\n");
		return EXIT_FAILURE;
	}

	b0_device_log(dev, B0_LOG_WARN);

	if (B0_ERR_RC(b0_aout_open(dev, &aout0, 0))) {
		fprintf(stderr, "unable to open module\n");
		goto done_device;
	}

	/* fill the buffer */
	double freq = SINE_WAVE_FREQ;

	/* Sine Wave of 1KHz 2V Peak to Peak with 2V offset  */
	for (i = 0; i < SINE_WAVE_POINTS; i++) {
		sine_wave[i] = sin(((i * freq) / 1000000.0) * 2 * M_PI) + 2;
	}

	/* prepare for realtime */
	if (B0_ERR_RC(b0_aout_snapshot_prepare(aout0))) {
		fprintf(stderr, "unable to prepare aout0 for snapshot \n");
		goto done_module;
	}

	unsigned int speed = 1000000;
	unsigned int bitsize = 12;

	if (B0_ERR_RC(b0_aout_bitsize_speed_set(aout0, bitsize, speed))) {
		fprintf(stderr, "unable to set bitsize, speed\n");
		goto done_module;
	}

	if (B0_ERR_RC(b0_aout_snapshot_start_double(aout0, sine_wave, SINE_WAVE_POINTS))) {
		fprintf(stderr, "unable to start aout0 arbitary \n");
		goto done_module;
	}

	block_till_sigkill();

	if (B0_ERR_RC(b0_aout_snapshot_stop(aout0))) {
		fprintf(stderr, "unable to stop aout0\n");
	}

	done_module:
	if (B0_ERR_RC(b0_aout_close(aout0))) {
		fprintf(stderr, "unable to close aout0\n");
	}

	done_device:
	b0_device_close(dev);

	return EXIT_SUCCESS;
}
