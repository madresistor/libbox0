/*
 * This file is part of libbox0.
 * Copyright (C) 2013-2015 Kuldeep Singh Dhaka <kuldeep@madresistor.com>
 *
 * libbox0 is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * libbox0 is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with libbox0.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef LIBBWF_H
#define LIBBWF_H

#include <math.h>
#include <stddef.h>

#ifdef __cplusplus
# define extern "C" {
#endif

/* Reference: http://gcc.gnu.org/wiki/Visibility */
#if defined(_WIN32) || defined(__CYGWIN__)
	#if defined(bwf_EXPORTS)
		#if defined(__GNUC__)
			#define BWF_API __attribute__ ((dllexport))
		#else /* defined(__GNUC__) */
			/* Note: actually gcc seems to also supports this syntax. */
			#define BWF_API __declspec(dllexport)
		#endif /* defined(__GNUC__) */
	#else /* defined(box0_EXPORTS) */
		#if defined(__GNUC__)
			#define BWF_API __attribute__ ((dllimport))
		#else
			/* Note: actually gcc seems to also supports this syntax. */
			#define BWF_API __declspec(dllimport)
		#endif
	#endif /* defined(box0_EXPORTS) */
#else /* defined(_WIN32) || defined(__CYGWIN__) */
	#if __GNUC__ >= 4
		#define BWF_API __attribute__ ((visibility ("default")))
	#else /* __GNUC__ >= 4 */
		#define BWF_API
	#endif /* __GNUC__ >= 4 */
#endif /* defined(_WIN32) || defined(__CYGWIN__) */

__BEGIN_DECLS

/*
 * all angle in radian.
 * all frequency in Hertz
 */

typedef struct bwf_param bwf_param;

struct bwf_param {
	double (* gen)(double);
	double sample_rate;
	double amplitude;
	double offset;
	double phase;
};

/*
 * all these functions return value in range -1 and +1
 */
#define bwf_sin sin
BWF_API double bwf_triangular(double);
BWF_API double bwf_sawtooth(double);
BWF_API double bwf_square(double);

/*
 * return 0 on success
 */
BWF_API int bwf_constant(struct bwf_param *param, double freq, double *arr,
							size_t len);
BWF_API int bwf_sweep(struct bwf_param *param, double start_freq,
						double end_freq, double *arr, size_t len);

#ifdef __cplusplus
# define }
#endif

#endif
