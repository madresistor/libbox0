/*
 * This file is part of libbox0.
 * Copyright (C) 2013-2015 Kuldeep Singh Dhaka <kuldeep@madresistor.com>
 *
 * libbox0 is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * libbox0 is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with libbox0.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "libbwf.h"

double bwf_triangular(double x)
{
	x = fmod(x, (2 * M_PI));

	if (x < (M_PI_2)) {
		return x / M_PI_2;
	} else if (x < (3 * M_PI_2)) {
		return 2 - (x / M_PI_2);
	} else {
		return (x / M_PI_2) - 4;
	}
}

double bwf_sawtooth(double x)
{
	x = fmod(x, (2 * M_PI));

	if (x < M_PI) {
		return x / M_PI;
	} else {
		return (x / M_PI) - 2;
	}
}

double bwf_square(double x)
{
	x = fmod(x, (2 * M_PI));

	return (x >= M_PI) ? -1 : 1;
}

int bwf_constant(struct bwf_param *param, double freq, double *arr, size_t len)
{
	size_t i;

	if (param == NULL) return -1;
	if (arr == NULL) return -1;
	if (param->gen == NULL) return -1;
	if (len == 0) return -1;
	if (isnan(param->sample_rate)) return -1;
	if (param->sample_rate <= 0) return -1;
	if (isnan(param->amplitude)) return -1;
	if (param->amplitude == 0) return -1;
	if (isnan(param->offset)) return -1;
	if (isnan(param->phase)) return -1;

	if (isnan(freq)) return -1;
	if (freq <= 0) return -1;

	/* conceptual problem */
	double nyquist_freq = param->sample_rate / 2;
	if (freq > nyquist_freq) return -2;

	double two_pi = 2.0 * M_PI;
	for (i = 0; i < len; i++) {
		double x = two_pi * freq * i;
		x = (x + param->phase) / param->sample_rate;
		arr[i] = (param->gen(x) * param->amplitude) + param->offset;
	}

	return 0;
}

int bwf_sweep(struct bwf_param *param, double freq_start, double freq_end,
				double *arr, size_t len)
{
	size_t i;

	if (param == NULL) return -1;
	if (arr == NULL) return -1;
	if (param->gen == NULL) return -1;
	if (len == 0) return -1;
	if (isnan(param->sample_rate)) return -1;
	if (param->sample_rate <= 0) return -1;
	if (isnan(param->amplitude)) return -1;
	if (param->amplitude == 0) return -1;
	if (isnan(param->offset)) return -1;
	if (isnan(param->phase)) return -1;

	if (isnan(freq_start)) return -1;
	if (freq_start <= 0) return -1;
	if (isnan(freq_end)) return -1;
	if (freq_end <= 0) return -1;

	/* conceptual problem */
	double nyquist_freq = param->sample_rate / 2;
	if (freq_start > nyquist_freq) return -2;
	if (freq_end > nyquist_freq) return -2;

	/* Ref: http://www.edn.com/electronics-news/4384064/ */
	double a = (M_PI * (freq_end - freq_start)) / len;
	double b = 2.0 * M_PI * freq_start;
	for (i = 0; i < len; i++) {
		double x = (a * i * i) + (b * i);
		x = (x + param->phase) / param->sample_rate;
		arr[i] = (param->gen(x) * param->amplitude) + param->offset;
	}

	return 0;
}
