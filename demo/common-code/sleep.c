/*
 * This file is part of libbox0.
 * Copyright (C) 2013-2015 Kuldeep Singh Dhaka <kuldeep@madresistor.com>
 *
 * libbox0 is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * libbox0 is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with libbox0.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef DEMO_COMMON_CODE_SLEEP
#define DEMO_COMMON_CODE_SLEEP

#if defined(_WIN32) || defined(__MINGW32__)
# include <windows.h>
# define sleep_seconds(x) Sleep(x * 1000)
# define sleep_milliseconds(x) Sleep(x)
#else /* defined(__MINGW32__) */
# include <unistd.h>
# define sleep_seconds(x) sleep(x)
# define sleep_milliseconds(x) usleep(x * 1000)
#endif

#endif
