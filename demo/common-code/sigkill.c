/*
 * This file is part of libbox0.
 * Copyright (C) 2013-2015 Kuldeep Singh Dhaka <kuldeep@madresistor.com>
 *
 * libbox0 is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * libbox0 is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with libbox0.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "sleep.c"

#ifndef DEMO_COMMON_CODE_SIGKILL
#define DEMO_COMMON_CODE_SIGKILL

#include <stdio.h>
#include <stdlib.h>
#include <signal.h>

volatile bool do_exit = false;
void sigkill_init(void);
bool sigkill_served(void);
void sigkill_fini(void);

#if defined(_WIN32) || defined(__MINGW32__)

/* WINDOWS */

#define WIN32_LEAN_AND_MEAN
#include <windows.h>
/* http://stackoverflow.com/questions/16826097 */

BOOL WINAPI ConsoleHandler(DWORD dwType)
{
	switch(dwType) {
		case CTRL_C_EVENT:
		fprintf(stderr, "CTRL-C pressed\n");
		do_exit = true;
	break;
	case CTRL_BREAK_EVENT:
		fprintf(stderr, "BREAK pressed\n");
		do_exit = true;
	break;
	default:
		fprintf(stderr, "Some other event\n");
	}

	return TRUE;
}

void _sigkill_init(void)
{
	if (!SetConsoleCtrlHandler((PHANDLER_ROUTINE)ConsoleHandler, true)) {
		fprintf(stderr, "Unable to install handler!\n");
		exit(EXIT_FAILURE);
	}
}

#else

/* POSIX */

// This will catch user initiated CTRL+C type events and allow the program to exit
void sighandler(int signum)
{
	fprintf(stderr, "Signal Handler Called (%i)\n\n", signum);
	do_exit = true;
}

void _sigkill_init(void)
{
	struct sigaction sigact;

	// Define signal handler to catch system generated signals
	// (If user hits CTRL+C, this will deal with it.)
	sigact.sa_handler = sighandler;  // sighandler is defined below. It just sets do_exit.
	sigemptyset(&sigact.sa_mask);
	sigact.sa_flags = 0;
	sigaction(SIGINT, &sigact, NULL);
	sigaction(SIGTERM, &sigact, NULL);
	sigaction(SIGQUIT, &sigact, NULL);
}

#endif

bool sigkill_served(void)
{
	return do_exit;
}

void sigkill_fini(void)
{
	do_exit = true;
}

void sigkill_init()
{
	_sigkill_init();

#if defined(_WIN32) || defined(__MINGW32__)
	printf("\nPress CTRL-C or BREAK to continue...\n");
#else
	printf("\nPress CTRL-C to continue...\n");
#endif
}

/* this will block till CTRL-C is not pressed */
void block_till_sigkill(void)
{
	sigkill_init();
	while (!sigkill_served()) {
		sleep_milliseconds(500);
	}
	sigkill_fini();
}

#endif
