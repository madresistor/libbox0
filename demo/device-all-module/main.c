/*
 * This file is part of libbox0.
 * Copyright (C) 2013-2016 Kuldeep Singh Dhaka <kuldeep@madresistor.com>
 *
 * libbox0 is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * libbox0 is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with libbox0.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <libbox0/libbox0.h>
#include <libbox0/backend/usb/usb.h>
#include <stdio.h>
#include <stdlib.h>
#include <inttypes.h>

int main(int argc, char * argv[])
{
	b0_device *dev = NULL;
	size_t i;

	if (B0_ERR_RC(b0_usb_open_supported(&dev))) {
		fprintf(stderr, "unable to open device\n");
		return EXIT_FAILURE;
	}

	for (i = 0; i < dev->modules_len; i++) {
		b0_module *mod = dev->modules[i];
		const char *type = NULL;
		switch(mod->type) {
		case B0_DIO:
			type = "DIO";
		break;
		case B0_AOUT:
			type = "AOUT";
		break;
		case B0_AIN:
			type = "AIN";
		break;
		case B0_SPI:
			type = "SPI";
		break;
		case B0_I2C:
			type = "I2C";
		break;
		case B0_PWM:
			type = "PWM";
		break;
		}

		if(type != NULL) {
			printf("we have: '%s' %s (%u)\n", mod->name, type, mod->index);
		} else {
			printf("we have: '%s' %"PRIx8" (%u)\n", mod->name, mod->type, mod->index);
		}
	}

	/* free the device */
	b0_device_close(dev);

	return EXIT_SUCCESS;
}
