/*
 * This file is part of libbox0.
 * Copyright (C) 2013-2016 Kuldeep Singh Dhaka <kuldeep@madresistor.com>
 *
 * libbox0 is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * libbox0 is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with libbox0.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <stdio.h>
#include <stdlib.h>
#include <libbox0/libbox0.h>
#include <libbox0/backend/usb/usb.h>
#include <inttypes.h>
#include "../common-code/sigkill.c"

#define CHAN_SEQ_COUNT 5
unsigned int ch_seq[CHAN_SEQ_COUNT] = {0, 1, 2, 3, 4};

int main(int argc, char *argv[])
{
	b0_device *dev = NULL;
	b0_ain *ain0 = NULL;
	size_t i;

	if (B0_ERR_RC(b0_usb_open_supported(&dev))) {
		fprintf(stderr, "unable to open device\n");
		return EXIT_FAILURE;
	}

	b0_device_log(dev, B0_LOG_WARN);

	if (B0_ERR_RC(b0_ain_open(dev, &ain0, 0))) {
		fprintf(stderr, "unable to open module\n");
		goto done_device;
	}

	if (ain0->stream.count == 0) {
		fprintf(stderr, "no stream speed\n");
		goto done_device;
	}

	/* update in 100ms window */

	if (B0_ERR_RC(b0_ain_stream_prepare(ain0))){
		fprintf(stderr, "unable to prepare stream ain0\n");
		goto done_module;
	}

	/* channel sequence */
	if (B0_ERR_RC(b0_ain_chan_seq_set(ain0, ch_seq, CHAN_SEQ_COUNT))){
		fprintf(stderr, "unable to set channel sequence\n");
		goto done_module;
	}

	unsigned int bitsize = 12;
	unsigned long speed = 10000;

	if (B0_ERR_RC(b0_ain_bitsize_speed_set(ain0, bitsize, speed))) {
		fprintf(stderr, "unable to set ain bitsize speed\n");
		goto done_device;
	}

	if (B0_ERR_RC(b0_ain_stream_start(ain0))) {
		fprintf(stderr, "unable to start ain0\n");
		goto done_device;
	}

	size_t read_count = speed / 10;
	read_count -= read_count % sizeof(ch_seq);
	double *data = (double *) malloc(read_count * sizeof(double));

	sigkill_init();
	size_t j = 0;
	while (!sigkill_served()) {
		/* read from ain */
		if (B0_ERR_RC(b0_ain_stream_read_double(ain0, data, read_count, NULL))) {
			fprintf(stderr, "unable to read from ain\n");
			break;
		}

		/* print data */
		size_t i;
		for (i = 0; i < read_count; i++) {
			printf("%.3f ", data[i]);

			j++;
			if ((j % sizeof(ch_seq)) == 0) {
				putchar('\n');
			}
		}
	}
	sigkill_fini();

	if (B0_ERR_RC(b0_ain_stream_stop(ain0))) {
		fprintf(stderr, "unable to stop ain0\n");
		goto done_device;
	}

	done_module:
	if (B0_ERR_RC(b0_ain_close(ain0))) {
		fprintf(stderr, "unable to close module\n");
	}

	done_device:
	b0_device_close(dev);

	return EXIT_SUCCESS;
}
