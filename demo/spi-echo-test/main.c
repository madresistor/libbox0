/*
 * This file is part of libbox0.
 * Copyright (C) 2013-2016 Kuldeep Singh Dhaka <kuldeep@madresistor.com>
 *
 * libbox0 is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * libbox0 is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with libbox0.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <stdio.h>
#include <stdio.h>
#include <libbox0/libbox0.h>
#include <libbox0/backend/usb/usb.h>
#include <inttypes.h>
#include <stdlib.h>

int main(int argc, char *argv[])
{
	b0_device *dev = NULL;
	b0_spi *spi0;

	if (B0_ERR_RC(b0_usb_open_supported(&dev))) {
		fprintf(stderr, "unable to open device\n");
		return EXIT_FAILURE;
	}

	if (B0_ERR_RC(b0_spi_open(dev, &spi0, 0))) {
		fprintf(stderr, "unable to open module\n");
		goto done_device;
	}

	uint8_t in = 0x78 /* deterministic value */, out = 0x9A;

	const b0_spi_task tasks[] = {{
		.addr = 0,
		.flags = B0_SPI_TASK_MODE0 | B0_SPI_TASK_FD |
				B0_SPI_TASK_MSB_FIRST | B0_SPI_TASK_LAST,
		.bitsize = 8,
		.count = 1,
		.speed = B0_SPI_TASK_SPEED_USE_FALLBACK,
		.wdata = &out,
		.rdata = &in
	}};

	if (argc > 1) {
		out = strtol(argv[1], NULL, 0);
	}

	if (B0_ERR_RC(b0_spi_master_prepare(spi0))) {
		fprintf(stderr, "unable to prepare module\n");
		goto done_module;
	}

	if (B0_ERR_RC(b0_spi_master_start(spi0, tasks, NULL, NULL))) {
		fprintf(stderr, "unable to execute transaction\n");
		goto done_module;
	}

	fprintf(stdout, "OUT: 0x%"PRIx8"\n", out);
	fprintf(stdout, "IN: 0x%"PRIx8"\n", in);
	fprintf(stdout, "Operation: %s\n", (in == out) ?
								"Successful" : "Failed");
	fflush(stdout);

	done_module:
	if (B0_ERR_RC(b0_spi_close(spi0))) {
		fprintf(stderr, "unable to close module\n");
	}

	done_device:
	b0_device_close(dev);

	return EXIT_SUCCESS;
}
