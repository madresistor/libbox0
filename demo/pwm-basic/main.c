/*
 * This file is part of libbox0.
 * Copyright (C) 2013-2016 Kuldeep Singh Dhaka <kuldeep@madresistor.com>
 *
 * libbox0 is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * libbox0 is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with libbox0.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <stdio.h>
#include <stdlib.h>
#include <libbox0/libbox0.h>
#include <libbox0/backend/usb/usb.h>
#include "../common-code/sigkill.c"

/* pwm-basic [FREQUENCY [DUTY_CYCLE]] */

int main(int argc, char *argv[])
{
	b0_device *dev = NULL;
	b0_pwm *pwm0 = NULL;

	if (B0_ERR_RC(b0_usb_open_supported(&dev))) {
		fprintf(stderr, "unable to open device\n");
		return EXIT_FAILURE;
	}

	if (B0_ERR_RC(b0_pwm_open(dev, &pwm0, 0))) {
		fprintf(stderr, "unable to open module\n");
		goto done_device;
	}

	if (B0_ERR_RC(b0_pwm_output_prepare(pwm0))) {
		fprintf(stderr, "unable to prepare module\n");
		goto done_module;
	}

	double value = 1000, duty_cycle = 50;
	if (argc > 1) value = atof(argv[1]);
	if (argc > 2) duty_cycle = atof(argv[2]);
	if (value <= 0) {
		fprintf(stderr, "get yourself some refreshment!\n");
		goto done_module;
	}

	if (duty_cycle <= 0 || duty_cycle >= 100) {
		fprintf(stderr, "get yourself some refreshment!\n");
		goto done_module;
	}

	unsigned int bitsize = pwm0->bitsize.values[0];
	unsigned long speed;
	b0_pwm_reg period;
	if (B0_ERR_RC(b0_pwm_output_calc(pwm0, bitsize, value, &speed, &period, 0.1, true))) {
		fprintf(stderr, "unable to perform calculation\n");
		goto done_module;
	}

	if (B0_ERR_RC(b0_pwm_speed_set(pwm0, speed))) {
		fprintf(stderr, "unable to set speed\n");
		goto done_module;
	}

	if (B0_ERR_RC(b0_pwm_period_set(pwm0, period))) {
		fprintf(stderr, "unable to set period\n");
		goto done_module;
	}

	b0_pwm_reg width = B0_PWM_OUTPUT_CALC_WIDTH(period, duty_cycle);
	if (B0_ERR_RC(b0_pwm_width_set(pwm0, 0, width))) {
		fprintf(stderr, "unable to set width\n");
		goto done_module;
	}

	if (B0_ERR_RC(b0_pwm_output_start(pwm0))) {
		fprintf(stderr, "unable to start pwm\n");
		goto done_module;
	}

	fprintf(stderr, "pwm running at freq = %f, duty cycle = %f\n", value, duty_cycle);

	block_till_sigkill();

	if (B0_ERR_RC(b0_pwm_output_stop(pwm0))) {
		fprintf(stderr, "unable to stop pwm\n");
	}

	done_module:
	if (B0_ERR_RC(b0_pwm_close(pwm0))) {
		fprintf(stderr, "unable to close module\n");
	}

	done_device:
	b0_device_close(dev);

	return EXIT_SUCCESS;
}
