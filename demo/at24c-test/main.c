/*
 * This file is part of libbox0.
 * Copyright (C) 2013-2016 Kuldeep Singh Dhaka <kuldeep@madresistor.com>
 *
 * libbox0 is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * libbox0 is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with libbox0.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <stdio.h>
#include <inttypes.h>
#include <stdlib.h>
#include <libbox0/libbox0.h>
#include <libbox0/backend/usb/usb.h>
#include <libbox0/driver/at24c/at24c.h>
#include "../common-code/sleep.c"

int main(int argc, char *argv[])
{
	b0_device *dev;
	b0_i2c *i2c0 = NULL;
	b0_at24c *at24c;

	if (B0_ERR_RC(b0_usb_open_supported(&dev))) {
		fprintf(stderr, "unable to open device\n");
		return EXIT_FAILURE;
	}

	if (B0_ERR_RC(b0_i2c_open(dev, &i2c0, 0))) {
		fprintf(stderr, "unable to open module\n");
		goto done_device;
	}

	if (B0_ERR_RC(b0_i2c_master_prepare(i2c0))) {
		fprintf(stderr, "unable to prepare module\n");
		goto done_module;
	}

	if (B0_ERR_RC(b0_at24c_open(i2c0, &at24c, false, false, false))) {
		fprintf(stderr, "Unable to allocate at24c\n");
		goto done_device;
	}

	uint8_t word_address = 0x2, data = 0xAA;

	if (argc > 1) {
		word_address = atoi(argv[1]);
	}

	if (argc > 2) {
		data = atoi(argv[2]);
	}

	fprintf(stdout, "writing 0x%"PRIx8" at 0x%"PRIx8"\n", data, word_address);

	if (B0_ERR_RC(b0_at24c_write(at24c, word_address, &data, 1))) {
		fprintf(stderr, "writing failed");
	}

	/* sleep for 5ms [Write Cycle Time for AT24C01C] */
	sleep_milliseconds(5);

	uint8_t tmp;
	if (B0_ERR_RC(b0_at24c_read(at24c, word_address, &tmp, 1))) {
		fprintf(stderr, "reading failed\n");
	} else if (tmp != data) {
		fprintf(stderr, "data didnt match!\n");
	} else {
		fprintf(stdout, "success!!!\n");
	}

	done_at24c:
	if (B0_ERR_RC(b0_at24c_close(at24c))) {
		fprintf(stderr, "unable to free at24c\n");
	}

	done_module:
	if (B0_ERR_RC(b0_i2c_close(i2c0))) {
		fprintf(stderr, "unable to close module\n");
	}

	done_device:
	b0_device_close(dev);

	return EXIT_SUCCESS;
}
