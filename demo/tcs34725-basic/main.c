/*
 * This file is part of libbox0.
 * Copyright (C) 2015, 2016 Kuldeep Singh Dhaka <kuldeep@madresistor.com>
 *
 * libbox0 is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * libbox0 is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with libbox0.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <stdio.h>
#include <inttypes.h>
#include <libbox0/libbox0.h>
#include <libbox0/backend/usb/usb.h>
#include <libbox0/driver/tcs34725/tcs34725.h>
#include "../common-code/sigkill.c"
#include "../common-code/sleep.c"

int main(int argc, char *argv[])
{
	b0_device *dev = NULL;
	b0_i2c *i2c0 = NULL;
	b0_tcs34725 *tcs34725 = NULL;
	double red, green, blue, clear;
	uint8_t atime;

	if (B0_ERR_RC(b0_usb_open_supported(&dev))) {
		fprintf(stderr, "unable to open device\n");
		return EXIT_FAILURE;
	}

	if (B0_ERR_RC(b0_i2c_open(dev, &i2c0, 0))) {
		fprintf(stderr, "unable to open module\n");
		goto done_device;
	}

	if (B0_ERR_RC(b0_i2c_master_prepare(i2c0))) {
		fprintf(stderr, "unable to prepare module\n");
		goto done_module;
	}

	if (B0_ERR_RC(b0_tcs34725_open(i2c0, &tcs34725))) {
		fprintf(stderr, "Unable to alloca tcs34725\n");
		goto done_device;
	}

	atime = B0_TCS34725_ATIME_FROM_INTEG_TIME(750);
	if (B0_ERR_RC(b0_tcs34725_atime_set(tcs34725, atime))) {
		fprintf(stderr, "Unable to set more ATIME\n");
		goto done_tcs34725;
	}

	sigkill_init();
	printf("TCS34725 [red, green, blue, clear]: \n");
	while (!sigkill_served()) {
		if (B0_ERR_RC(b0_tcs34725_read(tcs34725, &red, &green, &blue, &clear))) {
			fprintf(stderr, "\nunable to read\n");
			break;
		}

		printf("%f, %f, %f, %f\n", red, green, blue, clear);
		fflush(stdout);

		sleep_seconds(1);
	}
	printf("\n");

	done_tcs34725:
	if (B0_ERR_RC(b0_tcs34725_close(tcs34725))) {
		fprintf(stderr, "unable to free tcs34725\n");
	}

	done_module:
	if (B0_ERR_RC(b0_i2c_close(i2c0))) {
		fprintf(stderr, "unable to close module\n");
	}

	done_device:
	b0_device_close(dev);

	return EXIT_SUCCESS;
}
