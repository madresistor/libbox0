/*
 * This file is part of libbox0.
 * Copyright (C) 2013-2016 Kuldeep Singh Dhaka <kuldeep@madresistor.com>
 *
 * libbox0 is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * libbox0 is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with libbox0.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <stdio.h>
#include <stdint.h>
#include <stdlib.h>
#include <libbox0/libbox0.h>
#include <libbox0/backend/usb/usb.h>
#include <math.h>
#include "../common-code/libbwf/libbwf.c"
#include "../common-code/sigkill.c"
#include "../common-code/sleep.c"

/* USAGE: START_FREQ END_FREQ */

int main(int argc, char *argv[])
{
	b0_device *dev;
	b0_aout *aout0;

	size_t i;

	/* some waveform parameters */
	double freq_start = 1500;
	double freq_end = 10;
	double offset = 0;
	double amplitude = 1;
	double phase = 0;
	double (* func)(double) = bwf_sin;
	unsigned int duration /* sec */ = 10;

	if (argc > 1) {
		freq_start = atof(argv[1]);
	}

	if (argc > 2) {
		freq_end = atof(argv[2]);
	}

	if (!(freq_start > 0)) {
		fprintf(stderr, "got get some sleep!\n");
		return EXIT_FAILURE;
	}

	if (B0_ERR_RC(b0_usb_open_supported(&dev))) {
		fprintf(stderr, "unable to open device\n");
		return EXIT_FAILURE;
	}

	if (B0_ERR_RC(b0_aout_open(dev, &aout0, 0))) {
		fprintf(stderr, "unable to open module\n");
		goto done_device;
	}

	unsigned int bitsize = 12;
	unsigned long speed = 10000;

	/* prepare for realtime */
	size_t points = duration * speed;
	/* TODO: free wave and test for null */
	double *wave = malloc(points * sizeof(double));

	struct bwf_param param = {
		.gen = func,
		.sample_rate = speed,
		.amplitude = amplitude,
		.offset = offset,
		.phase = phase,
	};

	if (freq_end <= 0) {
		printf("preparing constant freq (%g Hz)\n", freq_start);
		if (bwf_constant(&param, freq_start, wave, points) < 0) {
			fprintf(stderr, "parameter error why??");
		}
	} else {
		printf("preparing sweep (%g hz to %g Hz)\n", freq_start, freq_end);
		if (bwf_sweep(&param, freq_start, freq_end, wave, points) < 0) {
			fprintf(stderr, "parameter error why??");
		}
	}

	if (B0_ERR_RC(b0_aout_stream_prepare(aout0))) {
		fprintf(stderr, "unable to prepare aout0 for realtime \n");
		goto done_module;
	}

	if (B0_ERR_RC(b0_aout_bitsize_speed_set(aout0, bitsize, speed))) {
		fprintf(stderr, "unable to set aout bitsize speed\n");
		goto done_device;
	}

	if (B0_ERR_RC(b0_aout_stream_start(aout0))) {
		fprintf(stderr, "unable to start aout0 arbitary \n");
		goto done_module;
	}

	if (B0_ERR_RC(b0_aout_stream_write_double(aout0, wave, points))) {
		fprintf(stderr, "unable to write aout0\n");
		goto done_module;
	}

	/* aout0 */
	printf("Running at %lu s/s\n", speed);
	fflush(stdout);
	fflush(stderr);

	sigkill_init();

	i = 0;
	while (!sigkill_served()) {
		i += 1;

		if (i > (duration * 10)) {
			sigkill_fini();
		} else {
			sleep_milliseconds(100);
		}

		fflush(stdout);
		fflush(stderr);
	}

	if (B0_ERR_RC(b0_aout_stream_stop(aout0))) {
		fprintf(stderr, "unable to stop aout0\n");
	}

	done_module:
	if (B0_ERR_RC(b0_aout_close(aout0))) {
		fprintf(stderr, "unable to close aout0\n");
	}

	done_device:
	if (B0_ERR_RC(b0_device_close(dev))) {
		fprintf(stderr, "unable to close device\n");
	}

	return EXIT_SUCCESS;
}
