/*
 * This file is part of libbox0.
 * Copyright (C) 2013-2016 Kuldeep Singh Dhaka <kuldeep@madresistor.com>
 *
 * libbox0 is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * libbox0 is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with libbox0.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <stdio.h>
#include <math.h>
#include <stdlib.h>
#include <libbox0/libbox0.h>
#include <libbox0/backend/usb/usb.h>
#include "../common-code/sigkill.c"
#include "../common-code/sleep.c"

/* Note: fixed 10Khz filter
 *
 *
 * you can generate the a 100Hz - 5Khz frequency from PWM to see it being filtered
 */

/* Digital filter designed by mkfilter/mkshape/gencode   A.J. Fisher
   Command line: /www/usr/fisher/helpers/mkfilter -Bu -Lp -o 8 -a 1.0416666667e-01 0.0000000000e+00 -l */

/*
 * http://www-users.cs.york.ac.uk/~fisher/mkfilter/
 *
 * filtertype 	= 	Butterworth
 * passtype 	= 	Lowpass
 * ripple 	=
 * order 	= 	8
 * samplerate 	= 	96000
 * corner1 	= 	10000
 * corner2 	=
 * adzero 	=
 * logmin 	=
 */

#define NZEROS 8
#define NPOLES 8
#define GAIN   3.159834166e+04

static double xv[NZEROS+1], yv[NPOLES+1];

static void butterworth_filter(double *data, size_t size)
{
	size_t i;
	for (i = 0; i < size; i++) {
		xv[0] = xv[1]; xv[1] = xv[2]; xv[2] = xv[3]; xv[3] = xv[4];
		xv[4] = xv[5]; xv[5] = xv[6]; xv[6] = xv[7]; xv[7] = xv[8];

		xv[8] = data[i] / GAIN;

		yv[0] = yv[1]; yv[1] = yv[2]; yv[2] = yv[3]; yv[3] = yv[4];
		yv[4] = yv[5]; yv[5] = yv[6]; yv[6] = yv[7]; yv[7] = yv[8];

		yv[8] =   (xv[0] + xv[8]) + 8 * (xv[1] + xv[7]) + 28 * (xv[2] + xv[6])
					+ 56 * (xv[3] + xv[5]) + 70 * xv[4]
					+ ( -0.0322233912 * yv[0]) + (  0.3657048465 * yv[1])
					+ ( -1.8462454726 * yv[2]) + (  5.4264052066 * yv[3])
					+ (-10.1817555660 * yv[4]) + ( 12.5300087200 * yv[5])
					+ ( -9.9209459984 * yv[6]) + (  4.6509499638 * yv[7]);

		data[i] = yv[8];

		/* Why This?
		 * to make overshoot visible on oscilloscope. :~) */
		data[i] /= 2;
		data[i] += .5;
	}
}

int main(int argc, char *argv[])
{
	b0_device *dev = NULL;
	b0_ain *ain0 = NULL;
	b0_aout *aout0 = NULL;

	if (B0_ERR_RC(b0_usb_open_supported(&dev))) {
		fprintf(stderr, "unable to open device\n");
		return EXIT_FAILURE;
	}

	if (B0_ERR_RC(b0_ain_open(dev, &ain0, 0))) {
		fprintf(stderr, "unable to get ain0");
		goto done_device;
	}

	if (B0_ERR_RC(b0_aout_open(dev, &aout0, 0))) {
		fprintf(stderr, "unable to get aout0");
		goto done_ain;
	}

	if (B0_ERR_RC(b0_ain_stream_prepare(ain0)) ||
		B0_ERR_RC(b0_aout_stream_prepare(aout0))) {
		fprintf(stderr, "unable to prepare some module for stream\n");
		goto done_device;
	}

	unsigned int bitsize = 12;
	unsigned long speed = 10000;

	if (B0_ERR_RC(b0_ain_bitsize_speed_set(ain0, bitsize, speed))) {
		fprintf(stderr, "unable to set ain bitsize speed\n");
		goto done_device;
	}

	if (B0_ERR_RC(b0_aout_bitsize_speed_set(aout0, bitsize, speed))) {
		fprintf(stderr, "unable to set aout bitsize speed\n");
		goto done_device;
	}

	if (B0_ERR_RC(b0_ain_stream_start(ain0))) {
		fprintf(stderr, "unable to start ain for stream\n");
		goto done_device;
	}

	/* wait for some to let some data acumulate */
	sleep_milliseconds(20);

	if (B0_ERR_RC(b0_aout_stream_start(aout0))) {
		fprintf(stderr, "unable to start ain for stream\n");
		goto done_device;
	}

	size_t read_count = speed / 100;
	void *data = malloc(read_count * sizeof(double));

	sigkill_init();
	while (!sigkill_served()) {
		/* read from ain */
		if (B0_ERR_RC(b0_ain_stream_read_double(ain0, data, read_count, NULL))) {
			fprintf(stderr, "unable to read from ain\n");
			break;
		}

		/* perform filtering */
		butterworth_filter(data, read_count);

		/* write to aout */
		if (B0_ERR_RC(b0_aout_stream_write_double(aout0, data, read_count))) {
			fprintf(stderr, "unable to write to aout\n");
			break;
		}
	}
	sigkill_fini();

	if (B0_ERR_RC(b0_ain_stream_stop(ain0))) {
		fprintf(stderr, "unable to stop ain module for stream\n");
	}

	if (B0_ERR_RC(b0_aout_stream_stop(aout0))) {
		fprintf(stderr, "unable to stop aout module for stream\n");
	}

	done_module:

	done_aout:
	if (B0_ERR_RC(b0_aout_close(aout0))) {
		fprintf(stderr, "unable to close aout0\n");
	}

	done_ain:
	if (B0_ERR_RC(b0_ain_close(ain0))) {
		fprintf(stderr, "unable to close ain0\n");
	}

	done_device:
	b0_device_close(dev);

	return EXIT_SUCCESS;
}
