/*
 * This file is part of libbox0.
 * Copyright (C) 2013-2016 Kuldeep Singh Dhaka <kuldeep@madresistor.com>
 *
 * libbox0 is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * libbox0 is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with libbox0.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <stdio.h>
#include <libbox0/libbox0.h>
#include <libbox0/backend/usb/usb.h>
#include <libbox0/driver/lm75/lm75.h>
#include <stdint.h>
#include <stdlib.h>
#include <inttypes.h>

int main(int argc, char *argv[])
{
	b0_device *dev = NULL;
	b0_i2c *i2c0 = NULL;
	if (B0_ERR_RC(b0_usb_open_supported(&dev))) {
		fprintf(stderr, "unable to open device\n");
		return EXIT_FAILURE;
	}

	if (B0_ERR_RC(b0_i2c_open(dev, &i2c0, 0))) {
		fprintf(stderr, "unable to open module\n");
		goto done_device;
	}

	if (B0_ERR_RC(b0_i2c_master_prepare(i2c0))) {
		fprintf(stderr, "unable to prepare module\n");
		goto done_module;
	}

	/* TODO: 10 bit slave detect */

	if (argc > 1) {
		int i;
		uint8_t slave_addr;
		bool slave_present;

		for (i = 1; i < argc; i++) {
			slave_addr = strtol(argv[i], NULL, 0);

			const b0_i2c_sugar_arg arg = {
				.addr = slave_addr,
				.version = B0_I2C_VERSION_SM
			};

			if (B0_ERR_RC(b0_i2c_master_slave_detect(i2c0, &arg, &slave_present))) {
				fprintf(stderr, "unable to query slave status (0x%"PRIx8")\n", slave_addr);
			} else {
				printf("slave (0x%"PRIx8") detected: %s\n", slave_addr,
					slave_present ? "Yes" : "No");
			}
		}
	} else {
		unsigned int count = 0;
		uint8_t slave_addr;
		bool slave_present;

		for (slave_addr = 0b0001000; slave_addr < 0b1111000; slave_addr++) {
			const b0_i2c_sugar_arg arg = {
				.addr = slave_addr,
				.version = B0_I2C_VERSION_SM
			};

			if (B0_ERR_RC(b0_i2c_master_slave_detect(i2c0, &arg, &slave_present))) {
				fprintf(stderr, "unable to query slave status (0x%"PRIx8")\n", slave_addr);
			} else if (slave_present) {
				printf("Slave At: 0x%"PRIx8"\n", slave_addr);
				/* TODO: print some more info on slave */
				count++;
			}
		}

		if (count == 0) {
			printf("Nothing found, go check the connections.\n");
		}
	}

	done_module:
	if (B0_ERR_RC(b0_i2c_close(i2c0))) {
		fprintf(stderr, "unable to close module\n");
	}

	done_device:
	b0_device_close(dev);

	return EXIT_SUCCESS;
}
