/*
 * This file is part of libbox0.
 * Copyright (C) 2013-2016 Kuldeep Singh Dhaka <kuldeep@madresistor.com>
 *
 * libbox0 is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * libbox0 is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with libbox0.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <stdio.h>
#include <stdlib.h>
#include <libbox0/libbox0.h>
#include <libbox0/backend/usb/usb.h>
#include "../common-code/sigkill.c"
#include "../common-code/sleep.c"

int main(int argc, char *argv[])
{
	b0_device *dev = NULL;
	b0_dio *dio0 = NULL;

	if (B0_ERR_RC(b0_usb_open_supported(&dev))) {
		fprintf(stderr, "unable to open device\n");
		return EXIT_FAILURE;
	}

	if (B0_ERR_RC(b0_dio_open(dev, &dio0, 0))) {
		fprintf(stderr, "unable to load module\n");
		goto done_device;
	}

	if (B0_ERR_RC(b0_dio_basic_prepare(dio0))) {
		fprintf(stderr, "unable to prepare module\n");
		goto done_module;
	}

	if (B0_ERR_RC(b0_dio_dir_set(dio0, 0, B0_DIO_OUTPUT))) {
		fprintf(stderr, "unable to configure pin0 to output\n");
		goto done_module;
	}

	if (B0_ERR_RC(b0_dio_value_set(dio0, 0, B0_DIO_LOW))) {
		fprintf(stderr, "unable to set pin0 low\n");
		goto done_module;
	}

	if (B0_ERR_RC(b0_dio_hiz_set(dio0, 0, B0_DIO_DISABLE))) {
		fprintf(stderr, "unable to disable pin0 hiz\n");
		goto done_module;
	}

	sigkill_init();
	while (!sigkill_served()) {
		if (B0_ERR_RC(b0_dio_value_toggle(dio0, 0))) {
			fprintf(stderr, "unable to toggle pin\n");
			break;
		}

		/* toggle at 1Hz */
		sleep_seconds(1);
	}

	done_module:
	if (B0_ERR_RC(b0_dio_close(dio0))) {
		fprintf(stderr, "unable to close module\n");
	}

	/* free the device */
	done_device:
	b0_device_close(dev);

	return EXIT_SUCCESS;
}
