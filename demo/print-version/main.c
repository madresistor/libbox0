/*
 * This file is part of libbox0.
 * Copyright (C) 2015 Kuldeep Singh Dhaka <kuldeep@madresistor.com>
 *
 * libbox0 is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * libbox0 is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with libbox0.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <inttypes.h>
#include <libbox0/libbox0.h>

int main(int argc, char *argv[])
{
	b0_version ver;
	uint32_t code = b0_version_extract(&ver);

	printf("Code: %"PRIx32"\n", code);
	printf("Major: %"PRIu8"\n", ver.major);
	printf("Minor: %"PRIu8"\n", ver.minor);
	printf("Patch: %"PRIu8"\n", ver.patch);
	printf("Version: %"PRIu8".%"PRIu8".%"PRIu8"\n",
		ver.major, ver.minor, ver.patch);

	return EXIT_SUCCESS;
}
