/*
 * This file is part of libbox0.
 * Copyright (C) 2013-2015 Kuldeep Singh Dhaka <kuldeep@madresistor.com>
 *
 * libbox0 is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * libbox0 is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with libbox0.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <libbox0/libbox0.h>
#include <libbox0/backend/usb/usb.h>
#include <stdio.h>
#include <stdlib.h>

int main(int argc, char * argv[])
{
	b0_device *dev = NULL;

	if (B0_ERR_RC(b0_usb_open_supported(&dev))) {
		fprintf(stderr, "unable to open device\n");
		return EXIT_FAILURE;
	}

	b0_device_log(dev, B0_LOG_WARN);

	printf("ping\n");
	if (B0_ERR_RC(b0_device_ping(dev))) {
		fprintf(stderr, "ping failed\n");
	} else {
		printf("pong\n");
	}

	/* free the device */
	b0_device_close(dev);

	return EXIT_SUCCESS;
}
