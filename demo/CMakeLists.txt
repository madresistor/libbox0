#
# This file is part of libbox0.
# Copyright (C) 2013-2016 Kuldeep Singh Dhaka <kuldeep@madresistor.com>
#
# libbox0 is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# libbox0 is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with libbox0.  If not, see <http://www.gnu.org/licenses/>.
#

#~ IF(CMAKE_COMPILER_IS_GNUCC)
	#~ SET(CMAKE_C_FLAGS "-Wmissing-prototypes -Wstrict-prototypes ${CMAKE_C_FLAGS}")
	#~ SET(CMAKE_C_FLAGS "-Wall -Wwrite-strings -Wsign-compare ${CMAKE_C_FLAGS}")
	#~ SET(CMAKE_C_FLAGS "${CMAKE_C_FLAGS} -Wextra -Wno-empty-body")
	#~ SET(CMAKE_C_FLAGS "${CMAKE_C_FLAGS} -Wformat-security -Wno-unused")
	#~ SET(CMAKE_C_FLAGS "${CMAKE_C_FLAGS} -Wshadow -D_FORTIFY_SOURCE=2 -Werror")
	#~ SET(CMAKE_C_FLAGS "${CMAKE_C_FLAGS} -Ofast -funroll-loops -flto -g3")
#~ ENDIF(CMAKE_COMPILER_IS_GNUCC)

FUNCTION (EASY_DEMO name)
	LINK_DIRECTORIES(${BOX0_BINARY_DIR}/libbox0)
	INCLUDE_DIRECTORIES(${PROJECT_SOURCE_DIR})
	ADD_EXECUTABLE ("demo-${name}" "${name}/main.c")
	TARGET_LINK_LIBRARIES ("demo-${name}" box0)
ENDFUNCTION()

EASY_DEMO(ain-stream-slowest-print)
EASY_DEMO(device-open)
EASY_DEMO(pwm-basic)
EASY_DEMO(ain-stream-basic)
EASY_DEMO(aout-stream-basic)
EASY_DEMO(aout-snapshot-basic)
EASY_DEMO(aout-snapshot-fft-test)
EASY_DEMO(dio-basic)
EASY_DEMO(servo-basic)
EASY_DEMO(adxl345-basic-print)
EASY_DEMO(i2c-detect-slaves)
EASY_DEMO(ain-snapshot-print)
EASY_DEMO(l3gd20-basic)
EASY_DEMO(lm75-basic)
EASY_DEMO(spi-echo-test)
EASY_DEMO(pcf8814-hello-world)
EASY_DEMO(74hc595-basic)
#EASY_DEMO(ad9833-basic)
EASY_DEMO(max31855-test)
EASY_DEMO(voltmeter)
EASY_DEMO(ain-aout-redirect)
EASY_DEMO(ain-aout-low-pass-filter-first-order)
EASY_DEMO(ain-aout-low-pass-filter-butterworth)
EASY_DEMO(at24c-test)
EASY_DEMO(h-bridge-test)
EASY_DEMO(spi-half-duplex-write-test)
EASY_DEMO(pcf8814-typing)
EASY_DEMO(tmp006-test)
EASY_DEMO(bmp180-test)
EASY_DEMO(tsl2591-basic)
EASY_DEMO(si1145-basic)
EASY_DEMO(mcp342x-basic)
EASY_DEMO(device-ping)
EASY_DEMO(device-all-module)
EASY_DEMO(print-version)
EASY_DEMO(tcs34725-basic)
EASY_DEMO(dac8564-test)
EASY_DEMO(ads1220-test)
#EASY_DEMO(pcf8833-basic)
EASY_DEMO(htu2xd-basic)
EASY_DEMO(print-calib)
EASY_DEMO(set-dummy-calib)
