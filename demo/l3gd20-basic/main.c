/*
 * This file is part of libbox0.
 * Copyright (C) 2013-2016 Kuldeep Singh Dhaka <kuldeep@madresistor.com>
 *
 * libbox0 is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * libbox0 is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with libbox0.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <stdio.h>
#include <stdlib.h>
#include <libbox0/libbox0.h>
#include <libbox0/backend/usb/usb.h>
#include <libbox0/driver/l3gd20/l3gd20.h>
#include <libbox0/misc/conv.h>
#include "../common-code/sigkill.c"
#include "../common-code/sleep.c"

int main(int argc, char *argv[])
{
	b0_device *dev = NULL;
	b0_i2c *i2c0 = NULL;
	b0_l3gd20 *l3gd20 = NULL;
	double x, y, z;

	if (B0_ERR_RC(b0_usb_open_supported(&dev))) {
		fprintf(stderr, "unable to open device\n");
		return EXIT_FAILURE;
	}

	if (B0_ERR_RC(b0_i2c_open(dev, &i2c0, 0))) {
		fprintf(stderr, "unable to open module\n");
		goto done_device;
	}

	if (B0_ERR_RC(b0_i2c_master_prepare(i2c0))) {
		fprintf(stderr, "unable to prepare module\n");
		goto done_module;
	}

	if (B0_ERR_RC(b0_l3gd20_open_i2c(i2c0, &l3gd20, false))) {
		fprintf(stderr, "Unable to allocate l3gd20\n");
		goto done_device;
	}

	if (B0_ERR_RC(b0_l3gd20_power_up(l3gd20))) {
		fprintf(stderr, "Unable to power up l3gd20\n");
		goto done_l3gd20;
	}

	sigkill_init();
	printf("Degrees per Second: \n");
	while (!sigkill_served()) {
		if (B0_ERR_RC(b0_l3gd20_read(l3gd20, &x, &y, &z))) {
			fprintf(stderr, "\nunable to read dps\n");
			break;
		}

		x = B0_RADIAN_TO_DEGREE(x);
		y = B0_RADIAN_TO_DEGREE(y);
		z = B0_RADIAN_TO_DEGREE(z);

		printf("\rx: %+.2f, y: %+.2f, z:%+.2f   ", x, y, z);
		fflush(stdout);

		/* sleep for 50ms */
		sleep_milliseconds(50);
	}
	printf("\n");

	done_l3gd20:
	if (B0_ERR_RC(b0_l3gd20_close(l3gd20))) {
		fprintf(stderr, "unable to free l3gd20\n");
	}

	done_module:
	if (B0_ERR_RC(b0_i2c_close(i2c0))) {
		fprintf(stderr, "unable to close module\n");
	}

	done_device:
	b0_device_close(dev);

	return EXIT_SUCCESS;
}
