/*
 * This file is part of libbox0.
 * Copyright (C) 2013-2016 Kuldeep Singh Dhaka <kuldeep@madresistor.com>
 *
 * libbox0 is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * libbox0 is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with libbox0.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <stdio.h>
#include <libbox0/libbox0.h>
#include <libbox0/backend/usb/usb.h>
#include "../common-code/sigkill.c"
#include "../common-code/sleep.c"

#define SAMPLING_SPEED 500
#define SAMPLES_SIZE 500
#define CALIB_OFFSET 0.0

double average(double *samples, size_t count);

int main(int argc, char *argv[])
{
	b0_device *dev = NULL;
	b0_ain *ain0 = NULL;

	if (B0_ERR_RC(b0_usb_open_supported(&dev))) {
		fprintf(stderr, "unable to open device\n");
		return EXIT_FAILURE;
	}

	if (B0_ERR_RC(b0_ain_open(dev, &ain0, 0))) {
		fprintf(stderr, "unable to open module\n");
		goto done_device;
	}

	/* AIM: take 100 samples from device at 100ks */
	if (B0_ERR_RC(b0_ain_snapshot_prepare(ain0))) {
		fprintf(stderr, "unable to prepare snapshot ain0\n");
		goto done_module;
	}

	if (B0_ERR_RC(b0_ain_bitsize_speed_set(ain0, 12, SAMPLING_SPEED))) {
		fprintf(stderr, "unable to set bitsize, speed\n");
		goto done_module;
	}

	double samples[SAMPLES_SIZE];

	sigkill_init();
	printf("Voltmeter\n");
	while (!sigkill_served()) {
		if (B0_ERR_RC(b0_ain_snapshot_start_double(ain0, samples, SAMPLES_SIZE))) {
			fprintf(stderr, "unable to read data from snapshot of ain0\n");
			goto done_module;
		}

		printf("\r%1.3f V", average(samples, SAMPLES_SIZE) + CALIB_OFFSET);
		fflush(stdout);
	}
	printf("\n");

	done_module:
	if (B0_ERR_RC(b0_ain_close(ain0))) {
		fprintf(stderr, "unable to close module\n");
	}

	done_device:
	b0_device_close(dev);

	return EXIT_SUCCESS;
}

double average(double *samples, size_t count)
{
	size_t i;
	double avg;

	for (i = 0; i < count; i++) {
		avg += samples[i];
	}

	avg /= count;

	avg += 0.0007701713;
	avg /= 1.0071241314;

	return avg;
}
