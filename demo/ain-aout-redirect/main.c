/*
 * This file is part of libbox0.
 * Copyright (C) 2013-2016 Kuldeep Singh Dhaka <kuldeep@madresistor.com>
 *
 * libbox0 is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * libbox0 is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with libbox0.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <stdio.h>
#include <stdlib.h>
#include <inttypes.h>
#include <libbox0/libbox0.h>
#include <libbox0/backend/usb/usb.h>
#include "../common-code/sigkill.c"

int main(int argc, char *argv[])
{
	b0_device *dev = NULL;
	b0_ain *ain0 = NULL;
	b0_aout *aout0 = NULL;

	if (B0_ERR_RC(b0_usb_open_supported(&dev))) {
		fprintf(stderr, "unable to open device\n");
		return EXIT_FAILURE;
	}

	if (B0_ERR_RC(b0_ain_open(dev, &ain0, 0))) {
		fprintf(stderr, "unable to get ain0");
		goto done_device;
	}

	if (B0_ERR_RC(b0_aout_open(dev, &aout0, 0))) {
		fprintf(stderr, "unable to get aout0");
		goto done_ain;
	}

	if (B0_ERR_RC(b0_ain_stream_prepare(ain0)) ||
		B0_ERR_RC(b0_aout_stream_prepare(aout0))) {
		fprintf(stderr, "unable to prepare some module for stream\n");
		goto done_device;
	}

	unsigned int bitsize = 12;
	unsigned long speed = 10000;

	if (B0_ERR_RC(b0_ain_bitsize_speed_set(ain0, bitsize, speed))) {
		fprintf(stderr, "unable to set ain bitsize speed\n");
		goto done_device;
	}

	if (B0_ERR_RC(b0_aout_bitsize_speed_set(aout0, bitsize, speed))) {
		fprintf(stderr, "unable to set aout bitsize speed\n");
		goto done_device;
	}

	/*
	 * note on Redirecting Ain to Aout
	 *   assuming: you are streaming at same speed for Ain and Aout.
	 *
	 * 1) use non-blocking
	 *    if the number of samples that were request not available
	 *     it will return instead of waiting for new samples.
	 *     procedure:
	 *     - start ain
	 *     - some sleep(some-finite-delay)
	 *     - start aout
	 *     - redirection
	 *       - read from ain
	 *       - check if got more than 0 samples
	 *       - write to aout
	 *       - goto (redirection)
	 *
	 * 2) use blocking
	 *    make sure that the number of samples request do not block for
	 *     that time which will lead to aout out of buffer
	 *
	 *    precaution:
	 *      give delay of (alteast double the blocking time) when starting ain and aout
	 *      now, for first few request will not block for any time
	 *        and will return data immediately.
	 *      (though after some it will start blocking because it has to wait for data)
	 *      but the first few aout-write will keep aout going while ain-read is blocking.
	 *
	 *    assuming you are requesting N samples
	 *    then their is a good approximation that we will be blocked for
	 *       blocking_time  = N / sampling-freq
	 *     - start ain
	 *     - some sleep(blocking_time * M) | where M >= 2
	 *     - start aout
	 *     - redirection
	 *       - read from ain
	 *       - write to aout
	 *       - goto (redirection)
	 *
	 * Note: results could vary as per the underlying transport.
	 */

	if (B0_ERR_RC(b0_ain_stream_start(ain0))) {
		fprintf(stderr, "unable to start ain for stream\n");
		goto done_device;
	}

	/* wait for some to let some data acumulate */
	sleep_milliseconds(100);

	if (B0_ERR_RC(b0_aout_stream_start(aout0))) {
		fprintf(stderr, "unable to start ain for stream\n");
		goto done_device;
	}

	printf("stream: %lu S/s at %u bits\n", speed, bitsize);

	size_t read_count = speed / 100;
	double *data = (double *) malloc(read_count * sizeof(double));

	sigkill_init();
	while (!sigkill_served()) {
		/* read from ain */
		if (B0_ERR_RC(b0_ain_stream_read_double(ain0, data, read_count, NULL))) {
			fprintf(stderr, "unable to read from ain\n");
			break;
		}

		/* write to aout */
		if (B0_ERR_RC(b0_aout_stream_write_double(aout0, data, read_count))) {
			fprintf(stderr, "unable to write to aout\n");
			break;
		}
	}
	sigkill_fini();

	if (B0_ERR_RC(b0_ain_stream_stop(ain0))) {
		fprintf(stderr, "unable to stop ain module for stream\n");
	}

	if (B0_ERR_RC(b0_aout_stream_stop(aout0))) {
		fprintf(stderr, "unable to stop aout module for stream\n");
	}


	done_module:

	done_aout:
	if (B0_ERR_RC(b0_aout_close(aout0))) {
		fprintf(stderr, "unable to close aout0\n");
	}

	done_ain:
	if (B0_ERR_RC(b0_ain_close(ain0))) {
		fprintf(stderr, "unable to close ain0\n");
	}

	done_device:
	b0_device_close(dev);

	return EXIT_SUCCESS;
}
