/*
 * This file is part of libbox0.
 * Copyright (C) 2016 Kuldeep Singh Dhaka <kuldeep@madresistor.com>
 *
 * libbox0 is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * libbox0 is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with libbox0.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <stdio.h>
#include <stdlib.h>
#include <libbox0/libbox0.h>
#include <libbox0/backend/usb/usb.h>
#include <libbox0/misc/box0-v5/calib.h>

static void print_calib(const char *name,
		const b0v5_calib_value *values, size_t count)
{
	unsigned i;

	printf("%s:\n", name);

	for (i = 0; i < count; i++) {
		printf(" [%u]: GAIN(%f) OFFSET(%f)\n", i, values[i].gain, values[i].offset);
	}
}

int main(int argc, char *argv[])
{
	b0_device *dev = NULL;

	if (B0_ERR_RC(b0_usb_open_supported(&dev))) {
		fprintf(stderr, "unable to open device\n");
		return EXIT_FAILURE;
	}

	/* TODO: check if the device is box0-v5 */

	b0v5_calib_value IDEAL_VALUE = {.gain = 1, .offset = 0};
	b0v5_calib_value ain0_values[4] = {
		IDEAL_VALUE, IDEAL_VALUE, IDEAL_VALUE, IDEAL_VALUE
	}, aout0_values[2] = {
		IDEAL_VALUE, IDEAL_VALUE
	};
	b0v5_calib calib = {
		.ain0 = {
			.values = ain0_values,
			.count = 4
		},

		.aout0 = {
			.values = aout0_values,
			.count = 2
		}
	};

	if (b0v5_calib_write(dev, &calib) < 0) {
		fprintf(stderr, "Unable to get calib data");
		goto done_device;
	}

	printf("dummy (Ideal) caliberation set\n");

	done_device:
	b0_device_close(dev);

	return EXIT_SUCCESS;
}
