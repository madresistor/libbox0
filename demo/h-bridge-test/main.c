/*
 * This file is part of libbox0.
 * Copyright (C) 2013-2016 Kuldeep Singh Dhaka <kuldeep@madresistor.com>
 *
 * libbox0 is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * libbox0 is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with libbox0.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <stdio.h>
#include <stdlib.h>
#include <libbox0/libbox0.h>
#include <libbox0/backend/usb/usb.h>
#include <libbox0/driver/h-bridge/h-bridge.h>
#include "../common-code/sigkill.c"
#include "../common-code/sleep.c"

int main(int argc, char *argv[])
{
	b0_device *dev = NULL;
	b0_dio *dio0 = NULL;
	b0_hbridge *hbridge;

	if (B0_ERR_RC(b0_usb_open_supported(&dev))) {
		fprintf(stderr, "unable to open device\n");
		return EXIT_FAILURE;
	}

	if (B0_ERR_RC(b0_dio_open(dev, &dio0, 0))) {
		fprintf(stderr, "unable to load module\n");
		goto done_device;
	}

	if (B0_ERR_RC(b0_dio_basic_prepare(dio0))) {
		fprintf(stderr, "unable to prepare module\n");
		goto done_module;
	}

	if (B0_ERR_RC(b0_hbridge_open(dio0, &hbridge, 0, 1, 2))) {
		fprintf(stderr, "Unable to allocate hbridge\n");
		goto done_device;
	}

	printf("press char for operation: \n");
	printf("<operation>:\t<key>\n");
	printf("forward:\tf\n");
	printf("backward:\tb\n");
	printf("disable:\td\n");
	printf("exit:\t\te\n");

	sigkill_init();
	while (!sigkill_served()) {
		printf("enter operation: ");
		/* wait for user */
		sleep_milliseconds(100);

		b0_result_code r = B0_OK;
		switch(fgetc(stdin)) {
		case 'f':
			r = b0_hbridge_set(hbridge, B0_HBRIDGE_FORWARD);
		break;
		case 'b':
			r = b0_hbridge_set(hbridge, B0_HBRIDGE_BACKWARD);
		break;
		case 'd':
			r = b0_hbridge_set(hbridge, B0_HBRIDGE_DISABLE);
		break;
		case 'e':
			sigkill_fini();
		break;
		case '\n':
		case '\r':
		break;
		default:
			printf("\nunknown command");
		break;
		}

		putchar('\n');

		if (B0_ERR_RC(r)) {
			fprintf(stderr, "error occured while operation, result code is %i\r", r);
			sigkill_fini();
		}

		fflush(stdout);

		//~ if (B0_ERR_RC(b0_hbridge_set(hbridge, B0_HBRIDGE_FORWARD)) ||
		//~		B0_ERR_RC(b0_hbridge_set(hbridge, B0_HBRIDGE_BACKWARD))) {
			//~ sigkill_fini();
		//~ }
		//~ sleep_milliseconds(20);
	}

	/* try to disable */
	b0_hbridge_set(hbridge, B0_HBRIDGE_DISABLE);

	done_hbridge:
	if (B0_ERR_RC(b0_hbridge_close(hbridge))) {
		fprintf(stderr, "unable to free hbridge\n");
	}

	done_module:
	if (B0_ERR_RC(b0_dio_close(dio0))) {
		fprintf(stderr, "unable to close module\n");
	}

	done_device:
	b0_device_close(dev);

	return EXIT_SUCCESS;
}
