/*
 * This file is part of libbox0.
 * Copyright (C) 2013-2016 Kuldeep Singh Dhaka <kuldeep@madresistor.com>
 *
 * libbox0 is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * libbox0 is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with libbox0.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <stdio.h>
#include <math.h>
#include <stdlib.h>
#include <libbox0/libbox0.h>
#include <libbox0/backend/usb/usb.h>
#include "../common-code/sigkill.c"

double sampling_freq = 0, cutoff_freq = 10;

/* inspired from pseudo code
 * http://en.wikipedia.org/wiki/Low-pass_filter
 */
void low_pass_filter(double *data, size_t size)
{
	static double last_data = 0;
	const double dt = 1 / sampling_freq;
	const double RC = 1 / (2 * M_PI * cutoff_freq);
	const double alpha = dt / (RC + dt);

	size_t i;
	for (i = 0; i < size; i++) {
		data[i] = (alpha * data[i]) + ((1 - alpha) * last_data);
		last_data = data[i];
	}
}

int main(int argc, char *argv[])
{
	b0_device *dev = NULL;
	b0_ain *ain0 = NULL;
	b0_aout *aout0 = NULL;

	/* store the previous value.
	 * if this is not stored,
	 * waveform will be weird (as previous value is not used)
	 */
	double previous_data = 0;

	if (argc > 1) {
		cutoff_freq = atof(argv[1]);
	}

	if (cutoff_freq <= 0) {
		fprintf(stderr, "get yourself some refreshment!\n");
		return EXIT_FAILURE;
	}

	if (B0_ERR_RC(b0_usb_open_supported(&dev))) {
		fprintf(stderr, "unable to open device\n");
		return EXIT_FAILURE;
	}

	if (B0_ERR_RC(b0_ain_open(dev, &ain0, 0))) {
		fprintf(stderr, "unable to get ain0");
		goto done_device;
	}

	if (B0_ERR_RC(b0_aout_open(dev, &aout0, 0))) {
		fprintf(stderr, "unable to get aout0");
		goto done_ain;
	}

	/* TODO: assure that the sampling frequency is acceptable. */

	if (B0_ERR_RC(b0_ain_stream_prepare(ain0)) ||
		B0_ERR_RC(b0_aout_stream_prepare(aout0))) {
		fprintf(stderr, "unable to prepare some module for stream\n");
		goto done_device;
	}

	unsigned int bitsize = 12;
	unsigned long speed = 10000;

	if (B0_ERR_RC(b0_ain_bitsize_speed_set(ain0, bitsize, speed))) {
		fprintf(stderr, "unable to set ain bitsize speed\n");
		goto done_device;
	}

	if (B0_ERR_RC(b0_aout_bitsize_speed_set(aout0, bitsize, speed))) {
		fprintf(stderr, "unable to set aout bitsize speed\n");
		goto done_device;
	}

	/* fill remaining parameters */
	sampling_freq = speed;

	if (B0_ERR_RC(b0_ain_stream_start(ain0))) {
		fprintf(stderr, "unable to start ain for stream\n");
		goto done_device;
	}

	/* wait for some to let some data acumulate */
	sleep_milliseconds(20);

	if (B0_ERR_RC(b0_aout_stream_start(aout0))) {
		fprintf(stderr, "unable to start ain for stream\n");
		goto done_device;
	}

	size_t read_count = speed / 100;
	double *data = (double *) malloc(read_count * sizeof(double));

	sigkill_init();
	while (!sigkill_served()) {
		/* read from ain */
		if (B0_ERR_RC(b0_ain_stream_read_double(ain0, data, read_count, NULL))) {
			fprintf(stderr, "unable to read from ain\n");
			break;
		}

		/* perform filtering */
		low_pass_filter(data, read_count);

		/* write to aout */
		if (B0_ERR_RC(b0_aout_stream_write_double(aout0, data, read_count))) {
			fprintf(stderr, "unable to write to aout\n");
			break;
		}
	}
	sigkill_fini();

	if (B0_ERR_RC(b0_ain_stream_stop(ain0))) {
		fprintf(stderr, "unable to stop ain module for stream\n");
	}

	if (B0_ERR_RC(b0_aout_stream_stop(aout0))) {
		fprintf(stderr, "unable to stop aout module for stream\n");
	}

	done_module:

	done_aout:
	if (B0_ERR_RC(b0_aout_close(aout0))) {
		fprintf(stderr, "unable to close aout0\n");
	}

	done_ain:
	if (B0_ERR_RC(b0_ain_close(ain0))) {
		fprintf(stderr, "unable to close ain0\n");
	}

	done_device:
	b0_device_close(dev);

	return EXIT_SUCCESS;
}
