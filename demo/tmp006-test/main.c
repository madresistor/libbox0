/*
 * This file is part of libbox0.
 * Copyright (C) 2013-2016 Kuldeep Singh Dhaka <kuldeep@madresistor.com>
 *
 * libbox0 is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * libbox0 is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with libbox0.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <stdio.h>
#include <libbox0/libbox0.h>
#include <libbox0/backend/usb/usb.h>
#include <libbox0/driver/tmp006/tmp006.h>
#include <libbox0/misc/conv.h>
#include "../common-code/sigkill.c"
#include "../common-code/sleep.c"

int main(int argc, char *argv[])
{
	b0_device *dev = NULL;
	b0_i2c *i2c0 = NULL;
	b0_tmp006 *tmp006 = NULL;
	double temp;

	if (B0_ERR_RC(b0_usb_open_supported(&dev))) {
		fprintf(stderr, "unable to open device\n");
		return EXIT_FAILURE;
	}

	if (B0_ERR_RC(b0_i2c_open(dev, &i2c0, 0))) {
		fprintf(stderr, "unable to open module\n");
		goto done_device;
	}

	if (B0_ERR_RC(b0_i2c_master_prepare(i2c0))) {
		fprintf(stderr, "unable to prepare module\n");
		goto done_module;
	}

	if (B0_ERR_RC(b0_tmp006_open(i2c0, &tmp006, B0_TMP006_DUMMY_CALIB_FACTOR,
									false, B0_TMP006_GND))) {
		fprintf(stderr, "Unable to alloca tmp006\n");
		goto done_device;
	}

	sigkill_init();
	printf("Temprature (Degree Celsius): ");
	while (!sigkill_served()) {
		if (B0_ERR_RC(b0_tmp006_read(tmp006, &temp))) {
			fprintf(stderr, "\nunable to read temprature\n");
			break;
		}

		printf("%.2f, ", B0_KELVIN_TO_CELSIUS(temp));
		fflush(stdout);

		sleep_seconds(1);
	}
	printf("\n");

	done_tmp006:
	if (B0_ERR_RC(b0_tmp006_close(tmp006))) {
		fprintf(stderr, "unable to free tmp006\n");
	}

	done_module:
	if (B0_ERR_RC(b0_i2c_close(i2c0))) {
		fprintf(stderr, "unable to close module\n");
	}

	done_device:
	b0_device_close(dev);

	return EXIT_SUCCESS;
}
