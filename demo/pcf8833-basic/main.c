/*
 * This file is part of libbox0.
 * Copyright (C) 2015, 2016 Kuldeep Singh Dhaka <kuldeep@madresistor.com>
 *
 * libbox0 is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * libbox0 is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with libbox0.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <stdio.h>
#include <stdlib.h>
#include <libbox0/libbox0.h>
#include <libbox0/backend/usb/usb.h>
#include <libbox0/driver/pcf8833/pcf8833.h>
#include "../common-code/sigkill.c"

int main(int argc, char *argv[])
{
	b0_device *dev = NULL;
	b0_spi *spi0;
	b0_pcf8833 *nokia6100;

	if (B0_ERR_RC(b0_usb_open_supported(&dev))) {
		fprintf(stderr, "unable to open device\n");
		return EXIT_FAILURE;
	}

	if (B0_ERR_RC(b0_spi_open(dev, &spi0, 0))) {
		fprintf(stderr, "unable to open module\n");
		goto done_device;
	}

	if (B0_ERR_RC(b0_spi_master_prepare(spi0))) {
		fprintf(stderr, "unable to prepare module\n");
		goto done_module;
	}

	if (B0_ERR_RC(b0_pcf8833_open(spi0, &nokia6100, 0))) {
		goto done_module;
	}

	//~ if (B0_ERR_RC(b0_pcf8833_puts(nokia6100, "Hello World!"))) {
		//~ fprintf(stderr, "unable to write hello world string");
		//~ goto done_driver;
	//~ }

	b0_pcf8833_col_set(nokia6100, 10, 50);
	b0_pcf8833_page_set(nokia6100, 10, 50);

	uint16_t i = 1500;
	while (i > 0) {
		b0_pcf8833_pixel(nokia6100, 0x7, 0x6, 0x5);
		i--;
	}

	//block_till_sigkill();

	done_driver:
	if (B0_ERR_RC(b0_pcf8833_close(nokia6100))) {
		fprintf(stderr, "error while freeing driver\n");
	}

	done_module:
	if (B0_ERR_RC(b0_spi_close(spi0))) {
		fprintf(stderr, "unable to close module\n");
	}

	done_device:
	b0_device_close(dev);

	return EXIT_SUCCESS;
}
