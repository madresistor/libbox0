/*
 * This file is part of libbox0.
 * Copyright (C) 2016 Kuldeep Singh Dhaka <kuldeep@madresistor.com>
 *
 * libbox0 is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * libbox0 is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with libbox0.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <stdio.h>
#include <stdlib.h>
#include <inttypes.h>
#include <libbox0/libbox0.h>
#include <libbox0/backend/usb/usb.h>
#include <libbox0/driver/ads1220/ads1220.h>
#include "../common-code/sigkill.c"

// ./<program> [GAIN]
// GAIN can be 1, 2, 4, 8, 16, 32, 64, 128

int main(int argc, char *argv[])
{
	b0_device *dev = NULL;
	b0_spi *spi0;
	b0_ads1220 *ads1220;

	b0_ads1220_gain gain = B0_ADS1220_GAIN_1;
	if (argc > 1) {
		char *gain_str = argv[1];
		switch (atoi(gain_str)) {
		case 1:
			gain = B0_ADS1220_GAIN_1;
		break;
		case 2:
			gain = B0_ADS1220_GAIN_2;
		break;
		case 4:
			gain = B0_ADS1220_GAIN_4;
		break;
		case 8:
			gain = B0_ADS1220_GAIN_8;
		break;
		case 16:
			gain = B0_ADS1220_GAIN_16;
		break;
		case 32:
			gain = B0_ADS1220_GAIN_32;
		break;
		case 64:
			gain = B0_ADS1220_GAIN_64;
		break;
		case 128:
			gain = B0_ADS1220_GAIN_128;
		break;
		default:
			fprintf(stderr, "\nunknown gain = '%s'\n", gain_str);
			exit(EXIT_FAILURE);
		break;
		}
	}

	if (B0_ERR_RC(b0_usb_open_supported(&dev))) {
		fprintf(stderr, "unable to open device\n");
		return EXIT_FAILURE;
	}

	if (B0_ERR_RC(b0_spi_open(dev, &spi0, 0))) {
		fprintf(stderr, "unable to open module\n");
		goto done_device;
	}

	if (B0_ERR_RC(b0_spi_master_prepare(spi0))) {
		fprintf(stderr, "unable to prepare module\n");
		goto done_module;
	}

	if (B0_ERR_RC(b0_ads1220_open(spi0, &ads1220, 0))) {
		goto done_module;
	}

	if (B0_ERR_RC(b0_ads1220_gain_set(ads1220, gain))) {
		fprintf(stderr, "\nunable to set gain\n");
		goto done_module;
	}

	sigkill_init();
	printf("Values:\n");
	while (!sigkill_served()) {
		if (B0_ERR_RC(b0_ads1220_start(ads1220))) {
			fprintf(stderr, "\nunable to start\n");
			break;
		}

		/* sleep for 50ms */
		sleep_milliseconds(50);

		double value;
		if (B0_ERR_RC(b0_ads1220_read(ads1220, &value))) {
			fprintf(stderr, "\nunable to read values\n");
			break;
		}

		printf("%f\n", value);
		fflush(stdout);
	}
	printf("\n");

	done_driver:
	if (B0_ERR_RC(b0_ads1220_close(ads1220))) {
		fprintf(stderr, "error while freeing driver\n");
	}

	done_module:
	if (B0_ERR_RC(b0_spi_close(spi0))) {
		fprintf(stderr, "unable to close module\n");
	}

	done_device:
	b0_device_close(dev);

	return EXIT_SUCCESS;
}
