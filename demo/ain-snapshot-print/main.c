/*
 * This file is part of libbox0.
 * Copyright (C) 2013-2016 Kuldeep Singh Dhaka <kuldeep@madresistor.com>
 *
 * libbox0 is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * libbox0 is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with libbox0.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <stdio.h>
#include <stdlib.h>
#include <libbox0/libbox0.h>
#include <libbox0/backend/usb/usb.h>

int main(int argc, char *argv[])
{
	b0_device *dev = NULL;
	b0_ain *ain0 = NULL;

	if (B0_ERR_RC(b0_usb_open_supported(&dev))) {
		fprintf(stderr, "unable to open device\n");
		return EXIT_FAILURE;
	}

	b0_device_log(dev, B0_LOG_WARN);

	if (B0_ERR_RC(b0_ain_open(dev, &ain0, 0))) {
		fprintf(stderr, "unable to get module\n");
		goto done_device;
	}

	/* AIM: take 100 samples from device at 100ks */
	if (B0_ERR_RC(b0_ain_snapshot_prepare(ain0))) {
		fprintf(stderr, "unable to prepare oneshot ain0\n");
		goto done_device;
	}

	if (B0_ERR_RC(b0_ain_bitsize_speed_set(ain0, 12,  500000 /* or 1000000 for 1M if hack is on */))) {
		fprintf(stderr, "unable to set bitsize, speed\n");
		goto done_module;
	}

#define SAMPLES_SIZE 500
	double samples[SAMPLES_SIZE];
	if (B0_ERR_RC(b0_ain_snapshot_start_double(ain0, samples, SAMPLES_SIZE))) {
		fprintf(stderr, "unable to read oneshot of ain0\n");
		goto done_device;
	}

	/* print the data */
	size_t i;
	for (i = 0; i < SAMPLES_SIZE; i++) {
		printf("%.2f ", samples[i]);
	}
	printf("\n");

	done_module:
	if (B0_ERR_RC(b0_ain_close(ain0))) {
		fprintf(stderr, "unable to close module\n");
	}

	done_device:
	b0_device_close(dev);

	return EXIT_SUCCESS;
}
