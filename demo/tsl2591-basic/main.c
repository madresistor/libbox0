/*
 * This file is part of libbox0.
 * Copyright (C) 2013-2016 Kuldeep Singh Dhaka <kuldeep@madresistor.com>
 *
 * libbox0 is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * libbox0 is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with libbox0.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <stdio.h>
#include <libbox0/libbox0.h>
#include <libbox0/backend/usb/usb.h>
#include <libbox0/driver/tsl2591/tsl2591.h>
#include "../common-code/sigkill.c"
#include "../common-code/sleep.c"

int main(int argc, char *argv[])
{
	b0_device *dev = NULL;
	b0_i2c *i2c0 = NULL;
	b0_tsl2591 *tsl2591 = NULL;
	double illuminance;

	if (B0_ERR_RC(b0_usb_open_supported(&dev))) {
		fprintf(stderr, "unable to open device\n");
		return EXIT_FAILURE;
	}

	if (B0_ERR_RC(b0_i2c_open(dev, &i2c0, 0))) {
		fprintf(stderr, "unable to open module\n");
		goto done_device;
	}

	if (B0_ERR_RC(b0_i2c_master_prepare(i2c0))) {
		fprintf(stderr, "unable to prepare module\n");
		goto done_module;
	}

	if (B0_ERR_RC(b0_tsl2591_open(i2c0, &tsl2591, NULL))) {
		fprintf(stderr, "Unable to alloca tsl2591\n");
		goto done_device;
	}

	if (B0_ERR_RC(b0_tsl2591_integ_set(tsl2591, B0_TSL2591_INTEG_300MS))) {
		fprintf(stderr, "Unable to set integeration 200ms\n");
	}

	if (B0_ERR_RC(b0_tsl2591_gain_set(tsl2591, B0_TSL2591_GAIN_MEDIUM))) {
		fprintf(stderr, "Unable to set gain high\n");
	}

	sigkill_init();
	printf("Illumninance (Lux): ");
	while (!sigkill_served()) {
		if (B0_ERR_RC(b0_tsl2591_read(tsl2591, &illuminance))) {
			fprintf(stderr, "\nunable to read illuminance\n");
			break;
		}

		//printf("%i, ", illuminance);
		printf("%.2f, ", illuminance);
		fflush(stdout);

		sleep_seconds(1);
	}
	printf("\n");

	done_tsl2591:
	if (B0_ERR_RC(b0_tsl2591_close(tsl2591))) {
		fprintf(stderr, "unable to free tsl2591\n");
	}

	done_module:
	if (B0_ERR_RC(b0_i2c_close(i2c0))) {
		fprintf(stderr, "unable to close module\n");
	}

	done_device:
	b0_device_close(dev);

	return EXIT_SUCCESS;
}
